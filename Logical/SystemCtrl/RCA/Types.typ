
TYPE
	TON_10msobj : 	STRUCT 
		PT : UDINT;
		ET : UDINT;
		STARTTIME : UDINT;
		IN : BOOL;
		Q : BOOL;
		M : BOOL;
	END_STRUCT;
	uMB_obj : 	STRUCT 
		master_adr : UDINT;
		open_adr : UDINT;
		LastBadStatus : UINT;
		PortOK : BOOL;
		LastGoodTime : UINT;
		RecordNum : UDINT;
		GoodMsg : UDINT;
		BadMsg : UDINT;
		nocomm_sp1 : UINT;
		nocomm_sp2 : UINT;
		CommFail : BOOL;
		CommFail_Crash : BOOL;
		Message : STRING[65];
		RecordStatus : ARRAY[0..199]OF SINT;
		ErrorNumber : UINT;
		_exec : UINT;
		_error : BOOL;
	END_STRUCT;
	TOF_10msobj : 	STRUCT 
		PT : UDINT;
		ET : UDINT;
		STARTTIME : UDINT;
		IN : BOOL;
		Q : BOOL;
		M : BOOL;
	END_STRUCT;
END_TYPE
