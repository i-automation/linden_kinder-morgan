/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Buckeye Pipe Line Company
General Library Version History

Piedmont Automation, Inc. 770-931-6500
------------------------------------------------------------------------------------------------------------------------------------------------------------
Function		Version	Date		By		Description
------------------------------------------------------------------------------------------------------------------------------------------------------------

ramp_init		V1.00	11.30.01	CHO		Allow NULL adrLocalMode input parameter
ramp_serv									Set LocalMode=0 with NULL ptrLocalMode=NULL
ramp_init/serv								Change Local parameters to Remote

Oip_control		V1.01	12.4.01		MMC		Clear auto_oip and manual_oip regardless of password Level.

PID_obj,		V1.10	12.5.01		CHO		Remove 6 pid loop limit. Now upto 254 loops per PID_obj.
pid_init,									Remove loopDefault and deselect items.
pid_serv									Add loopSelect item to manually select a particular loop.
											Add Constants pidMIN, pidMAX for lower and upper limits.
											Add Constant pidAUTOSEL. When assigned to loopSelect allow Loop Auto Select Mode.
											Remove direct0_or_Indirect1 item. Add type item in its place.
											Add Constants pidDIRECT and pidINDIRECT to be assigned to type.

Oip_contol		V1.11	12.12.01	MMC		Change manual led to blinking (2).
pid_init									Add RemoteStatus input.
											Prevent Manual mode or switch to auto while in Remote.
pid_obj			V1.12	12.26.01	MMC		Change auto_led type from USINT to BOOL.

ramp_obj		V1.13	2/12/02		MMC		Added _64k for SelectedSP and LocalSP.

scale_obj		V1.14	3/5/03		MMC/COR	Changed RCA value to reflect scaling for 4-20 mA.

scale_obj		V1.15	7/7/03		MMC		Changed value_64k to limit to 0 when rawvalue indicates negative value

Library			V2.00	10/16/03	COR		Library Upgrade to Latest LoopCont.  Library designed for both SG3 and SG4

PIDLOOP_oip		V2.01	02/06/04	CHO/SAM	Added New Data Type PIDLOOP_oip for displaying PID Loop Info on OIP Display
											Updated Data Type PID_obj
											Updated Data Type PIDLOOP_obj

pid_init		V2.02	08/24/04	SAM		Added p->initStatus = 0.

PIDLOOP_obj		V2.03	03/21/06	SAM		Added items init, PVrange_adr, PVrange, deadband
pidloop_init								Added Deadband Functionality for individual loop
pidloop_serv
PIDpara_obj									Added item deadband

PIDLOOP_obj		V2.04	06/21/06	SAM		Added RampOpen functionality
pidloop_serv

PIDLOOP_obj		V2.05	08/19/06	SAM		Added items Output_mA
pidloop_serv

RAMP_obj  		V2.06	12/04/06	SAM		Added field "Type" to RAMP_obj. Corrected Bumpless Set-Point Transfer Functionality
ramp_init
ramp_serv

RAMP_obj  		V2.07	03/21/07	SAM		Added Low/High limits for PV/SP.
ramp_init									Local SP is not initialised to zero anymore.
ramp_serv
PIDLOOP_obj
pidloop_init
pidloop_serv

RAMP_obj  		V2.08	08/16/07	SAM		Added RampExcp. Needed for Pump Station PID control.
ramp_serv									When no pump is running Setpoint changes should be Instantaneous rather than Ramping.


























































*/


