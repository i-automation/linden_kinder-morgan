/*
---------------------------------------------------------------------------------------------------------------------------
Buckeye Pipe Line Company
General Library Version History

Piedmont Automation, Inc. 770-931-6500
---------------------------------------------------------------------------------------------------------------------------
Function			Version		Date		By		Description
---------------------------------------------------------------------------------------------------------------------------
New Library			V1.00		11/20/03	CHO		Simulator Library 		New Library Introduced

IQ1Sim_serv			V2.00		03/08/05	SAM		New Function			New Function to handle new IQ1_TYPE of IQ Valves


