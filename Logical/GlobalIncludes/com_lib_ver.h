/*
User Created Common library version History:
Piedmont Automation, Inc. 770-931-6500

-----------------------------------------------------------------------------------------------------------------------
Function/Datatype	Version		Date		By			Description						Comment/Changes
-----------------------------------------------------------------------------------------------------------------------
New Library			V1.00		11.20.03	CHO			Common Library 					New Library Introduced

Analog64kToUINT		V1.01		02.10.04	SAM			Analog64kToUINT					Added Function Analog64kToUINT
																						Converts a RCA 64K value to appropriate scaled value 																					based on high and low values

GetWordWrite		V2.00		04.15.04	JIR			GetWordWrite					Added new Function
GLW_Init						04.16.04	JIR			GetLongWrite Timer Init			Added New Function
GetLongWrite											GetLongWrite					Added New Function

Trans_				V3.00		05.23.05	JIR			Provides Byte swapping			Added new Function
Trans_word												Provides Word swapping			Added new Function
SetBit													Set bit from Byte				Added new Function
ByteToBit												Converts Byte to Bit array		Added new Function
GetBit													Read Bit from Byte 				Added new Function

GetLongWrite		V3.01		09.02.05	JIR			Seperate I386,M68k targets to keep RCA program consistant for both targets
GetLongRead			V3.02		10.19.05	SAM			Seperate I386,M68k targets to keep RCA program consistant for both targets

FloatToAnalog64k	V3.03		10.27.05	SAM			Added new Function				Converts a Float value to SCADA 64K value
																						User can VOID out negative inputs based on selection NegVoid/NegOk

CreateDateString	V3.03		10.31.05	SAM			Added New Function 				Creates a Date String of format "MM/DD/YY" or "MM/DD/YYYY" from a UDINT number of format 103105
DATESTRING_obj

CreateRTCString 	V3.04		11.10.05	SAM			Added New Function 				Creates a Date/Time String of format "MM/DD/YY" or "MM/DD/YYYY" and "HH:MM:SS" From PLC RTC.
RTCSTRING_obj

CreateRTCString 	V3.05		11.22.05	SAM			Added New Element DateTime		Creates a DateTime String of format "MMDDYYHHMMSS" or "MMDDYYYYHHMMSS" From PLC RTC.
RTCSTRING_obj

com_lib.c			v3.05.1		28Dec09		PKC			Add #include <string.h> and <AsString.h> for use w/ AS3.0.80

