/* 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
Buckeye Pipe Line Company
General Library Version History
 
Piedmont Automation, Inc. 770-931-6500														 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
Function			Version		Date		By			Description													
-----------------------------------------------------------------------------------------------------------------------------------------------------------------


term_serv			1.01		12.04.01	cho			Add .message to TERM_obj and logic to term_serv.

recipe_serv			1.02		1.02.02		CHO/MMC		Add line at 2597 to revise when FullOpen=OFF.
												  		This fixes prblem with Terminal_CloseAll command, delays A manifold closure. 
												  
recipe_serv			1.03		1.04.02		CHO			Modify RECIPE_object: Add .close and .crash
														Remove Terminal.crash and .close from function and replace with Recipe.close/crash.												  
			
								1.08.02		CHO/MMC 	Modify shipper valve close command to work with recipe close. 
														Modify shipper_oip, shipper_rca to ignore step changes while delay timer is active.


alarm_init			1.04		1.28.02		CHO/MMC		For priority alarms, Added alarmgroup.oldOUT and .new.
														Added newcomm, oldcomm, newfault, oldfault.
													
mxvalve_obj			1.05		2/12/02		CHO/MMC		Added ValveChError.

analog_obj			1.06		2/13/02		CHO/MMC 	Changed scaling for _64k value to 4-20 ma.
HWvalve_obj,											Remove oip_hold.
Valve_obj												Added oip_hold.
								
valvegroup_obj		1.07		2/27/02		MMC			Added valvegroup.horn to inidicate alarm presence.	

recipe_serv			1.08		9/18/02		MMC/CHO 	Modified close control ln 529 to allow ticket switch without delay
														when next step valve is same as current step.
												
clock_serv			1.09		10/25/02	JIR			Modified to get correct format of clock data from Master RCA clock
RCASetclock	
clock_obj												Increse length of "rcatime" from USINT[8] to USINT[16]
RCAtyp													Increse length of "Time" from USINT[8] to USINT[16]
												

alarm_serv			1.10		10/28/02	JIR			Process AckImg for Panel ware and PP41 Panel differently beacuse PP41 only work with Array of BOOL[16] as AckImg & 
											    		Panel ware can work with UINT as AckImg 
														HMI Acknowledge image length must be equal to Alarm Bit array (alarm->OUT) in PP41
									            		Modified to resolve the problem for HMI LED status during active/non-acknowledged alarms 

ALARM_Obj												Add new data item " AckImg_PP41 " as BOOL[16]

iq_serv				1.11		12/03/02	JIR			Add new functions and related subroutines for Rotork valves
iqv_init

sola_init	     				12/09/02	JIR			Add new functions for Solatron device
sola_serv

sump_init						12/11/02	JIR			Add new functions for Sump motor control
sump_serv					

ALL Functions					12/13/02	JIR			Pass direct Object as First argument in each function to reduce extra steps

FASTLOOP_obj		1.12		01/03/03	JIR			Add data type to control Fast Loop PUMP & MOV operations

FSL								01/07/03	JIR			Add Function block to handle FlowSwitch operation.

VALVEGROUP_obj		1.12		01/10/03	JIR			Problem : If you use "AckImg(UINT)" as 'Acknowledgement Image' Field in PP41 Alarm Group then If you have any un-ack alarm (specially alarm bits greater than 2),
vgroup_serv		  										it can not set '.UnAckAlrm' bit True and because of this we can not get correct 'alarm_led' value, 
		  												this cause wrong LED status on Panel
		  
   														Solution : 'AckImg' must be an array of BOOL for PP41 panel
		  
   														Code Change: 1. For VALVEGROUP_obj: Following new data items are added
		  		 													 		faultAckImg_PP41		BOOL[16]
		  			    													commfailAckImg_PP41	BOOL[16]
		  			    													localAckImg_PP41		BOOL[16]
		  			    											   2. In vgroup_serv Function: Process AckImg for Panel ware and PP41 Panel differently to
		  			    											   	   create UnAckAlm bit 		

VALVEGROUP_obj		1.12		01/10/03	JIR			Problem : For Panel PP41, in AlarmGroup, 'Alarm Bit Field' must be an array of BOOL. In VALVEGROUP_obj for
vgroup_serv												fault,status,local and commfail we have UINT dataitem defined to generate different alarms/status. 
														This can work with PP41 panel and is not able to diaplay all alarms on panel.		

														Solution :   'Alarm Bit Field' must be an array of BOOL
   
   														Code Change : 1.  For VALVEGROUP_obj: Following new Date Items are added
																			FaultGroup		BOOL[16]
																			CommfailGroup 	BOOL[16]
																			LocalGroup		BOOL[16]
																			StatusGroup	BOOL[32]
				 														2. In vgroup_serv Function : Related code change in function to get value for FaultGroup, CommfailGroup, 
				 							  								LocalGroup, StatusGroup.

iq_serv				1.12.1		3/14/03		MMC			Add oip_hold to reset commands				 							  								
	
				 								
HWVALVE_obj			1.13		1.23.03		CHO			Added 'stop' capability to Hard Wired Valves.  Changed data type,
hwvalve_init,											init and serv functions.  New HWVALVE_obj items include:
hwvalve_serv											stop_oip, stop_remote, stop_cmd, stop_cmd_adr and stoptimer.

RTUV				1.13		1.23.03		CHO			Created RTUV function block.  Issues Stop prior to issueing Open or Close.
						
					1.20		3.18.03		CHO			New Version: No new changes!
analog_serv			1.21		4.29.03		CHO			Allow .value_64k to be zero in AX type calculation.
														Improved scaling.

scale_init			1.22		6.10.03		JIR			Add scale_init, scale_serv, GetBitWrite functions
scale_serv												scale_init,scale_serv used for Toldedo PN # 311

omni_serv			1.23		7.20.03		JIR			new functionality
omni_init

omni_serv			1.23		7.25.03		MMC			changed prover run counts loop.

meter_obj
recipe_obj			1.90		3/11/03		COR 		Modified Room Count and Resetable Count (1.90)

					1.90		5/08/03		MMC			Modified Room Count and Resetable Counter(1.90)	
														Zero OldSemi on batch change.

floop_init			1.30		9/5/03		MMC/COR		Modify new fastloop object.	
floop_serv

					2.00		09/23/03	JIR			Latest Version: Which has all previous version possible changes

alarm_init			2.01		10/9/03		MMC			Changed scaling of alarm duration - enter alarm duration in whole seconds		

alarm_serv												Added AckImg_C100. Allow acknoknowledge from two panels.					

Pump_obj			2.02 		01/01/04	MMC/SaPa	Added on_led and off_led		
pump_serv						01/13/03	JIR			Update pump_serv function to set on_led & off_led
analog_serv						01/15/03	JIR			In case of Analog Input (Case 0):
														Problem: 1. high_alarm once SET will not get RESET until raw value >= 6553 and value_f > high_sp
																 2. low_alarm is not working at all.
														Solution: SET/RESET of high_alarm and low_alarm should not be depended on raw value limit check: 
														That means, they should not be in depends on (raw >= 6553) condition check loop.
														Code change: Condition check for high_alarm and low_alarm moved outside of if(raw >= 6553) loop.

								01/16/03	JIR			Delete not required Data Types:
														FASTLOOP_obj (Because this data type is been replaced by FL_obj)
														RCAtyp (All elements are coverd in uMB_obj and implemented in uMB library)

DSUMP_obj			2.10		02/04/04	CHO/SaPa	Added New Data Type DSUMP_obj														
SUMP_obj												Updated Data Type SUMP_obj
ANALOG_obj												Updated Data Type ANALOG_obj
															
								02/16/04	CHO			Major Modifications to multiple objects.														
														term_serv
														meter_serv
														vgroup_serv 
														secur_serv
														hwv_init
														hwv_serv
														SearchIQNextCommand
														FailedIQResponse
														GoodIQResponse
														AnyIQValveCommFail
														iqv_init
														BitToAdr
														
TriggerCommands					02/25/04	SaPa/MMC	Added logic for recipe->OIP_Trigger 
RunTime_init		2.11		04/06/04	SaPa		Added New Data Type RunTime_obj		
RunTime
SUMP_obj		  	2.12		04/06/04	JIR			Added sump->_motor_run_command
														Updated Data Type, init, serv functions
														Replace HHL functionality with HL

TIMER_obj									JIR			Added 2s flag

GoodIQResponse		2.12		04/14/04	JIR/CHO		Corrected Case for Flow Quip Valve (Generate ->fault based on 

hwv_init			2.13		06/07/04	JIR			Corrected order of arguments for hwv_init function	

hscount_init		2.14		06/08/04	CHO/MMC		Added function to library for use in ALF.
hscount_serv
SUMP_obj			2.15		07/13/04	JIR			Add Flow Switch functionality in object
sump_init,												Add: FS, AlarmNoFlow, FlowDelaySecs to the DataType and init,serv 
sump_serv												functions.					
																													
BitToWOrd			2.16		07/22/04	SaPa		BitToWord made as a global function. Previously it was internal function.

PUMP_obj			2.16		08/05/04	SaPa		Added SatusNum USINT field used for PanelWare. 

ANALOG_obj			2.17		08/26/04	SaPa		Added case 3 - Analog Input (0-5 V) Scaling. 

ANALOG_obj			2.18		09/14/04	SaPa		Added hihi_alarm logic. 
PUMP_obj												Added toggle_oip
DETECTOR_obj      				  						Added New Object for UV/IR Detectors

RCASetClock 					11/05/04	MMC/SaPa	Modified to get correct format of clock data from Master RCA clock
sump_serv			2.19		11/29/04 	SaPa		Corrected sump->HL and sump->HHL logic.	
STATION_obj						12/30/04	SaPa		Added New Object.
stn_init
stn_serv

GoodIQResponse		2.20		02/28/05	SaPa/DJP	Added new case(IQ1_TYPE): Mapping for new Rotork Valve Modbus Card (MK2)
SearchIQNextCommand
sump_serv						02/23/05	DJP			Revised sump_serv so sump->Auto & sump->HL will auto start pump.
								03/16/05	SaPa		Updated logic for Checking Flow Switch while Motor Running
								
iq_serv				3.00		05/23/05	JIR			Updaed iq_serv and Supported subroutines to Support Dual Channel topology
GetIQActiveValve										(Search with "V3.00" to find the changes)
FailedIQResponse										Updated mx.h (IQ Valve Polling Implementation for Channel 2)
AnyIQValveCommFail
IQCommandFail	

scale_serv			3.01		06/08/05	JIR			Add (scale->RawValue != 0) condition to start scaling
AnyIQValveCommFail	3.02		09/02/05	JIR			Update the way to generate "FailedToCloseAlarm" and "FailedToOpenAlarm". 
														It will look for fully opened valve(LSO & LSC) rather than just look for LSO
floop_serv			3.03		9/16/05		SaPa		Modified function for fl->open_cmd & fl->close_cmd.	

recipe_serv			3.04		10/16/05	SaPa		Recipe CloseAll was continously writing into VALVE_obj close_cmd. 
														Which iq_serv function was not able to handle properly those continous commands.

GetIQActiveValve	3.05		11/07/05	SaPa		Changed Logic for Single Channel Polling 
GetActiveValve

SCALE_obj			3.06		06/09/06 	SaPa		Updated SCALE_obj, scale_init, scale_serv
scale_init												Changed types for RawMax, RawMin, RawValue from UNIT to REAL
scale_serv												Added new item typ, _valueUINT, _valueINT, _valueFLOAT
STATION_obj												Deleted STATION_obj, stn_init, stn_serv
TERMINAL_obj											Added items lockout, lockout_reset_oip, shutdown, ControlPowerFail
DETECTOR_obj      				  						Added items FaultAlarmTimer, FireAlarmTimer
ANALOG_obj												Added item "range"
analog_init
analog_serv												Added break in case AX, was Missing.
recipe_init						06/26/06				Made Gravity high and low ranges as variables to pass from function
recipe_serv												Enabled analog_serv functions, Added Gravity.
meter_serv												Fixed the problem - ResetCounter & RoomCount were not updating after batch change.
		
			
recipe_serv			3.07		07/13/06 	SaPa		Now only cycles through defined number of valves. 
vgroup_init
vgroup_serv
SV_Open
SV_Close
SequenceOverun

HWValve_obj			3.08		07/18/06	SaPa		Added continous command functionality.  
hwv_serv
hwv_init																				
VALVE_obj												Added opn, cls, stp to object. Changed FailToOpen, FailtoClose logic.
recipe_serv						07/10/06				Initialized newbbls, oldCloseAll and AllClose. 													


ANALOG_obj			3.09		08/19/06	SaPa		Added Transmitter-Current logic.  
analog_init												Changed Alarm SP's logic. If Alarm SP's are not initialized then only asign SP's to high and low range.
analog_serv


analog_serv			4.00		12/14/06	SaPa		Corrected anal->value_f and anal->value_i Calculations  
hscount_serv											Corrected h->Flow Calculations	

meter_init			4.01		01/23/07	SaPa		Added Hardwired IO's functionality into Recipes. Select type as HW_IO or SW_IO in meter_init function.
meter_serv												Replaced local newnetbbls to meter->newnetbbls. 			
recipe_init
recipe_serv													  
GravityTriggerCalc										Replaced newbbls to meter->newnetbbls for calculating DisplaceVolume.


fl_init				4.02		03/16/07	SaPa		ReWrote using Switch-Case.
fl_serv
FL_obj

recipe_init												Corrected Null Pointer for Gravity, in case of Recipe w/o Gravity.
recipe_serv
scale_serv												Removed patch (scale->RawValue != 0) 


iq2_serv			4.03		03/29/07	SaPa		Dual Channel ROTRK functions.
GetIQ2ActiveValve
FailedIQ2Response
IQ2CommandFail
AnyIQ2ValveCommFail

RECIPECMDS_obj		4.04		10/31/07	SaPa		Added Gravity Window Open Arm/Unarm Command. 
GravityTriggerCalc
STEP_obj												GravityWindowOpen
recipe_init												Gravity Change mantained Timer changed to 2 Sec from 1 Sec.
RECIPE_obj												Added GtyWindowOpenArm, GtyWindowOpenUnarm, GravityTrigTimer, MainlineRecipe, GravityTrigOnly for Main Line Recipes. 														
SequenceOverun											Inverted OverRunEnable bit to function it correctly.
recipe_serv												Removed recipe->DelayCloseTimer.IN for New shipper valve commands from OIP/RCA 	
														Corrected recipe->RoomExpired & recipe->RoomWarning - Count on recipe->CloseAll & recipe->LSO
														
ALARM_obj												Added ingroup - 16 Alarm bits packed into a word
alarm_serv												Corrected alarm holding.

meter_serv			4.05		11/12/07	SaPa		Treat meter->BatchAStatus as +ve edge instead of change of state. 

FSL_obj				4.06		11/15/07	SaPa		New Object. Corrected false No Flow Alarm on first scan. 
FSL

meter_serv			4.07		04/16/08	SaPa		meter->BatchAStatus made as an one shot.



recipe_init			5.00		08/28/08	SaPa		Added 32 bit and 64 bit shippers recipe.			
recipe_serv											

recipe_serv			5.01		11/23/08	KeCo		Added 16 bit divider capability			

CalculateStep		5.02		02/28/11	PMW		Added 16 bit and 32 bit recursive divider capability
CalculateStep32	
CalculateDividerValue
CalculateDividerValue32	
ShipperValveClose											Fixed Bounds checking in V16

recipe_serv			5.03		02/28/11	PMW		Added Third and Fourth next

recipe_serv			5.04		05/17/11	PMW		Prevent Erroneous "Gravity Window Missed" alarm when changing to a step with a gravity window

analog_serv			5.05		11/03/11	PMW		Modified AO to have correct float value if the low range is not zero.

GoodResponse		5.06		11/03/11	PMW		Prevent a "No Indication" in the MXVALVE when it initially goes open.

hwv_init			5.07		12/05/12	PMW		Added flat input and output variables to HWValve_obj.  These variables are used for inputs and
hwv_serv											outputs if no address is specified for them in hwv_init.

recipe_init			5.08		01/09/12	PMW		Initialize the Ticket Trigger for all four steps to ON.

GravityTriggerCalc  5.09		05/15/12    DES 	Fixed gravity miss alarm when exiting gravity window on meter reset

GravityTriggerCalc  5.10		12/04/12    PMW 	Added Product Incompatibility alarms and logic.

TriggerCommands		5.11		05/30/13	PMW		Changed trigger so that Recipe cannot trigger if the EOB command is still on.

analog_serv			5.12		11/15/17	CW/HBB	Changed analog obj fail to reset above 3.5mA
meter_serv											Added HW_OMNI type to meter objects for ON/OFF Batch Status from Omni

*/