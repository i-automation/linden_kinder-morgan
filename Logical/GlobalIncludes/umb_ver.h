/* User Created MODBUS library version History:
Piedmont Automation, Inc. 770-931-6500

-----------------------------------------------------------------------------------------------------------------------
Function/Datatype	Version		Date		By			Description						Comment/Changes
-----------------------------------------------------------------------------------------------------------------------
uMBM_init			V1.00		01/09/04	JIR/CHO		Modbus Master Initilization		New Function Introduced
uMBM_serv												Modbus Master Server			New Function Introduced

uMBS_init												Modbus Slave Initilization		New Function Introduced
uMBS_serv												Modbus Slave Server			New Function Introduced

uMB_obj
uMB_Time_obj

uMBS_serv			V1.01		09/24/04	CHO			MBSlave .BadMsg				Don't increment with no connection
uMB_obj													uMB_obj .RecordFail			Deleted
uMB_obj													uMB_obj .RecordStatus		Added. 1=Good,-1=Bad,0=Reset
Enraf_init																			New Function Introduced
Enraf_serv																			New Function Introduced
ENRAFMB_obj
ENRAFTANK_obj

uMBM_serv()						10/04/04	SAM			CommFail & CommFail_Crash	Improved Logic
uMBS_serv()
Enraf_serv			V1.02		01/14/05	SAM			Enraf_serv					Corrected for mb->RecordStatus
ENRAFTANK_obj																		Added Logic for Tank Alarms, Guage Status .

uMBS_serv			V1.02		01/21/05	JIR			MBSlave .BadMsg				Require to increment if status is greater than 0 and not equal to 65535
																					This BadMsg Counts are used for MX/IQ valve to Fail the perticular channel

ENRAFTANK_obj		V1.03		07/26/05	SAM			ENRAFTANK_obj				Updated ENRAFTANK_obj - Added ProductTemperature, NetStandardVolume, UsableVolume, ProductTempStatus

ENRAFTANK_obj		V1.04		09/08/05	SAM			ENRAFTANK_obj				Updated ENRAFTANK_obj - Added SafeFill, LevelHiHiSP, LevelHiSP, LevelLowSP

ENRAFTANK_obj		V1.05		11/02/05	SAM			ENRAFTANK_obj & Enraf_serv	Updated ENRAFTANK_obj - Added Disable. This will not poll Future Tanks in ModBus table.

ENRAFTANK_obj		V1.06		11/11/05	SAM			ENRAFTANK_obj & Enraf_serv	Updated ENRAFTANK_obj - Added Room and derived in Enraf_serv.

ENRAFTANK_obj		V1.07		01/12/06	SAM			ENRAFTANK_obj 				Updated ENRAFTANK_obj - Added GrossStandardVolume.

ENRAFTANK_obj		V1.08		01/24/06	SAM			ENRAFTANK_obj 				Updated ENRAFTANK_obj - Added MaxOperatingLevel, TotalObservedVolume
																					Now SafeFill is initialized in PLC.
																					Now Room = TotalObservedVolume - SafeFill

ENRAFTANK_obj		V1.09		01/31/06	SAM			ENRAFTANK_obj 				Updated ENRAFTANK_obj - Added ObservedDensity, Gravity, VCF.


ENRAFTANK_obj		V2.00		10/12/06	SAM			ENRAFTANK_obj, Enraf_serv	Updated ENRAFTANK_obj - Added Temperature_i
Enraf_serv
LNJ_init						10/27/06	SAM										New Function Introduced
LNJ_serv																			New Function Introduced
LNJMB_obj
LNJTANK_obj

Enraf_serv			V2.01		10/30/06	SAM			Enraf_serv, LNJ_serv		Keep Monitoring Tank level Alarms (DI), even if there is Communication Failure between ENRAF and PLC.
LNJ_serv
uMBM_serv												uMBM_serv					On Port Error before Exit Declare Communication Failures.


EnrafXS_serv		V2.02		11/08/06	SAM										New Function Introduced to communicate with ENRAF EntiXS Tank Gauging Package
ENRAFTANKXS_obj

EnrafXS_serv		V2.03		12/11/06	SAM										Updated New Programmable Alarm Status Register in ENRAFTANKXS_obj.
ENRAFTANKXS_obj

uMBM_init,		    V2.04		5/25/07		SAM/MMC		TIME type					Change UDINT to plctime for timeout, nocomm_sp1, nocomm_sp2
uMBS_init

umb.c				v2.04.1		28Dec09		PKC			Add #include <string.h> for use in AS3.0.80





















*/


