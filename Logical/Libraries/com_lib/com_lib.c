
/*****************************************************************************
**************************  Common Tool Library  *****************************
***************************** V3.05 - 11/22/05 *******************************/

#include <bur/plc.h>
#include <bur/plctypes.h>
#include <standard.h>
#include <com_lib.h>
#include <sys_lib.h>
#include <string.h>
#include <AsString.h>

/* SwapLong: Creates UDINT from 2 UINT Values */
UDINT SwapLong( UINT word1, UINT word2 )
{
UDINT x;
UINT hold[2];

	/* Get Consecutive */
	hold[1] = word1;
	hold[0] = word2;

	/* Create UDINT */
	memcpy( &x, hold, 4);

	/* Return UDINT */
	return x;
}

/* LongSwap: Creates 2 UINTs  from 1 UDINT Value */
BOOL LongSwap( UDINT value, UDINT word_adr )
{
UINT hold[2];

	hold[1] = (UINT) (value >> 16);
	hold[0] = (UINT) (value & 0x0000FFFF);

	/* Create UDINT */
	memcpy( (void *) word_adr, hold, 4);

	/* Return UDINT */
	return 0;
}

/* Bit will stay high */
BOOL GetBitLatch( UDINT word_adr, UDINT bit_adr )
{
BOOL *pBit	= (BOOL *) bit_adr;
UINT *pWord	= (UINT *) word_adr;

	/* Set PLC Value */
	if ( *pWord > 0 )
		*pBit	= 1;

	/* Reset Input */
	*pWord		= 0;

	/* Return: */
	return *pBit;
}


/*  Bit will stay high for 1 cycle*/
BOOL GetBitWrite( UDINT word_adr, UDINT bit_adr )
{
BOOL *pBit	= (BOOL *) bit_adr;
UINT *pWord	= (UINT *) word_adr;

	/* Set PLC Value */
	*pBit	= (BOOL)*pWord ;

	/* Reset Input */
	*pWord		= 0;

	/* Return: */
	return *pBit;
}

/* GetWordWrite: Only sets rcavalue and plcvalue when value is non zero. Resets value always */
BOOL GetWordWrite( UDINT value_adr, UDINT plcvalue_adr )
{

UINT *pValue	= (UINT *) value_adr;
UINT *pPlcValue	= (UINT *) plcvalue_adr;

/* New UINT Value */
if ( *pValue != 0xFFFF )
	*pPlcValue = *pValue;

	/* Reset Input */
	*pValue 	= 0xFFFF;

	/* Return: */
	return 0;

}


/* LongNoSwap: Sets */
BOOL LongNoSwap( UDINT value, UDINT word_adr )
{

	/* Create UDINT */
	memcpy( &value, (void *) word_adr, 4);

	/* Return BOOL */
	return 0;
}

/* GetLongRead: Only sets rcavalue and plcvalue when value is non zero. Resets value always */
BOOL GetLongRead( UDINT value_adr, UDINT plcvalue_adr, USINT type )
{
UINT hold[2];

UINT *value 		= (UINT *) value_adr;
UDINT *plcvalue  	= (UDINT *) plcvalue_adr;

/* Get Consecutive: Handle differently for M68K and I386 targets */
switch (type) {


/* m68k: Swapped */
case 1:
	hold[1] = *plcvalue >> 16;
	hold[0] = *plcvalue & 0x0000FFFF;

	/* Create UDINT */
	memcpy( value, hold, 4);
break;

/* i386: Consecutive */
case 2:
	memcpy( value, plcvalue, 4);
break;

default:
break;
}

/* Return BOOL */
return (*value > 0);
}

/* Converts a RCA 64K value to appropriate scaled value based on high and low values */
BOOL Analog64kToUINT( UDINT mb64k_adr , REAL high, REAL  low, UDINT out_adr )
{
UINT *mb64k 		= (UINT *) mb64k_adr;
UINT *out	 		= (UINT *) out_adr;

float holdf;

if ( mb64k > 0 ) {

	/* Convert to Analog Output */
	holdf 	= ( (float) 0x7FFF * (float) (*mb64k) ) / 64000.0;
	/* Float value engineering unit conversion */
	holdf 	= (holdf * (high - low) / 32767.0) + low;
	mb64k = 0;
	*out = (UINT) (holdf + .5);
}
return ( mb64k > 0 ) ;
}


/* Timer Initlization for GetLongWrite Function : GetLongWriteInit*/
BOOL GLW_Init( TONtyp *timer, USINT num_long_writes, UDINT pt )
{
USINT i;
	/* Set Preset Time for Defined number of Timers */
	for (i=0;i<num_long_writes;i++,timer++)
		timer->PT	= pt;
return 0;
}

BOOL GetLongWrite( UDINT value_adr, UDINT plcvalue_adr, TONtyp *timer, USINT type )
{
UDINT x;
UINT hold[2];

	UINT *pValue		= (UINT *) value_adr;
	UDINT *pPlcValue	= (UDINT *) plcvalue_adr;

	/* Get Consecutive: Handle differently for M68K and I386 targets */
	switch (type) {

	/* m68k: Consecutive */
	case 1:
		hold[0] = *pValue;
		hold[1] = *(pValue + 1);
	break;

	/* i386: Swapped */
	case 2:
		hold[0] = *(pValue + 1);
		hold[1] = *pValue;
	break;

	default:
	break;
	}

	/* Create UDINT */
	memcpy( &x, hold, 4);

	/* New Value in either MB Words */
	if ( (x != *pPlcValue) && (x != 0xFFFFFFFF) )
		timer->IN = 1;
	else
		timer->IN = 0;

	/* Execute Timer */
	TON( timer );

	/* Time to Update PLC Location */
	if ( timer->Q ) {

		/* Reset Inputs RCA Values to All FFFF */
		*pValue			= 0xFFFF;
		*(pValue + 1) 	= 0xFFFF;

		*pPlcValue	= x;

		return 1;
		}
	/* No New Value Transfered */
	else
		return 0;
}

USINT SetBit( USINT x, USINT index, BOOL value )
{
USINT y = 1;

	if (index > 7 )
		return x;
	else if ( value )
		return (x | (y << index));
	else
		return (x & ((y << index) ^ 0xFF));
}

/* Byte Swapping */
BOOL Trans_byte( UDINT dest_adr, UDINT src_adr, USINT length )
{
USINT *dest = (USINT *) dest_adr;
USINT *src	= (USINT *) src_adr;
USINT i;

	for (i=0;i<length/2;i++) {
		*(dest+i*2+0)	= *(src+i*2+1);
		*(dest+i*2+1)	= *(src+i*2+0);
	}

	return 0;
}

/* Byte Swapping */
BOOL Trans_word( UDINT dest_adr, UDINT src_adr )
{
USINT *dest = (USINT *) dest_adr;
USINT *src	= (USINT *) src_adr;
USINT i;

	for (i=0;i<1;i++) {

		*(dest+i*2+0)	= *(src+i*2+2);
		*(dest+i*2+1)	= *(src+i*2+3);

		*(dest+i*2+2)	= *(src+i*2+0);
		*(dest+i*2+3)	= *(src+i*2+1);

	}

	return 0;
}

/* Return: Quantity of High Bits */
USINT ByteToBit( USINT x, UDINT bit_adr )
{
USINT i, q = 0;
BOOL *b = (BOOL *) bit_adr;

    /* Assign Masks */

    for (i=0;i<8;i++,b++) {
        *b = (BOOL) ( x >> i ) & 0x01;
        if (*b)
            q++;
    }
    return q;
}

/*  Get LSB High Bit from Byte */
BOOL GetBit( UDINT byte_adr, UDINT bit_adr )
{
BOOL *pBit		= (BOOL *) bit_adr;
USINT *pByte	= (USINT *) byte_adr;

	/* Get LSB Bit */
	if ( *pByte == 1 )
		*pBit	= 1;
	else
		*pBit	= 0;

	/* Return: */
	return *pBit;
}


/* V3.03: Converts a Float Value to SCADA 64K value */
BOOL FloatToAnalog64k( UDINT in_adr, REAL high, REAL  low, UDINT mb64k_adr, BOOL Neg )
{
UINT *mb64k = (UINT *) mb64k_adr;
REAL *in	= (REAL *) in_adr;

float holdf, m, c;

if (((float)(*in) < 0) & Neg)
return 1;

if ( high > low ) {

	m = 64000/(high - low);
	c = 64000 - m * high;

	holdf = (m * (float)(*in)) + c;

	*mb64k = (UINT) (holdf + .5);
}
return ( high > low ) ;
}


/* V3.05: : Create Date String of format "MM/DD/YY" or "MM/DD/YYYY" */
BOOL CreateDateString( DATESTRING_obj *datestr, UDINT in_adr, BOOL typ)
{
UDINT dateYYYY, dateYY, dateDD, dateMM, hold, hold1, hold2;

UDINT *in;

	in  = (UDINT *) in_adr;

	dateYY = *in % 100;

	hold = *in / 100;

	hold1 = hold - dateYY / 100;

	dateDD = hold1 % 100;

	hold2 = hold1 / 100;

	dateMM = hold2 - dateDD / 100;

	if (typ)
		dateYYYY = dateYY + 2000;
	else
		dateYYYY = dateYY;


	/* Generate String */
	itoa(dateMM,   (UDINT) &datestr->sDate);
	itoa(dateDD,   (UDINT) &datestr->ddString);
	itoa(dateYYYY, (UDINT) &datestr->yyString);

	strcat(datestr->sDate, "/");
	strcat(datestr->sDate, datestr->ddString);
	strcat(datestr->sDate, "/");
	strcat(datestr->sDate, datestr->yyString);

/* Return Value */
return 0;
}

/* V3.05: : Create Date/Time String of format "MM/DD/YY" or "MM/DD/YYYY" and  "HH:MM:SS" and "MMDDYYHHMMSS" or "MMDDYYYYHHMMSS" */
BOOL CreateRTCString( RTCSTRING_obj *rtcstr, BOOL typ)
{
RTCtime_typ rtc;
UINT year, x = 0;

	/* Get Time from PLC */
	RTC_gettime( &rtc );

	if (typ)
		year = rtc.year;
	else
		year = rtc.year - 2000;

	/* Date: Generate String */
	if ( rtc.month < 10 ){
		itoa( x, (UDINT) &rtcstr->Date );
		itoa( rtc.month, (UDINT) &rtcstr->_mString );
		strcat( rtcstr->Date, rtcstr->_mString );
	}
	else
		itoa( rtc.month, (UDINT) &rtcstr->Date );

	if ( rtc.day < 10 ){
		itoa( x, (UDINT) &rtcstr->ddString );
		itoa( rtc.day, (UDINT) &rtcstr->_dString );
		strcat( rtcstr->ddString, rtcstr->_dString );
	}
	else
		itoa( rtc.day, (UDINT) &rtcstr->ddString );

	if ( year < 10 ){
		itoa( x, (UDINT) &rtcstr->yyyyString );
		itoa( year, (UDINT) &rtcstr->_yString );
		strcat( rtcstr->yyyyString, rtcstr->_yString );
	}
	else
		itoa( year, (UDINT) &rtcstr->yyyyString );

	strcat( rtcstr->Date, "/" );
	strcat( rtcstr->Date, rtcstr->ddString );
	strcat( rtcstr->Date, "/" );
	strcat( rtcstr->Date, rtcstr->yyyyString );


	/* Time: Generate String */
	if ( rtc.hour < 10 ){
		itoa( x, (UDINT) &rtcstr->Time );
		itoa( rtc.hour, (UDINT) &rtcstr->_hString );
		strcat( rtcstr->Time, rtcstr->_hString );
	}
	else
		itoa( rtc.hour, (UDINT) &rtcstr->Time );

	if ( rtc.minute < 10 ){
		itoa( x, (UDINT) &rtcstr->mmString );
		itoa( rtc.minute, (UDINT) &rtcstr->_minString );
		strcat( rtcstr->mmString, rtcstr->_minString );
	}
	else
		itoa( rtc.minute, (UDINT) &rtcstr->mmString );

	if ( rtc.second < 10 ){
		itoa( x, (UDINT) &rtcstr->ssString );
		itoa( rtc.second, (UDINT) &rtcstr->_sString );
		strcat( rtcstr->ssString, rtcstr->_sString );
	}
	else
		itoa( rtc.second, (UDINT) &rtcstr->ssString );

	strcat( rtcstr->Time, ":" );
	strcat( rtcstr->Time, rtcstr->mmString );
	strcat( rtcstr->Time, ":" );
	strcat( rtcstr->Time, rtcstr->ssString );


	/* DateTime: Generate String */
	if ( rtc.month < 10 ){
		itoa( x, (UDINT) &rtcstr->DateTime );
		itoa( rtc.month, (UDINT) &rtcstr->_mString );
		strcat( rtcstr->DateTime, rtcstr->_mString );
	}
	else
		itoa( rtc.month, (UDINT) &rtcstr->DateTime );

	strcat( rtcstr->DateTime, rtcstr->ddString );
	strcat( rtcstr->DateTime, rtcstr->yyyyString );

	memcpy( &rtcstr->_Date, &rtcstr->DateTime, sizeof(rtcstr->_Date));

	/* Time: Generate String */
	if ( rtc.hour < 10 ){
		itoa( x, (UDINT) &rtcstr->_Time );
		itoa( rtc.hour, (UDINT) &rtcstr->_hString );
		strcat( rtcstr->_Time, rtcstr->_hString );
	}
	else
		itoa( rtc.hour, (UDINT) &rtcstr->_Time );

	strcat( rtcstr->_Time, rtcstr->mmString );
	strcat( rtcstr->_Time, rtcstr->ssString );

	strcat( rtcstr->DateTime, rtcstr->_Time );

/* Return Value */
return 0;
}

UINT EngTo64k( UINT f, float low, float high )
{
float m;

	m = 64000.0/(high-low);
	return (m*(f-low));
}

UDINT GetLong( UINT *value )
{
UDINT x;
UINT hold[2];

	/* Get Consecutive */
	hold[0] = *value;
	hold[1] = *(value + 1);
	
	/* Create UDINT */
	memcpy( &x, hold, 4);
	
	/* Return UDINT */
	return x;
}

