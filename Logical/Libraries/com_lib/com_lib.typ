
TYPE
	DATESTRING_obj : 	STRUCT 
		sDate : STRING[10];
		ddString : STRING[2];
		yyString : STRING[4];
	END_STRUCT;
	RTCSTRING_obj : 	STRUCT 
		ddString : STRING[2];
		yyyyString : STRING[4];
		mmString : STRING[2];
		ssString : STRING[2];
		Date : STRING[10];
		Time : STRING[8];
		DateTime : STRING[14];
		_Date : STRING[8];
		_Time : STRING[6];
		_yString : STRING[4];
		_mString : STRING[2];
		_dString : STRING[2];
		_hString : STRING[2];
		_minString : STRING[2];
		_sString : STRING[2];
	END_STRUCT;
END_TYPE
