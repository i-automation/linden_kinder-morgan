
FUNCTION GetLongWrite : BOOL
	VAR_INPUT
		value_adr : UDINT;
		plcvalue_adr : UDINT;
		timer : TON;
		type : USINT;
	END_VAR
END_FUNCTION

FUNCTION GLW_Init : BOOL
	VAR_INPUT
		timer : TON;
		num_long_writes : USINT;
		pt : UDINT;
	END_VAR
END_FUNCTION

FUNCTION GetWordWrite : BOOL
	VAR_INPUT
		value_adr : UDINT;
		plcvalue_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION GetBit : BOOL
	VAR_INPUT
		byte_adr : UDINT;
		bit_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION ByteToBit : USINT
	VAR_INPUT
		x : USINT;
		bit_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION Trans_word : BOOL
	VAR_INPUT
		dest_adr : UDINT;
		src_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION Trans_byte : BOOL
	VAR_INPUT
		dest_adr : UDINT;
		src_adr : UDINT;
		length : USINT;
	END_VAR
END_FUNCTION

FUNCTION SetBit : USINT
	VAR_INPUT
		x : USINT;
		index : USINT;
		value : BOOL;
	END_VAR
END_FUNCTION

FUNCTION GetLongRead : BOOL
	VAR_INPUT
		value_adr : UDINT;
		plcvalue_adr : UDINT;
		type : USINT;
	END_VAR
END_FUNCTION

FUNCTION LongSwap : BOOL
	VAR_INPUT
		value : UDINT;
		word_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION SwapLong : UDINT
	VAR_INPUT
		word1 : UINT;
		word2 : UINT;
	END_VAR
END_FUNCTION

FUNCTION GetBitLatch : BOOL
	VAR_INPUT
		word_adr : UDINT;
		bit_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION FloatToAnalog64k : BOOL
	VAR_INPUT
		in_adr : UDINT;
		high : REAL;
		low : REAL;
		mb64k_adr : UDINT;
		Neg : BOOL;
	END_VAR
END_FUNCTION

FUNCTION CreateDateString : BOOL
	VAR_INPUT
		datestr : DATESTRING_obj;
		in_adr : UDINT;
		typ : BOOL;
	END_VAR
END_FUNCTION

FUNCTION CreateRTCString : BOOL
	VAR_INPUT
		rtcstr : RTCSTRING_obj;
		typ : BOOL;
	END_VAR
END_FUNCTION

FUNCTION GetBitWrite : BOOL
	VAR_INPUT
		word_adr : UDINT;
		bit_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION Analog64kToUINT : BOOL
	VAR_INPUT
		mb64k_adr : UDINT;
		high : REAL;
		low : REAL;
		out_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION LongNoSwap : BOOL
	VAR_INPUT
		value : UDINT;
		word_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION EngTo64k : UINT
	VAR_INPUT
		f : UINT;
		low : REAL;
		high : REAL;
	END_VAR
END_FUNCTION

FUNCTION GetLong : UDINT
	VAR_INPUT
		value : REFERENCE TO UINT;
	END_VAR
END_FUNCTION
