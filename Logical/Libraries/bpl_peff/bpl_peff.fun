FUNCTION timer1s : BOOL 
VAR_INPUT
		t	:TIMER1S_obj;	
	END_VAR
END_FUNCTION
FUNCTION avg_serv : BOOL 
VAR_INPUT
		avg	:AVG_obj;	
	END_VAR
END_FUNCTION
FUNCTION avg_init : BOOL 
VAR_INPUT
		avg	:AVG_obj;	
		raw_adr	:UDINT;	
		number	:USINT;	
		t_adr	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION peff_serv : BOOL 
VAR_INPUT
		pf	:PUMPEFF_obj;	
	END_VAR
END_FUNCTION
FUNCTION peff_init : BOOL 
VAR_INPUT
		pf	:PUMPEFF_obj;	
		LowPumpSP	:REAL;	
		MpPerSP	:UINT;	
		MpHPSP	:UINT;	
		DpDB	:UINT;	
		FlowRateDB	:UINT;	
		MtrEfficiency	:REAL;	
	END_VAR
END_FUNCTION
