/*****************************************************************************
**************************  Buckeye Pump Efficiency Library ******************
*********************************** V1.00 - 06.08.2004***********************/

#include <bur/plc.h>
#include <bur/plctypes.h>
#include "SYS_lib.h"
#include "Standard.h"
#include "bpl_peff.h"

BOOL avg_init( AVG_obj *avg, UDINT raw_adr, USINT number, UDINT t_adr )
{

	/* Bad Object */
	if ( !avg )
		return 0;

	/* Assign Raw Address */
	avg->raw_adr 		= raw_adr;
	avg->timer_adr		= t_adr;

	/* Assign Number */
	if ( (30>number) && (number>0) )
		avg->number = number;
	/* Return Value */
	return 1;
}

BOOL avg_serv( AVG_obj *avg )
{
USINT i,j;
float holdf = 0;

	/* get Timer */
	TIMER1S_obj *t = (TIMER1S_obj *) avg->timer_adr;
	/* Get Raw */
	avg->Raw = *(UINT *) avg->raw_adr;

	/* Float value engineering unit conversion
	anal->Value_f = ( anal->Raw - anal->Raw_0 ) * (anal->Span_Value) / ( anal->Raw_1 - anal->Raw_0 );*/
	avg->Value_f	= (float) avg->Raw;

	/* Shift Well */
	if (t->F10s) {
		for (i=29;i>0;i--)
			avg->well[i] = avg->well[i-1];
		/* Insert New Point in Well */
		avg->well[0] = avg->Value_f;
		}

	/* */
	for (j=0;j<avg->number;j++)
		holdf	   = holdf + avg->well[j];

	/* Float Average */
	avg->Avg_f = holdf / (float) avg->number;

	/* Return Value */
	return 0;
}

/* V1.01: Allow C100ms, C1s, C2s and C10s to increment to 65535 each */
BOOL timer1s( TIMER1S_obj *t )
{
UINT ticks;
UINT rem1s,rem100ms,rem10s;

	/* Get Ticks */
	ticks = TIM_ticks();

	/* New Tick: Base */
	if ( ticks != t->old )
	    t->C10ms++;

	/* 100ms */
	rem100ms 	= t->C10ms % 10;
	if ( rem100ms < 5 ) {
		if (t->F100ms)
			t->C100ms++;
		t->F100ms = 0;
	}
	else
		t->F100ms = 1;


	/* 1s */
	rem1s 		= t->C100ms % 10;
	if ( rem1s < 5 ) {
		if (t->F1s)
			t->C1s++;
		t->F1s = 0;
	}
	else
		t->F1s = 1;

	/* 10s */
	rem10s 		= t->C1s % 10;
	if ( rem10s < 5 ) {
		if (t->F10s)
			t->C10s++;
		t->F10s = 0;
	}
	else
		t->F10s = 1;



	/* Update Old */
	t->old = ticks;

	/* Return Value */
	return 0;
}

BOOL peff_init( PUMPEFF_obj *pf, REAL LowPumpSP, UINT MpPerSP, UINT MpHPSP, UINT DpDB, UINT FlowRateDB, REAL MtrEfficiency )
{
	/* Bad Object */
	if ( !pf )
		return 0;

	/* Assign Values */
	pf->LowPumpSP			= LowPumpSP;
	pf->MpPerSP				= MpPerSP;
	pf->MpHPSP				= MpHPSP;
	pf->DpDB				= DpDB;
	pf->FlowRateDB			= FlowRateDB;
	pf->MtrEfficiency		= MtrEfficiency;

	/* Return Value */
	return 1;

}

BOOL peff_serv( PUMPEFF_obj *pf )
{
REAL x;

	/* Calculation for Pump Efficiency (%) */
	pf->_x = 0.04088 * (pf->Dp_Avg) * (pf->FlowRate_Avg);
	pf->_y = (pf->MtrEfficiency) * (pf->Horsepower);
	if ( pf->_y  > 0 )
		pf->PumpEff	= pf->_x / pf->_y;
	else
		pf->PumpEff	= 0.0;

	/* Convert 64k Value for Pump Efficiency */
	x = pf->PumpEff * 64000.0/100.0;
	pf->PumpEff_64k	= (UINT)x;

	/* Check for Low Pump Efficiency */
	if ( (pf->PumpEff < pf->LowPumpSP) & (pf->_lowEffActive) )
		pf->_lowEffEnable	= 1;
	else
		pf->_lowEffEnable	= 0;

	/* Differential Pressure */
	pf->Dp_HighDB	= pf->Dp_Avg + pf->DpDB;						/* Calculate High DB */
	pf->Dp_LowDB	= pf->Dp_Avg - pf->DpDB;						/* Calculate Low DB */
	/* Flow Rate */
	pf->FlowRate_HighDB	= pf->FlowRate_Avg + pf->FlowRateDB;		/* Calculate High DB */
	pf->FlowRate_LowDB	= pf->FlowRate_Avg - pf->FlowRateDB;		/* Calculate Low DB */

	/* Differential Pressure is Within Deadband */
	if ( (pf->Dp_Avg < pf->Dp_HighDB) & (pf->Dp_Avg > pf->Dp_LowDB) )
		pf->Dp_constant	= 1;
	else
		pf->Dp_constant	= 0;

	/* Flow Rate is Within Deadband */
	if ( (pf->FlowRate_Avg < pf->FlowRate_HighDB) & (pf->FlowRate_Avg > pf->FlowRate_LowDB) )
		pf->FlowRate_constant	= 1;
	else
		pf->FlowRate_constant	= 0;


	/*Calculate HP Percentage Increase */
	pf->_oldHP		= pf->_newHP;
	pf->_newHP		= pf->Horsepower;

	if ( (pf->_oldHP != pf->_newHP) & (pf->_newHP > pf->_oldHP) )
		pf->_diffHP	= pf->_newHP - pf->_oldHP;

	/* Generate Lockout */
	if (pf->_diffHP > pf->MpPerSP)
		pf->HPIncrease	= 1;
	else
		pf->HPIncrease	= 0;

	if (pf->_newHP < pf->_oldHP){
		pf->_diffHP		= 0;
		pf->HPIncrease	= 0;
		}

	/* Motor HP increase SP : Enable when Pump is Running */
	if (pf->PumpStatus) {
		if ( (pf->Horsepower >= pf->MpHPSP) & (pf->_HPLimitET < pf->MpIncTimeSP) )
			pf->HPIncTime 	= 1;
		else
			pf->HPIncTime 	= 0;
		}
	else
		pf->HPIncTime 		= 0;

	/* Return Value */
	return 0;

}


