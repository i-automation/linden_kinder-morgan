TYPE
	tr_axisLabel_typ : STRUCT
		margin	:INT;	
		leadingZero	:USINT;	
		nbDecimalPlaces	:USINT;	
		seperator	:USINT;	
		prescaler	:UINT;	
		nbChar	:USINT;	
		res0	:USINT;	
		pValue	:UDINT;	
		pEnable	:UDINT;	
	END_STRUCT;
			tr_axisText_typ : STRUCT
		fontIndex	:UINT;	
		width	:USINT;	
		height	:USINT;	
		foreColor	:UINT;	
		backColor	:UINT;	
		textOutPosX	:UINT;	
		textOutPosY	:UINT;	
	END_STRUCT;
			tr_cursor_typ : STRUCT
		enable	:BOOL;	
		sizeEyeCatcher	:USINT;	
		color	:UINT;	
		x	:DINT;	
		y	:DINT;	
		sampleIndex	:DINT;	
	END_STRUCT;
END_TYPE
