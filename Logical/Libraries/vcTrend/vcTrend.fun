FUNCTION_BLOCK TR_autoCurve
VAR
		cursor	:tr_cursor_typ;	
		labelText	:tr_axisText_typ;	
		labelX	:tr_axisLabel_typ;	
		labelY	:tr_axisLabel_typ;	
		enableLabelX	:BOOL;	
		enableLabelY	:BOOL;	
		labelValueX	:DINT;	
		pCurveColor	:UDINT;	
		labelCfgDone	:UINT;	
		fDTGetTime	:DTGetTime;	
		minYzoom	:DINT;	
		maxYzoom	:DINT;	
		labelHandleX	:UDINT;	
		labelHandleY	:UDINT;	
		samplingTimer	:UDINT;	
		cycT	:UDINT;	
		fRTInfo	:RTInfo;	
	END_VAR
	VAR_INPUT
		visible	:BOOL;	
		trendId	:UDINT;	
		tracedValue	:DINT;	
		minY	:DINT;	
		maxY	:DINT;	
		color	:UINT;	
	END_VAR
	VAR_OUTPUT
		status	:UINT;	
		id	:UDINT;	
		pData	:UDINT;	
	END_VAR
END_FUNCTION_BLOCK
FUNCTION_BLOCK TR_autoTrend
VAR
		curveIdOld	:UDINT;	
		scrollXold	:DINT;	
		zoomXold	:TIME;	
		scrollYold	:DINT;	
		zoomYold	:UINT;	
		cursorA	:tr_cursor_typ;	
		cycT	:UDINT;	
		timePeriodX	:TIME;	
		zoomFactorX	:INT;	
	END_VAR
	VAR_INPUT
		visible	:BOOL;	
		reset	:BOOL;	
		vcHandle	:UDINT;	
		left	:UINT;	
		top	:UINT;	
		width	:UINT;	
		height	:UINT;	
		backColor	:UINT;	
		traceLength	:UDINT;	
		gridX	:TIME;	
		gridY	:UINT;	
		gridColor	:UINT;	
		curveIndex	:UINT;	
		cursorEnable	:BOOL;	
		cursorColor	:UINT;	
		cursorPosX	:DINT;	
		fontIndex	:UINT;	
		fontForeColor	:UINT;	
		fontBackColor	:UINT;	
		labelModeX	:UDINT;	
		nbDecimalPlacesY	:USINT;	
		separatorY	:USINT;	
		nbDigitsY	:UINT;	
		scrollX	:DINT;	
		scrollY	:DINT;	
		zoomX	:TIME;	
		zoomY	:UINT;	
		samplingTime	:TIME;	
	END_VAR
	VAR_OUTPUT
		status	:UINT;	
		id	:UDINT;	
		cursorPosY	:DINT;	
	END_VAR
END_FUNCTION_BLOCK
FUNCTION TR_addLabel : UINT 
VAR_INPUT
		trendId	:UDINT;	
		curveId	:UDINT;	
		vcHandle	:UDINT;	
		pTextCfg	:UDINT;	
		pLabelCfgX	:UDINT;	
		pLabelCfgY	:UDINT;	
		waitCycle	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_setCursorXY : UINT 
VAR_INPUT
		curveId	:UDINT;	
		pCursorAx	:UDINT;	
		pCursorAy	:UDINT;	
		pCursorBx	:UDINT;	
		pCursorBy	:UDINT;	
		pDeltaX	:UDINT;	
		pDeltaY	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_deinit : UINT 
VAR_INPUT
		trendId	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_action : UINT 
VAR_INPUT
		trendId	:UDINT;	
		action	:UINT;	
		value	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_reset : UINT 
VAR_INPUT
		enable	:BOOL;	
		trendId	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_redraw : UINT 
VAR_INPUT
		enable	:BOOL;	
		trendId	:UDINT;	
		vcHandle	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_draw : UINT 
VAR_INPUT
		enable	:BOOL;	
		trendId	:UDINT;	
		vcHandle	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_record : UINT 
VAR_INPUT
		enable	:BOOL;	
		trendId	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_addChart : UINT 
VAR_INPUT
		trendId	:UDINT;	
		pNbPoints	:REFERENCE TO UINT;	
		pData	:REFERENCE TO DINT;	
		pEnable	:REFERENCE TO BOOL;	
		minY	:DINT;	
		maxY	:DINT;	
		minX	:DINT;	
		maxX	:DINT;	
		color	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_addCurve : UINT 
VAR_INPUT
		trendId	:UDINT;	
		pLiveVal	:REFERENCE TO DINT;	
		ppData	:REFERENCE TO UDINT;	
		pEnable	:REFERENCE TO BOOL;	
		minY	:DINT;	
		maxY	:DINT;	
		color	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_addGrid : UINT 
VAR_INPUT
		trendId	:UDINT;	
		gridX	:UINT;	
		gridY	:UINT;	
		color	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION TR_init : UINT 
VAR_INPUT
		pTrendId	:REFERENCE TO UDINT;	
		nbValues	:UINT;	
		left	:UINT;	
		top	:UINT;	
		sampleWidth	:UINT;	
		height	:UINT;	
		backColor	:UINT;	
	END_VAR
END_FUNCTION
