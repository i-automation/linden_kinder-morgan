/* Automation Studio Generated Header File, Format Version 1.00 */
/* do not change */
#ifndef VCTREND_H_
#define VCTREND_H_
#define _WEAK	__attribute__((__weak__))

#include <bur/plctypes.h>

#include <AsTime.h>
#include <brsystem.h>


/* Constants */
_WEAK const unsigned short COLOR_TR_AQUA = 196;
_WEAK const unsigned short COLOR_TR_BLACK = 0;
_WEAK const unsigned short COLOR_TR_BLUE = 201;
_WEAK const unsigned short COLOR_TR_FUCHSIA = 21;
_WEAK const unsigned short COLOR_TR_GRAY = 8;
_WEAK const unsigned short COLOR_TR_GREEN = 2;
_WEAK const unsigned short COLOR_TR_LIME = 10;
_WEAK const unsigned short COLOR_TR_MAROON = 159;
_WEAK const unsigned short COLOR_TR_NAVY = 1;
_WEAK const unsigned short COLOR_TR_OLIVE = 157;
_WEAK const unsigned short COLOR_TR_PURPLE = 5;
_WEAK const unsigned short COLOR_TR_RED = 51;
_WEAK const unsigned short COLOR_TR_SILVER = 7;
_WEAK const unsigned short COLOR_TR_TEAL = 217;
_WEAK const unsigned short COLOR_TR_WHITE = 15;
_WEAK const unsigned short COLOR_TR_YELLOW = 46;
_WEAK const unsigned short ERR_TR_BUSY = 65534;
_WEAK const unsigned short ERR_TR_DENIED = 38709;
_WEAK const unsigned short ERR_TR_INVADDRESS = 38706;
_WEAK const unsigned short ERR_TR_INVCURVECONF = 38704;
_WEAK const unsigned short ERR_TR_INVHANDLE = 38700;
_WEAK const unsigned short ERR_TR_INVVALUE = 38707;
_WEAK const unsigned short ERR_TR_NOTACTIVE = 65535;
_WEAK const unsigned short ERR_TR_TOFEWVALUES = 38703;
_WEAK const unsigned short ERR_TR_UNKNOWN = 38708;
_WEAK const unsigned short TR_ACTION_DRAW_VIRTUAL = 7;
_WEAK const unsigned short TR_ACTION_FREEZE = 11;
_WEAK const unsigned short TR_ACTION_GET_CURVE_ID = 3;
_WEAK const unsigned short TR_ACTION_GET_CURVE_LABEL_BUF_X = 21;
_WEAK const unsigned short TR_ACTION_GET_DRAW_HANDLE = 1;
_WEAK const unsigned short TR_ACTION_GET_GRID_HANDLE = 2;
_WEAK const unsigned short TR_ACTION_GET_LABEL_BUFFER_X = 15;
_WEAK const unsigned short TR_ACTION_GET_LABEL_HANDLE_X = 24;
_WEAK const unsigned short TR_ACTION_GET_LABEL_HANDLE_Y = 25;
_WEAK const unsigned short TR_ACTION_GET_P_CURVE_COLOR = 9;
_WEAK const unsigned short TR_ACTION_GET_QUEUE_INDEX = 6;
_WEAK const unsigned short TR_ACTION_GET_QUEUE_LEN = 34;
_WEAK const unsigned short TR_ACTION_GET_START_INDEX = 31;
_WEAK const unsigned short TR_ACTION_GET_TOTAL_SAMPLES = 35;
_WEAK const unsigned short TR_ACTION_IGNORE_LABEL_AREA = 32;
_WEAK const unsigned short TR_ACTION_ROTATE = 33;
_WEAK const unsigned short TR_ACTION_SET_BUFFER = 10;
_WEAK const unsigned short TR_ACTION_SET_DATA_SOURCE = 22;
_WEAK const unsigned short TR_ACTION_SET_GRID_ENABLE = 0;
_WEAK const unsigned short TR_ACTION_SET_GRID_OFFSET = 20;
_WEAK const unsigned short TR_ACTION_SET_LABEL_CURVE = 16;
_WEAK const unsigned short TR_ACTION_SET_LIVE_DATA_TYPE = 13;
_WEAK const unsigned short TR_ACTION_SET_LIVE_SOURCE = 14;
_WEAK const unsigned short TR_ACTION_SET_MAX_X = 29;
_WEAK const unsigned short TR_ACTION_SET_MAX_Y = 5;
_WEAK const unsigned short TR_ACTION_SET_MIN_X = 30;
_WEAK const unsigned short TR_ACTION_SET_MIN_Y = 4;
_WEAK const unsigned short TR_ACTION_SET_PAINT_MODE = 23;
_WEAK const unsigned short TR_ACTION_SET_QUEUE_INDEX = 27;
_WEAK const unsigned short TR_ACTION_SET_QUEUE_LEN = 28;
_WEAK const unsigned short TR_ACTION_SET_REFRESH = 8;
_WEAK const unsigned short TR_ACTION_SET_TOTAL_SAMPLES = 26;
_WEAK const unsigned short TR_ACTION_SET_ZOOM = 12;
_WEAK const unsigned short TR_ACTION_SET_ZOOM_CURSOR_X = 18;
_WEAK const unsigned short TR_ACTION_SET_ZOOM_CURSOR_Y = 19;
_WEAK const unsigned short TR_ACTION_SET_ZOOM_DISPLAY_MODE = 17;


/* Datatypes */
typedef struct tr_axisLabel_typ
{
	signed short margin;
	unsigned char leadingZero;
	unsigned char nbDecimalPlaces;
	unsigned char seperator;
	unsigned short prescaler;
	unsigned char nbChar;
	unsigned char res0;
	unsigned long pValue;
	unsigned long pEnable;
} tr_axisLabel_typ;

typedef struct tr_axisText_typ
{
	unsigned short fontIndex;
	unsigned char width;
	unsigned char height;
	unsigned short foreColor;
	unsigned short backColor;
	unsigned short textOutPosX;
	unsigned short textOutPosY;
} tr_axisText_typ;

typedef struct tr_cursor_typ
{
	plcbit enable;
	unsigned char sizeEyeCatcher;
	unsigned short color;
	signed long x;
	signed long y;
	signed long sampleIndex;
} tr_cursor_typ;



/* Datatypes of function blocks */
typedef struct TR_autoCurve
{
	/* VAR_INPUT (analogous) */
	unsigned long trendId;
	signed long tracedValue;
	signed long minY;
	signed long maxY;
	unsigned short color;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	unsigned long id;
	unsigned long pData;
	/* VAR (analogous) */
	struct tr_cursor_typ cursor;
	struct tr_axisText_typ labelText;
	struct tr_axisLabel_typ labelX;
	struct tr_axisLabel_typ labelY;
	signed long labelValueX;
	unsigned long pCurveColor;
	unsigned short labelCfgDone;
	struct DTGetTime fDTGetTime;
	signed long minYzoom;
	signed long maxYzoom;
	unsigned long labelHandleX;
	unsigned long labelHandleY;
	unsigned long samplingTimer;
	unsigned long cycT;
	struct RTInfo fRTInfo;
	/* VAR_INPUT (digital) */
	plcbit visible;
	/* VAR (digital) */
	plcbit enableLabelX;
	plcbit enableLabelY;
} TR_autoCurve_typ;

typedef struct TR_autoTrend
{
	/* VAR_INPUT (analogous) */
	unsigned long vcHandle;
	unsigned short left;
	unsigned short top;
	unsigned short width;
	unsigned short height;
	unsigned short backColor;
	unsigned long traceLength;
	plctime gridX;
	unsigned short gridY;
	unsigned short gridColor;
	unsigned short curveIndex;
	unsigned short cursorColor;
	signed long cursorPosX;
	unsigned short fontIndex;
	unsigned short fontForeColor;
	unsigned short fontBackColor;
	unsigned long labelModeX;
	unsigned char nbDecimalPlacesY;
	unsigned char separatorY;
	unsigned short nbDigitsY;
	signed long scrollX;
	signed long scrollY;
	plctime zoomX;
	unsigned short zoomY;
	plctime samplingTime;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	unsigned long id;
	signed long cursorPosY;
	/* VAR (analogous) */
	unsigned long curveIdOld;
	signed long scrollXold;
	plctime zoomXold;
	signed long scrollYold;
	unsigned short zoomYold;
	struct tr_cursor_typ cursorA;
	unsigned long cycT;
	plctime timePeriodX;
	signed short zoomFactorX;
	/* VAR_INPUT (digital) */
	plcbit visible;
	plcbit reset;
	plcbit cursorEnable;
} TR_autoTrend_typ;



/* Prototyping of functions and function blocks */
void TR_autoCurve(TR_autoCurve_typ* inst);
void TR_autoTrend(TR_autoTrend_typ* inst);
unsigned short TR_addLabel(unsigned long trendId, unsigned long curveId, unsigned long vcHandle, unsigned long pTextCfg, unsigned long pLabelCfgX, unsigned long pLabelCfgY, unsigned short waitCycle);
unsigned short TR_setCursorXY(unsigned long curveId, unsigned long pCursorAx, unsigned long pCursorAy, unsigned long pCursorBx, unsigned long pCursorBy, unsigned long pDeltaX, unsigned long pDeltaY);
unsigned short TR_deinit(unsigned long trendId);
unsigned short TR_action(unsigned long trendId, unsigned short action, unsigned long value);
unsigned short TR_reset(plcbit enable, unsigned long trendId);
unsigned short TR_redraw(plcbit enable, unsigned long trendId, unsigned long vcHandle);
unsigned short TR_draw(plcbit enable, unsigned long trendId, unsigned long vcHandle);
unsigned short TR_record(plcbit enable, unsigned long trendId);
unsigned short TR_addChart(unsigned long trendId, unsigned short* pNbPoints, signed long* pData, plcbit* pEnable, signed long minY, signed long maxY, signed long minX, signed long maxX, unsigned short color);
unsigned short TR_addCurve(unsigned long trendId, signed long* pLiveVal, unsigned long* ppData, plcbit* pEnable, signed long minY, signed long maxY, unsigned short color);
unsigned short TR_addGrid(unsigned long trendId, unsigned short gridX, unsigned short gridY, unsigned short color);
unsigned short TR_init(unsigned long* pTrendId, unsigned short nbValues, unsigned short left, unsigned short top, unsigned short sampleWidth, unsigned short height, unsigned short backColor);



#endif /* VCTREND_H_ */