FUNCTION ramp_init : DINT 
VAR_INPUT
		r	:RAMP_obj;	
		SPlow	:REAL;	
		SPhigh	:REAL;	
		speed_ms	:USINT;	
		remote_adr	:UDINT;	
		type	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION pidloop_init : DINT 
VAR_INPUT
		pl	:PIDLOOP_obj;	
		para_adr	:UDINT;	
		SP_adr	:UDINT;	
		PV_adr	:UDINT;	
		PVlow	:REAL;	
		PVhigh	:REAL;	
		Y_adr	:UDINT;	
		type	:BOOL;	
	END_VAR
END_FUNCTION
FUNCTION ramp_serv : DINT 
VAR_INPUT
		r	:RAMP_obj;	
	END_VAR
END_FUNCTION
FUNCTION pidloop_serv : DINT 
VAR_INPUT
		pl	:PIDLOOP_obj;	
	END_VAR
END_FUNCTION
FUNCTION pid_serv : DINT 
VAR_INPUT
		p	:PID_obj;	
	END_VAR
END_FUNCTION
FUNCTION pid_init : DINT 
VAR_INPUT
		p	:PID_obj;	
		level0_adr	:UDINT;	
		LoopCount	:USINT;	
		loop_adr	:UDINT;	
		remote_adr	:UDINT;	
		basetimer_adr	:UDINT;	
		tick_ms	:UINT;	
	END_VAR
END_FUNCTION
