
/* VERSION 2.00 see bpl_pid_ver.h for revisions */
/**
@file bpl_pid.c
@brief Implementaion of the BPL PID object library
@author Piedmont Automation
@version 2.00
@date 10/16/2003

This is an implementation of the PID object for BPL. Functionality
is derived from the available PID implementation titled:
** File:		PID_CTRL.C
** Title:		PID Control Loops
** Customer:	BUCKEYE PIPE LINE COMPANY.
** Date:		May 12, 2000
*/
/*********************************************************************
**************************  Buckeye PID Library	**********************
****************************** V2.08 08/16/07 ***********************/
#include <bur/plc.h>
#include <bur/plctypes.h>
#include <loopcont.h>
#include <bpl_pid.h>
#include <string.h>

/*Prototypes of local functions*/
static UINT LoopSelect(PID_obj * );
DINT para_update( PIDLOOP_obj * , BOOL  );
void   Oip_control( PID_obj * );
void GetRates( RAMP_obj * );


/*
@fn DINT pidloop_init(	PIDLOOP_obj *pl,
						UDINT para_adr,
						UDINT SP_adr,
						UDINT adrPV,
						UDINT save_adr,
						USINT type
						)

@brief Init for the PID loop object.
@param PIDLOOP_obj *pl			Object address
@param UDINT para_adr			Address of the LC parameter table
@param UDINT SP_adr				Address of the variable for Set Point (Var Type: short, INT)
@param UDINT PV_adr				Address of the variable for Process Variable (Var Type: short, INT)
@param USINT type				Direct or Indirect
@return 0-OK; >0-Error
*/

/* V2.07 */
DINT pidloop_init( PIDLOOP_obj *pl, UDINT para_adr, UDINT SP_adr, UDINT PV_adr, float PVlow, float PVhigh, UDINT Y_adr, USINT type )
{
	/* Store ptrs for SP and PV variables*/
	pl->SP_adr			= SP_adr;
	pl->PV_adr			= PV_adr;
	pl->PVlow			= PVlow;	/* V2.07 */
	pl->PVhigh			= PVhigh;	/* V2.07 */
	pl->para_adr			= para_adr;
	pl->Y_adr			= Y_adr;
	pl->type				= type;

	/* Initialize: User Parameters */
	pl->Update = 1;

	/* Return: InitStatus */
	return pl->initStatus;
}

/**
@fn DINT pidloop_serv( PIDLOOP_obj *pl )
@brief Server for the PID loop object.
*/
/* V2.07 */
DINT pidloop_serv( PIDLOOP_obj *pl )
{
INT		SP, PV, SPx;
UINT 	DB;
float		m, b;

	if( pl->initStatus > 0 ) {
		/* Run LCPIDpara Function: */
		para_update( pl, 0 );
		return pl->initStatus;
	}

	if( pl->pidUser.status )
		LCPIDpara( (LCPIDpara_typ *) &pl->paraUser );

	/* Get SP and PV values */
	SPx	= * (INT *) pl->SP_adr;
	PV 	= * (INT *) pl->PV_adr;

	/* V2.07 */
	pl->PVrange	= pl->PVhigh - pl->PVlow;

	/* V2.03: Deadband should be greater than PV range */
	if (pl->deadband > pl->PVhigh)
		pl->deadband = 0;

	/* V2.07:  y = mx + b */
	m	= 32767.0 / pl->PVrange;
	b 	= 32767.0 - m * pl->PVhigh;

	/* V2.07: Rescale Deadband to 0-32767 range */
	DB	= (UINT)(m * pl->deadband + b + 0.5);

	/* V2.03: Check bounds */
	if ((DB == 0)|| (SPx < DB * 2) || (SPx > (32767 - (DB * 2)))) {
		SP = SPx;
	}
	else {
		if ((PV > (SPx - DB)) & (PV < (SPx + DB))){
			SP = PV;
		}
		else {
			SP = SPx;
		}
	}


	/* Loop: Indirect */
	if( pl->type == pidINDIRECT ) {
		pl->pidUser.W	= PV;
		pl->pidUser.X	= SP;
	}
	/* Loop: Direct */
	else {
		pl->pidUser.W	= SP;
		pl->pidUser.X	= PV;
	}

	/* RampClose: Command from PID_obj */
	if ( pl->RampClose ) {
		pl->pidUser.W	= 0;
		pl->pidUser.X	= 32767;
	}

	/* V2.04: RampOpen: Command from PID_obj */
	if ( pl->RampOpen ) {
		pl->pidUser.X	= 0;
		pl->pidUser.W	= 32767;
	}

	/* De-reference: Basetime */
	pl->pidUser.basetime = * (UDINT *) pl->basetime_adr;

	/* User Parameter: Update */
	if( pl->Update ) {
		para_update( pl, 0 );
		pl->Update = 0;
	}

	/* Get PID Y Output: Feedback to Loop */
	pl->pidUser.Y_fbk = * (INT *) pl->Y_adr;

	/* Execute PID algorithm */
	LCPID( (LCPID_typ *) &pl->pidUser );

	/* Get Last Error Status */
	if ( pl->pidUser.status > 0 )
		pl->lastStatus = pl->pidUser.status;

	/* Return: */
	return 0;
}

/**
@fn DINT pid_init(	UDINT pid_adr, UDINT security, USINT loopCount,	UDINT adrLoop )
@brief Init for the loop select object.
@param PID_obj * p			Object address
@param UDINT level0_adr		Security Level 0 Address
@param USINT loopCount		Number of loops
@param UDINT loop_adr		Loop array Address
@param UDINT remote_adr		Remote status address
@param UDINT para_adr		PID parameter address
@param UDINT basetime_adr	Counter address base
@return 0-OK; >0-Error
*/
DINT pid_init( PID_obj *p, UDINT level0_adr, USINT LoopCount, UDINT loop_adr, UDINT remote_adr, UDINT basetime_adr, UINT tick_ms)
{
PIDLOOP_obj *pl;
USINT i;

	/* Address Assignment */
	p->level0_adr		= level0_adr;
	p->LoopCount	  	= LoopCount;
	p->remote_adr		= remote_adr;
	p->loop_adr 		= loop_adr;

	/* Establish Base Loop Pointer */
	pl = (PIDLOOP_obj *) p->loop_adr;

	/* Assign Base Timer */
	for (i=0;i<p->LoopCount;i++,pl++) {
		pl->basetime_adr = basetime_adr;
		pl->tick_ms		 = tick_ms;
	}

	p->initStatus  = 0;		/* V2.02 */

	/* Return Value: Ok */
	return p->initStatus;
}


/* V2.07 */
DINT para_update( PIDLOOP_obj *pl, BOOL manual )
{
PIDpara_obj *para = (PIDpara_obj *) pl->para_adr;

	/* Loop controller parameters: */
	pl->paraUser.Y_max		= 32767;
	pl->paraUser.Y_min		= 0;
	pl->paraUser.Kp			= para->Kp;
	pl->paraUser.Tn			= para->Tn;
	pl->paraUser.Tv			= 0;
	pl->paraUser.Tf			= 0;
	pl->paraUser.Kw			= para->Kw;
	pl->paraUser.Kfbk		= para->Kfbk;
	pl->paraUser.fbk_mode	= LCPID_FBK_MODE_EXTERN;		/* ChOr */
	pl->paraUser.d_mode		= LCPID_D_MODE_X;
	pl->paraUser.calc_mode	= LCPID_CALC_MODE_FAST;
	pl->paraUser.enable		= 1;
	pl->paraUser.enter		= 1;

	/* V2.07 */
	if (para->deadband > pl->PVhigh){
		para->deadband = pl->deadband;
	}
	else {
		pl->deadband = para->deadband;
	}

	/* Manual Mode: Allow dY_max Full */
	if (manual)
		pl->paraUser.dY_max		= 32767;
	else
		pl->paraUser.dY_max		= para->dY_max_pct * pl->tick_ms / 1000.0 * 327.67;

	/* Call Param Function */
	LCPIDpara( (LCPIDpara_typ *) &pl->paraUser );

	/* Initialization: Good */
	if ( pl->paraUser.status == 0 )
		/* Enable Loop I/O */
		pl->pidUser.enable		= 1;
	/* Initialization: Bad */
	else {
		pl->initStatus			= pl->paraUser.status;
		return pl->initStatus;
	}

	/* Set Up PID fields */
	pl->pidUser.ident 	= pl->paraUser.ident;
	pl->pidUser.enable 	= 1;

	/* Return Value: Ok */
	pl->initStatus = 0;
	return pl->initStatus;
}

/**  V2.04:
@fn DINT pid_serv( UDINT pid_adr )
@brief Server for the loop selection object.
*/
DINT pid_serv( PID_obj *p )
{
USINT i, LoopIndex=0;
PIDLOOP_obj *pl;

	/* Bad Initialization */
	if( p->initStatus )
		return p->initStatus;

	/* Remote: Status */
	if (!p->remote_adr)
		p->Remote = 0;
	else
		p->Remote = * (BOOL *) p->remote_adr;

	/* Process OIP Commands & Leds */
	Oip_control( p );

	/* Auto Mode: Determine Loop Index */
	if ( !p->manual ) {

		/* Auto Loop Select */
		if ( p->LoopSelect == pidAUTOSEL )
			LoopIndex 		= LoopSelect( p );

		/* Manual Loop Select: Assign */
		else if ( p->LoopSelect < p->LoopCount )
			LoopIndex 		= p->LoopSelect;

		/* Manual Loop Select Out of Bounds: Set to Auto Loop Select */
		else {
			LoopIndex 		= LoopSelect( p );
			p->LoopSelect 	= pidAUTOSEL;
		}

	}

	/* Establish Base Loop Pointer */
	pl = (PIDLOOP_obj *) p->loop_adr;

	/********** LOOP CONTROL ******************/
	for (i=0;i<p->LoopCount;i++,pl++) {

		/* RampClose Setting */
		pl->RampClose = p->RampClose;

		/* V2.04: RampOpen Setting */
		pl->RampOpen  = p->RampOpen;

		/* Mode: Manual */
		if ( p->manual ) {

			/* MANUAL Mode Entry: Update Parameters and Set Output Mode */
			if( pl->pidUser.out_mode != LCPID_OUT_MODE_MAN ) {
				para_update( pl, 1 );
				pl->pidUser.out_mode	= LCPID_OUT_MODE_MAN;
			}

			/* */
			p->Output_man	  = p->Output_pct / 100.0 * 32767.0;

			/* Set Manual to PID Manual */
			pl->pidUser.Y_man = p->Output_man;
			p->Output		  = p->Output_man;
		}

		/* Mode: Auto */
		else {

			/* In Control Loop: Set AUTO mode */
			if ( i == LoopIndex ) {

				/* AUTO Mode Entry: Update Parameters and Set Output Mode */
				if( pl->pidUser.out_mode != LCPID_OUT_MODE_AUTO) {
					para_update( pl, 0 );
					pl->pidUser.out_mode	= LCPID_OUT_MODE_AUTO;
				}

				p->Output 		= pl->pidUser.Y;
				p->Output_man	= pl->pidUser.Y;
				p->LoopIndex 	= LoopIndex;								/* Active Loop Index 	*/
				p->Output_pct	= 100.0   / 32767.0 * (float) p->Output;	/* %: 					*/
			}

			/* Not In Control Loop: Set MANUAL mode */
			else {

				/* MANUAL Mode Entry: Update Parameters and Set Output Mode */
				if( pl->pidUser.out_mode != LCPID_OUT_MODE_MAN ) {
					para_update( pl, 1 );
					pl->pidUser.out_mode	= LCPID_OUT_MODE_MAN;
				}
			}

			/* Set Manual to PID Output */
			pl->pidUser.Y_man 			= p->Output;
		}
	}

	/* Set Outputs: */
	p->Output_ao	= p->Output * 0.8 + 6553.0;									/* AO: 				*/
	p->Output_64k	= (UINT) (64000.0 / 32767.0 * (float) p->Output + 0.5);		/* RCA:				*/
	p->Output_mA	= 16.0 / 26214.0 * (float) p->Output_ao;					/* V2.05: 4-20 mA 	*/


	/* Return */
	return 0;
}


/**
@fn static UINT LoopSelect( PID_obj *p )
@brief Function to select loop that will be active (controlling).
@param p Pointer to PID Object
@return none
*/
static UINT LoopSelect( PID_obj *p )
{
PIDLOOP_obj *pl;
INT Deviation, delta;
USINT i, ActiveLoop = 0;

	/* Establish Base Loop Pointer */
	pl = (PIDLOOP_obj *) p->loop_adr;

	/* Establish: First Deviation */
	Deviation = pl->pidUser.X - pl->pidUser.W;

	/* Locate 'In Control' Loop: Maximum Negative Deviation */
	for (i=0;i<p->LoopCount;i++,pl++) {
		if ( strcmp(pl->OIP.LoopName, "") != 0) {
			/* Get Deviation */
			delta = pl->pidUser.X - pl->pidUser.W;
	
			if ( delta > Deviation) {
				ActiveLoop 	= i;
				Deviation 	= delta;
			}
		}
	}

	/* Re-establish Base Loop Pointer */
	pl = (PIDLOOP_obj *) p->loop_adr;

	/* Get Active Loop */
	pl = pl + ActiveLoop;

	/* */
	strcpy( p->LoopName, pl->OIP.LoopName );

	/* Return: Active Loop */
	return (ActiveLoop);
}



/**
@fn DINT ramp_init( RAMP_obj *r , float rateDown, float rateUp, UDINT remote_adr)
@brief Init for the ramp set point object.

@param RAMP_obj *r			Object address
@param float 	rateDown	Set Point increase (+) rate
@param float 	rateUp		Set Point decrease (-) rate
@param UDINT 	remote_adr	Address of the Local Mode variable (Var Type: char, BOOL)
t, UINT)

@return 0-OK; >0-Error
		1 - Object address is 0
		2 - remote_adr is 0
 0
*/

/* V2.07 */
DINT ramp_init( RAMP_obj *r, float SPlow, float SPhigh, USINT Speed_ms, UDINT remote_adr , USINT type)
{

	/* Address */
	r->remote_adr		= remote_adr;

	/* Presets */
	r->SPlow			= SPlow;
	r->SPhigh			= SPhigh;
	r->Speed_ms			= Speed_ms;
	r->RampSP       		= 0;
	/* V2.07 r->LocalSP_range 	= 0; */
	r->RemoteSP_64k		= 0;
	/* V2.06 */
	r->Type				= type;

	/* V2.07 */
	if (r->SPlow > r->SPhigh)
		r->initStatus 		= 1;		/* SP low limit is more than SP high limit */
	else if (r->Speed_ms == 0)
		r->initStatus		= 2;		/* Ramping Speed is Zero! */


	return 0;
}

/* V2.07:
@fn DINT ramp_serv( RAMP_obj *r )
@brief Server for the ramp set point object.
*/
DINT ramp_serv( RAMP_obj *r )
{
float	m, b;

	if ( r->initStatus )
		return r->initStatus;

	if (!r->remote_adr)
		r->Remote = 0;
	else
		r->Remote = * (BOOL *) r->remote_adr;

	/* V2.07 */
	r->Range	= r->SPhigh - r->SPlow;

	/* If requested, use new user supplied values for +/- Rate */
	if( r->Update ) {
		GetRates( r );
		r->Update	= 0;
	}

	/* V2.07: Calculate RemoteSP Range Value */
	/* y = mx + b */
	m	= r->Range / 64000.0;
	b 	= r->SPhigh - m *  64000.0;
	r->RemoteSP_range	= m * r->RemoteSP_64k + b;

	/* V2.07: Check Bound For LocalSP */
	if (r->LocalSP_range < r->SPlow)
		r->LocalSP_range = r->SPlow;
	else if (r->LocalSP_range > r->SPhigh)
		r->LocalSP_range = r->SPhigh;

	/* V2.07:  y = mx + b */
	m	= 32767.0 / r->Range;
	b 	= 32767.0 - m * r->SPhigh;
	/* V2.07: Convert Local Setpoint:  Local Input is in EU Format */
	r->LocalSP	= (UINT)(m * r->LocalSP_range + b + 0.5);

	/* V2.07: Convert Remote Setpoint:  */
	r->RemoteSP	= (UINT)(m * r->RemoteSP_range + b + 0.5);

	/* V2.06: Remote Mode */
	if (r->Remote) {

		/* Bumpless Transfer: Previously Local */
		if (r->Type == spBUMPLESS) {
			if ( r->Remote != r->oldRemote ) {
				r->RemoteSP_64k 	= r->LocalSP_64k;
				r->RemoteSP 		= r->LocalSP;
			}
		}

		r->SelectedSP = r->RemoteSP;
	}
	/* V2.06: Local Mode */
	else {

		/* Bumpless Transfer: Previously Remote */
		if (r->Type == spBUMPLESS) {
 			if ( r->Remote != r->oldRemote ) {
				r->LocalSP_range 	= r->RemoteSP_range;
				r->LocalSP 		= r->RemoteSP;
			}
		}

		r->SelectedSP = r->LocalSP;
	}

	/* V2.06: Store */
	r->oldRemote = r->Remote;


	if(r->RampSP != r->SelectedSP ) {
  		if (r->RampSP < r->SelectedSP ) {
    		if(r->RampSP < (65535 - (UINT)r->RateUp)) {
    			/* V2.08 */
    			if(r->RampExcp)
    				r->RampSP = r->SelectedSP;
    			else
    				r->RampSP += (UINT)r->RateUp;

    			/* Limit ramp to Sp*/
    			if(r->RampSP > r->SelectedSP )
    				r->RampSP = r->SelectedSP;
   			}
    		else
    			r->RampSP = r->SelectedSP;
   		}

    	if (r->RampSP > r->SelectedSP ){
    		if((UINT)r->RateDown < r->RampSP) {
    			/* V2.08 */
    			if(r->RampExcp)
    				r->RampSP = r->SelectedSP;
    			else
    				r->RampSP -= (UINT)r->RateDown;

    			/* Limit ramp to Sp*/
    			if (r->RampSP < r->SelectedSP )
    				r->RampSP = r->SelectedSP;
    		}
    		else
    			r->RampSP = r->SelectedSP;
    	}
    }

	/* Calculate RCA Values */
	r->RampSP_64k 		= (UINT) ((float) r->RampSP 	* (64000.0 / 32767.0) );
	r->LocalSP_64k 		= (UINT) ((float) r->LocalSP 	* (64000.0 / 32767.0) );
	r->SelectedSP_64k 	= (UINT) ((float) r->SelectedSP 	* (64000.0 / 32767.0) );

	/* Calculate Percent Value */
	r->RampSP_pct 		= (float) r->RampSP_64k 	/ 64000.0 * 100.0;
	r->RemoteSP_pct 		= (float) r->RemoteSP_64k 	/ 64000.0 * 100.0;
	r->SelectedSP_pct 	= (float) r->SelectedSP_64k / 64000.0 * 100.0;
	r->LocalSP_pct 		= (float) r->LocalSP_64k 	/ 64000.0 * 100.0;

	/* V2.07: Calculate Range Values */
	/* y = mx + b */
	m	= r->Range / 100.0;
	b 	= r->SPhigh - m *  100.0;
	r->RampSP_range		= m * r->RampSP_pct + b;

	return 0;
}

void Oip_control( PID_obj *p ) /* Modified for Lima Location for by Obed with Mike Craver approval 9/25/2013 */
{
BOOL Level0;

	/* Establish Security Level 0 */
	Level0 = * (BOOL *) p->level0_adr;

	/* If user presses button AUTO */ /* Added auto_rca 10/31/2013 requested by Mike Craver*/
	if( p->auto_oip || p->auto_rca) /* Removed if( p->auto_oip && !Level0) to allow SCADA to change to auto regardless the security level 9/25/2013 */ 
		p->manual	= 0;
	p->auto_oip	= 0;
	p->auto_rca = 0;

	/* If user presses button MANUAL */ /* Added manual_rca 10/31/2013 requested by Mike Craver*/
	if( p->manual_oip || p->manual_rca && !p->manual) /* Removed !p->Remote if( p->manual_oip && !Level0 && !p->Remote) to allow SCADA to change PID control to Manual regardless being in Remote Mode and not in local 9/25/2013 */
		p->manual	= 1;
	p->manual_oip	= 0;
	p->manual_rca	= 0;
	/* If in Remote, mode is auto */ 
	/*if( p->Remote)	 */				/* Commented this section of code */
	/*	p->manual	= 0; */

	/* Control LED for the OIP */
	if( p->manual ) {
		p->manual_led	= 2;
		p->auto_led		= 0;
	}
	else {
		p->manual_led	= 0;
		p->auto_led		= 1;
	}

	/*if( p->Remote)		  */		/* Commented this section of code */
	/*	p->auto_led		= 1;  */
}



/* V2.07 */
/* Example:Rate = (  120 PSI/min    * ( FSBin  / FSEng) ) / (60s/m*  1000ms / TC speed_ms) */
void GetRates( RAMP_obj *r )
{

	/* Check Divide by Zero */
	if ( r->Speed_ms == 0 )
		return;

	/* V2.07: Check User Input */
	if ( r->UserRateUp > r->SPhigh)
		r->UserRateUp	= r->SPhigh;
	else if ( r->UserRateUp < (r->SPhigh / 100.0) )
		r->UserRateUp	= r->SPhigh / 100.0;

	/* Get RateUp */
	r->RateUp	= ( r->UserRateUp * (32767.0 / r->Range) ) / (60.0 * (1000.0 / r->Speed_ms));

	/* V2.07: Check User Input */
	if ( r->UserRateDown > r->SPhigh )
		r->UserRateDown	= r->SPhigh;
	else if ( r->UserRateDown < (r->SPhigh / 100.0) )
		r->UserRateDown	= r->SPhigh / 100.0;

	/* Get RateDown */
	r->RateDown	= ( r->UserRateDown * (32767.0 / r->Range) ) / (60.0 * (1000.0 / r->Speed_ms));

	/* V2.07: Set Minimum Rates */
	if ( r->RateUp < 1.0  )
		r->RateUp	= 1;
	if ( r->RateDown < 1.0 )
		r->RateDown	= 1;

	return;
}



