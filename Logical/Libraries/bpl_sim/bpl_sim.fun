FUNCTION MetSim_init : BOOL 
VAR_INPUT
		m	:METER_obj;	
	END_VAR
END_FUNCTION
FUNCTION HWSim_serv : BOOL 
VAR_INPUT
		s	:HWVALVE_sim;	
		tick	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION HWSim_init : BOOL 
VAR_INPUT
		s	:HWVALVE_sim;	
		v_adr	:UDINT;	
		number	:UINT;	
		speed	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION FQSim_serv : BOOL 
VAR_INPUT
		v	:FQVALVE_sim;	
		mb1_adr	:UDINT;	
		cmd	:BOOL;	
		tick	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION FQSim_init : BOOL 
VAR_INPUT
		v	:FQVALVE_sim;	
		number	:UINT;	
		speed	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION IQSim_init : BOOL 
VAR_INPUT
		v	:IQVALVE_sim;	
		number	:UINT;	
		speed	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION IQSim_serv : BOOL 
VAR_INPUT
		v	:IQVALVE_sim;	
		mb4_adr	:UDINT;	
		mb3_adr	:UDINT;	
		mb1_adr	:UDINT;	
		tick	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION IQ1Sim_serv : BOOL 
VAR_INPUT
		v	:IQVALVE_sim;	
		mb4_adr	:UDINT;	
		mb3_adr	:UDINT;	
		mb1_adr	:UDINT;	
		tick	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION MetSim_serv : BOOL 
VAR_INPUT
		m	:METER_obj;	
	END_VAR
END_FUNCTION
FUNCTION MXSim_serv : BOOL 
VAR_INPUT
		v	:MXVALVE_sim;	
		mb_adr	:UDINT;	
		offset	:UINT;	
		tick	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION MXSim_init : BOOL 
VAR_INPUT
		v	:MXVALVE_sim;	
		num	:UINT;	
		speed	:UINT;	
	END_VAR
END_FUNCTION
