/*****************************************************************************
**************************  Buckeye Simulator Library ************************
******************************** V3.00 11/21/07	******************************/

#include <bur/plc.h>
#include <bur/plctypes.h>
#include <standard.h>
#include <sys_lib.h>
#include <AsString.h>

#include "bpl_lib.h"
#include "bpl_sim.h"

#define _IQ_CLOSE_CMD 	1
#define _IQ_NULL_CMD 	10
#define _IQ_OPEN_CMD 	2
#define _IQ_STOP_CMD 	0

#define _MX_OPEN_CMD 	256
#define _MX_CLOSE_CMD 	768
#define _MX_STOP_CMD 	512


BOOL MetSim_init( METER_obj *m )
{

	m->Gravity 		= 0;
	m->ActiveNet 	= m->PreviousNet = m->ActiveSemiNet = 0;
	m->NetFlow 		= 0;
	m->eob_cmd 		= 0;
	m->Pressure 	= 0;
	return OFF;
}

/* Call Once per Second */
BOOL MetSim_serv( METER_obj *m )
{

	/* */
	m->Gravity	 		= m->Gravity + (REAL) m->NetFlow / 3600.0;			/* Use Gravity in Simulation: Only REAL in object */
	m->ActiveNet 		= (UDINT) m->Gravity;
	m->ActiveSemiNet 	= m->ActiveNet;

	if ( m->eob_cmd && (m->Pressure == 0) ) {
		m->Pressure		= 1;
		m->PreviousNet  = m->ActiveNet;
		m->Gravity		= 0;
		m->BatchSequence++;
	}
	else if ( !m->eob_cmd && (m->Pressure == 1) ) {
		m->Pressure		= 0;
	}

	/* Return */
	return OFF;
}

BOOL HWSim_init(HWVALVE_sim *s, UDINT v_adr, UINT number, UINT speed )
{
UINT i;
HWVALVE_obj *v = (HWVALVE_obj *) v_adr;

	/* Loop Through Valves */
	for (i=0;i<number;i++,v++,s++) {
		s->v_adr 			= (UDINT) v;
		s->count			= 0;
		s->operation 		= 0;
		s->speed 			= speed;

		/* Default */
		if ( v->LSO_adr ) {
			* (BOOL *) v->LSC_adr = ON;
			* (BOOL *) v->LSO_adr = OFF;
		}

	}

	return OFF;
}

BOOL HWSim_serv( HWVALVE_sim *s, UINT tick )
{
HWVALVE_obj *v = (HWVALVE_obj *) s->v_adr;

	/* v Movement Allowed */
	if ( tick != s->_tick ) {

		/* Valve Defined */
		if ( v->LSO_adr ) {

			/* Open Command */
			if ( v->opentimer.Q )
				s->operation = 1;

			/* Close Command */
			else if ( v->closetimer.Q )
				s->operation = 2;

			/* Switch on Operation */
			switch (s->operation) {

				/* Opening */
				case 1:
					s->count++;
				break;

				/* Closing */
				case 2:
					s->count--;
				break;

				default:
				break;
			}

			/* Closed */
			if ( s->count <= 0 ) {
				s->Position 			= 2;
				s->count 				= 0;
				s->operation 			= 0;
				* (BOOL *) v->LSO_adr 	= OFF;
				* (BOOL *) v->LSC_adr 	= ON;
			}

			/* Opened */
			else if ( s->count >= (s->speed * 10) ) {
				s->Position 			= 1;
				s->count 				= s->speed * 10;
				s->operation 			= 0;
				* (BOOL *) v->LSC_adr 	= OFF;
				* (BOOL *) v->LSO_adr 	= ON;
			}

			/* Travel */
			else {
				* (BOOL *) v->LSC_adr 	= ON;
				* (BOOL *) v->LSO_adr 	= ON;
			}
		}
	}

	/* Store Old */
	s->_tick = tick;

	return OFF;
}

BOOL FQSim_init( FQVALVE_sim *v, UINT number, UINT speed )
{
UINT i;

	/* Loop Through Valves */
	for (i=0;i<number;i++,v++) {
		v->count		= 0;
		v->operation 	= 0;
		v->speed 		= speed;
	}

	return OFF;
}

BOOL FQSim_serv( FQVALVE_sim *v, UDINT mb1_adr, BOOL cmd, UINT tick )
{
BOOL *mb1 = (BOOL *) mb1_adr;

	/* Establish Command */
	v->operation = cmd;

	/* v Movement Allowed */
	if ( tick != v->_tick ) {

		/* Switch on v Operation */
		switch (v->operation) {

			/* Close */
			case 0:

				/* Decrement */
				v->count--;
				if ( v->count < 0 ) {
					v->operation = 0;
					v->count++;
				}
				/* Closing */
				else {
					v->LSO 		= 1;
					v->LSC 		= 1;
					v->Position = 8;
				}
				break;

			/* Open */
			case 1:

				/* Increment */
				v->count++;
				if ( v->count > (v->speed * 10) ) {
					v->count--;
					v->operation = 0;
				}
				/* Opening */
				else {
					v->LSO 		= 1;
					v->LSC 		= 1;
					v->Position = 8;
				}

				break;
		}
	}

	/* Store Old */
	v->_tick = tick;

	/* Closed  */
	if ( v->count == 0 ) {
		v->Position = 2;
		v->LSO 		= 0;
		v->LSC 		= 1;
	}
	/* Opened */
	else if ( v->count >= (v->speed * 10) ) {
		v->Position = 1;
		v->LSO 		= 1;
		v->LSC 		= 0;
	}

	/* Assignments */
	*(mb1 + 1) = v->LowPress;
	*(mb1 + 2) = v->LowLevel;
	*(mb1 + 3) = v->LSO;
	*(mb1 + 4) = v->LSC;
	*(mb1 + 5) = v->LocalOpen;
	*(mb1 + 6) = v->Remote;
	*(mb1 + 7) = OFF;

	/* */
	return OFF;
}

BOOL IQSim_init( IQVALVE_sim *v, UINT number, UINT speed )
{
UINT i;

	/* Loop Through Valves */
	for (i=0;i<number;i++,v++) {
		v->count		= 0;
		v->operation 	= 0;
		v->speed 		= speed;
	}

	return OFF;
}

BOOL IQSim_serv( IQVALVE_sim *v, UDINT mb4_adr, UDINT mb3_adr, UDINT mb1_adr, UINT tick )
{
float Percent, Analog;
UINT *mb4 = (UINT *) mb4_adr;
UINT *mb3 = (UINT *) mb3_adr;
BOOL *mb1 = (BOOL *) mb1_adr;

	/* Establish Command */
	if ( *(mb4 + 2) < _IQ_NULL_CMD )
		v->operation = *(mb4 + 2);

	/* v Movement Allowed */
	if ( tick != v->_tick ) {

		/* Switch on v Operation */
		switch (v->operation) {

			case _IQ_OPEN_CMD:

				/* Increment */
				v->count++;
				if ( v->count > (v->speed * 10) ) {
					v->count--;
					v->operation = 0;
					}
				/* Opening */
				else
					v->Position = 8;

				break;

			case _IQ_CLOSE_CMD:

				/* Decrement */
				v->count--;
				if ( v->count < 0 ) {
					v->operation = 0;
					v->count++;
					}
				/* Closing */
				else
					v->Position = 16;
				break;

			case _IQ_STOP_CMD:

				/* Clear Operation */
				v->operation 	= 0;
				v->Position 	= 4;

				break;

			default:

				break;
		}
	}

	/* Store Old */
	v->_tick = tick;

	/* Closed  */
	if ( v->count == 0 )
		v->Position = 2;
	/* Opened */
	else if ( v->count >= (v->speed * 10) )
		v->Position = 1;

	/* Switch on v Operation */
	switch (v->Position) {

		/* Opened */
		case 1:
			*(mb1 + 16) = 0;	/* CTR */
			*(mb1 + 17) = 0;	/* LSC */
			*(mb1 + 18) = 1;	/* LSO */
			*(mb1 + 8) = 0;	/* CLOSING */
			*(mb1 + 9) = 0;	/* OPENING */
			*(mb3 + 0)  = 0;
			break;

		/* Closed */
		case 2:
			*(mb1 + 16) = 0;	/* CTR */
			*(mb1 + 17) = 1;	/* LSC */
			*(mb1 + 18) = 0;	/* LSO */
			*(mb1 + 8) = 0;	/* CLOSING */
			*(mb1 + 9) = 0;	/* OPENING */
			*(mb3 + 0)  = 0;
			break;

		/* Stopped */
		case 4:
			*(mb1 + 16) = 1;	/* CTR */
			*(mb1 + 17) = 0;	/* LSC */
			*(mb1 + 18) = 0;	/* LSO */
			*(mb1 + 8) = 0;	/* CLOSING */
			*(mb1 + 9) = 0;	/* OPENING */
			*(mb3 + 0)  = 0;
			break;

		/* Opening */
		case 8:
			*(mb1 + 16) = 1;	/* CTR */
			*(mb1 + 17) = 0;	/* LSC */
			*(mb1 + 18) = 0;	/* LSO */
			*(mb1 + 8) = 0;	/* CLOSING */
			*(mb1 + 9) = 1;	/* OPENING */
			*(mb3 + 0)  = 32767;
			break;

		/* Closing */
		case 16:
			*(mb1 + 16) = 1;	/* CTR */
			*(mb1 + 17) = 0;	/* LSC */
			*(mb1 + 18) = 0;	/* LSO */
			*(mb1 + 8) = 1;	/* CLOSING */
			*(mb1 + 9) = 0;	/* OPENING */
			*(mb3 + 0)  = 32767;
			break;

		default:
			break;
	}

	/* Clear Modbus Command */
	*(mb4 + 2) = _IQ_NULL_CMD;

	/* Calculate Percent */
	Percent 		= (10.0 * (float) v->count) / (float) v->speed;
	Analog			= Percent / 100.0 * 65535;
	v->Percent 	= (UINT) Analog;

	/* Assign MB4 Registers
	*(mb3 + 0)  = 32767;*/
	*(mb3 + 1) 	= v->Percent;

	/* */
	return OFF;


}

BOOL IQ1Sim_serv( IQVALVE_sim *v, UDINT mb4_adr, UDINT mb3_adr, UDINT mb1_adr, UINT tick )
{
float Percent, Analog;
UINT *mb4 = (UINT *) mb4_adr;
UINT *mb3 = (UINT *) mb3_adr;
BOOL *mb1 = (BOOL *) mb1_adr;

	/* Establish Command */
	if ( *(mb4 + 2) < _IQ_NULL_CMD )
		v->operation = *(mb4 + 2);

	/* v Movement Allowed */
	if ( tick != v->_tick ) {

		/* Switch on v Operation */
		switch (v->operation) {

			case _IQ_OPEN_CMD:

				/* Increment */
				v->count++;
				if ( v->count > (v->speed * 10) ) {
					v->count--;
					v->operation = 0;
					}
				/* Opening */
				else
					v->Position = 8;

				break;

			case _IQ_CLOSE_CMD:

				/* Decrement */
				v->count--;
				if ( v->count < 0 ) {
					v->operation = 0;
					v->count++;
					}
				/* Closing */
				else
					v->Position = 16;
				break;

			case _IQ_STOP_CMD:

				/* Clear Operation */
				v->operation 	= 0;
				v->Position 	= 4;

				break;

			default:

				break;
		}
	}

	/* Store Old */
	v->_tick = tick;

	/* Closed  */
	if ( v->count == 0 )
		v->Position = 2;
	/* Opened */
	else if ( v->count >= (v->speed * 10) )
		v->Position = 1;

	/* Switch on v Operation */
	switch (v->Position) {

		/* Opened */
		case 1:
			/**(mb1 + 16) = 0;	 CTR */
			*(mb1 + 1) = 0;	/* LSC */
			*(mb1 + 2) = 1;	/* LSO */
			*(mb1 + 4) = 0;	/* CLOSING */
			*(mb1 + 3) = 0;	/* OPENING */
			*(mb3 + 10)  = 0;
			break;

		/* Closed */
		case 2:
			/**(mb1 + 16) = 0;	 CTR */
			*(mb1 + 1) = 1;	/* LSC */
			*(mb1 + 2) = 0;	/* LSO */
			*(mb1 + 4) = 0;	/* CLOSING */
			*(mb1 + 3) = 0;	/* OPENING */
			*(mb3 + 10)  = 0;
			break;

		/* Stopped */
		case 4:
			/**(mb1 + 16) = 1;	 CTR */
			*(mb1 + 1) = 0;	/* LSC */
			*(mb1 + 2) = 0;	/* LSO */
			*(mb1 + 4) = 0;	/* CLOSING */
			*(mb1 + 3) = 0;	/* OPENING */
			*(mb3 + 10)  = 0;
			break;

		/* Opening */
		case 8:
			/**(mb1 + 16) = 1;	 CTR */
			*(mb1 + 1) = 0;	/* LSC */
			*(mb1 + 2) = 0;	/* LSO */
			*(mb1 + 4) = 0;	/* CLOSING */
			*(mb1 + 3) = 1;	/* OPENING */
			*(mb3 + 10)  = 32767;
			break;

		/* Closing */
		case 16:
			/**(mb1 + 16) = 1;	 CTR */
			*(mb1 + 1) = 0;	/* LSC */
			*(mb1 + 2) = 0;	/* LSO */
			*(mb1 + 4) = 1;	/* CLOSING */
			*(mb1 + 3) = 0;	/* OPENING */
			*(mb3 + 10)  = 32767;
			break;

		default:
			break;
	}

	/* Clear Modbus Command */
	*(mb4 + 2) = _IQ_NULL_CMD;

	/* Calculate Percent */
	Percent 		= (10.0 * (float) v->count) / (float) v->speed;
	Analog			= Percent / 100.0 * (float) 0x03E8;
	v->Percent 	= (UINT) Analog;

	/* Assign MB4 Registers
	*(mb3 + 0)  = 32767;*/
	*(mb3 + 1) 	= v->Percent;

	/* */
	return OFF;
}

BOOL MXSim_init( MXVALVE_sim *v, UINT num, UINT speed )
{
UINT i;

	/* Loop Through Valves */
	for (i=0;i<num;i++,v++) {
		v->count		= 0;
		v->operation 	= 0;
		v->speed 		= speed;
	}

	return OFF;
}

BOOL MXSim_serv( MXVALVE_sim *v, UDINT mb_adr, UINT offset, UINT tick )
{
float Percent, Analog;
UINT *mb = (UINT *) mb_adr;

	/* Calculate Percent */
	Percent 	= (10.0 * (float) v->count) / (float) v->speed;
	Analog		= Percent * 40.95;
	v->Percent 	= (UINT) Analog;

	/* Assign MB Registers */
	*(mb + 0)	= v->Percent;
	*(mb + 1) 	= v->Position;
	*(mb + 2) 	= v->Alarm;

	/* Establish Command */
	if ( *(mb + offset) > 0 )
		v->operation = *(mb + offset);

	/* Valve Movement Allowed */
	if ( tick != v->_tick ) {

		/* Switch on Valve Operation */
		switch (v->operation) {

			case _MX_OPEN_CMD:

				/* Increment */
				v->count++;
				if ( v->count > (v->speed * 10) ) {
					v->count--;
					v->operation = 0;
					}
				/* Opening */
				else
					v->Position = 8;

				break;

			case _MX_CLOSE_CMD:

				/* Decrement */
				v->count--;
				if ( v->count < 0 ) {
					v->operation = 0;
					v->count++;
					}
				/* Closing */
				else
					v->Position = 16;
				break;

			case _MX_STOP_CMD:

				/* Clear Operation */
				v->operation 	= 0;
				v->Position 	= 4;

				break;

			default:

				break;
		}

	}

	/* Store Old */
	v->_tick = tick;

	/* Closed  */
	if ( v->count == 0 )
		v->Position = 2;

	/* Opened */
	else if ( v->count >= (v->speed * 10) )
		v->Position = 1;

	/* Clear Modbus Command */
	*(mb + offset)  = 0;

	return OFF;
}



