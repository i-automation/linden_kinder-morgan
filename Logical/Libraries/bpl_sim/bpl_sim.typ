TYPE
	FQVALVE_sim : STRUCT
		Position	:UINT;	
		LowPress	:BOOL;	
		LowLevel	:BOOL;	
		LSO	:BOOL;	
		LSC	:BOOL;	
		LocalOpen	:BOOL;	
		Remote	:BOOL;	
		operation	:UINT;	
		speed	:UINT;	
		count	:INT;	
		_tick	:UINT;	
	END_STRUCT;
			HWVALVE_sim : STRUCT
		Position	:UINT;	
		operation	:UINT;	
		speed	:UINT;	
		count	:INT;	
		v_adr	:UDINT;	
		_tick	:UINT;	
	END_STRUCT;
			IQVALVE_sim : STRUCT
		Position	:UINT;	
		Percent	:UINT;	
		Alarm	:UINT;	
		operation	:UINT;	
		speed	:UINT;	
		count	:INT;	
		_tick	:UINT;	
	END_STRUCT;
			MXVALVE_sim : STRUCT
		Position	:UINT;	
		Percent	:UINT;	
		Alarm	:UINT;	
		operation	:UINT;	
		speed	:UINT;	
		count	:INT;	
		_tick	:UINT;	
	END_STRUCT;
END_TYPE
