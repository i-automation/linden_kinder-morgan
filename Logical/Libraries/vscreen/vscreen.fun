FUNCTION vsSetBmp : UINT 
VAR_INPUT
		vcHandle	:UDINT;	
		adrBmpFile	:UDINT;	
		x	:UINT;	
		y	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION vsRect : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		x1	:INT;	
		y1	:INT;	
		x2	:INT;	
		y2	:INT;	
		color	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION vsGetBmp : UINT 
VAR_INPUT
		vcHandle	:UDINT;	
		mode	:USINT;	
		xpoint1	:UINT;	
		ypoint1	:UINT;	
		xpoint2	:UINT;	
		ypoint2	:UINT;	
		startAdr	:REFERENCE TO USINT;	
		len	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION vsInfo : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		parId	:UINT;	
		pData	:REFERENCE TO UDINT;	
	END_VAR
END_FUNCTION
FUNCTION vsGetBmpVS : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		vcHandle	:UDINT;	
		mode	:USINT;	
		startAdr	:REFERENCE TO USINT;	
		len	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION vsCopys : UINT 
VAR_INPUT
		hScreenSrc	:UDINT;	
		hScreenDst	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION vsCopy : UINT 
VAR_INPUT
		hScreenSrc	:UDINT;	
		xSrc	:UINT;	
		ySrc	:UINT;	
		width	:UINT;	
		height	:UINT;	
		hScreenDst	:UDINT;	
		xDst	:UINT;	
		yDst	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION vsLineV : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		x	:INT;	
		y	:INT;	
		len	:UINT;	
		color	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION vsLineH : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		x	:INT;	
		y	:INT;	
		len	:UINT;	
		color	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION vsLine : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		x1	:INT;	
		y1	:INT;	
		x2	:INT;	
		y2	:INT;	
		color	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION vsRotate : UINT 
VAR_INPUT
		hScreenSrc	:UDINT;	
		hScreenDst	:UDINT;	
		angle	:REAL;	
	END_VAR
END_FUNCTION
FUNCTION vsCombine : UINT 
VAR_INPUT
		hScreenBack	:UDINT;	
		hScreenObj	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION vsPaint : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		vcHandle	:UDINT;	
		x	:UINT;	
		y	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION vsScan : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		vcHandle	:UDINT;	
		x	:UINT;	
		y	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION vsSetBackColor : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		color	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION vsPutPixel : UINT 
VAR_INPUT
		hScreen	:UDINT;	
		x	:UINT;	
		y	:UINT;	
		color	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION vsGetPixel : USINT 
VAR_INPUT
		hScreen	:UDINT;	
		x	:UINT;	
		y	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION vsClear : UINT 
VAR_INPUT
		hScreen	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION vsDelete : UINT 
VAR_INPUT
		hScreen	:UDINT;	
	END_VAR
END_FUNCTION
FUNCTION vsCreate : UINT 
VAR_INPUT
		width	:UINT;	
		height	:UINT;	
		phScreen	:REFERENCE TO UDINT;	
	END_VAR
END_FUNCTION
