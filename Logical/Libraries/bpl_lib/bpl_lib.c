 /*****************************************************************************
**************************  Buckeye Terminal Library **********************
*********************************** V5.01 - 11/23/08 *************************/
#include <bur/plc.h>
#include <bur/plctypes.h>
#include <standard.h>
#include <sys_lib.h>
#include <AsString.h>

#include "mx.h"
#include "bpl_lib.h" 

#include "sm.h"

/* Task Class Scan Time */
#define MX_SCAN_TIME 10

/* Declare predefined functions */
char * strcat( char *pDest, char *pSrc);

/* Function Prototypes defined elsewhere */
MXVALVE_obj * GetActiveValve( POLL_obj * );
MXVALVE_obj * GetCommandValve( POLL_obj * );
MXVALVE_obj * GetMovingValve( POLL_obj * , USINT );
void SendPollMessage( POLL_obj * , MXVALVE_obj *, USINT );
void SendCommandMessage( POLL_obj * , MXVALVE_obj *, USINT );
void FailedResponse( POLL_obj * , MXVALVE_obj *, USINT );
void GoodResponse( POLL_obj * , MXVALVE_obj *, USINT  );
void CommandExec( POLL_obj * , MXVALVE_obj *, USINT );
void CommandFail( POLL_obj * , MXVALVE_obj *, USINT );
void AddToMovingTable( MXVALVE_obj * );
void UpdateMovingTable( POLL_obj * );
BOOL SearchNextCommand( POLL_obj * );
void AnyValveCommFail( POLL_obj * );
plcbit GetBit(UINT , UINT );
BOOL RCASetClock( USINT * );
BOOL GetTimeString( UDINT );
void GravityTriggerCalc ( RECIPE_obj * , METER_obj * , TERMINAL_obj * );
void QuotaTriggerCalc ( RECIPE_obj * , METER_obj * );
void TicketTriggerCalc( RECIPE_obj * , METER_obj * );
void TriggerCommands( RECIPE_obj * , SECURITY_obj * );
void PortCheck( UDINT );
void WordToBit( UINT , BOOL * );
UINT GetShipperNumber( UINT );
void SequenceOverun( RECIPE_obj * );

IQVALVE_obj * GetIQActiveValve( POLL_obj * );
IQVALVE_obj * GetIQ2ActiveValve( POLL_obj * );
IQVALVE_obj * GetIQCommandValve( POLL_obj * );
IQVALVE_obj * GetIQMovingValve( POLL_obj * , USINT );
void SendIQPollMessage( POLL_obj * , IQVALVE_obj *, USINT, USINT );
void SendIQCommandMessage( POLL_obj * , IQVALVE_obj *, USINT );
void FailedIQResponse( POLL_obj * , IQVALVE_obj *, USINT );
void FailedIQ2Response( POLL_obj * , IQVALVE_obj *, USINT );
void GoodIQResponse( POLL_obj * , IQVALVE_obj *, USINT  );
void IQCommandExec( POLL_obj * , IQVALVE_obj *, USINT );
void IQCommandFail( POLL_obj * , IQVALVE_obj *, USINT );
void IQ2CommandFail( POLL_obj * , IQVALVE_obj *, USINT );
void AddIQToMovingTable( IQVALVE_obj * );
void UpdateIQMovingTable( POLL_obj * );
BOOL SearchIQNextCommand( POLL_obj * );
void AnyIQValveCommFail( POLL_obj * );
void AnyIQ2ValveCommFail( POLL_obj * );
USINT GetIQPosition( BOOL * , BOOL ); 
USINT GetIQ1Position( BOOL * , BOOL ); 

UDINT BitToLong( plcbit * );
void LongToBit( UDINT , BOOL * );
void CalculateStep64( UDINT * , UDINT * , USINT * );
UDINT CalculateStep32( UDINT Step_Request, USINT *pDiv );
UINT CalculateStep( UINT Step_Request, USINT *pDiv );
UDINT CalculateDividerValue32( UDINT Step_Request, USINT arraynum, USINT *pDiv);
BOOL FindNewStep64( UDINT * , UDINT * );
BOOL ShipperValveOpen( UDINT , STEP_obj * , USINT  );
BOOL ShipperValveClose( UDINT , STEP_obj * , USINT  );

char * strcpy ( char * destination, const char * source );
void * memcpy ( void * destination, const void * source, int num );
void * memset ( void * ptr, int value, int num );

/* V4.02 */
BOOL floop_init( FL_obj *fl, UDINT psi_pt )
{

	/* Assign */
	fl->psitimer.PT	= psi_pt;
	fl->State 		= 0;
	
	strcpy( fl->message, "Initialized" );

	return OFF;
}

/* V4.02 */
BOOL floop_serv( FL_obj *fl )
{


	switch (fl->State){

		case 0:

			strcpy(fl->message, "Wait Open Loop Command");

			if ( fl->open_cmd) { 
				fl->mov.open_remote	= ON;
				fl->State = 1;				
			}
		break;

		case 1:

			strcpy(fl->message, "Wait MOV Open");
	
			if(fl->mov.LSO) {
				fl->pump.start_remote	= ON;
				fl->State	 = 2;
			}
		break;	

		case 2:

			strcpy(fl->message, "Wait Pump ON");

			if(fl->pump.Status) {
				fl->State	 = 3;
			}

		break;

		case 3:

			strcpy(fl->message, "Fast Loop is Open");

			/* Pump Stopped In Auto */	
			if(!fl->pump.Status & fl->pump.Auto) {
				strcpy(fl->message, "Pump Stopped In Auto");
				fl->mov.close_remote	= ON;
				fl->State	 = 6;
			}

			/* Pump Stopped In Hand */	
			if(!fl->pump.Status & fl->pump.Hand) {
				fl->State	 = 4;
			}

			/* Loop Close Command Or MOV Closed */	
			if ((fl->close_cmd) || (!fl->mov.LSO)) {
				fl->pump.stop_remote	= ON;
				fl->State	 = 5;
			}
		break;

		case 4:
			
			strcpy(fl->message, "Pump Stopped In Hand");

			if(fl->pump.Status) {
				fl->State	 = 3;
			}
		break;

		case 5:

			strcpy(fl->message, "Wait Pump Stopped");

			if (!fl->pump.Status) {
				fl->mov.close_remote	= ON;
				fl->State	 = 6;
			}

		break;	

		case 6:

			strcpy(fl->message, "Wait MOV Closed");

			if ((fl->mov.LSC) & (!fl->mov.LSO)) {
				fl->State = 0;
			}

		break;	

		/* Default */
		default:
		break;
	}	


		/* Timer Enable: Low Pressure and Valve Partially Opened */
	fl->psitimer.IN	= !fl->PSL & fl->mov.LSO;
	
	/* ON Delay Timer Execution */
	TON_10ms( (TON_10mstyp *) &fl->psitimer );
		
	/* Assign Output */	
	fl->LowPressure	= fl->psitimer.Q; 

	/* Auto Shutdown Status */
	fl->pump.AutoSD = fl->LowPressure; 


	/* Handle Satate Machine Timer */
	if (fl->State != fl->oldState){
		fl->smtimer.IN	= ON;
		fl->smtimer.PT	= fl->t[fl->State] * 100;
	}
	
	/* State machine Timer */
	TON_10ms( (TON_10mstyp *) &fl->smtimer);

	if (fl->smtimer.Q){
		fl->smtimer.IN	= OFF;
	}

	/* Store State */
	fl->oldState	= fl->State;

	
	/* Reset Commands: Main */
	fl->open_cmd	= OFF;
	fl->close_cmd 	= OFF;
	
	/* Reset Commands: OIP and Remote */
	if ( fl->oip_hold ) 
		fl->open_remote	= fl->open_oip = fl->close_remote = fl->close_oip = OFF;
		
	/* Store For One Scan */	
	fl->oip_hold	= fl->open_remote | fl->open_oip | fl->close_remote | fl->close_oip;
			
	return OFF;
}

BOOL pump_init( PUMP_obj *p, UDINT status_adr, UDINT run_adr, UDINT pb_pt )
{

	/* Assign */
	p->status_adr 		= status_adr;
	p->run_adr 			= run_adr;
	p->starttimer.PT	= pb_pt;
	
	strcpy( p->message, "Init" );
	
	return OFF;
}

BOOL pump_serv( PUMP_obj *p , BOOL Auto, BOOL Hand, BOOL Overload )
{
	/* Assign */
	p->Auto		= Auto;
	p->Hand		= Hand;
	p->Overload = Overload;
	
	/* AutoSD: Set by FL_obj */

	/* De-Reference */
	p->Status 		= * (BOOL *) p->status_adr;

	/* Toggle Command: Stop */
	if ( p->Status & p->toggle_oip ) 
		p->stop_oip  = ON;
	/* Toggle Command: Start */		
	else if ( p->toggle_oip )
		p->start_oip = ON;

	/* Assign Off Delay Timer Input */
	p->starttimer.IN	= p->start_cmd;
	
	/* Off Delay Timer Execution */
	TOF_10ms( (TOF_10mstyp *) &p->starttimer );
		
	/* Run: Maintained Run Logic */
	p->Run			= (p->Run | p->start_cmd) & 
					  (p->Status | p->starttimer.Q) & 
					  ( (!p->AutoSD & !p->NoFlow & p->Auto ) | p->Hand) & 
					  !p->Overload &
					  !p->stop_cmd;
	
	/* Set Pump Commands */
	* (BOOL *) p->run_adr 	= p->Run;
	
	/* Reset Commands: Main */
	p->start_cmd 	= OFF;
	p->stop_cmd 	= OFF;

	/* Reset Commands: OIP and Remote */
	if ( p->oip_hold ) 
		p->start_remote = p->start_oip = p->stop_remote = p->stop_oip = OFF;
		
	/* Store For One Scan */	
	p->oip_hold		 = p->start_remote | p->start_oip | p->stop_remote | p->stop_oip;
	
	
	/* Set ON/OFF led based on Status */
	p->on_led	= p->Status;
	p->off_led	= !p->Status;
	
	/* V2.16: Message */
	if ( p->starttimer.Q ){
		p->StatusNum = 1;	
		strcpy( p->message, "Starting" );
		}
	else if ( p->NoFlow & p->Auto ){
		p->StatusNum = 2;	
		strcpy( p->message, "No Flow" );
		}
	else if ( p->Overload){
		p->StatusNum = 3;
		strcpy( p->message, "Overload" );
		}
	else if ( p->AutoSD & p->Auto ){
		p->StatusNum = 4;
		strcpy( p->message, "AutoSD" );
		}	
	else if (p->Status & p->Hand ){
		p->StatusNum = 5;
		strcpy( p->message, "Run: Hand" );
		}
	else if (p->Status & p->Auto ){
		p->StatusNum = 6;
		strcpy( p->message, "Run: Auto" );
		}
	else if ( !p->Status & p->Hand ){
		p->StatusNum = 7;
		strcpy( p->message, "Stopped: Hand" );
		}
	else if (!p->Status & p->Auto ){
		p->StatusNum = 8;
		strcpy( p->message, "Stopped: Auto" );
		}
	else if ( !p->Status & !p->Auto & !p->Hand ){
		p->StatusNum = 9;
		strcpy( p->message, "Stopped: OFF" );
		}								
	else {
		p->StatusNum = 10;
		strcpy( p->message, "Stopped" );
		}
	return OFF;
}

void AND4(AND4_typ* inst)
{

inst->output = inst->input1 & inst->input2 & inst->input3 & inst->input4;

}

/* Functional Description: RTUV
Upon 'cmd_in' issue 'stop' output and store 'active' state.
The Stop Output will Latch the VALVE_obj stop_cmd.
The 'active' output will also be set.
When the stop_cmd has reset the 'cmd' output will energize
and Latch the VALVE_obj open_cmd (or close_cmd) and
reset the 'active' output.
*/
void RTUV(RTUV_typ* inst)
{

	/* Initialization */
	if ( inst->first ) {
		inst->active 		= OFF;
		inst->stop  		= OFF;
		inst->cmd			= OFF;
	}

	/* Command Input: High */
	if ( inst->cmd_in ) {
		inst->active 		= ON;
		inst->stop 			= ON;
	}
	/* While Active: */
	else if ( inst->active ) {
		/* Detect F_TRIG: stop_in */
		if ( inst->stop_in ) {
			inst->active 	= OFF;
			inst->cmd 		= ON;
		}
		/* Waiting */
		else {
			inst->stop		= OFF;
			inst->cmd		= OFF;
		}
	}
	/* Not Active */
	else {
		inst->stop 			= OFF;
		inst->cmd			= OFF;
	}
	
}
void LD_UINT(LD_UINT_typ* inst)
{

if ( inst->EN )
	inst->OUT = inst->IN1;

}

BOOL booster_init( BOOSTER_obj *b, UDINT StartupOverride, UDINT StartDelay, UDINT LowFlow, UINT horn_mask, UINT crash_mask, UDINT duration  )
{

/* Assign */
b->StartupOverridePT 	= StartupOverride;
b->StartDelayPT			= StartDelay;
b->LowFlowPT			= LowFlow;
b->cmdtimer.PT			= 100; 				/* 1.00 Seconds */

/* Alarm Group Definition */
alarm_init( &b->AlarmGroup, horn_mask, crash_mask, duration );

/* Return Value */
return OFF;
}

BOOL booster_serv( BOOSTER_obj *b )
{

/* OIP Command Clearing */
b->cmdtimer.IN = b->start_oip | b->stop_oip | b->loreset_oip;
TON_10ms( (TON_10mstyp *) &b->cmdtimer );

/* Timer Expired: Reset OIP Commands */
if ( b->cmdtimer.Q ) {
	b->start_oip 		= OFF;
	b->stop_oip 		= OFF;
	b->loreset_oip 		= OFF;
	}
	
/* MMR Parsing and Logic */
/* Breakdown Multilin MM2 Read Array */
b->MMR_GeneralStatus 	= b->MMR_read[0];
b->MMR_LastTrip 		= b->MMR_read[1];
b->MMR_Amp 				= b->MMR_read[2];
b->MMR_Hours 			= b->MMR_read[3];

/* Trip from MMR Multilin 269 */
if ( (b->MMR_GeneralStatus & 0x0002) > 0 ) {
	b->MotorOverload = ON;
	/* Determine if Trip is due to Motor Overload */
	if ( (b->MMR_LastTrip >= 69) && (b->MMR_LastTrip <= 86) )
		b->Trip = ON;
	}
/* No Trip from MMR */
else {
	b->Trip 			= OFF;
	b->MotorOverload 	= OFF;
	}
	
/* Check for Bstr A Alarm Condition */
if ( (b->MMR_GeneralStatus & 0x0008) > 0 )
 	b->MMR_Alarm = ON;
else
 	b->MMR_Alarm = OFF;
 	
/* OIP Booster Messages */
if ( b->MotorOnInd )
	b->oip_status = 1;		/* "Booster On" */
else if ( !b->Lockout )
	b->oip_status = 2;		/* "Booster Lockout" */
else
	b->oip_status = 0;		/* "Booster Off" */
	
/* Handle Booster Alarm Group */
alarm_serv( &b->AlarmGroup );
	
/* Return Value */
return OFF;
}

BOOL term_init( TERMINAL_obj *terminal, UDINT security_adr, UDINT mlpressure_adr, UINT terminalvolume, UINT provervolume )
{
	/* Assign */
	terminal->security_adr		= security_adr;
	terminal->mlpressure_adr	= mlpressure_adr;
	terminal->TerminalVolume 	= terminalvolume;
	terminal->ProverVolume 		= provervolume;
	
	/* Default to Remote Mode */
	terminal->remote			= ON;
	
	/* Default Bypass */
	terminal->GasBypass			= OFF;
	terminal->FireBypass		= OFF;
	terminal->SmokeBypass		= OFF;
	
	/* Return Value */
	return OFF;
}

BOOL term_serv( TERMINAL_obj *terminal )
{
SECURITY_obj *security;

	/* Get Security Object */
	if ( !terminal->security_adr ) 		security->Level0 = 1;
	else								security = (SECURITY_obj *) terminal->security_adr;
	
	/* Get Mainline Pressure */
	if ( !terminal->mlpressure_adr )	terminal->MLPressure = 0;
	else								terminal->MLPressure = * (UINT *) terminal->mlpressure_adr;
	
	/************************************/
	/***   Total Volume Calculations  ***/
	/************************************/
	
	/* Include Terminal Volume Only */
	if ( terminal->ProverBypass )
		terminal->TotalVolume = terminal->TerminalVolume;
	/* Include Terminal Volume and Prover Volume */
	else
		terminal->TotalVolume = terminal->TerminalVolume + terminal->ProverVolume;	
	
	/************************************/
	/***   Terminal Crash Logic       ***/
	/************************************/
	
	
	/******************************************/
	/*** Terminal Local/Remote and OIP LEDs ***/
	/******************************************/
	
	/* OIP: Local Command and Security Level 1 or 2 */
	if ( terminal->local_oip ) {
		terminal->local_oip = OFF;
		if ( !security->Level0 ) 
			terminal->remote	= OFF;
		}
	/* OIP: Remote Command and Security Level 1 or 2 */
	if ( terminal->remote_oip ) {
		terminal->remote_oip = OFF;
		if ( !security->Level0 ) 
			terminal->remote	 = ON;
		}
		
	/* OIP: LED Controls:  0 = OFF  1 = ON   2 = SLOW BLINK    3 = FAST BLINK */
	/* Terminal in Remote */
	if ( terminal->remote ) {
		terminal->remote_led = 1;  /* ON */
		terminal->local_led	 = 0;  /* OFF */
		}
	/* Terminal in Local */	
	else {
		terminal->remote_led = 0;	/* OFF */
		terminal->local_led	 = 2;  	/* SLOW BLINK */						   
		}				   
	
	/* Return Value */
	return OFF;
}

/* V4.01 */
BOOL meter_init(METER_obj *meter, UDINT security_adr, UDINT net1ppb_adr, UDINT snet1ppb_adr, UDINT batch_sts_adr, UDINT eob_cmd_adr, USINT type)
{
	/* Assign */
	meter->security_adr = security_adr;
	
	/* Clear */
	meter->OldSequence 		= 0;
	meter->BatchSequence 	= 0;
	meter->OldNet	 		= 0;
	meter->OldSemi		 	= 0;
	meter->ActiveA			= OFF;

	/* V4.01 */
	meter->type				= type;	
	meter->net1ppb_adr		= net1ppb_adr;
	meter->snet1ppb_adr		= snet1ppb_adr;
	meter->batch_sts_adr	= batch_sts_adr;
	meter->eob_cmd_adr		= eob_cmd_adr;
	meter->BatchAStatus		= OFF;
	meter->net1ppb			= OFF;
	meter->snet1ppb		= OFF;

	if  ((meter->type == HW_IO) || (meter->type == HW_OMNI)) {
		meter->BatchAStatus	= * (BOOL *) meter->batch_sts_adr;
		meter->ActiveA		= meter->BatchAStatus;
		}

	
	/* Return Value */
	return OFF;
}

/* V4.01 */
BOOL meter_serv( METER_obj *meter )
{
SECURITY_obj *security;
	
	/* Get Security Object */
	if ( !meter->security_adr )		security->Level0 = OFF;
	else							security		 = (SECURITY_obj *) meter->security_adr;

	/* V4.01 */
	if ((meter->type == HW_IO) || (meter->type == HW_OMNI)) {

		/* De-Reference */
		meter->Net1PPB					= * (BOOL *) meter->net1ppb_adr;
		meter->SNet1PPB					= * (BOOL *) meter->snet1ppb_adr;
		meter->BatchAStatus				= * (BOOL *) meter->batch_sts_adr;
		* (BOOL *) meter->eob_cmd_adr 	= meter->eob_cmd;
		
		if ((meter->Net1PPB) && !(meter->net1ppb)){
			meter->ActiveNet++;
		}
		/* Store */
		meter->net1ppb = meter->Net1PPB;


		if ((meter->SNet1PPB) && !(meter->snet1ppb)){
			meter->ActiveSemiNet++;
		}
		/* Store */
		meter->snet1ppb = meter->SNet1PPB;	

	}	
	/************************************/
	/***   EOB Command: PanelWare	 ****/
	/***   Recipe Object can set EOB ****/
	/************************************/
	
	/* OIP End of Batch Command: EOB */	
	if ( meter->eob_oip ) {
		/* Reset Command */
		meter->eob_oip = OFF;
		/* Check Security */
		if ( !security->Level0 )
			/* Set EOB: */ 
			meter->eob_cmd = ON;	/* Alf Command */
		}	
		
	/************************************/
	/***   Panel Ware Reset Counter   ***/
	/************************************/
		
	/* OIP Counter Reset Command */	
	if ( meter->reset_oip ) {
		/* Reset Command */
		meter->reset_oip 	= OFF;	
		/* Reset Counter */
		meter->ResetCounter	= 0;
		}		
		
	/************************************/
	/***   New Barrels: Net			 ****/
	/************************************/
	
	/* V3.06: Detect Counter Reset at ALF */ 
	if ((meter->ResetOldNet) & (meter->ActiveNet < meter->OldNet)){
	
		/* V4.01: Calculate New Barrels */
		meter->newnetbbls	  = (DINT) (meter->ActiveNet);
		
		/* Save Old Count for Next Scan */
		meter->OldNet = meter->ActiveNet;
		/* Increment Resetable Counter */
		meter->ResetCounter	+= meter->newnetbbls;
		/* Reset */
		meter->ResetOldNet = OFF;
		}
	/* Detect Counter Reset at ALF */
	else if ( meter->ActiveNet < meter->OldNet )
		meter->error_reset++;
	
	/* Normal: Detect Counter Reset at ALF */
	else if (meter->ActiveNet >= meter->OldNet )  {

		/* V4.01: Calculate New Barrels */
		meter->newnetbbls	  = (DINT) (meter->ActiveNet - meter->OldNet);

		/* Save Old Count for Next Scan */
		meter->OldNet = meter->ActiveNet;
		/* Increment Resetable Counter */
		meter->ResetCounter	+= meter->newnetbbls;
		}
		
	/************************************/
	/***   New Barrels: Semi-Net	 ****/
	/************************************/
	
	/* 
	This is handled by the Recipe Object 
	
	It utilizes: ALF Input	-> meter->ActiveSemiNet
				 Internal	-> meter->OldSemi	
	*/
	  				
	/*****************************************/
	/***   Detect Alf EOB: Assign A/B Counts */
	/*****************************************/
	
	/* V4.07 */
	if (meter->type == HW_IO) {
		if (!meter->batchAStatus && meter->BatchAStatus){
			meter->BatchSequence++;
		}	
		meter->batchAStatus	= meter->BatchAStatus;
	}

	/* V5.12 */
	if (meter->type == HW_OMNI) {
		if (meter->batchAStatus ^ meter->BatchAStatus){
			meter->BatchSequence++;
		}	
		meter->batchAStatus	= meter->BatchAStatus;
	}

	/* A Active */
	if ( meter->ActiveA ) {
		meter->BatchNetA = meter->ActiveNet;
		meter->BatchNetB = meter->PreviousNet;
		}
		
	/* B Active */
	else {
		meter->BatchNetB = meter->ActiveNet;
		meter->BatchNetA = meter->PreviousNet;
		}		

	/* Change Detected */
	if ( meter->BatchSequence != meter->OldSequence ) {

		/* Switch Counters */
		meter->ActiveA	= !meter->ActiveA;

		/* Reset EOB Command */
		meter->eob_cmd 	= OFF;
		
		/* Save Old Sequence for Next Scan */
		meter->OldSequence = meter->BatchSequence;
	
		/* V3.06: Batch changed */
		meter->ResetOldNet = ON;
		meter->ResetOldSemi	= ON;

		/* V5.12 */
		if  ((meter->type == HW_IO) || (meter->type == HW_OMNI)) {

			meter->PreviousNet 		= meter->ActiveNet;		
			meter->ActiveNet 		= 0;
			meter->ActiveSemiNet	= 0;
		}
		
	}
	
	/* Return Value */
	return OFF;
}

/* V5.00 */
BOOL recipe_init( RECIPE_obj *recipe, UDINT security_adr, UDINT terminal_adr, UDINT vg_adr, UDINT meter_adr, UDINT gravity_adr,
									UDINT closedelay, UDINT RoomWarningBbls, 
									UDINT OverRunPT, UDINT repulsePT, UDINT divider_adr, USINT type)
{
	/* V3.06 */
	ANALOG_obj *gravity; 
	int i;

	/* Assign */
	recipe->security_adr 		= security_adr;
	recipe->terminal_adr 		= terminal_adr;
	recipe->vg_adr	 			= vg_adr;
	recipe->DelayCloseTimer.PT 	= closedelay * 100;
	recipe->meter_adr			= meter_adr;
	recipe->gravity_adr			= gravity_adr;		/* V3.06 */
	recipe->divider_adr			= divider_adr;
	recipe->Type		 		= type;

	recipe->RoomWarningBbls  	= RoomWarningBbls;
	recipe->TriggerTimer.PT	    = 500;	
	recipe->GravityTimer.PT		= 200; 				/* V4.04: 2 sec */
	recipe->OverRunTimer.PT	    	= OverRunPT * 100;
	recipe->RepulseCloseTimer.PT 	= repulsePT * 100; 	/* V3.04 */
	recipe->GravityTrigTimer.PT	= 10;				/* V4.04:  */
	
	/* V4.02: Get Security Object */
	if (!recipe->gravity_adr) {
		recipe->Gravity = 0;
	}
	else {
		gravity	= (ANALOG_obj *) recipe->gravity_adr;
	
		/* V4.01: Configure Next and Second Gravity Setpoint Analog Objects */
		analog_init( &recipe->next.GravSP,    gravity->raw_adr, gravity->high, gravity->low, AX, 10 );
		analog_init( &recipe->second.GravSP, gravity->raw_adr, gravity->high, gravity->low, AX, 10 );
		analog_init( &recipe->third.GravSP, gravity->raw_adr, gravity->high, gravity->low, AX, 10 );		/* V5.03 */
		analog_init( &recipe->fourth.GravSP, gravity->raw_adr, gravity->high, gravity->low, AX, 10 );		/* V5.03 */
	}	
	
	recipe->current.TicketEnable = ON;	
	recipe->next.TicketEnable = ON;
	recipe->second.TicketEnable = ON;	
	recipe->third.TicketEnable = ON;	
	recipe->fourth.TicketEnable = ON;	

	/* Return Value */
	return OFF;
}

/* V5.00 */
BOOL recipe_serv( RECIPE_obj *recipe )
{
UDINT newbbls = 0;
SECURITY_obj *security;
TERMINAL_obj *terminal;
METER_obj *meter;
ANALOG_obj *gravity; /* V3.06 */
VALVEGROUP_obj *vg;
BOOL oldCloseAll = OFF, AllClose = OFF, RoomWarning, RoomExpired, V64_CloseAll = OFF, AnyValveSentOpen = OFF; 

	/* Get Security Object */
	security	= (SECURITY_obj *) recipe->security_adr;
	
	/* Get Terminal Object */
	terminal	= (TERMINAL_obj *) recipe->terminal_adr;
	
	/* Get Meter Object */
	meter	= (METER_obj *) recipe->meter_adr;
	
	/* V4.02: Get Gravity Data */ 
	if (!recipe->gravity_adr) {
		recipe->Gravity = 0;
	}
	else {

		gravity	= (ANALOG_obj *) recipe->gravity_adr;
		recipe->Gravity = gravity->value_f;
		
		/* V3.06: Convert 64k value from RCA */
		analog_serv(&recipe->next.GravSP);
		analog_serv(&recipe->second.GravSP);
		analog_serv(&recipe->third.GravSP);	/* V5.03 */
		analog_serv(&recipe->fourth.GravSP);		/* V5.03 */
	}

	/* Get Valve Group Object */
	vg	= (VALVEGROUP_obj *) recipe->vg_adr;

	/***********************************************/
	/***   Trigger Commands and Enables          ***/
	/***********************************************/
	
	/* Process Recipe Commands and Enables: Pack Enables */
	TriggerCommands( recipe, security );
	
	/***********************************************/
	/***  PanelWare Interface: Shipper Valve Cmds **/
	/***********************************************/

	/* New shipper valve command from OIP (1.03) */
	/* V4.04: Removed recipe->DelayCloseTimer.IN */
	/* V32 Processing */
	if (recipe->Type == V32) {
		/* New shipper valve command from Remote */
		if ( recipe->shipper_rca32 != 0xFFFFFFFF ) { 
			/* V5.01: */
			recipe->current.Step32 = CalculateStep32( recipe->shipper_rca32, (USINT *) recipe->divider_adr );
			/* Detects new shipper valve command */
			recipe->NewShipCmd	 = ON;
			}	
		/* No New Command from OIP */	
		else
			recipe->NewShipCmd	 = OFF;	
	
		/* Reset Remote Command */
		recipe->shipper_rca32 	= 0xFFFFFFFF;
		}
		
	/* V64 Processing */
	else if (recipe->Type == V64) {
	
		/* New shipper valve command from Remote */
		if ( FindNewStep64( recipe->current.Step64, recipe->current.Step64_Raw) ) {
			CalculateStep64( recipe->current.Step64, recipe->current.Step64_Raw, (USINT *) recipe->divider_adr ); 
			/* Detects new shipper valve command */
			recipe->NewShipCmd	 = ON;
			}	
		/* No New Command from OIP */	
		else
			recipe->NewShipCmd	 = OFF;	
		}	
	
	/* V16 Processing */
	else {
		/* New shipper valve command from OIP */
		if ( (recipe->shipper_oip != 0xFFFF) && !security->Level0 ) { 
			/* V5.01: */
			recipe->current.Step = CalculateStep( recipe->shipper_oip, (USINT *) recipe->divider_adr );
			recipe->current.Step_Raw = recipe->shipper_oip;
			/* Detects new shipper valve command */
			recipe->NewShipCmd	 = ON;
			}
		/* New shipper valve command from Remote (1.03) */
		else if ( recipe->shipper_rca != 0xFFFF ) { 
			/* V5.01: */
			recipe->current.Step = CalculateStep( recipe->shipper_rca, (USINT *) recipe->divider_adr );
			recipe->current.Step_Raw = recipe->shipper_rca;
			/* Detects new shipper valve command */
			recipe->NewShipCmd	 = ON;
			}	
		/* No New Command from OIP */	
		else
			recipe->NewShipCmd	 = OFF;	
		
		/* Reset OIP Command */
		recipe->shipper_oip 	= 0xFFFF;
		/* Reset Remote Command */
		recipe->shipper_rca 	= 0xFFFF;
		}
	
	/*******************************************/
	/***  Miscellaneous:                     ***/
	/*******************************************/
	
	/* Event Trigger */
	recipe->Event = recipe->Trigger | recipe->NewShipCmd;
	
	/* V32 Processing */
	if (recipe->Type == V32) {
		/* Shipper Number */
		if ( recipe->current.Step32 > 0 ) 	recipe->current.ShipNum = 32;
		else 								recipe->current.ShipNum = 0;
		if ( recipe->next.Step32 > 0 ) 		recipe->next.ShipNum 	= 32;
		else 								recipe->next.ShipNum 	= 0;
		if ( recipe->second.Step32 > 0 ) 	recipe->second.ShipNum 	= 32;
		else 								recipe->second.ShipNum 	= 0;
		}
	/* V64 Processing */
	if (recipe->Type == V64) {
		/* Shipper Number */
		if ( (recipe->current.Step64[0] > 0) || (recipe->current.Step64[1] > 0) ) 	recipe->current.ShipNum = 64;
		else 								recipe->current.ShipNum = 0;
		if ( (recipe->next.Step64[0] > 0) || (recipe->next.Step64[1] > 0) ) 		recipe->next.ShipNum = 64;
		else 								recipe->next.ShipNum 	= 0;
		if ( (recipe->second.Step64[0] > 0) || (recipe->second.Step64[1] > 0) ) 	recipe->second.ShipNum = 64;
		else 								recipe->second.ShipNum 	= 0;
		}		
	/* V16 Processing */
	else {
		/* Get Current Shipper Number from Step Value */
		recipe->current.ShipNum = GetShipperNumber( recipe->current.Step );
		/* Get Next Shipper Number from Step Value */
		recipe->next.ShipNum 	= GetShipperNumber( recipe->next.Step );
		/* Get Second Shipper Number from Step Value */
		recipe->second.ShipNum 	= GetShipperNumber( recipe->second.Step );
		/* Get Third Shipper Number from Step Value - V5.02 */
		recipe->third.ShipNum 	= GetShipperNumber( recipe->third.Step );
		/* Get Fourth Shipper Number from Step Value - V5.02 */
		recipe->fourth.ShipNum 	= GetShipperNumber( recipe->fourth.Step );
		}
	/*******************************************/
	/***  Room Count: Calculations & Security **/
	/*******************************************/
	
	/* V3.06: Update Current Room Count */
	 if ((meter->ResetOldSemi) & (meter->ActiveSemiNet < meter->OldSemi)) {
		/* V4.01: Calculate New Barrels */
		newbbls = meter->ActiveSemiNet;
		/* Save Old Count for Next Scan */
		meter->OldSemi = meter->ActiveSemiNet;
		/* Update Current Room Count */
		recipe->current.RoomCount = recipe->current.RoomCount - newbbls;
		
		/* Reset */
		meter->ResetOldSemi = OFF;
		}
		
	/* Ignore Possible Error: Maintain Old OldSemi */
	else if ( meter->ActiveSemiNet < meter->OldSemi ) 
		meter->error_room++;

	/* Normal: Update Current Room Count */
	else if ( meter->ActiveSemiNet > meter->OldSemi ) {
		/* Calculate New Barrels */
		newbbls = meter->ActiveSemiNet - meter->OldSemi;
		/* Save Old Count for Next Scan */
		meter->OldSemi = meter->ActiveSemiNet;
		/* Update Current Room Count */
		recipe->current.RoomCount = recipe->current.RoomCount - newbbls;
		}
	
	/* V4.04: Room Warning */
	if ( (DINT) recipe->current.RoomCount <= (DINT) recipe->RoomWarningBbls ) 
		RoomWarning = ON;
	else 
		RoomWarning = OFF;	

	if ( RoomWarning && !recipe->CloseAll &&  recipe->LSO )
		recipe->RoomWarning = ON;
	else 
		recipe->RoomWarning = OFF;		

	/* V4.04: Room Expired */
	if ( (DINT) recipe->current.RoomCount <= 0 ) {
		recipe->current.RoomCount 	= 0;
		RoomExpired 	= ON;
		}
	else
		RoomExpired 	= OFF;
		
	if ( RoomExpired && !recipe->CloseAll && recipe->LSO ) {
		recipe->RoomExpired 			= ON;
		}
	else
		recipe->RoomExpired 			= OFF;

	/* Security: OIP Entry of Room Count: room_enb Reset by OIP */
	if ( recipe->room_oip && !security->Level0 )
		recipe->room_enb = ON;
		recipe->room_oip = OFF;		
	
	/* V5.01: V16 Processing */
	if (recipe->Type == V16) {	
		/* Append with Divider Valves: Save to Recipe Step64 */	
		recipe->next.Step     = CalculateStep( recipe->next.Step_Raw,    (USINT *) recipe->divider_adr ); 	
		recipe->second.Step  = CalculateStep( recipe->second.Step_Raw,  (USINT *) recipe->divider_adr ); 		
		recipe->third.Step  = CalculateStep( recipe->third.Step_Raw,  (USINT *) recipe->divider_adr ); 		
		recipe->fourth.Step  = CalculateStep( recipe->fourth.Step_Raw,  (USINT *) recipe->divider_adr ); 		
	}
	/* V5.01: V32 Processing */
	else if (recipe->Type == V32) {	
		/* Append with Divider Valves: Save to Recipe Step64 */	
		recipe->next.Step32     = CalculateStep32( recipe->next.Step32_Raw,    (USINT *) recipe->divider_adr ); 	
		recipe->second.Step32  = CalculateStep32( recipe->second.Step32_Raw,  (USINT *) recipe->divider_adr ); 		
	}
	/* V5.00: V64 Processing */
	else if (recipe->Type == V64) {	
		/* Append with Divider Valves: Save to Recipe Step64 */	
		CalculateStep64( recipe->next.Step64,    recipe->next.Step64_Raw,    (USINT *) recipe->divider_adr ); 	
		CalculateStep64( recipe->second.Step64,  recipe->second.Step64_Raw,  (USINT *) recipe->divider_adr ); 		
	}
	
	/***********************************************/
	/***  Recipe Trigger Activities: Shift Steps ***/
	/***********************************************/
			
	/* Shift Steps */
	if ( recipe->Trigger ) {
			
		/* Next to Current */
		memcpy( &recipe->current, &recipe->next, sizeof(recipe->current) );
	
		/* Second to Next  */
		memcpy( &recipe->next, &recipe->second,  sizeof(recipe->current) );
		
		/* Third to Second - V5.03 */
		memcpy( &recipe->second, &recipe->third,  sizeof(recipe->current) );
	
		/* Fourth to Third - V5.03 */
		memcpy( &recipe->third, &recipe->fourth,  sizeof(recipe->current) );
	
		/* V3.06: Reset Second */
		/* V5.01: Step_Raw, Step32_Raw */
		/* V5.03: Reset fourth */
		recipe->fourth.Step = 0;
		recipe->fourth.Step_Raw = 0;	
		recipe->fourth.Step32_Raw = 0;	
		recipe->fourth.ShipNum = 0;
		recipe->fourth.QuotaEnable = OFF;
		recipe->fourth.GravityEnable = OFF;
		recipe->fourth.ColorEnable = OFF;
		recipe->fourth.TicketEnable = ON;	
		recipe->fourth.RoomCount = 0;
		recipe->fourth.Quota = 0;
		recipe->fourth.GravSP.value_64k = 0;
		}
	
	/***********************************************/
	/***  Shipper Valve Commands: Open and Close ***/
	/***********************************************/
	
		
	/* Event && No Terminal Crash: Send Current Step to Shipper Open Control */
	if ( recipe->Event && !recipe->crash ) 
		/* Shipper Valve: Open */
		AnyValveSentOpen = ShipperValveOpen( recipe->vg_adr, &recipe->current, recipe->Type );
	
	/* Close Delay Timer: Execution */
	TON_10ms( (TON_10mstyp *) &recipe->DelayCloseTimer );
		
	/* Terminal Valve Full Closed and in Close: */
	if ( !recipe->LSO && recipe->close )	
		recipe->current.Step = 0;

	/* Close All: V64 Processing */
	if ( recipe->Type == V64 ) {
		if ( (recipe->current.Step64[0] == 0) && (recipe->current.Step64[1] == 0) ) 
			V64_CloseAll = ON;
		}
			
		/* Close All: */	
	if ( (((recipe->current.Step == 0) && (recipe->Type != V32))) 	|| 
		 (((recipe->current.Step32 == 0)&& (recipe->Type == V32)))	|| 
		 V64_CloseAll )
			recipe->CloseAll = ON;
		else
			recipe->CloseAll = OFF;	
		
	/* V3.04 */	
	if ( recipe->CloseAll && !recipe->RepulseCloseTimer.Q )	
		recipe->RepulseCloseTimer.IN = ON;
	else
		recipe->RepulseCloseTimer.IN = OFF;
	
	/* Close Repulse Delay Timer: Execution */
	TON_10ms( (TON_10mstyp *) &recipe->RepulseCloseTimer );
	
	if (( recipe->RepulseCloseTimer.Q) || (recipe->CloseAll && !oldCloseAll))
		AllClose = ON;
	else
		AllClose = OFF;		
		
	/* Store Old: */
	oldCloseAll = recipe->CloseAll ; 		
		
	/*  V4.04 */
	/* Close Control: 1: Normal 2: Terminal Closure (1.03): Valve Full Open at Event (1.08) */ 	
	if (  recipe->DelayCloseTimer.Q || (AllClose && !recipe->LSO) || ((recipe->DelayCloseTimer.IN && recipe->FullOpen )  &&  (!recipe->MainlineRecipe)) ) {
		
		/* Close Shipper Valves:  */
		ShipperValveClose( recipe->vg_adr, &recipe->current, recipe->Type );
		
		/* Call Ticket Trigger Sub-routine */
		TicketTriggerCalc( recipe, meter );
		
		/* Terminate Delay Close Timer */
		recipe->DelayCloseTimer.IN = OFF;
		}
	
	/***********************************************/
	/***  Quota Trigger Operations:              ***/
	/***********************************************/
	
	/* Call Quota Trigger Sub-routine */
	QuotaTriggerCalc( recipe, meter );
	
	/***********************************************/
	/***  Gravity Trigger Operations:            ***/
	/***********************************************/
	
	/* V4.01: Call Gravity Trigger Sub-routine */
	GravityTriggerCalc( recipe, meter, terminal);
	
	/***********************************************/
	/***  Sequence Overrun Operations:           ***/
	/***********************************************/
	SequenceOverun( recipe );
	
	/* Return Value */
	return OFF;
}

/* V3.07:  Must Be Called Prior to Individual Valve Initializations */
BOOL vgroup_init( VALVEGROUP_obj *vg, UDINT valve_adr, USINT number, BOOL type )
{
VALVE_obj *v = (VALVE_obj *) valve_adr;
UINT i;	

	/* Assign */
	vg->number 		= number;
	vg->valve_adr 	= valve_adr;
	vg->type		= type;
	
	/* Clear Memory Locations */
	for (i=0;i<vg->number;i++,v++) 
		memset( v, 0, sizeof(VALVE_obj) );

	/* Return Value */
	return OFF;
}

/* V3.07: */
BOOL vgroup_serv( VALVEGROUP_obj *vg)
{
VALVE_obj *valve = (VALVE_obj *) vg->valve_adr;
USINT i;
UINT faultAckImg_value, commAckImg_value, localAckImg_value, failedAckImg_value;
BOOL AllClosed = 1,OneOpened=0;

	/* V3.07: Cycle through defined valves */
	for (i=0;i<vg->number;i++,valve++) {
		
		/* Enabled */
		if ( valve->enable ) {
					
			if (valve->LSO)
				AllClosed = 0;
			if (valve->LSO && !valve->LSC)
		 		OneOpened = 1;
		 		
	 		/* In PP41 Panel AlarmBit field must be array of BOOL
	 		StatusGroup, FaultGroup, LocalGroup, CommfailGroup are defined as
	 		AlarmBit in PP41 alarm group system */
			vg->StatusGroup[i*2]		= valve->LSO;
			vg->StatusGroup[i*2+1]		= valve->LSC;
			vg->FaultGroup[i]			= valve->fault;
			vg->LocalGroup[i]			= valve->local;
			vg->CommfailGroup[i]		= valve->commfail;
			vg->FailedGroup[i]			= valve->FailedToOpenAlarm | valve->FailedToCloseAlarm;		 	
		}
		
		/* Not Enabled */	
		else {
		
			vg->StatusGroup[i*2]		= 0;
			vg->StatusGroup[i*2+1]		= 0;
			vg->FaultGroup[i]			= 0;
			vg->LocalGroup[i]			= 0;
			vg->CommfailGroup[i]		= 0;
			vg->FailedGroup[i]			= 0;	
		}
	}
			
	/* Convert To UINT */
	vg->status[0]	= BitToWord( (UDINT)&vg->StatusGroup[0] 	);
	vg->status[1] 	= BitToWord( (UDINT)&vg->StatusGroup[16] 	);	
	vg->fault 		= BitToWord( (UDINT)vg->FaultGroup 		);
	vg->local 		= BitToWord( (UDINT)vg->LocalGroup 		);
	vg->comm  		= BitToWord( (UDINT)vg->CommfailGroup 		);	
	vg->failed  	= BitToWord( (UDINT)vg->FailedGroup 		);
					
	/* Determine if alarm exists 1.07 */
	if (( vg->local | vg->comm | vg->fault) > 0 )
		vg->horn = ON;
	else
		vg->horn = OFF;
	
	/* Process AckImg for Panel ware and PP41 Panel differently beacuse PP41 only work with Array of BOOL[16] as AckImg & 
	   Panel ware can work with UINT as AckImg */
	/* Commfail AckImg Process : Unack Alarms  */	
	commAckImg_value = BitToWord( (UDINT)vg->commAckImg_PP41 );
	/* Process AckImg for Panel Ware */
	if ( vg->commAckImg > 0 )
		vg->commUnAckAlm = ON;
	/* Process AckImg for PP41 */
	else if ( commAckImg_value	> 0 ) 
		vg->commUnAckAlm = ON;
	/* No Unack Alarms */	
	else
		vg->commUnAckAlm = OFF;	
			
	/* Fault AckImg Process : Unack Alarms  */	
	faultAckImg_value = BitToWord( (UDINT)vg->faultAckImg_PP41 );
	/* Process AckImg for Panel Ware */
	if ( vg->faultAckImg > 0 )
		vg->faultUnAckAlm = ON;
	/* Process AckImg for PP41 */
	else if ( faultAckImg_value	> 0 ) 
		vg->faultUnAckAlm = ON;
	/* No Unack Alarms */	
	else
		vg->faultUnAckAlm = OFF;	
				
	/* Local AckImg Process : Unack Alarms  */	
	localAckImg_value = BitToWord( (UDINT)vg->faultAckImg_PP41 );
	/* Process AckImg for Panel Ware */
	if ( vg->localAckImg > 0 )
		vg->localUnAckAlm = ON;
	/* Process AckImg for PP41 */
	else if ( localAckImg_value	> 0 ) 
		vg->localUnAckAlm = ON;
	/* No Unack Alarms */	
	else
		vg->localUnAckAlm = OFF;	
	/* End */
	
	/* Failed AckImg Process : Unack Alarms  */						
	failedAckImg_value = BitToWord( (UDINT)vg->failedAckImg_PP41 );
	/* Process AckImg for Panel Ware */
	if ( vg->failedAckImg > 0 )
		vg->failedUnAckAlm = ON;
	/* Process AckImg for PP41 */
	else if ( failedAckImg_value	> 0 ) 
		vg->failedUnAckAlm = ON;
	/* No Unack Alarms */	
	else
		vg->failedUnAckAlm = OFF;
					
	/* Determine new alarm */
	if ( vg->fault > vg->oldfault )
		vg->newfault = ON;
	if (vg->comm > vg->oldcomm )
		vg->newcomm = ON;
	if (vg->failed > vg->oldfailed )			
		vg->newfailed = ON;
				
	/* Old=New */
	vg->oldfault 	= vg->fault;
	vg->oldcomm 	= vg->comm;
	vg->oldfailed 	= vg->failed;			
	
	/* All Closed Logic: Are All 16 Valves Closed? 1=YES 0=NO */
	vg->AllClosed = AllClosed;
	
	/* One Opened Logic: Is at least 1 Valve Full Open? 1=YES 0=NO */
	vg->OneOpened = OneOpened;
	
	/* Return Value */				
	return OFF;
}

BOOL clock_init( CLOCK_obj *clock, UDINT rcatime_adr, UDINT duration )
{
	/* Assign Address to RCA Memory Area */
	clock->rcatime_adr 	= rcatime_adr;
	
	/* PanelWare Trigger Signal Duration */
	clock->timer.PT		= duration * 100;
	
	/* Return Value */
	return OFF;
}

BOOL clock_serv( CLOCK_obj *clock )
{
	/* Show Time from RCA */
	memcpy( (USINT *) clock->rcatime, (USINT *) clock->rcatime_adr, sizeof(clock->rcatime) );
	
	/* Set Clock if Commanded by RCA */
	clock->timer.IN = RCASetClock( (USINT *) clock->rcatime_adr );
	
	/* Execute TOF Timer */
	TOF_10ms( (TOF_10mstyp *) &clock->timer );
	
	/* Panel Ware Trigger */
	clock->Trigger = clock->timer.Q ;
	
	/* Get Time String For Panel Ware */
	GetTimeString( (UDINT) clock->String );
	
	/* Return Time Set */
	return clock->Trigger;
}

BOOL record_init( RECORDER_obj *record, USINT num_ch, UDINT c1, UDINT c2, UDINT c3, UDINT c4, UDINT c5, 
												  UDINT c6, UDINT c7, UDINT c8, UDINT c9, UDINT c10,
												  UDINT duration )
{

	/* Comm Failure Timeout */
	record->timer.PT = duration / 100;
	
	/* Establish Addresses To Analog Signals */
	record->num_ch = num_ch;
	
	/* Establish Addresses To Analog Signals */
	record->ch_adr[0] = c1;
	record->ch_adr[1] = c2;
	record->ch_adr[2] = c3;
	record->ch_adr[3] = c4;
	record->ch_adr[4] = c5;
	record->ch_adr[5] = c6;
	record->ch_adr[6] = c7;
	record->ch_adr[7] = c8;
	record->ch_adr[8] = c9;
	record->ch_adr[9] = c10;
	
	/* Return Value */
	return OFF;
}	

BOOL record_serv( RECORDER_obj *record )
{
USINT i;

	/* Cycle Through All Channels */
	for (i=0;i<record->num_ch;i++) 
		record->channels[i] = * (UINT *) record->ch_adr[i];
		
	/* Recorder System Error: read[0] */
	record->SystemError = GetBit(record->read[0], 0x0001);
	/* Recorder Disk Almost Full: read[1] */
	record->DiskAlmostFull = GetBit(record->read[1], 0x0001);
		
	/* Timer Input */	
	record->timer.IN = record->GoodMsg;	
		
	/* Execute Timer */   		
	TON_10ms( (TON_10mstyp *) &record->timer );
	
	/* Establish Comm Failure Bit */
	record->CommFail = record->timer.Q;   	
		
	/* Return Value */
	return OFF;
}

BOOL alarm_init( ALARM_obj *alarm, UINT horn_mask, UINT crash_mask, UDINT duration )
{

	/* Clear Alarm Structure */
	memset( alarm, 0, sizeof(*alarm) );
	
	/* Assign Unlatch Time */
	alarm->timer.PT 	= duration * 100;
	alarm->horn_mask 	= horn_mask;
	alarm->crash_mask	= crash_mask;
	
	/* Return Value */
	return OFF;
}

/* V4.04 */
UINT alarm_serv( ALARM_obj *alarm )
{
USINT i;
UINT AckImg_value_PP41, AckImg_value_C100;
BOOL common = OFF, horn = OFF, crash = OFF, horn_bit[16], crash_bit[16];

	/* Extract Horn and Crash Bit Masks */
	WordToBit( alarm->horn_mask, horn_bit );
	WordToBit( alarm->crash_mask, crash_bit );
	
	/* Reset Timer: Allow 30 Seconds */
	if ( alarm->ingroup != alarm->group_old )
		alarm->timer.IN = OFF;
	/* Keep Timing */
	else
		alarm->timer.IN = ON;
				
	/* Remember Old */
	alarm->group_old = alarm->ingroup;
	/* Timer Execute */
	TON_10ms( (TON_10mstyp *) &alarm->timer );
	
	/* Cycle through alarms */
	for (i=0;i<16;i++) {
	
		/* Time to Unlatch Alarms */ /* OUT = IN for all 16 */
		if ( alarm->timer.Q )
			/* OUT = IN */ 
			alarm->OUT[i] = alarm->IN[i];
		/* Set if Alarm is Active */
		else if ( alarm->IN[i] ) 
			/* Set Alarm ON */
			alarm->OUT[i] = ON;
			
		/* Alarm Point is Active */
		if ( alarm->OUT[i] ) {
			
			/* New Alarm */
			if ( !alarm->oldOUT[i] )
				alarm->new = ON;
			
			/* Set Common Alarm */
			common = ON;
			
			/* Horn Mask is ON: Set Horn Output */	
			if ( horn_bit[i] )	
				horn = ON;
				
			/* Crash Mask is ON: Set Crash Output */	
			if ( crash_bit[i] )	
				crash = ON;	
			}
			
		/* Old = New */	
		alarm->oldOUT[i] = alarm->OUT[i];		
		}
	 
	/* V4.04: Pack into Word */	
	alarm->ingroup = BitToWord( (UDINT)alarm->IN); 
	alarm->group = BitToWord( (UDINT)alarm->OUT); 

	/* Process AckImg for Panel ware and PP41 Panel differently beacuse PP41 only work with Array of BOOL[16] as AckImg & 
	   Panel ware can work with UINT as AckImg */
	AckImg_value_PP41 = BitToWord( (UDINT)alarm->AckImg_PP41 );
	AckImg_value_C100 = BitToWord( (UDINT)alarm->AckImg_C100 );
	
	/* Panel Ware Acknowledge Image: Unack Alarms  */
	
	/* Process AckImg for Panel Ware */
	if ( alarm->AckImg > 0 )
		alarm->UnAckAlm = ON;
	else
		alarm->UnAckAlm	= OFF;	
	
	/* Process AckImg for PP41 */
	if ( AckImg_value_PP41	> 0 ) 
		alarm->UnAckAlm_PP41 = ON;
	else
		alarm->UnAckAlm_PP41 = OFF;
		
	/* Process AckImg for C100 */
	if ( AckImg_value_C100	> 0 ) 
		alarm->UnAckAlm_C100 = ON;
	else
		alarm->UnAckAlm_C100 = OFF;		
	/* END */

	/* Common, Horn and Crash Boolean Alarms */
	alarm->common 	= common;
	alarm->horn	= horn;
	alarm->crash	= crash;
	
	/* Return Value */
	return alarm->group;
}

/* V3.09 */
BOOL analog_init( ANALOG_obj *anal, UDINT raw_adr, float high, float low, USINT type, UINT mult )
{
	/* Assign Object */
	anal->raw_adr	= raw_adr;
	anal->high		= high;
	anal->low		= low;
	anal->type		= type;
	anal->mult		= mult;
	
	/* V3.09: Alarm SP's */
	if (anal->hihi_sp	== 0.0)
		anal->hihi_sp	= high;
	
	if (anal->high_sp	== 0.0)
		anal->high_sp	= high;
	
	if (anal->low_sp	== 0.0)
		anal->low_sp	= low;
	
	/* V3.06 */
	anal->range = anal->high - anal->low;
	
	/* Return Value */
	return OFF;
}

/* V3.09 */
/* ChOr: 10.20.08 */
BOOL analog_serv( ANALOG_obj *anal )
{
INT *rawh;
float  raw, m,b;

	/* Check Null Pointer */ 
	if ( !anal->raw_adr) {
		return 0;	
	}

	/* Assign */
	rawh = (INT *) anal->raw_adr;
	raw = (float) *rawh;
	
	switch ( anal->type ) {
	
		/* Analog Input (4-20 mA) */
		case 0:
	
			if ( raw >= 6553 ) {
				/* y = mx + b */
				m =  (anal->high - anal->low)/ 26214.0;
				b = anal->high - m * 32767.0;
				/* Float value engineering unit conversion */
				anal->value_f = m * raw + b;
				
				/* y = mx + b */
				m =  64000.0 / 26214.0;
				b = 64000.0 - m * 32767.0;
				/* Calculate RCA Value */
				anal->value_64k =  m * raw + b;
				
				/* Integer */
				anal->value_i 		= (INT)((anal->value_f * (float) anal->mult) + .5);
				/* */
				anal->fail = OFF;
				}
			/* Low Signal */
			else {
				anal->value_f 	= anal->low;
				anal->value_64k = 0.0;
				anal->value_i   = (INT)((anal->value_f * (float) anal->mult) + .5);
				/* 2mA Failure Logic: */
				if (raw < 3276.7) 
					anal->fail 		= ON;
				else if (raw > 5734)	// Reset fail at 3.5mA *V5.12
					anal->fail		= OFF;
			}
			
			/* V2.18: HiHi Setpoint Logic: */
			if (anal->value_f >= anal->hihi_sp) 
				anal->hihi_alarm = ON;
			else
				anal->hihi_alarm = OFF;
			
			/* Hi Setpoint Logic: */
			if (anal->value_f >= anal->high_sp) 
				anal->high_alarm = ON;
			else
				anal->high_alarm = OFF;
				
			/* Low Setpoint Logic: */
			if (anal->value_f <= anal->low_sp) 
				anal->low_alarm = ON;
			else
				anal->low_alarm = OFF;				
			
			/* PID Value */	
			anal->value_pid	  	= anal->value_64k / 64000.0 * 32767.0;
			
			/* V3.09: Transmitter Current: 20mA -> 32760 */
			anal->Current = 0.000611 * raw;
			
			break;
		
		/* V4.00: Analog Output: x = RCA, y = AO */	
		case 1:
		
			/* y = mx + b */
			m 		= 26214.0 / 64000.0;
			b 		= 32767.0 - m * 64000.0;
			/* Calculate Raw Value */
			raw 	=  m * anal->value_64k + b;
			*rawh 	= (INT) raw + .5;
			
			/* y = mx + b */
			m 		= (anal->high - anal->low)/64000.0;
			
			/* Float value engineering unit conversion - */
			anal->value_f = m * anal->value_64k + anal->low;
			
			/* Integer */
			anal->value_i = (INT) ((anal->value_f * (float) anal->mult) + .5);
			
			/* V3.06 */
			anal->value_pid	= anal->value_64k / 64000.0 * 32767.0;
			break;
		                                
		/* RCA: X = RCA, Y = value_f */
		case 2:
			
			/* y = mx + b */
			m 		= (anal->high - anal->low) / 64000.0;
			b 		= anal->high - m * 64000.0;
			
			/* Float value engineering unit conversion */
			anal->value_f = m * anal->value_64k + b;
			
			/* Integer */
			anal->value_i = (INT) ((anal->value_f * (float) anal->mult) + .5);
		
			/* V3.06 */
			anal->value_pid	= anal->value_64k / 64000.0 * 32767.0;
			
			/* V3.06: break was misssing */
			break;

		/* V2.17: Analog Input (0-5 V) */
		case 3:
	
			if ( raw >=  3277) {
				/* y = mx + b */
				m =  (anal->high - anal->low)/ 13104.0;
				b = anal->high - m * 16380.0;
				/* Float value engineering unit conversion */
				anal->value_f = m * raw + b;
				
				/* y = mx + b */
				m =  64000.0 / 13104.0;
				b = 64000.0 - m * 16380.0;
				/* Calculate RCA Value */
				anal->value_64k =  m * raw + b;
				
				/* Integer */
				anal->value_i 		= (INT)((anal->value_f * (float) anal->mult) + .5);
				/* */
				anal->fail = OFF;
				}
			/* Low Signal */
			else {
				anal->value_f 	= anal->low;
				anal->value_64k = 0.0;
				anal->value_i   = (INT)((anal->value_f * (float) anal->mult) + .5);
				/* 500 mV Failure Logic: */
				if (raw < 1638) 
					anal->fail 		= ON;
			}
			
			/* V2.18: HiHi Setpoint Logic: */
			if (anal->value_f >= anal->hihi_sp) 
				anal->hihi_alarm = ON;
			else
				anal->hihi_alarm = OFF;
				
			/* Hi Setpoint Logic: */
			if (anal->value_f >= anal->high_sp ) 
				anal->high_alarm = ON;
			else
				anal->high_alarm = OFF;
		
			/* Low Setpoint Logic: */
			if (anal->value_f <= anal->low_sp ) 
				anal->low_alarm = ON;
			else
				anal->low_alarm = OFF;
						
			/* PID Value */	
			anal->value_pid	  	= anal->value_64k / 64000.0 * 32767.0;
			
			/* V3.09: Transmitter Current: 20mA -> 32760 */
			anal->Current = 0.000611 * raw;

			break;

		/* Default */
		default:
			break;
	}		        
	
	/* Return Value */
	return OFF;
}

/* Comm Failure Routine */
void PortCheck( UDINT port_adr )
{
PORTtyp *port;

/* De-reference */
port = (PORTtyp *) port_adr; 

/* No Activity */
if ( port->exec == port->oldexec )
	port->timer.IN = ON;
/* Activity */	
else
	port->timer.IN = OFF;
	
/* Timer Execution */	
TON_10ms( (TON_10mstyp *) &port->timer );
/* Old equals New */
port->oldexec = port->exec;
/* Assign Comm Failure */
port->fail	= port->timer.Q;

}

BOOL secur_init( SECURITY_obj *s, UDINT intruder_adr, UDINT bypass_adr, UINT pw1, UINT pw2, UDINT duration )
{

	/* Clear Object */
	memset( s, 0, sizeof(SECURITY_obj) );

	/* Assign Object */
	s->intruder_adr 	= intruder_adr;
	s->bypass_adr 		= bypass_adr;
	s->EnterTimer.PT 	= duration;
	s->ExitTimer.PT 	= duration;
	s->Passcode_L1 		= pw1;
	s->Passcode_L2 		= pw2;
	
	/* Default Security Level 0 */
	s->Level0 = ON;
	s->Level1 = OFF;
	s->Level2 = OFF;
	
	/* Return */
	return ON;
}


BOOL secur_serv( SECURITY_obj *s )
{

	/* De-Reference */
	if (!s->intruder_adr)	s->Intruder			= OFF;							
	else 					s->Intruder 		= * (BOOL *) s->intruder_adr;
	if (!s->bypass_adr)		s->IntruderBypass	= OFF;
	else 					s->IntruderBypass 	= * (BOOL *) s->bypass_adr;
	
	/* Set Output for Level 1 Passcode */
	if (s->Passcode == s->Passcode_L1) {
		s->Level0 = OFF;
		s->Level1 = ON;
		s->Level2 = OFF;
		}
	/* Set Output for Level 2 Passcode */
	else if (s->Passcode == s->Passcode_L2) {
		s->Level0 = OFF;
		s->Level1 = OFF;
		s->Level2 = ON;
		}
	/* Set Output for Level 0 Passcode */
	else {
		s->Level0 = ON;
		s->Level1 = OFF;
		s->Level2 = OFF;
		}
	
	/* Exit Timer */	
	s->ExitTimer.IN = s->Level0 & !s->IntruderBypass;
	TON_10ms( (TON_10mstyp *) &s->ExitTimer );
	s->ExitTime = s->ExitTimer.Q;
	
	/* First Entry Calculation */
	s->FirstEntry = (!s->Intruder | s->FirstEntry) & s->ExitTime;
	
	/* Enter Timer */
	s->EnterTimer.IN = s->FirstEntry;
	TON_10ms( (TON_10mstyp *) &s->EnterTimer );
	s->EnterTime = s->EnterTimer.Q;
	
	/* Alarm Calculation */
	s->IntruderAlarm = s->Intruder | !s->EnterTime | !s->Level0 | !s->ExitTime | s->IntruderBypass;
	
	/************************************/
	/***   Security Access OIP LED	  ***/
	/************************************/
	
	/* OIP: LED Controls:  0 = OFF  1 = ON   2 = SLOW BLINK    3 = FAST BLINK */
	/* Level 0: LED OFF */
	if ( s->Level0 ) 
		s->access_led	= 0; 
	/* Level 1: LED Slow Blink */	
	else if ( s->Level1 )
		s->access_led	= 2;
	/* Level 2: LED Fast Blink */	
	else if ( s->Level2 )
		s->access_led	= 3;	
	
	/* Return Value */
	return s->IntruderAlarm;	
}

BOOL mxv_init( MXVALVE_obj *m, UDINT Valve_adr, BOOL Enable, UDINT ScanTime, USINT ID, USINT Type )
{
VALVE_obj *valve;

	/* Assign Pointer */
	valve	= (VALVE_obj *) Valve_adr;
	
	/* Assign Generic Valve Object to MX Valve */
	m->Valve_adr  	= Valve_adr;
	
	/* Enable Valve if In Loop */
	m->Enable			= Enable;
	valve->enable		= Enable;
	
	/* Cycle Time */
	m->ScanTimer.PT 	= ScanTime * 100;
	m->ScanTimer.IN 	= OFF;
	
	/* Miscellaneous Defaults */
	m->ID 				= ID;
	/* Reconnection Reset */
	m->ReconnectCount[CH1]	= 0;
	m->ReconnectCount[CH2]	= 0;
	m->Retry[CH1]			= OFF;
	m->Retry[CH2]			= OFF;
	/* General Diagnostic Reset */
	m->NoReply[CH1] 		= 0;
	m->NoReply[CH2] 		= 0;
	m->Reply[CH1] 			= 0;
	m->Reply[CH2] 			= 0;
	m->Polls[CH1] 			= 0;
	m->Polls[CH2] 			= 0;
	/* Comm Status Variables */
	m->RemoveCount[CH1] 	= 0;
	m->RemoveCount[CH2] 	= 0;
	m->Comm_Stat[CH1] 		= OK;
	m->Comm_Stat[CH2] 		= OK;
	m->Status 				= OK;
	/* Valve communication type. UEC registers are older and slightly different */
	m->Type					= Type;
	
	/* Return */
	return ON;
}

/* V3.08 */
BOOL hwv_init( HWVALVE_obj *HWValve, UDINT Valve_adr, UDINT LSO_adr, UDINT LSC_adr, UDINT LCL_adr,
			   UDINT open_cmd_adr, UDINT close_cmd_adr, UDINT stop_cmd_adr, UDINT cmdduration, UDINT stopduration, 
			   UDINT travelduration, USINT cmd_typ )			   
{
VALVE_obj *GVO;

	/* Assign Generic Valve Object to HW Valve */
	HWValve->Valve_adr = Valve_adr;
	
	/* Get Generic Valve Object */
	GVO			= (VALVE_obj *) HWValve->Valve_adr;
	GVO->enable	= ON;								
	
	/* Assign Input/Output Addresses */
	HWValve->LSO_adr 		= LSO_adr;
	HWValve->LSC_adr 		= LSC_adr;
	HWValve->open_cmd_adr 	= open_cmd_adr;
	HWValve->close_cmd_adr 	= close_cmd_adr;
	HWValve->stop_cmd_adr 	= stop_cmd_adr;
	HWValve->LCL_adr 		= LCL_adr;					
	HWValve->opentimer.PT   = cmdduration;
	HWValve->closetimer.PT  = cmdduration;
	HWValve->stoptimer.PT  	= stopduration;
	HWValve->traveltimer.PT = travelduration;			
	HWValve->_open			= HWValve->_close	= OFF;	
	HWValve->cmd_typ		= cmd_typ;	/* V3.08 */
		
	/* Set Default Conditions to HW Valve */
	GVO->local = OFF;
	GVO->fault = OFF;
	
	/* Return */
	return ON;
}

/* Remote Valve Object */
BOOL rmv_init( RMVALVE_obj *r, UDINT v_adr, TIME timerPT )
{

	/* Assign */
	r->v_adr	= v_adr;
	r->_timer.PT = (UDINT) timerPT / 10;

	/* Return */
	return ON;
}

/* Remote Valve Object */
BOOL rmv_serv( RMVALVE_obj *r )
{
VALVE_obj *v = (VALVE_obj *) r->v_adr;
	
	/* Reset Commands: Main */
	r->_timer.IN		= v->open_oip | v->close_oip | v->open_cmd | v->close_cmd;
	TON_10ms( (TON_10mstyp  *) &r->_timer );

	/* Reset Commands */
	if ( r->_timer.Q ) {
		v->open_oip = v->close_oip	= v->open_cmd = v->close_cmd	= OFF;
	}	
		
	/* Return */
	return ON;
}

/* V3.08 */
/* 1 Valve Only */
UINT hwv_serv( HWVALVE_obj *HWValve, UINT number )
{
VALVE_obj *GVO;
UINT i;
	
	/* */
	for (i=0;i<number;i++,HWValve++) {
	
		/* Get Valve Object */
		GVO	= (VALVE_obj *) HWValve->Valve_adr;
	
		/* Assign Limit Switches and Local */ 
		if ( !HWValve->LSO_adr ) 	GVO->LSO 	= HWValve->LSO;
		else 						GVO->LSO 	= * (BOOL *) HWValve->LSO_adr;
		if ( !HWValve->LSC_adr ) 	GVO->LSC 	= HWValve->LSC;
		else 						GVO->LSC 	= * (BOOL *) HWValve->LSC_adr;
		if ( !HWValve->LCL_adr ) 	GVO->local 	= HWValve->LCL;
		else 						GVO->local 	= * (BOOL *) HWValve->LCL_adr;
		
		/* Calculate Position and Percent: Only ZSO */
		GVO->position = ( (USINT) GVO->LSO * 2) + (USINT) GVO->LSC;
		
		/* Valve Opened: 100 */
		if (GVO->position == OPENED ) {
			strcpy( GVO->message, "Opened" );
			GVO->percent = 100;
		}
		/* Valve Travel: 50 */	
		else if (GVO->position == TRAVEL ) {
			strcpy( GVO->message, "Travel" );
			GVO->percent = 50;
		}
		/* Valve Closed: 0 */	
		else if (GVO->LSC) {
			strcpy( GVO->message, "Closed" );
			GVO->percent = 0;
		}
		/* No Indication: 0 */	
		else {
			strcpy( GVO->message, "NoInd" );
			GVO->percent = 0;
		}
			
		/* Mode: Open Attempt */
		if ((GVO->open_cmd) && (!GVO->opn)) {
			GVO->FailedToCloseAlarm	= OFF;
			GVO->FailedToOpenAlarm	= OFF;
			HWValve->_open 			= ON;
		}	
		/* Mode: Close Attempt */ 
		else if ((GVO->close_cmd) && (!GVO->cls)) {
			GVO->FailedToCloseAlarm	= OFF;
			GVO->FailedToOpenAlarm	= OFF;
			HWValve->_close 		= ON;		
		}			
		/* Stop Command: Reset Modes */ 
		else if ((GVO->stop_cmd) && (!GVO->stp)) {
			HWValve->_close 		= OFF;
			HWValve->_open 			= OFF;	
			GVO->FailedToCloseAlarm	= OFF;
			GVO->FailedToOpenAlarm	= OFF;	
		}	
		
		/* Store Commands */
		GVO->opn = GVO->open_cmd;
		GVO->cls = GVO->close_cmd;
		GVO->stp = GVO->stop_cmd;
	
		/* Assign Off Delay Timer Input */
		HWValve->opentimer.IN 	= GVO->open_cmd;
		HWValve->closetimer.IN 	= GVO->close_cmd;
		HWValve->stoptimer.IN 	= GVO->stop_cmd;
		HWValve->traveltimer.IN = HWValve->_open | HWValve->_close;		
		
		/* Off Delay Timer Execution */
		TOF_10ms( (TOF_10mstyp *) &HWValve->opentimer 	);
		TOF_10ms( (TOF_10mstyp *) &HWValve->closetimer 	);
		TON_10ms( (TON_10mstyp *) &HWValve->stoptimer 	);
		TON_10ms( (TON_10mstyp *) &HWValve->traveltimer );		
		
	
		/* V3.08: Set Valve Commands */	
		switch ( HWValve->cmd_typ ) {
	
		/* Pulse Output */
		case 0:
			
			/* Set Valve Commands */
				/* Turn Off Output Early */
				if ( !HWValve->open_cmd_adr ) 	HWValve->OPNCMD 	= HWValve->opentimer.Q & GVO->LSC;
				else 			* (BOOL *) HWValve->open_cmd_adr 	= HWValve->opentimer.Q & GVO->LSC;

				/* Turn Off Output Early */			
				if ( !HWValve->open_cmd_adr ) 	HWValve->CLSCMD 	= HWValve->closetimer.Q & GVO->LSO;
				else 			* (BOOL *) HWValve->close_cmd_adr 	= HWValve->closetimer.Q & GVO->LSO;

			/* Reset Commands: Main */
			GVO->open_cmd 	= OFF;
			GVO->close_cmd 	= OFF;
		
			break;
			
		/* Continuos Output */	
		case 1:
			
			/* Set Valve Commands */

				/* GVO->LSC should be tied in ladder to generate GVO->open_cmd */;
				if ( !HWValve->open_cmd_adr ) 	HWValve->OPNCMD 	= GVO->open_cmd;
				else 			* (BOOL *) HWValve->open_cmd_adr 	= GVO->open_cmd;

				/* GVO->LSO should be tied in ladder to generate GVO->close_cmd */					
				if ( !HWValve->open_cmd_adr ) 	HWValve->CLSCMD 	= GVO->close_cmd;
				else 			* (BOOL *) HWValve->close_cmd_adr 	= GVO->close_cmd;

			break;
		
		/* Default */
		default:
			break;
		}
					
		/* Stop Command */
		if ( !HWValve->stop_cmd_adr ) 	HWValve->STPCMD	= GVO->stop_cmd;
		else 		* (BOOL *) HWValve->stop_cmd_adr 	= GVO->stop_cmd;
		
		if ( HWValve->stoptimer.Q )
			GVO->stop_cmd 	= OFF;
		
		/* Reset Commands: OIP and Remote */
		if ( GVO->oip_hold ) 
			GVO->open_remote = GVO->open_oip = GVO->close_remote = GVO->close_oip	= GVO->stop_remote = GVO->stop_oip = OFF;
			
		/* Store For One Scan */	
		GVO->oip_hold		 = GVO->open_remote | GVO->open_oip | GVO->close_remote | GVO->close_oip | GVO->stop_remote | GVO->stop_oip;
		
		/* Alarms: */							
		/* Open Travel: Complete */		
		if ( HWValve->_open & GVO->LSO )
			HWValve->_open 			= OFF;
		/* Close Travel: Complete */	
		if ( HWValve->_close & GVO->LSC )
			HWValve->_close 		= OFF;
			
		/* Open Travel: Failure */
		if ( HWValve->traveltimer.Q & HWValve->_open ) {
			GVO->FailedToOpenAlarm	= ON;
			HWValve->_open 			= OFF;
		}
		/* Close Travel: Failure  */	
		if ( HWValve->traveltimer.Q & HWValve->_close ) {
			GVO->FailedToCloseAlarm	= ON;
			HWValve->_close			= OFF;
		}	
	}	
	
	return(0);
}

BOOL mx_init( POLL_obj *Poll, UDINT Valve_adr, UDINT SM_adr, 
					USINT NumValves, USINT NumRetry, USINT Reconnect, REAL InterPollDelay, 
					UDINT PortOKCh1_adr, UDINT PortOKCh2_adr )
{
STATEMACHINE *sm;

	/* Pre-process Input */
	InterPollDelay			= (InterPollDelay / 10.0) + 0.5;
	
	/* Assignments */
	Poll->NumValves 		= NumValves;
	Poll->NumRetry	 		= NumRetry;
	
	Poll->Valve_adr 		= Valve_adr;
	Poll->MovingValve_adr 	= Valve_adr;
	Poll->CommandValve_adr	= Valve_adr;
	Poll->ActiveValve_adr 	= Valve_adr;
	
	Poll->StateMachine_adr 	= SM_adr;
	Poll->PortOK[CH1]		= PortOKCh1_adr;
	Poll->PortOK[CH2]		= PortOKCh2_adr;
	Poll->Reconnect			= Reconnect;
	Poll->InterPollDelay	= (UDINT) InterPollDelay; 
	/* General Diagnostic Reset */
	Poll->Polls[CH1]		= 0;
	Poll->Polls[CH2]		= 0;
	Poll->Reply[CH1]		= 0;
	Poll->Reply[CH2]		= 0;
	Poll->NoReply[CH1]		= 0;
	Poll->NoReply[CH2]		= 0;
	Poll->MovingScanToggle  = OFF;
	Poll->MovingTableFull 	= OFF;

	/* Subsequent References */
	sm						= (STATEMACHINE * ) Poll->StateMachine_adr;
	
	/* Initial Conditions */
	Poll->Poll_Num = -1; 
	sm->statenext  = -1;
	
	/* Return Value */
	return OFF;
}

UINT mx_serv( POLL_obj *Poll )
{
MXVALVE_obj *ActiveValve, *CommandValve, *MovingValve;
STATEMACHINE *sm;

	/* Subsequent References */
	ActiveValve 	= (MXVALVE_obj *) 	Poll->ActiveValve_adr ;
	CommandValve 	= (MXVALVE_obj *) 	Poll->CommandValve_adr;
	MovingValve 	= (MXVALVE_obj *) 	Poll->MovingValve_adr;
	sm				= (STATEMACHINE *) 	Poll->StateMachine_adr;

	/* Update Moving Valve Table */
	UpdateMovingTable( Poll );
	
	/***********************************************/
	/***********************************************/
	/****  STATE MACHINE MX_POLL: STARTS HERE   ****/
	/***********************************************/
	/***********************************************/
	
	SM_BEGIN(*sm)
	
	/*****************************/
	/* Initialize: State INIT: 0 */
	/*****************************/
	SM_STATE(MX_INITIALIZE,"Initialize") {
	/* Init */
	
	/* Start with Channel 1 */
	Poll->Active_Ch 	= CH1;
	Poll->Old_Ch    	= CH1;
	Poll->MovingScan 	= 0;
	Poll->Enable_Ch2 	= OFF;
	Poll->Enable_Ch1	= ON;
	
	/* End Init */
	}
	/* Cyclic */
	
	/* If either Port is opened properly */
	if ( * (BOOL *) Poll->PortOK[CH1] || * (BOOL *) Poll->PortOK[CH2] )    
	   SM_NEXT(MX_GETVALVE); 
	/**** END STATE: MX_INITIALIZE ****/
	
	/************************************/
	/* Increment Valve: MX_GETVALVE: 1  */
	/************************************/
	SM_STATE(MX_GETVALVE,"Get Valve") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* Get Active Valve */
	ActiveValve = GetActiveValve( Poll );
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Go to Poll Channel #1 */
		if ( (Poll->Active_Ch == CH1) && (Poll->Old_Ch == CH1) ) {
	
	 	  	/* Locate Moving Valve */
	 	  	MovingValve = GetMovingValve( Poll, CH1 );
	   
		   	/* Valid Moving Valve is Located */
		   	if ( Poll->MovingScan != -1 ) {
	   
	  	 		/* Interleave Moving Polling */
	  	 	   	if ( Poll->MovingScanToggle )
	 	  	   	   	SM_NEXT(MX_POLL_MOVE_CH1);
	  	 	   	/* Interleave Normal Polling */   	
		   	   	else 
	 	  	   	   	SM_NEXT(MX_POLL_CH1);
	 	  	   	}
	   		/* No Valid Moving Valve is Located */	      
	  	 	else	   
	  	 		/* Poll Ch1 */
	 		 	SM_NEXT(MX_POLL_CH1);
			}
	
		/* Switch Channels */
		if ( ( (Poll->Active_Ch == CH1) && (Poll->Old_Ch == CH2) ) ||
		     ( (Poll->Active_Ch == CH2) && (Poll->Old_Ch == CH1) ) )
	  	 	/* Change Channels */
	   		SM_NEXT(MX_CHANGE_CH);
	
		/* Go to Poll Channel #2 */
		if  ( (Poll->Active_Ch == CH2) && (Poll->Old_Ch == CH2) ) {
	
	   		/* Locate Moving Valve */
	   		MovingValve = GetMovingValve( Poll, CH2);
	   
	   		/* Valid Moving Valve is Located */
	   		if ( Poll->MovingScan != -1 ) {
	   
	   			/* Interleave Moving Polling */
	   	  	 	if ( Poll->MovingScanToggle )
	   		   	   	SM_NEXT(MX_POLL_MOVE_CH2);
	  	 	   	/* Interleave Normal Polling */   	
	  	 	   	else 
	   		   	   	SM_NEXT(MX_POLL_CH2);
	   	   	   	}
	   		/* No Valid Moving Valve is Located */	      
	   		else	   
	   	   		/* Poll Ch1 */
	 	 		SM_NEXT(MX_POLL_CH2);
	   	   	}
		}   	
	/**** END STATE: MX_GETVALVE ****/
	
	/***********************************/
	/* Poll Channel 1: MX_POLL_CH1: 10 */
	/***********************************/
	SM_STATE(MX_POLL_CH1,"Poll Ch1") {
	/* Init */
	
	/* Valve Comm OK: Poll Valve and Check Results */
	if ( ActiveValve->Comm_Stat[CH1] == OK ) {
	     /* Poll Valve */
	     SendPollMessage( Poll, ActiveValve, CH1 );
	     /* Check Results */
	     SM_NEXT(MX_RESULT_CH1);
	     }
	     
	/* Valve Retry has occurred: Poll Valve and Check Results */               
	else if ( ActiveValve->Retry[CH1] ){
	     /* Poll Valve */
	     SendPollMessage( Poll, ActiveValve, CH1 );
	     /* Check Results */
	     SM_NEXT(MX_RESULT_CH1);
	     }  
	       
	/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
	else {
		/* Update Failure Points */
	   	FailedResponse( Poll, ActiveValve, CH1 );
		/* Continue Polling */
	    SM_NEXT(MX_GETVALVE);   
	    }  
	/* End Init */
	}
	/* Cyclic */
	
	/**** END STATE: MX_POLL_CH1 ****/
	
	/***********************************/
	/* Poll Channel 2: MX_POLL_CH2: 20 */
	/***********************************/
	SM_STATE(MX_POLL_CH2,"Poll Ch2") {
	/* Init */
	
	/* Valve Comm OK: Poll Valve and Check Results */
	if ( ActiveValve->Comm_Stat[CH2] == OK ){
	     /* Poll Valve */
	     SendPollMessage( Poll, ActiveValve, CH2 );
	     /* Check Results */
	     SM_NEXT(MX_RESULT_CH2);
	     }
	
	/* Valve Retry has occurred: Poll Valve and Check Results */               
	else if ( ActiveValve->Retry[CH2] ){
	     /* Poll Valve */
	     SendPollMessage( Poll, ActiveValve, CH2 );
	     /* Check Results */
	     SM_NEXT(MX_RESULT_CH2);
	     }    
	
	/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
	else {
		/* Update Failure Points */
	   	FailedResponse( Poll, ActiveValve, CH2 );
		/* Continue Polling */
	    SM_NEXT(MX_GETVALVE);   
	    }
	/* End Init */
	}
	/* Cyclic */              
	  
	/**** END STATE: MX_POLL_CH2 ****/
	
	/***************************************/
	/* Result Channel 1: MX_RESULT_CH1: 11 */
	/***************************************/
	SM_STATE(MX_RESULT_CH1,"Result Ch1") {
	/* Init */
	
	/* Search For Next Command */
	SearchNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	
	/* Channel 1 Fail: No Response */
	if ( ActiveValve->MessageFail[CH1] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedResponse( Poll, ActiveValve, CH1 );
	   	
	/* Channel 1 OK: Good Response */
	if ( ActiveValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodResponse( Poll, ActiveValve, CH1 );   		
	
	/* Channel 1 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && !Poll->CommandReady )
	   	/* Continue Polling */		
	   	SM_NEXT(MX_GETVALVE);                              
	
	/* Channel 1 Message Complete and Command Pending: Issue Command on Channel 1 */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(MX_CMD_CH1);  
	/**** END STATE: MX_RESULT_CH1 ****/
	
	/***************************************/
	/* Result Channel 2: MX_RESULT_CH2: 21 */
	/***************************************/
	SM_STATE(MX_RESULT_CH2,"Result Ch2") {
	/* Init */
	
	/* Search For Next Command */
	SearchNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	
	/* Channel 2 Fail: No Response */
	if ( ActiveValve->MessageFail[CH2] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedResponse( Poll, ActiveValve, CH2 );	
	   	
	/* Channel 2 OK: Good Response */
	if ( ActiveValve->MessageOK[CH2] ) 
	   	/* Update Good Response */
	   	GoodResponse( Poll, ActiveValve, CH2 );      	
	
	/* Channel 2 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH2] || ActiveValve->MessageFail[CH2] || SMT10Done(0) ) && !Poll->CommandReady )
		/* Continue Polling */
	   	SM_NEXT(MX_GETVALVE);                              
	
	/* Channel 2 Message Complete and Command Pending: Issue Command on Channel #2 */
	if ( (ActiveValve->MessageOK[CH2] || ActiveValve->MessageFail[CH2] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(MX_CMD_CH2);        
	/**** END STATE: MX_RESULT_CH2 ****/
	
	/*************************************/
	/* Command Channel 1: MX_CMD_CH1: 12 */
	/*************************************/
	SM_STATE(MX_CMD_CH1,"Ch1 Command") {
	/* Init */
	
	/* Get Command Valve */
	CommandValve = GetCommandValve( Poll );
	  
	/* Valve Comm on Channel 1 OK: Send Command and Check Results */
	if ( CommandValve->Comm_Stat[CH1] == OK ) {
		/* Send Command */
		SendCommandMessage( Poll, CommandValve, CH1 );
		/* Check Results */
		SM_NEXT(MX_CMD_RESULT_CH1);    
		}
		
	/* Valve Comm on Channel 1 Fail and Channel 2 OK: Fail Channel 1 and Try Channel 2 */
	else if ( CommandValve->Comm_Stat[CH2] == OK ) {
		/* Command Failed on Channel 1 */
	   	CommandFail( Poll, CommandValve, CH1 );
	   	/* Try Channel 2 */
	   	SM_NEXT(MX_CMD_CH2);
	   	}
	   
	/* Valve Comm Fail on Both Channels: Fail Channel 1 and Continue Polling */
	else {
		/* Command Failed on Channel 1 */
	   	CommandFail( Poll, CommandValve, CH1 );
	   	/* Continue Polling */
	   	SM_NEXT(MX_GETVALVE);
	   	}
	/* End Init */
	}
	/* Cyclic */
	    
	/**** END STATE: MX_CMD_CH1 ****/
	
	/*************************************/
	/* Command Channel 2: MX_CMD_CH2: 22 */
	/*************************************/
	SM_STATE(MX_CMD_CH2,"Ch2 Command") {
	/* Init */
	
	/* Get Command Valve */
	CommandValve = GetCommandValve( Poll );
	  
	/* Valve Comm on Channel 2 OK: Send Command and Check Results */
	if ( CommandValve->Comm_Stat[CH2] == OK ) {
		/* Send Command */
		SendCommandMessage( Poll, CommandValve, CH2 );
		/* Check Results */
		SM_NEXT(MX_CMD_RESULT_CH2);    
		}
		
	/* Valve Comm on Channel 2 Fail and Channel 1 OK: Fail Channel 2 and Try Channel 1 */
	else if ( CommandValve->Comm_Stat[CH1] == OK ) {
		/* Command Failed on Channel 2 */
	   	CommandFail( Poll, CommandValve, CH2 );
	   	/* Try Channel 1 */
	   	SM_NEXT(MX_CMD_CH1);
	   	}
	   
	/* Valve Comm Fail on Both Channels: Continue Polling */
	else {
		/* Command Failed on Channel 2 */
	   	CommandFail( Poll, CommandValve, CH2 );
	   	/* Continue Polling */
	   	SM_NEXT(MX_GETVALVE);
	   	}  	
	/* End Init */
	}
	/* Cyclic */
	    
	/**** END STATE: MX_CMD_CH2 ****/
	
	/****************************************************/
	/* Command Results Channel 1: MX_CMD_RESULT_CH1: 13 */
	/****************************************************/
	SM_STATE(MX_CMD_RESULT_CH1,"Cmd Result Ch1") {
	/* Init */
	
	/* TimeOut: 1.5 Seconds */
	SMT10Start(0,150 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Command Executed: Update Good Command Statistics and Continue Polling */
	if ( CommandValve->MessageOK[CH1] ) { 
	    /* Update Good Command Statistics */ 
	    CommandExec( Poll, CommandValve, CH1 );
	    /* Continue Polling */
	    SM_NEXT( MX_GETVALVE );
	    }
	                           
	/* Command Failed: Update Failure Command Statistics and Continue Polling */                                                
	if ( (CommandValve->MessageFail[CH1]) || SMT10Done(0) ) {
		/* Update Failure Command Statistics */
		CommandFail( Poll, CommandValve, CH1 );
	    /* Continue Polling */
	    SM_NEXT( MX_GETVALVE );
	    }
	/**** END STATE: MX_CMD_RESULT_CH1 ****/
	
	/****************************************************/
	/* Command Results Channel 2: MX_CMD_RESULT_CH2: 23 */
	/****************************************************/
	SM_STATE(MX_CMD_RESULT_CH2,"Cmd Result Ch2") {
	/* Init */
	
	/* TimeOut: 1.5 Seconds */
	SMT10Start(0, 150 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Command Executed: Update Good Command Statistics and Continue Polling */
	if ( CommandValve->MessageOK[CH2] ) { 
	    /* Update Good Command Statistics */ 
	    CommandExec( Poll, CommandValve, CH2 );
	    /* Continue Polling */
	    SM_NEXT( MX_GETVALVE );
	    }
	                           
	/* Command Failed: Update Failure Command Statistics and Continue Polling */                                                
	if ( (CommandValve->MessageFail[CH2]) || SMT10Done(0) ) {
		/* Update Failure Command Statistics */
		CommandFail( Poll, CommandValve, CH2 );
	    /* Continue Polling */
	    SM_NEXT( MX_GETVALVE );
	    }
	/**** END STATE: MX_CMD_RESULT_CH2 ****/
	       
	/************************************************/
	/* Poll Moving Valve CH 1: MX_POLL_MOVE_CH1: 14 */
	/************************************************/
	SM_STATE(MX_POLL_MOVE_CH1,"Poll Moving Valve Ch1") {
	/* Init */
	
	/* Valve Comm OK: Poll Valve and Check Results */
	if ( MovingValve->Comm_Stat[CH1] == OK ){
	     /* Poll Function */
	     SendPollMessage( Poll, MovingValve, CH1 );
	     /* Check Results */
	     SM_NEXT(MX_MOVE_RESULT_CH1);
	     }
	     
	/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
	else {
		/* Update Failure Points */
	   	FailedResponse( Poll, MovingValve, CH1 );
		/* Continue Polling */
	    SM_NEXT(MX_GETVALVE);   
	    }       
	/* End Init */
	}
	/* Cyclic */
	
	/**** END STATE: MX_POLL_MOVE_CH1 ****/
	
	/************************************************/
	/* Poll Moving Valve CH 2: MX_POLL_MOVE_CH2: 24 */
	/************************************************/
	SM_STATE(MX_POLL_MOVE_CH2,"Poll Moving Valve Ch2") {
	/* Init */
	
	/* Valve Comm OK: Poll Valve and Check Results */
	if ( MovingValve->Comm_Stat[CH2] == OK ){
	     /* Poll Function */
	     SendPollMessage( Poll, MovingValve, CH2 );
	     /* Check Results */
	     SM_NEXT(MX_MOVE_RESULT_CH2);
	     }
	     
	/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
	else {
		/* Update Failure Points */
	   	FailedResponse( Poll, MovingValve, CH2 );
		/* Continue Polling */
	    SM_NEXT(MX_GETVALVE);   
	    }
	/* End Init */
	}
	/* Cyclic */
	                       
	/**** END STATE: MX_POLL_MOVE_CH2 ****/
	       
	/*********************************************/
	/* Result Moving Ch1: MX_MOVE_RESULT_CH1: 15 */
	/*********************************************/
	SM_STATE(MX_MOVE_RESULT_CH1,"Result Move") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 1 Fail: No Response */
	if ( (MovingValve->MessageFail[CH1]) || SMT10Done(0) ) 
	   /* Update Failure Points */
	   FailedResponse( Poll, MovingValve, CH1 );
	   
	/* Channel 1 OK: Good Response */
	if ( MovingValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodResponse( Poll, MovingValve, CH1 );      
	  
	/* Channel 1 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH1] || MovingValve->MessageFail[CH1] || SMT10Done(0) )
		/* Continue Polling */
	   	SM_NEXT(MX_GETVALVE);                              
	/**** END STATE: MX_MOVE_RESULT_CH1 ****/
	
	/*********************************************/
	/* Result Moving Ch2: MX_MOVE_RESULT_CH2: 25 */
	/*********************************************/
	SM_STATE(MX_MOVE_RESULT_CH2,"Result Move") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 2 Fail: No Response */
	if ( (MovingValve->MessageFail[CH2]) || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedResponse( Poll, MovingValve, CH2 );
	   
	/* Channel 2 OK: Good Response */
	if ( MovingValve->MessageOK[CH2] ) 
	   	/* Update Good Response */
	   	GoodResponse( Poll, MovingValve, CH2 );      
	  
	/* Channel 2 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH2] || MovingValve->MessageFail[CH2] || SMT10Done(0) )
		/* Continue Polling */
	   	SM_NEXT(MX_GETVALVE);
	/**** END STATE: MX_MOVE_RESULT_CH2 ****/
	       
	/*******************************************/
	/* Change Master Channels: MX_CHANGE_CH: 2 */
	/*******************************************/
	SM_STATE(MX_CHANGE_CH,"Change Channels") {
	/* Init */
	
	/* Wait 200 mSecs to change channels */
	SMT10Start(0, 20 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Delay between Channels: Poll Channel 1 */
	if ( (SMT10Done(0)) && (Poll->Active_Ch == CH1) )
		/* Poll Channel 1 */ 
	   	SM_NEXT(MX_POLL_CH1);
	   
	/* Delay between Channels: Poll Channel 2 */
	if ( (SMT10Done(0)) && (Poll->Active_Ch == CH2) ) 
	   	/* Poll Channel 2 */ 
	   	SM_NEXT(MX_POLL_CH2);
	/**** END STATE: MX_CHANGE_CH ****/
	
	/***********************************************/
	/****  STATE MACHINE MX_POLL: ENDS HERE     ****/
	/***********************************************/
	SM_END
	
	/* Valve Comm Failure */
	AnyValveCommFail( Poll );
	
	/* Return Value */
	return sm->state;
}

/*****************************/
/** Function mx_command()   **/
/*****************************/
BOOL mx_command( POLL_obj *Poll, USINT Valve_Num, UINT Command )
{

MXVALVE_obj *Valve;

	/* Subsequent References */
	Valve 	= (MXVALVE_obj *) Poll->Valve_adr + Valve_Num;
	
	/* If Valve Comm Status is Good */
	if (Valve->Status != FAIL) {
	
		/* Establish Command Signal to Valve: */
	    Valve->Command = Command;
	
	   	/* New Command */
		return(1);
			
	    } 
	    
	/* No New Command */	
	return(0);   

}

/************************************/
/** Function SearchNextCommand()   **/
/************************************/
BOOL SearchNextCommand( POLL_obj *Poll )
{
MXVALVE_obj *Valve;
VALVE_obj *GVO;
USINT i;

/* Subsequent References */
Valve 	= (MXVALVE_obj *) Poll->Valve_adr;

/* If a command is not currently pending */
if ( !Poll->CommandReady ) {

	/* Count through All Valves */ 	
	for (i=0;i<Poll->NumValves;i++,Valve++) {
	
		/* Only Process Command if MX Valve is Enabled */
		if ( Valve->Enable ) {
	
			/* Generic Valve Reference */
			GVO	= (VALVE_obj *) Valve->Valve_adr;
		
			/* Generic Valve Open Command: Only if Not Opening and Not already Opened */
			if ( GVO->open_cmd && (Valve->Position != MX_OPENING) && (Valve->Position != MX_OPENED) )  
				Valve->Command 		= MX_OPEN_CMD;
				
			/* Generic Valve Close Command: Only if Not Closing and Not already Closed */
			if ( GVO->close_cmd && (Valve->Position != MX_CLOSING) && (Valve->Position != MX_CLOSED) ) 
				Valve->Command 		= MX_CLOSE_CMD;
				
			/* Generic Valve Stop Command */
			if ( GVO->stop_cmd ) 
				Valve->Command 		= MX_STOP_CMD;
			
			/* Reset Commands: OIP and Remote */
			if ( GVO->oip_hold ) 
				GVO->open_remote = GVO->open_oip = GVO->close_remote = GVO->close_oip = GVO->stop_remote = GVO->stop_oip = OFF;
	
			/* Store For One Scan */	
			GVO->oip_hold	= GVO->open_remote | GVO->open_oip | GVO->close_remote | GVO->close_oip | GVO->stop_remote | GVO->stop_oip;
				
			/* Reset Commands: OIP and Remote */
			GVO->open_cmd 	= OFF;
			GVO->close_cmd 	= OFF;
			GVO->stop_cmd 	= OFF;
						
			/* Valve is Requesting a Command Action */
			if ( Valve->Command > 0 ) {		
			
				/* Set Command Pending Signal */
   	    		Poll->CommandReady 	= ON;
   				/* Set Valve number */
   	    		Poll->CommandValve 	= Valve->ID;
		       	/* One Command at a Time */
				return(1);
				}	
			}
	    } 
  	}

/* No New Command */	
return(0);   

}
/* This Function adds a commanded valve to a table */
void AddToMovingTable( MXVALVE_obj *Valve )
{
 
/* Enable Multiscan */	
Valve->ScanTimer.IN = ON;

}

/* This function determines how long to keep valves in table */
void UpdateMovingTable( POLL_obj *Poll )
{
USINT i;
MXVALVE_obj *CandidateValve;

/* Default Reference */
CandidateValve = (MXVALVE_obj *) Poll->Valve_adr;

/* Default Value */
Poll->MovingTableFull = OFF;

/* Count through All Valves */ 	
for (i=0;i<Poll->NumValves;i++,CandidateValve++) {
	
	/* Execute Timer for Valve */
	TON_10ms( (TON_10mstyp *) &CandidateValve->ScanTimer );
	/* When Complete: Remove from table */
	if ( CandidateValve->ScanTimer.Q )
		CandidateValve->ScanTimer.IN = OFF;
	/* At Least 1 Valve in Table */
    if ( CandidateValve->ScanTimer.IN )
    	Poll->MovingTableFull = ON;
    	
    }

/* No Valves in Table: Make sure Enable is OFF 	
if ( !Poll->MovingTableFull )
	Poll->MovingScanToggle  = OFF;*/

}

/*******************************/
/** Function GetMovingValve   **/
/*******************************/
MXVALVE_obj *GetMovingValve( POLL_obj *Poll, USINT Channel )
{
USINT i;
MXVALVE_obj *CandidateValve;

/* Moving Table is Empty: Reset Index and Return */
if ( !Poll->MovingTableFull ) { 
	/* Signal */
	Poll->MovingScan 		= -1;
	/* Toggle OFF */
	Poll->MovingScanToggle  = OFF;
	/* Return Value */
	return (MXVALVE_obj *) Poll->MovingValve_adr; 
	}

/* Return if Moving Scan Interleave is Not Enabled */
if ( Poll->MovingScanToggle ) {
	/* Previous Scan was ON: Disable scan this cycle */
	Poll->MovingScanToggle = OFF;
	/* Return Value */
	return (MXVALVE_obj *) Poll->MovingValve_adr; 
	}
else
	/* Disable Moving Scan */
	Poll->MovingScanToggle = ON;	

/* Get Valve Candidate */	
CandidateValve = (MXVALVE_obj *) Poll->Valve_adr;
    
/* Count through all valves unless return */	
for (i=0;i<Poll->NumValves;i++) {

	/* Increment Moving Scan Ring Index */
	Poll->MovingScan++;
	if (Poll->MovingScan >= Poll->NumValves)
    	Poll->MovingScan = 0;
    	
    /* Get Valve Candidate */	
    CandidateValve = (MXVALVE_obj *) Poll->Valve_adr + Poll->MovingScan;	
        
    /* Moving Valve Located and Enabled */
    if ( (CandidateValve->ScanTimer.IN) && (CandidateValve->Enable) ) {
    
    	/* Store */
		Poll->MovingValve_adr = (UDINT) CandidateValve;
		/* Establish Moving Valve Reference */
		return CandidateValve;
    	
    	}
	}
	
/* No Moving Valve Located */	

/* Signal */
Poll->MovingScan = -1;

/* Return Value */
return (MXVALVE_obj *) Poll->MovingValve_adr; 

}	

/*******************************/
/** Function: SendPollMessage **/
/*******************************/ 
void SendPollMessage( POLL_obj *Poll, MXVALVE_obj *Valve, USINT Channel )
{

/* Active Channel Reset Fail/Ok bits */
Valve->MessageFail[Channel] 	= OFF;
Valve->MessageOK[Channel]   	= OFF;

/* Reset Valve Retry Statistics */
Valve->Retry[Channel] 			= OFF;
Valve->ReconnectCount[Channel]	= 0;

/* Establish Command Enable */
Valve->bPoll[Channel]		 	= ON;
	
/* Polls: Absolute Number per Valve per Channel */
Valve->Polls[Channel]++;

/* Polls: Absolute Number per Channel */
Poll->Polls[Channel]++;
}

/******************************/
/** Function: FailedResponse **/
/******************************/
void FailedResponse( POLL_obj *Poll, MXVALVE_obj *Valve, USINT Channel )
{
USINT ChannelX = CH1;
VALVE_obj *GVO;

/* Assign Pointer */
GVO	= (VALVE_obj *) Valve->Valve_adr;

/* ChannelX is other channel */
if ( Channel == CH1 )
	ChannelX = CH2;
	
/* Reset Comm Enable */
Valve->bPoll[Channel] = OFF;	

/* Valve Channel Comm OK: Handle Remove Count, Establish Comm Status, Increment Diagnostics */
if ( Valve->Comm_Stat[Channel] == OK ) {

	/* Increment Remove Counter */
	Valve->RemoveCount[Channel]++;

	/* Valve has Not replied in several attempts */
	if ( Valve->RemoveCount[Channel] > Poll->NumRetry ) {

    	/* Set Channel Communication Status to Failure */
    	Valve->Comm_Stat[Channel] = FAIL;
    	/* Reset Remove Counter Scratch Pad Variable */
    	Valve->RemoveCount[Channel]   = 0;
    	/* Check Other Channel for Failure */
    	if ( Valve->Comm_Stat[ChannelX] == FAIL )
        	/* Both Channels are in Failure */
        	Valve->Status = FAIL;
        }
        
    /* No Reply: Absolute Number per Valve per Channel */        
	Valve->NoReply[Channel]++;
	/* No Reply: Absolute Number per Channel */
	Poll->NoReply[Channel]++;   
        	
    }
    
/* Valve Channel Comm Failure: Increment and Check Reconnect Status */    
else {

	/* Increment Valve Reconnect Count */
	Valve->ReconnectCount[Channel]++;
	
	/* Valve Exceeded Reconnect Cycles */
	if ( Valve->ReconnectCount[Channel] > Poll->Reconnect ) {
		/* Reset Valve Reconnect Count */
		Valve->ReconnectCount[Channel] 	= 0;
		Valve->Retry[Channel] 			= ON;
		}
	}   
	
/* Update GVO Comm Failure Status */
if ( Valve->Status == FAIL ) {
	strcpy( GVO->message, "ComFail" );
	GVO->commfail	= ON;
	}	
}

/* This function determines */
void AnyValveCommFail( POLL_obj *Poll )
{
USINT i;
MXVALVE_obj *Valve;
BOOL AnyValveCommFail = OFF, Ch1Fail = OFF, Ch2Fail = OFF;

/* Default Reference */
Valve = (MXVALVE_obj *) Poll->Valve_adr;

/* Count through All Valves */ 	
for (i=0;i<Poll->NumValves;i++,Valve++) {

	/* Valve in Comm Fail: Both Channels */
	if ( Valve->Status == FAIL ) 
		AnyValveCommFail = ON;
		
	/* Valve in Ch1 Comm Fail: */
	if ( Valve->Comm_Stat[CH1] == FAIL ) 
		Ch1Fail = ON;
		
	/* Valve in Ch2 Comm Fail: */
	if ( Valve->Comm_Stat[CH2] == FAIL ) 
		Ch2Fail = ON;		
	}

/* Make Assignment */	
Poll->ValveError 		= AnyValveCommFail;	
Poll->ValveChError[CH1]	= Ch1Fail;
Poll->ValveChError[CH2]	= Ch2Fail;

}	
	
/******************************/
/** Function: GoodResponse   **/
/******************************/
void GoodResponse( POLL_obj *Poll, MXVALVE_obj *Valve, USINT Channel )
{
VALVE_obj *GVO;

/* Assign Pointer */
GVO	= (VALVE_obj *) Valve->Valve_adr;

/* Reset Comm Enable */
Valve->bPoll[Channel] 		= OFF;

/* RemoveCount: Reset Scratch pad type variable */
Valve->RemoveCount[Channel] = 0;

/* Valve Communication Status: Set Channel 1 and Global to OK */
Valve->Comm_Stat[Channel] 	= OK;
Valve->Status        	  	= OK;
	
/* Set GVO Comm Failure Status to Good */
GVO->commfail	= OFF;

/* Reply: Absolute Number per Valve per Channel */
Valve->Reply[Channel]++;

/* Reply: Absolute Number per Channel */
Poll->Reply[Channel]++;

/* Convert Percent: 0-4095 */
if (Valve->Type == 0) 
	Valve->Percent       		= (UINT) (Valve->Register[0] / 40.95 + 0.5);
else if (Valve->Type == 1) 
//* Convert Percent: 0-100 *09/28/2016 HBB  */
	Valve->Percent       		= (UINT) (Valve->Register[0] );

/* Update Valve Object Percent */
GVO->percent				= Valve->Percent;
/* Convert Position: Use First 5 Bits */
Valve->Position 	   		= 0x001F & Valve->Register[1];
/* Convert Alarm Word 1: Use Last 11 Bits */
Valve->Alarm[0] 	   		= 0xFFE0 & Valve->Register[1];
/* Store Alarm Word 2 */
Valve->Alarm[1] 	   		= Valve->Register[2];

/* Update GVO Local Status */
if ( (Valve->Register[1] & 0x0040) > 0 )
	GVO->local	= ON;
else
	GVO->local	= OFF;

/* Update GVO Fault Status */
if ( (Valve->Register[1] & 0xB3A0) > 0 )
	GVO->fault	= ON;
else
	GVO->fault	= OFF;
		
/* Update Valve Objectt Position */
switch ( Valve->Position ) {
	/* Opened */
	case MX_OPENED:	
		GVO->position 		= OPENED; 
		GVO->LSO			= ON;
		GVO->LSC			= OFF;
		strcpy( GVO->message, "Opened" );
		break;
	/* Closed */
	case MX_CLOSED:
		GVO->position 		= CLOSED; 
		GVO->LSO			= OFF;
		GVO->LSC			= ON;
		strcpy( GVO->message, "Closed" );
		break;	
	/* Travel */
	case MX_STOPPED:
		strcpy( GVO->message, "Stopped" );
		GVO->position 		= STOPPED; 
		GVO->LSO			= ON;
		GVO->LSC			= ON;
		break;
	case 17:	/* v5.06 */
	case 18:	/* v5.06 */
	case MX_OPENING:
		strcpy( GVO->message, "Opening" );
		GVO->position 		= OPENING; 
		GVO->LSO			= ON;
		GVO->LSC			= ON;
		break;
	case 9:		/* v5.06 */
	case 10:	/* v5.06 */
	case MX_CLOSING:
		strcpy( GVO->message, "Closing" );
		GVO->position 		= CLOSING; 
		GVO->LSO			= ON;
		GVO->LSC			= ON;
		break;
	/* No Indication */
	default:
		strcpy( GVO->message, "NoInd" );
		GVO->position		= NOIND; 
		GVO->LSO			= OFF;
		GVO->LSC			= OFF;
		break;
	}
}

/***************************/
/** Function: CommandExec **/
/***************************/
void CommandExec( POLL_obj *Poll, MXVALVE_obj *Valve, USINT Channel )
{

/* Reset Command Comm Enable */
Valve->cPoll[Channel] 		= OFF;
/* Reset Valve Command Word */
Valve->Command       		= 0;
/* Reset Command Enable */
Valve->Cmd_Stat[Channel]  	= OK;
/* Reset Command Pending Flag */
Poll->CommandReady 			= OFF;

}

/***************************/
/** Function: CommandFail **/
/***************************/
void CommandFail( POLL_obj *Poll, MXVALVE_obj *Valve, USINT Channel )
{
USINT ChannelX = CH1;

/* ChannelX is other channel */
if ( Channel == CH1 )
	ChannelX = CH2;
  	
/* Reset Command Comm Enable */
Valve->cPoll[Channel] 		= OFF;
  	
/* Command Failed on this channel */  	
Valve->Cmd_Stat[Channel] 	= FAIL;
    
/* Valve Command has Failed on both channels or
   General Valve Comm Failure on Both Channels */
if ( (Valve->Cmd_Stat[ChannelX] == FAIL) || (Valve->Status == FAIL) ) {
    
    /* Reset Valve Command */ 
    Valve->Command 		= 0;
    /* Reset Poll Command Pending */
    Poll->CommandReady 	= OFF;
    }

}

/*******************************/
/** Subroutine GetActiveValve **/
/*******************************/
MXVALVE_obj *GetActiveValve( POLL_obj *Poll ) 
{
MXVALVE_obj *Valve;
USINT i;
BOOL Change = OFF;

/* Default Value */
Valve = (MXVALVE_obj *) Poll->ActiveValve_adr; 

/* Return if Moving Scan Interleave is Enabled */
if ( Poll->MovingScanToggle ) 	
	/* Return Value */
	return Valve;
		
/* Record Old Channel */
Poll->Old_Ch = Poll->Active_Ch;

/* Cycle Through Valves */
for (i=0;i<Poll->NumValves;i++) {
	
	/* Increment Poll Number */
	Poll->Poll_Num++;
  
	/* Check to see if at End */  
	if (Poll->Poll_Num >= Poll->NumValves) { 
		/* Reset to Beginning */
    	Poll->Poll_Num = 0;
    	/* Change Channel Signal */
    	Change = ON;
    	}
    	
    /* Establish Active Valve Reference */
	Valve = (MXVALVE_obj *) Poll->Valve_adr + Poll->Poll_Num;
	
	/* Found Enabled Valve */
	if ( Valve->Enable )
		break;
	}		
     
/* Change Channels */
if ( Change ) {    
    
    /* Change Active Channel to CH2 */	/* V3.05 */
    /* Change Active Channel to CH2 */	/* V3.05.1 : Remove the change with v3.05*/
    /*if (( Poll->Active_Ch == CH1 ) & ( Poll->PortOK[CH2] )){ */
	if (Poll->Active_Ch == CH1) {    
       Poll->Enable_Ch1 = OFF;
       Poll->Enable_Ch2 = ON; 
       Poll->Active_Ch 	= CH2;
       } 
       
    /* Change Active Channel to CH1 */   
    else{
       Poll->Enable_Ch2 = OFF;
       Poll->Enable_Ch1 = ON;
       Poll->Active_Ch 	= CH1;
       }
    
    }
    
/* Store */
Poll->ActiveValve_adr = (UDINT) Valve;

/* Return Value */
return Valve;
}

/********************************/
/** Subroutine GetCommandValve **/
/********************************/
MXVALVE_obj * GetCommandValve( POLL_obj *Poll ) 
{
MXVALVE_obj *Valve;

/* Establish Command Valve Reference */
Valve = (MXVALVE_obj *) Poll->Valve_adr + Poll->CommandValve;

/* Store */
Poll->CommandValve_adr = (UDINT) Valve;

/* Return Value */
return Valve;
}

/*******************************/
/** Function: SendPollMessage **/
/*******************************/ 
void SendCommandMessage( POLL_obj *Poll, MXVALVE_obj *Valve, USINT Channel )
{

/* Is Channel OK to Send Command */
if ( Valve->Comm_Stat[Channel] == OK ) {

    /* Increment Diagnostics */
    Valve->Polls[Channel]++;
	/* Polls: Absolute Number per Channel */
	Poll->Polls[Channel]++;

	/* Reset Fail/Ok bits */
	Valve->MessageFail[Channel]	= OFF;
	Valve->MessageOK[Channel]  	= OFF;

    /* Establish Command Enable: Channel #1 */
    /* Establish Command Enable */
	Valve->cPoll[Channel]		 	= ON;
			
	/* Place Valve into Moving Array */
    AddToMovingTable( Valve );
    }

}

UINT BitToWord( UDINT bit_adr )
{
BOOL *bit_ptr = (BOOL *) bit_adr;
UINT wordh = 0;
USINT i; 

	for (i=0;i<16;i++,bit_ptr++) 
		wordh = wordh + ((UINT) *bit_ptr << i);
		
return wordh;
}	


/*************************************/
/*********  Get Bit Function  ********/
/*************************************/
plcbit GetBit(UINT word, UINT Bit_Num )

{
UINT Answer;

Answer = ( word & Bit_Num );

if (Answer>0)
	return 1;
else
	return 0;
		
}

/* V2.18 */
BOOL RCASetClock( USINT *rcatime )
{
RTCtime_typ time;
USINT *set;

/* Assign USINT Pointer to Set Location */
 set = rcatime + 6;

/* Clock Set Request */
if ( *set > 0 ) {

	/* Assign Time Structure Elements */
	time.second	= * (rcatime + 0);
	time.minute	= * (rcatime + 1);
	time.hour	= * (rcatime + 2);
	time.day	= * (rcatime + 3);
	time.month 	= * (rcatime + 4);
	time.year	= * (rcatime + 5);
	
	/* Set PLC Time from RCA */
	RTC_settime( &time );
		
	/* Reset RCA Set Location */
	*set = 0;
	
	/* Return Clock Has Been Set */
	return ON;
 	}

/* Clock Not Set */
else
	return OFF;  
}

BOOL GetTimeString( UDINT time_adr )
{
RTCtime_typ time;
UINT year, ms;
USINT *timestring;

/* */
timestring = (USINT *) time_adr;

/* Get Time from PLC */
RTC_gettime( &time );

/* Year */
year = time.year - 2000;
if ( year > 9 )
	itoa( year, (UDINT) &timestring[0] );
else {
	itoa( year, (UDINT) &timestring[1] );
	timestring[0] 	= '0';
	}
	
/* Month */
if ( time.month > 9 )
	itoa( time.month, (UDINT) &timestring[3] );
else {
	itoa( time.month, (UDINT) &timestring[4] );
	timestring[3] 	= '0';
	}
	
/* day */
if ( time.day > 9 )
	itoa( time.day, (UDINT) &timestring[6] );
else {
	itoa( time.day, (UDINT) &timestring[7] );
	timestring[6] 	= '0';
	}
	
/* Hour */
if ( time.hour > 9 )
	itoa( time.hour, (UDINT) &timestring[9] );
else {
	itoa( time.hour, (UDINT) &timestring[10] );
	timestring[9] 	= '0';
	}

/* Minute */
if ( time.minute > 9 )
	itoa( time.minute, (UDINT) &timestring[12] );
else {
	itoa( time.minute, (UDINT) &timestring[13] );
	timestring[12] 	= '0';
	}
	
/* Second */
if ( time.second > 9 )
	itoa( time.second, (UDINT) &timestring[15] );
else {
	itoa( time.second, (UDINT) &timestring[16] );
	timestring[15] 	= '0';
	}
	
/* Hundreths */
ms = time.millisec / 10;
if ( ms > 9 )
	itoa( ms, (UDINT) &timestring[18] );
else {
	itoa( ms, (UDINT) &timestring[19] );
	timestring[18] 	= '0';
	}
	
/* Delimitters */	
timestring[2] 	= ' ';
timestring[5] 	= ' ';
timestring[8] 	= ' ';	
timestring[11] 	= ':';
timestring[14] 	= ':';
timestring[17] 	= '.';

return OFF;	
}

BOOL WtoB( UINT w, UDINT bit_adr )
{
	WordToBit( w, (BOOL *) bit_adr );
	return ON;
}

void WordToBit( UINT Wordh, plcbit bith[16] )
{
UINT Hold = 0;
UINT Mask = 1;
UINT i;

/* Assign Masks */
for (i=0;i<16;i++) {
	 Hold = Wordh & Mask;
	 if (Hold > 0)
	 	bith[i] = 1;
	 else
	 	bith[i] = 0;	
     Mask = Mask * 2;
     }

}

/* Modified: Only send valve open when not opened 
			 return TRUE if at least 1 valve sent open
			 return FALSE if NO valves sent open 
*/			 
BOOL ShipperValveOpen( UDINT vg_adr, STEP_obj *step, USINT type )
{
VALVEGROUP_obj *vg;
VALVE_obj *valve;
BOOL bith[16], bit32[32], bit64[64], return_value = OFF;
USINT i=0, k;

/* Get Start Shipper Valve Group */
vg		= (VALVEGROUP_obj *) vg_adr;

/* Get Start Shipper Valve */
valve 	= (VALVE_obj *) vg->valve_adr;

/* Valve 64 Configuration Enabled */
if ( type == V64 ) {

	/* Get Bit Representation */
	LongToBit( step->Step64[0], &bit64[0] );
	LongToBit( step->Step64[1], &bit64[32] );
	
	/* Loop through Valve Groups */
	for (k=0;k<4;k++,vg++) {
	
		/* Get Start Shipper Valve */
		valve 	= (VALVE_obj *) vg->valve_adr;
		
		/* Cycle Through Shipper Valves */
		for (i=k*16;i<16+k*16;i++,valve++) {
			/* Open Command Located */
			if ( bit64[i] && (valve->position != OPENED) ) { 
				/* Set Open Command */
				valve->open_cmd = ON;
				/* At least 1 valve sent open */
				return_value 	= ON;
				}
			}
		}	
	}

	/* Valve 32 Configuration Enabled */
	if ( type == V32 ) {

		/* Get Bit Representation */
		LongToBit( step->Step32, bit32 );

		/* Loop through Valve Groups */
		for (k=0;k<2;k++,vg++) {
		
			/* Get Start Shipper Valve */
			valve 	= (VALVE_obj *) vg->valve_adr;
			
			/* Cycle Through Shipper Valves */
			for (i=k*16;i<16+k*16;i++,valve++) {
				/* Open Command Located */
				if ( bit32[i] && (valve->position != OPENED) ) { 
					/* Set Open Command */
					valve->open_cmd = ON;
					/* At least 1 valve sent open */
					return_value 	= ON;
					}
				}
			}
		}		
				
	/* Valve 16 Configuration Enabled */
	else {	
	 
		/* Get Bit Representation */
		WordToBit( step->Step, bith );

		/* Cycle Through Shipper Valves */
		for (i=0;i<16;i++,valve++) {

			/* Open Command Located */
			if ( bith[i] && (valve->position != OPENED) ) { 
				/* Set Open Command */
				valve->open_cmd = ON;
				/* At least 1 valve sent open */
				return_value 	= ON;
			}
		}
}

/* Return Value */
return return_value;
}

BOOL ShipperValveClose( UDINT vg_adr, STEP_obj *step, USINT type )
{
VALVEGROUP_obj *vg;
VALVE_obj *valve;
BOOL bith[16], bit32[32],bit64[64];
USINT i, k;

/* Get Start Shipper Valve Group */
vg		= (VALVEGROUP_obj *) vg_adr;

/* Get Start Shipper Valve */
valve 	= (VALVE_obj *) vg->valve_adr;

/* Valve 64 Configuration Enabled */
if ( type == V64 ) {

	/* Get Bit Representation */
	LongToBit( step->Step64[0], &bit64[0] );
	LongToBit( step->Step64[1], &bit64[32] );
	
	/* Loop through Valve Groups */
	for (k=0;k<4;k++,vg++) {
	
		/* Get Start Shipper Valve */
		valve 	= (VALVE_obj *) vg->valve_adr;
		
		/* Cycle Through Shipper Valves */
		for (i=k*16;i<16+k*16;i++,valve++) {
			/* Close Command Located */
			if ( !bit64[i] && valve->LSO ) 
				/* Set Close Command */
				valve->close_cmd = ON;
			}
		}	
	}

/* Valve 32 Configuration Enabled */
else if ( type == V32 ) {

	/* Get Bit Representation */
	LongToBit( step->Step32, bit32 );

	/* Loop through Valve Groups */
	for (k=0;k<4;k++,vg++) {
	
		/* Get Start Shipper Valve */
		valve 	= (VALVE_obj *) vg->valve_adr;
		
		/* Cycle Through Shipper Valves */
		for (i=k*16;i<16+k*16;i++,valve++) {
			/* Close Command Located */
			if ( !bit64[i] && valve->LSO ) 
				/* Set Close Command */
				valve->close_cmd = ON;
			}
		}
	}
	
/* Valve 16 Configuration Enabled */
else {

	/* Get Bit Representation */
	WordToBit( step->Step, bith );

	/* Cycle Through Shipper Valves */
	for (i=0;i<vg->number;i++,valve++) {		/* V5.03 */
		/* Close Command Located: Verify Valve is partially opened */
		if ( !bith[i] && valve->LSO ) 
			/* Set Close Command */
			valve->close_cmd = ON;
		}
	}		

/* Return Value */
return OFF;
}

/************************************************
Deterimines if Valves are in the current Step
position after a defined timelimit
************************************************/
/* V4.04 */
void SequenceOverun( RECIPE_obj *recipe ) 
{
VALVEGROUP_obj *vg;
VALVE_obj *valve;
BOOL InTravel, AnyInTravel, FullOpen, FullClose, step[16];
USINT i;

/* Get Valve Group Object */
vg		= (VALVEGROUP_obj *) recipe->vg_adr;

/* Get Start Shipper Valve */
valve 	= (VALVE_obj * ) vg->valve_adr;

/* V4.04: Inhibit OverRun Time: User Input to Enable SOR */
recipe->OverRunTimer.IN = !recipe->OverRunEnable & !recipe->Event;
/* OverRun Timer Execution */
TON_10ms( (TON_10mstyp *) &recipe->OverRunTimer );

/* Get Current Step Bit Representation */
WordToBit( recipe->current.Step, step );

/* Defaults */
InTravel 	= OFF;
AnyInTravel = OFF;
FullOpen 	= ON;	/* Assume Open: Logic to reset if any selected valve is NOT full open */
FullClose 	= ON;	/* Assume Close: Logic to reset if any valve is NOT full closed */

/* V1.03: Special Case Handling: Close All Selected */
if ( recipe->CloseAll )
	FullOpen = OFF;

/* V3.07: Cycle Through Shipper Valves */
for (i=0;i<vg->number;i++,valve++) {

	/* Enabled Valve: Current Step Valve In Travel */
	if ( valve->enable && valve->LSO && valve->LSC && step[i] ) 
		InTravel = ON;
	
	/* Enabled Valve: Current Step Valve Full Open */	
	if ( valve->enable && valve->LSC && step[i] )
		FullOpen = OFF;
 
	/* Enabled Valve: Any Valve In Travel or Partially Opened Non-Selected Valve */
	if ( valve->enable && ((valve->LSO && valve->LSC) || (valve->LSO && !step[i])) ) 
		AnyInTravel = ON;
		
	/* Enabled Valve: Any Valve Partially Opened */
	if ( valve->enable && valve->LSO )
		FullClose = OFF;
	
	}
	
/* Initiate Delay Close Timer: Wait Selected Valve LSO */  
/* Change: Add Initiate if Full Open at Event Scan */
if ( (InTravel && !recipe->InTravel) ||
	 (recipe->Event && FullOpen) )	
	/* Selected Valve In Travel or All selected valves full open at event: Start Timer */
	recipe->DelayCloseTimer.IN = ON;
		
/* Assign Outputs */
recipe->InTravel 		= InTravel;
recipe->FullOpen 		= FullOpen;	
recipe->AnyInTravel 	= AnyInTravel;
recipe->OverRunArmed 	= recipe->OverRunTimer.Q;	
/* OverRun: 	If OverRunTimer Elapses and Not in Bypass;
			 	Any Valve in Travel or Selected Valve not Full Open when Current Step is NOT zero V1.03 */
recipe->OverRun 		= recipe->OverRunArmed & (recipe->AnyInTravel | (!recipe->FullOpen & !recipe->CloseAll)) & !recipe->OverRunBypass;

}

/* V4.04 */
void GravityTriggerCalc ( RECIPE_obj *recipe, METER_obj *meter, TERMINAL_obj *terminal)
{

/* Gravity Trigger: Enabled */
if ( recipe->next.GravityEnable )	{

	/* High Deviation */		
	recipe->QuotaDevHigh = recipe->current.Quota + (UDINT) recipe->QuotaDev;
	/* Low Deviation  */
	recipe->QuotaDevLow = recipe->current.Quota - (UDINT) recipe->QuotaDev;
	/* Low Deviation: Prevent Negative Value */	
	if ( recipe->current.Quota < (UDINT) recipe->QuotaDev )
		recipe->QuotaDevLow = 0;
	
	/* Current Batch is within Deviation Window: Gravity Trigger Enabled */
	if (((meter->ActiveNet >= recipe->QuotaDevLow) &&
	     (meter->ActiveNet <= recipe->QuotaDevHigh) && (!recipe->GravityTrigOnly)) ||
	     ((recipe->current.GravityWindowOpen) && (recipe->GravityTrigOnly))) {
		
		/* Entering Window: Gravity Trigger Enabled */
		if ( !recipe->QuotaWindow ) {
		
			/* Set Rising Gravity Trigger */
			if (  recipe->next.GravSP.value_f > recipe->Gravity ) {
				recipe->GravityRise = ON;
				recipe->GravityFall = OFF;
				}
	 		/* Set Falling Gravity Trigger */
	 		else {
	 			recipe->GravityRise = OFF;
				recipe->GravityFall = ON;
				}
			/* Set Signal: Presently Within Deviation Window */	
			recipe->QuotaWindow = ON; 	
			}
		
		/* Rising Change Mode: Gravity Trigger Enabled  */
		if ( recipe->GravityRise ) {
		
			/* Current Gravity is now Greater than Next Gravity Setpoint */
			if ( recipe->Gravity > recipe->next.GravSP.value_f )
				/* Set Maintained Gravity Change */
				recipe->GravityTimer.IN = ON;
				
			/* Current Gravity is still Less than Gravity Trigger Setpoint */	
			else
				/* Reset Maintained Gravity Change */
				recipe->GravityTimer.IN = OFF;		
			}
						
		/* Falling Change Mode: Gravity Trigger Enabled  */	
		else if ( recipe->GravityFall ) {
		
			/* Current Gravity is now Less than Gravity Trigger Setpoint */
			if ( recipe->Gravity < recipe->next.GravSP.value_f )
				/* Set Maintained Gravity Change */
				recipe->GravityTimer.IN = ON;
				
			/* Current Gravity is still Greater than Gravity Trigger Setpoint */	
			else
				/* Reset Maintained Gravity Change */
				recipe->GravityTimer.IN = OFF;
			}
		
		/* Mainted Timer Execution */
		TON_10ms( (TON_10mstyp *) &recipe->GravityTimer );
		}

	/* Current Batch Outside Deviation Window: Gravity Trigger Enabled */
	else {
		/* Reset Signal: Deviation Window */	
		recipe->QuotaWindow 	= OFF;		
		}

	/* Exiting Deviation Window */
	 if ( !recipe->QuotaWindow && recipe->_oldQuotaWindow ) {
		
		/* Gravity Change didn't occur within Deviation Window *//* V4.04: */
		if ( /*(recipe->GravityRise || recipe->GravityFall) && */ !recipe->GravityChange & !meter->ResetOldNet )  { /* V5.09 */ 
			/* Alarm: GravityMiss */
			recipe->GravityMiss 			= ON;
			/* Reset Signal: Gravity Rise */
			recipe->GravityRise 			= OFF;
			/* Reset Signal: Gravity Fall */
			recipe->GravityFall 			= OFF;
			/* Reset Signal: Deviation Window */	
			recipe->QuotaWindow 			= OFF; 
			/* V4.04: Reset Signal: Gravity Window Open */	
			recipe->current.GravityWindowOpen = OFF;
			/* Reset Gravity Timer */
			recipe->GravityTimer.IN 		= OFF;
			TON_10ms( (TON_10mstyp *) &recipe->GravityTimer );
			}
		}
	 else
	 	/* Reset Signal: Gravity Change Missed Alarm */ 
		recipe->GravityMiss 	= OFF;

	 /* Store Old */
	 recipe->_oldQuotaWindow = recipe->QuotaWindow;
	}
	
/* Gravity Trigger: Disabled */			
else {	
		 
	/* Reset Signal: Deviation Window */	
	recipe->QuotaWindow 	= OFF;
	recipe->_oldQuotaWindow	= OFF;	/* v5.04 */
	/* V4.04: Reset Signal: Gravity Window Open */	
	recipe->current.GravityWindowOpen = OFF;
	/* Reset Signal: Gravity Change Missed Alarm */ 
	recipe->GravityMiss 	= OFF;
	/* Reset Signal: Gravity Change */
	recipe->GravityChange 	= OFF; 
	/* Reset Signal: Gravity Rise */
	recipe->GravityRise 	= OFF;
	/* Reset Signal: Gravity Fall */
	recipe->GravityFall 	= OFF;
	/* Reset Gravity Timer */
	recipe->GravityTimer.IN 	= OFF;
	TON_10ms( (TON_10mstyp *) &recipe->GravityTimer );
	/* Reset Gravity Trigger Timer */
	recipe->GravityTrigTimer.IN  = OFF;
	/* Reset Gravity Trigger Recipe */
	recipe->TriggerOnGravity 	= OFF;
	}


/* Gravity Change Detected */
if ( recipe->GravityTimer.Q )	{
		
	/* Entering Gravity Change Mode */
	if ( !recipe->GravityChange ) {
		
		/* Initialize Displacement Volume */
		recipe->DisplaceVolume 	= 0;
		/* Set Gravity Change Signal */
		recipe->GravityChange 	= ON;

		/* V4.04: */
		/* Reset Signal: Gravity Rise */
		recipe->GravityRise 			= OFF;
		/* Reset Signal: Gravity Fall */
		recipe->GravityFall 			= OFF;		
		
		}
	/* Continue in Gravity Change Mode */	
	else {
		/* V4.1: Increment Displace Volume */
		recipe->DisplaceVolume = recipe->DisplaceVolume + meter->newnetbbls;	
		/* Displace Volume is greater than Terminal Volume */
		if ((( recipe->DisplaceVolume >= terminal->TotalVolume ) && (!recipe->GravityTrigOnly)) ||
	     ((recipe->current.GravityWindowOpen) && (recipe->GravityTrigOnly))) 
			/* Arm: Gravity Trigger Timer */
			recipe->GravityTrigTimer.IN  = ON;
		/* Displace Volume still counting */	
		else
			/* Not Yet */
			recipe->GravityTrigTimer.IN = OFF;	
		}
	}	
	
/* No Gravity Change Detected */	
else {
	/* Reset Gravity Change Signal */
	recipe->GravityChange 		= OFF;	
	/* Reset Gravity Trigger Recipe */
	recipe->TriggerOnGravity 	= OFF;
	/* Reset Gravity Trigger Timer */
	recipe->GravityTrigTimer.IN  = OFF;
	}

/* Gravity Trigger Timer */
TON_10ms( (TON_10mstyp *) &recipe->GravityTrigTimer );
/* V4.04: Gravity Trigger Recipe */
if ( recipe->GravityTrigTimer.Q )
	recipe->TriggerOnGravity 	= ON;
		
/* V4.1: Reset on Recipe Trigger */
if (( recipe->Trigger) || (!recipe->QuotaWindow && !recipe->GravityChange)) {

	/* Reset Signal: Deviation Window */	
	recipe->QuotaWindow 	= OFF;
	/* V4.04: Reset Signal: Gravity Window Open */	
	recipe->current.GravityWindowOpen = OFF;
	/* Reset Signal: Gravity Change */
	recipe->GravityChange  = OFF; 
	/* Reset Signal: Gravity Rise */
	recipe->GravityRise 	= OFF;
	/* Reset Signal: Gravity Fall */
	recipe->GravityFall 	= OFF;
	/* Reset Gravity Timer */
	recipe->GravityTimer.IN 	= OFF;
	/* Reset Gravity Trigger Timer */
	recipe->GravityTrigTimer.IN  = OFF;
	TON_10ms( (TON_10mstyp *) &recipe->GravityTimer );
	}
	
	/* Product Gravity Incompatibility - v5.10 */
	recipe->current.isGas		= (recipe->GravityLimits[recipe->current.ShipNum-1].High > apiGAS) && (recipe->GravityLimits[recipe->current.ShipNum-1].Low < apiGAS);
	recipe->next.isGas			= (recipe->GravityLimits[recipe->next.ShipNum-1].High > apiGAS) && (recipe->GravityLimits[recipe->next.ShipNum-1].Low < apiGAS);
	recipe->current.isDiesel	= (recipe->GravityLimits[recipe->current.ShipNum-1].High > apiDIESEL) && (recipe->GravityLimits[recipe->current.ShipNum-1].Low < apiDIESEL);
	recipe->next.isDiesel		= (recipe->GravityLimits[recipe->next.ShipNum-1].High > apiDIESEL) && (recipe->GravityLimits[recipe->next.ShipNum-1].Low < apiDIESEL);
	
	if ( recipe->current.isGas && recipe->current.isDiesel ) {
		recipe->current.isGas = OFF;
		recipe->current.isDiesel = OFF;
	}
	
	if ( recipe->next.isGas && recipe->next.isDiesel ) {
		recipe->next.isGas = OFF;
		recipe->next.isDiesel = OFF;
	}

	if ( (meter->ActiveNet > (recipe->bufferBarrels)) ) {
		recipe->useBufferBarrels = OFF;
	}
	recipe->useBufferBarrels = recipe->useBufferBarrels || (recipe->Trigger && (recipe->current.isDiesel && recipe->next.isGas));
	
	
	if ( (meter->ActiveNet > terminal->TerminalVolume) && !recipe->GravityChange && (recipe->current.Step > 0) &&
		((recipe->GravityLimits[recipe->current.ShipNum-1].High < recipe->Gravity) || (recipe->GravityLimits[recipe->current.ShipNum-1].Low > recipe->Gravity)) ){
		
		if ( 	!(	recipe->QuotaWindow || 
					((meter->ActiveNet > (recipe->current.Quota - recipe->bufferBarrels)) && (recipe->current.isGas && recipe->next.isDiesel)) || 
					(recipe->useBufferBarrels)
				)
			){
			recipe->ProductIncompatible	= ON;
		}
		else {
			recipe->ProductIncompatible = OFF;
		}
		if ( (recipe->next.Step > 0) && ((recipe->GravityLimits[recipe->next.ShipNum-1].High < recipe->Gravity) || (recipe->GravityLimits[recipe->next.ShipNum-1].Low > recipe->Gravity)) ) {
			recipe->NextProductIncompatible = ON;
		}
		else {
			recipe->NextProductIncompatible = OFF;
		}
	}
	else {
		recipe->ProductIncompatible = OFF;
		recipe->NextProductIncompatible = OFF;
	}
}

/************************************************
Determine When to Issue EOB Request!!
************************************************/
void TicketTriggerCalc( RECIPE_obj *recipe, METER_obj *meter )
{
/* Note: Use Current Ticket Enable because Steps 
		 have already Shifted at this Stage     */

/* Batch Change Armed, Next Ticket Trigger Enabled and Master Recipe Enabled: */ 
if ( recipe->BatchChangeArmed && recipe->current.TicketEnable && recipe->Enable ) 

	/* Issue EOB */
	meter->eob_cmd				= ON;
	
/* Reset Recipe Batch Change: */
recipe->BatchChangeArmed 	= OFF;
			
}			

/************************************************
Use Next Step Quota Enable and Current Step Quota
to determine TriggerOnQuota Signal!!
************************************************/
void QuotaTriggerCalc( RECIPE_obj *recipe, METER_obj *meter )
{


/* Quota Trigger: Enabled */
if ( recipe->next.QuotaEnable ) {

	/* Active Counter Greater than Quota */
	if ( meter->ActiveNet > recipe->current.Quota ) {
	
		/* Entering */
		if ( !recipe->TriggerOnQuota )
			/* Set Quota Trigger Recipe */
			recipe->TriggerOnQuota = ON;
		}	
	/* Not Yet */	
	else
		/* Reset Quota Trigger Recipe */
		recipe->TriggerOnQuota = OFF;	
	}
	
/* Quota Trigger: Disabled */		
else
	/* Reset Quota Trigger Recipe */
	recipe->TriggerOnQuota = OFF;	
	
}	

/* V4.04 */
void TriggerCommands( RECIPE_obj *recipe, SECURITY_obj *security )
{
BOOL bith[16];
METER_obj *meter;

	meter = (METER_obj *) recipe->meter_adr;
	
/************************************/
/* Trigger Commands: Recipe Enables */
/************************************/	


/**** Master Enable ****/

/* RCA Recipe Enable Command */
if ( recipe->command.MasterArm )
	recipe->Enable = ON;
/* RCA Recipe Disable Command */
if ( recipe->command.MasterUnarm )
	recipe->Enable = OFF;

/* OIP Recipe Command: Only when NOT in Level 0: This only works if $ */
if ( recipe->command.master_oip && !security->Level0 ) 
	recipe->Enable = !recipe->Enable;
/* OIP Recipe Command: Clear */
recipe->command.master_oip = OFF;	


/**** CURRENT STEP ****/

/* V4.04: RCA Recipe Gravity Window Open: Enable*/
if ( recipe->command.GtyWindowOpenArm)
	recipe->current.GravityWindowOpen = ON;
/* V4.04: RCA Recipe Gravity Window Open: Disable*/
if ( recipe->command.GtyWindowOpenUnarm)
	recipe->current.GravityWindowOpen = OFF;	

	
/**** NEXT STEP ****/
	
/* Next Step Quota: Enable */	
if ( recipe->command.NextQuotaArm )
	recipe->next.QuotaEnable = ON;
/* Next Step Quota: Disable */	
if ( recipe->command.NextQuotaUnarm )
	recipe->next.QuotaEnable = OFF;	
	
/* Next Step Gravity: Enable */	
if ( recipe->command.NextGravArm )
	recipe->next.GravityEnable = ON;
/* Next Step Gravity: Disable */	
if ( recipe->command.NextGravUnarm )
	recipe->next.GravityEnable = OFF;
	
/* Next Step Color: Enable */	
if ( recipe->command.NextColorArm )
	recipe->next.ColorEnable = ON;
/* Next Step Color: Disable */	
if ( recipe->command.NextColorUnarm )
	recipe->next.ColorEnable = OFF;				

/* Next Step Ticket: Enable */	
if ( recipe->command.NextTickArm )
	recipe->next.TicketEnable = ON;
/* Next Step Ticket: Disable */	
if ( recipe->command.NextTickUnarm )
	recipe->next.TicketEnable = OFF;
	
/**** SECOND STEP ****/
	
/* Second Step Quota: Enable */	
if ( recipe->command.SecondQuotaArm )
	recipe->second.QuotaEnable = ON;
/* Second Step Quota: Disable */	
if ( recipe->command.SecondQuotaUnarm )
	recipe->second.QuotaEnable = OFF;	
	
/* Second Step Gravity: Enable */	
if ( recipe->command.SecondGravArm )
	recipe->second.GravityEnable = ON;
/* Second Step Gravity: Disable */	
if ( recipe->command.SecondGravUnarm )
	recipe->second.GravityEnable = OFF;
	
/* Second Step Color: Enable */	
if ( recipe->command.SecondColorArm )
	recipe->second.ColorEnable = ON;
/* Second Step Color: Disable */	
if ( recipe->command.SecondColorUnarm )
	recipe->second.ColorEnable = OFF;				

/* Second Step Ticket: Enable */	
if ( recipe->command.SecondTickArm )
	recipe->second.TicketEnable = ON;
/* V4.01: Second Step Ticket: Disable 
Do not Dis arm on recipe->Trigger */	
if ( recipe->command.SecondTickUnarm )
	recipe->second.TicketEnable = OFF;	
	
/**** THIRD STEP - V5.03 ****/
	
/* Third Step Quota: Enable */	
if ( recipe->command.ThirdQuotaArm )
	recipe->third.QuotaEnable = ON;
/* Third Step Quota: Disable */	
if ( recipe->command.ThirdQuotaUnarm )
	recipe->third.QuotaEnable = OFF;	
	
/* Third Step Gravity: Enable */	
if ( recipe->command.ThirdGravArm )
	recipe->third.GravityEnable = ON;
/* Third Step Gravity: Disable */	
if ( recipe->command.ThirdGravUnarm )
	recipe->third.GravityEnable = OFF;
	
/* Third Step Color: Enable */	
if ( recipe->command.ThirdColorArm )
	recipe->third.ColorEnable = ON;
/* Third Step Color: Disable */	
if ( recipe->command.ThirdColorUnarm )
	recipe->third.ColorEnable = OFF;				

/* Third Step Ticket: Enable */	
if ( recipe->command.ThirdTickArm )
	recipe->third.TicketEnable = ON;
/* Third Step Ticket: Disable */
if ( recipe->command.ThirdTickUnarm )
	recipe->third.TicketEnable = OFF;	
	
/**** FOURTH STEP - V5.03****/
	
/* Fourth Step Quota: Enable */	
if ( recipe->command.FourthQuotaArm )
	recipe->fourth.QuotaEnable = ON;
/* Fourth Step Quota: Disable */	
if ( recipe->command.FourthQuotaUnarm || recipe->Trigger )
	recipe->fourth.QuotaEnable = OFF;	
	
/* Fourth Step Gravity: Enable */	
if ( recipe->command.FourthGravArm )
	recipe->fourth.GravityEnable = ON;
/* Fourth Step Gravity: Disable */	
if ( recipe->command.FourthGravUnarm || recipe->Trigger )
	recipe->fourth.GravityEnable = OFF;
	
/* Fourth Step Color: Enable */	
if ( recipe->command.FourthColorArm )
	recipe->fourth.ColorEnable = ON;
/* Fourth Step Color: Disable */	
if ( recipe->command.FourthColorUnarm || recipe->Trigger )
	recipe->fourth.ColorEnable = OFF;				

/* Fourth Step Ticket: Enable */	
if ( recipe->command.FourthTickArm )
	recipe->fourth.TicketEnable = ON;
/* Fourth Step Ticket: Disable */
if ( recipe->command.FourthTickUnarm )
	recipe->fourth.TicketEnable = OFF;	

/*********************************/
/* Trigger Logic: Recipe Enabled */
/*********************************/	

if ( recipe->Enable ) {
	
	/* Trigger On Quota */
	if ( recipe->next.QuotaEnable && recipe->TriggerOnQuota )
		recipe->TriggerTimer.IN = ON;
	/* Trigger on Gravity */	
	else if ( recipe->next.GravityEnable && recipe->TriggerOnGravity )
		recipe->TriggerTimer.IN = ON;
	/* Trigger on Color */	
	else if ( recipe->next.ColorEnable && recipe->TriggerOnColor )
		recipe->TriggerTimer.IN = OFF;
	/* Remote Trigger */
	else if ( recipe->command.RemoteTrigger ) 
		recipe->TriggerTimer.IN = ON; 
	/* OIP Trigger  MMC v5_3*/
	else if ( recipe->OIP_Trigger && !security->Level0  ) {
		recipe->TriggerTimer.IN = ON;
		recipe->OIP_Trigger = OFF;
		}
	/* All Else */	
	else
			if ( !meter->eob_cmd && !recipe->BatchChangeArmed )
				recipe->TriggerTimer.IN = OFF;
	}

/*
Trigger Logic: Recipe Disabled */		
else
	recipe->TriggerTimer.IN = OFF; 		
		
/* Get Timer Value Before Execution */		
recipe->TriggerAlarm = recipe->TriggerTimer.Q;
/* Delay Between Trigger Timer */
TOF_10ms( (TOF_10mstyp *) &recipe->TriggerTimer );
	
/* Generate One Shot when Timer Starts */
if ( !recipe->TriggerAlarm && recipe->TriggerTimer.Q ) {
	
	/* Batch Change Armed: Used in Ticket Switch */
	recipe->BatchChangeArmed	= ON;	
	/* Set Trigger: One Time */
	recipe->Trigger 			= ON;
	}
else
	/* Reset Trigger: All other times */
	recipe->Trigger = OFF;	

/* V3.06: Reset OIP_Trigger */
if (( recipe->OIP_Trigger ) || ( !recipe->Enable )) 
	recipe->OIP_Trigger = OFF;
		
/********************************************/
/* Load Trigger Enables to consecutive bits */
/********************************************/		
bith[0]  = recipe->Enable;
bith[1]  = recipe->next.QuotaEnable;
bith[2]  = recipe->next.GravityEnable;
bith[3]  = recipe->next.ColorEnable;
bith[4]  = recipe->next.TicketEnable;
bith[5]  = recipe->second.QuotaEnable;
bith[6]  = recipe->second.GravityEnable;
bith[7]  = recipe->second.ColorEnable;
bith[8]  = recipe->second.TicketEnable;
bith[9]  = OFF;
bith[10] = OFF;
bith[11] = OFF;
bith[12] = OFF;
bith[13] = OFF;
bith[14] = OFF;
bith[15] = OFF;
/* Pack to UINT */
recipe->TriggerPacked = BitToWord( (UDINT)bith );

bith[0]  = recipe->third.QuotaEnable;
bith[1]  = recipe->third.GravityEnable;
bith[2]  = recipe->third.ColorEnable;
bith[3]  = recipe->third.TicketEnable;
bith[4]  = recipe->fourth.QuotaEnable;
bith[5]  = recipe->fourth.GravityEnable;
bith[6]  = recipe->fourth.ColorEnable;
bith[7]  = recipe->fourth.TicketEnable;
bith[8]  = OFF;
bith[9]  = OFF;
bith[10] = OFF;
bith[11] = OFF;
bith[12] = OFF;
bith[13] = OFF;
bith[14] = OFF;
bith[15] = OFF;
/* Pack to UINT */
recipe->TriggerPacked2 = BitToWord( (UDINT)bith );

}

UINT GetShipperNumber( UINT step )
{
BOOL bith[16];
USINT i;

/* Get Bit Representation */
WordToBit( step, bith );

/* Find 1st High Bit */
for (i=0;i<16;i++)
	if ( bith[i] )
		return (i + 1);

/* Return Failed Status */
return 0;
}

/* Serv function for Rotork valve */
UINT iq_serv( POLL_obj *Poll )
{
IQVALVE_obj *ActiveValve, *CommandValve, *MovingValve;
STATEMACHINE *sm;

	/* Subsequent References */
	ActiveValve 	= (IQVALVE_obj *) 	Poll->ActiveValve_adr;
	CommandValve 	= (IQVALVE_obj *) 	Poll->CommandValve_adr;
	MovingValve 	= (IQVALVE_obj *) 	Poll->MovingValve_adr;
	sm				= (STATEMACHINE *) 	Poll->StateMachine_adr;
	
	/* Update Moving Valve Table */
	UpdateIQMovingTable( Poll );
	
	/***********************************************/
	/***********************************************/
	/****  STATE MACHINE IQ_POLL: STARTS HERE   ****/
	/***********************************************/
	/***********************************************/

	SM_BEGIN(*sm)
	
	/*****************************/
	/* Initialize: State INIT: 0 */
	/*****************************/
	SM_STATE(IQ_INITIALIZE,"Initialize") {
	/* Init */
	
	/* Start with Channel 1 */
	Poll->Active_Ch 	= CH1;
	Poll->Old_Ch    	= CH1;
	Poll->MovingScan 	= 0;
	Poll->Enable_Ch1	= ON;

	/* End Init */
	}
	/* Cyclic */
	
	/* If either Port is opened properly */
	if ( * (BOOL *) Poll->PortOK[CH1] )    
	   SM_NEXT(IQ_GETVALVE); 
	/**** END STATE: IQ_INITIALIZE ****/
	
	/************************************/
	/* Increment Valve: IQ_GETVALVE: 1  */
	/************************************/
	SM_STATE(IQ_GETVALVE,"Get Valve") {
	/* Init */

	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* Get Active Valve */
	ActiveValve = GetIQActiveValve( Poll );
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
	  	/* Locate Moving Valve */
	 	MovingValve = GetIQMovingValve( Poll, CH1 );
	   
	   	/* Valid Moving Valve is Located */
	   	if ( Poll->MovingScan != -1 ) {
	   
	 		/* Interleave Moving Polling */
	 	   	if ( Poll->MovingScanToggle )
	 	   	   	SM_NEXT(IQ_POLL_MOVE_02);
	 	   	/* Interleave Normal Polling */   	
	  	   	else 
	  	   	   	SM_NEXT(IQ_POLL_02);
	  	   	}
		/* No Valid Moving Valve is Located */	      
	 	else	   
	 		/* Poll Ch1 */
		 	SM_NEXT(IQ_POLL_02);
		}
	
	/**** END STATE: IQ_GETVALVE ****/
	
	/***********************************/
	/* Poll Channel 1: IQ_POLL_02: 10  */
	/***********************************/
	SM_STATE(IQ_POLL_02,"Poll Code 02") {
	/* Init */
	
	/* Valve Comm OK: Poll Valve and Check Results */
	if ( ActiveValve->Comm_Stat[CH1] == OK ) {
	     /* Poll Valve */
	     SendIQPollMessage( Poll, ActiveValve, CH1, 02);
	     /* Check Results */
	     SM_NEXT(IQ_RESULT_02);
	     }
	     
	/* Valve Retry has occurred: Poll Valve and Check Results */               
	else if ( ActiveValve->Retry[CH1] ){
	     /* Poll Valve */
	     SendIQPollMessage( Poll, ActiveValve, CH1, 02 );
	     /* Check Results */
	     SM_NEXT(IQ_RESULT_02);
	     }  
	       
	/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
	else {
		/* Update Failure Points */
	   	FailedIQResponse( Poll, ActiveValve, CH1 );
		/* Continue Polling */
	    SM_NEXT(IQ_GETVALVE);   
	    }  
	/* End Init */
	}
	/* Cyclic */
	
	/**** END STATE: IQ_POLL_02 ****/
	
	/***************************************/
	/* Result Code 02:   IQ_RESULT_02: 11  */
	/***************************************/
	SM_STATE(IQ_RESULT_02,"Result 02 Code") {
	/* Init */
	
	/* Search For Next Command */
	SearchIQNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start( 0, 500 );
	/* End Init */
	}
	
	/* Channel 1 Fail: No Response */
	if ( ActiveValve->MessageFail[CH1] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedIQResponse( Poll, ActiveValve, CH1 );
	   	
	/* Channel 1 OK: Good Response */
	if ( ActiveValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, ActiveValve, CH1 );   		
	
	/* Channel 1 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && !Poll->CommandReady ) {
		/* No Percent Poll: Flow Quip Valve Type */	
	   	if ( Poll->Turbo | (ActiveValve->Type == 1) )
	   		SM_NEXT(IQ_GETVALVE);
	   	/* Poll Percent */		
		else 
	   		SM_NEXT(IQ_POLL_04);
	   		    
	   	}	                          
	
	/* Channel 1 Message Complete and Command Pending: Issue Command on Channel 1 */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(IQ_CMD);  
	/**** END STATE: IQ_RESULT_02 ****/
	
	/*********************************/
	/* Poll Code 04: IQ_POLL_04: 12  */
	/*********************************/
	SM_STATE(IQ_POLL_04,"Poll Code 04") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( ActiveValve->Comm_Stat[CH1] == OK ) {
	     	/* Poll Valve */
	     	SendIQPollMessage( Poll, ActiveValve, CH1, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_RESULT_04);
	     	}
	     
		/* Valve Retry has occurred: Poll Valve and Check Results */               
		else if ( ActiveValve->Retry[CH1] ){
	     	/* Poll Valve */
	     	SendIQPollMessage( Poll, ActiveValve, CH1, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_RESULT_04);
	     	}  
	       
		/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQResponse( Poll, ActiveValve, CH1 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}
	    }	  
	
	/**** END STATE: IQ_POLL_04 ****/
	
	/***************************************/
	/* Result Channel 1: IQ_RESULT_04: 13  */
	/***************************************/
	SM_STATE(IQ_RESULT_04,"Result Code 04") {
	/* Init */
	
	/* Search For Next Command */
	SearchIQNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	
	/* Channel 1 Fail: No Response */
	if ( ActiveValve->MessageFail[CH1] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedIQResponse( Poll, ActiveValve, CH1 );
	   	
	/* Channel 1 OK: Good Response */
	if ( ActiveValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, ActiveValve, CH1 );   		
	
	/* Channel 1 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && !Poll->CommandReady )
	   	/* Continue Polling */		
	   	SM_NEXT(IQ_GETVALVE);                              
	
	/* Channel 1 Message Complete and Command Pending: Issue Command on Channel 1 */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(IQ_CMD);  
	/**** END STATE: IQ_RESULT_02 ****/
	
	/*************************************/
	/* Command Channel 1: IQ_CMD: 14     */
	/*************************************/
	SM_STATE(IQ_CMD,"Command") {
	/* Init */
	
	/* Get Command Valve */
	CommandValve = GetIQCommandValve( Poll );
	  
	/* Valve Comm on Channel 1 OK: Send Command and Check Results */
	if ( CommandValve->Comm_Stat[CH1] == OK ) {
		/* Send Command */
		SendIQCommandMessage( Poll, CommandValve, CH1 );
		CommandValve->NoReply[CH2] = 99;
		/* Check Results */
		SM_NEXT(IQ_CMD_RESULT);    
		}
		
	/* Valve Comm Fail on Both Channels: Fail Channel 1 and Continue Polling */
	else {
		/* Command Failed on Channel 1 */
	   	IQCommandFail( Poll, CommandValve, CH1 );
	   	/* Continue Polling */
	   	SM_NEXT(IQ_GETVALVE);
	   	}
	/* End Init */
	}
	/* Cyclic */
	    
	/**** END STATE: IQ_CMD ****/
	
	/****************************************************/
	/* Command Results Channel 1: IQ_CMD_RESULT: 15 	*/
	/****************************************************/
	SM_STATE(IQ_CMD_RESULT,"Cmd Result") {
	/* Init */
	
	/* TimeOut: 1.5 Seconds */
	SMT10Start(0,150 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Command Executed: Update Good Command Statistics and Continue Polling */
	if ( CommandValve->MessageOK[CH1] ) { 
	    /* Update Good Command Statistics */ 
	    IQCommandExec( Poll, CommandValve, CH1 );
	    /* Continue Polling */
	    SM_NEXT( IQ_GETVALVE );
	    }
	                           
	/* Command Failed: Update Failure Command Statistics and Continue Polling */                                                
	if ( (CommandValve->MessageFail[CH1]) || SMT10Done(0) ) {
		/* Update Failure Command Statistics */
		IQCommandFail( Poll, CommandValve, CH1 );
	    /* Continue Polling */
	    SM_NEXT( IQ_GETVALVE );
	    }
	    
	/***************************************************/
	/* Poll Moving Valve Code 02: IQ_POLL_MOVE_02:  16 */
	/***************************************************/
	SM_STATE(IQ_POLL_MOVE_02,"Poll Moving Valve 02") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( MovingValve->Comm_Stat[CH1] == OK ){
	     	/* Poll Function */
	     	SendIQPollMessage( Poll, MovingValve, CH1, 02 );
	     	/* Check Results */
	     	SM_NEXT(IQ_MOVE_RESULT_02);
	     	}
	     
		/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQResponse( Poll, MovingValve, CH1 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}       
		}
		    
	/**** END STATE: IQ_POLL_MOVE_02 ****/
	
	/*************************************************/
	/* Result Moving Code 02: IQ_MOVE_RESULT_02: 17  */
	/*************************************************/
	SM_STATE(IQ_MOVE_RESULT_02,"Result Move 02") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 1 Fail: No Response */
	if ( (MovingValve->MessageFail[CH1]) || SMT10Done(0) ) 
	   /* Update Failure Points */
	   FailedIQResponse( Poll, MovingValve, CH1 );
	   
	/* Channel 1 OK: Good Response */
	if ( MovingValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, MovingValve, CH1 );      
	  
	/* Channel 1 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH1] || MovingValve->MessageFail[CH1] || SMT10Done(0) ) {
		/* Turbo or FlowQuip */
		if ( Poll->Turbo | (MovingValve->Type == 1) ) 
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
		else
			/* Continue Polling */
		   	SM_NEXT(IQ_POLL_MOVE_04);   
	}	   	                           
	/**** END STATE: IQ_MOVE_RESULT_02 ****/
	
	/***************************************************/
	/* Poll Moving Valve Code 04: IQ_POLL_MOVE_04:  18 */
	/***************************************************/
	SM_STATE(IQ_POLL_MOVE_04,"Poll Moving Valve 04") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( MovingValve->Comm_Stat[CH1] == OK ){
	     	/* Poll Function */
	     	SendIQPollMessage( Poll, MovingValve, CH1, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_MOVE_RESULT_04);
	     	}
	     
		/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQResponse( Poll, MovingValve, CH1 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}
	    }	       
	
	/**** END STATE: IQ_POLL_MOVE_04 ****/
	
	/*************************************************/
	/* Result Moving Code 04: IQ_MOVE_RESULT_04: 19  */
	/*************************************************/
	SM_STATE(IQ_MOVE_RESULT_04,"Result Move 04") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 1 Fail: No Response */
	if ( (MovingValve->MessageFail[CH1]) || SMT10Done(0) ) 
	   /* Update Failure Points */
	   FailedIQResponse( Poll, MovingValve, CH1 );
	   
	/* Channel 1 OK: Good Response */
	if ( MovingValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, MovingValve, CH1 );      
	  
	/* Channel 1 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH1] || MovingValve->MessageFail[CH1] || SMT10Done(0) )
		/* Continue Polling */
	   	SM_NEXT(IQ_GETVALVE);                              
	/**** END STATE: IQ_MOVE_RESULT_04 ****/
	      
	/***********************************************/
	/****  STATE MACHINE IQ_POLL: ENDS HERE     ****/
	/***********************************************/
	SM_END
	
	/* Valve Comm Failure */
	AnyIQValveCommFail( Poll );
	
		
	/* Return Value */
	return sm->state;
}

/* iq2_serv function for Rotork valve: Start */
UINT iq2_serv( POLL_obj *Poll )
{
IQVALVE_obj *ActiveValve, *CommandValve, *MovingValve;
STATEMACHINE *sm;

	/* Subsequent References */
	ActiveValve 	= (IQVALVE_obj *) 	Poll->ActiveValve_adr;
	CommandValve 	= (IQVALVE_obj *) 	Poll->CommandValve_adr;
	MovingValve 	= (IQVALVE_obj *) 	Poll->MovingValve_adr;
	sm				= (STATEMACHINE *) 	Poll->StateMachine_adr;
	
	/* Update Moving Valve Table */
	UpdateIQMovingTable( Poll );
	
	/***********************************************/
	/***********************************************/
	/****  STATE MACHINE IQ_POLL: STARTS HERE   ****/
	/***********************************************/
	/***********************************************/

	SM_BEGIN(*sm)
	
	/*****************************/
	/* Initialize: State INIT: 0 */
	/*****************************/
	SM_STATE(IQ_INITIALIZE,"Initialize") {
	/* Init */
	
	/* Start with Channel 1 :V3.00 */
	Poll->Active_Ch 	= CH1;
	Poll->Old_Ch    	= CH1;
	Poll->MovingScan 	= 0;
	Poll->Enable_Ch2 	= OFF;
	Poll->Enable_Ch1	= ON;

	/* End Init */
	}
	/* Cyclic */
	
	/* If either Port is opened properly :V3.00 */
	if ( * (BOOL *) Poll->PortOK[CH1] || * (BOOL *) Poll->PortOK[CH2] )    
	   SM_NEXT(IQ_GETVALVE); 
   
	/**** END STATE: IQ_INITIALIZE ****/
	
	/************************************/
	/* Increment Valve: IQ_GETVALVE: 1  */
	/************************************/
	SM_STATE(IQ_GETVALVE,"Get Valve") {
	/* Init */

	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* Get Active Valve */
	ActiveValve = GetIQ2ActiveValve( Poll );
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Go to Poll Channel #1 */
		if ( (Poll->Active_Ch == CH1) && (Poll->Old_Ch == CH1) ) {

	  		/* Locate Moving Valve */
		 	MovingValve = GetIQMovingValve( Poll, CH1 );
		   
		   	/* Valid Moving Valve is Located */
		   	if ( Poll->MovingScan != -1 ) {
		   
		 		/* Interleave Moving Polling */
		 	   	if ( Poll->MovingScanToggle )
		 	   	   	SM_NEXT(IQ_POLL_MOVE_CH1_02);
		 	   	/* Interleave Normal Polling */   	
		  	   	else 
		  	   	   	SM_NEXT(IQ_POLL_CH1_02);
		  	   	}
			/* No Valid Moving Valve is Located */	      
		 	else	   
		 		/* Poll Ch1 */
			 	SM_NEXT(IQ_POLL_CH1_02);	
			}

		/* Switch Channels : V3.00 */
		if ( ( (Poll->Active_Ch == CH1) && (Poll->Old_Ch == CH2) ) ||
		     ( (Poll->Active_Ch == CH2) && (Poll->Old_Ch == CH1) ) )
	  	 	/* Change Channels */
	   		SM_NEXT(IQ_CHANGE_CH);
	   	
	   	/* Go to Poll Channel #2 */
		if  ( (Poll->Active_Ch == CH2) && (Poll->Old_Ch == CH2) ) {
	
	   		/* Locate Moving Valve : CH2 */
		 	MovingValve = GetIQMovingValve( Poll, CH2 );
		   
		   	/* Valid Moving Valve is Located */
		   	if ( Poll->MovingScan != -1 ) {
		   
		 		/* Interleave Moving Polling */
		 	   	if ( Poll->MovingScanToggle )
		 	   	   	SM_NEXT(IQ_POLL_MOVE_CH2_02);
		 	   	/* Interleave Normal Polling */   	
		  	   	else 
		  	   	   	SM_NEXT(IQ_POLL_CH2_02);
		  	   	}
			/* No Valid Moving Valve is Located */	      
		 	else	   
		 		/* Poll Ch2 */
			 	SM_NEXT(IQ_POLL_CH2_02);
			}	
	}	

	/**** END STATE: IQ_GETVALVE ****/
	
	/***********************************/
	/* Poll Channel 1: IQ_POLL_CH1_02: 10  */
	/***********************************/
	SM_STATE(IQ_POLL_CH1_02,"Poll Code 02") {
	/* Init */
	
	/* Valve Comm OK: Poll Valve and Check Results */
	if ( ActiveValve->Comm_Stat[CH1] == OK ) {
	     /* Poll Valve */
	     SendIQPollMessage( Poll, ActiveValve, CH1, 02);
	     /* Check Results */
	     SM_NEXT(IQ_RESULT_CH1_02);
	     }
	     
	/* Valve Retry has occurred: Poll Valve and Check Results */               
	else if ( ActiveValve->Retry[CH1] ){
	     /* Poll Valve */
	     SendIQPollMessage( Poll, ActiveValve, CH1, 02 );
	     /* Check Results */
	     SM_NEXT(IQ_RESULT_CH1_02);
	     }  
	       
	/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
	else {
		/* Update Failure Points */
	   	FailedIQ2Response( Poll, ActiveValve, CH1 );
		/* Continue Polling */
	    SM_NEXT(IQ_GETVALVE);   
	    }  
	/* End Init */
	}
	/* Cyclic */	

	/**** END STATE: IQ_POLL_CH1_02 ****/
	
	/***********************************/
	/* Poll Channel 2: IQ_POLL_CH2_02: 20  V3.00 */
	/***********************************/
	SM_STATE(IQ_POLL_CH2_02,"Poll Code 02") {
	/* Init */
	
	/* Valve Comm OK: Poll Valve and Check Results */
	if ( ActiveValve->Comm_Stat[CH2] == OK ) {
	     /* Poll Valve */
	     SendIQPollMessage( Poll, ActiveValve, CH2, 02);
	     /* Check Results */
	     SM_NEXT(IQ_RESULT_CH2_02);
	     }
	     
	/* Valve Retry has occurred: Poll Valve and Check Results */               
	else if ( ActiveValve->Retry[CH2] ){
	     /* Poll Valve */
	     SendIQPollMessage( Poll, ActiveValve, CH2, 02 );
	     /* Check Results */
	     SM_NEXT(IQ_RESULT_CH2_02);
	     }  
	       
	/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
	else {
		/* Update Failure Points */
	   	FailedIQ2Response( Poll, ActiveValve, CH2 );
		/* Continue Polling */
	    SM_NEXT(IQ_GETVALVE);   
	    }  
	/* End Init */
	}
	/* Cyclic */

	/**** END STATE: IQ_POLL_CH2_02 ****/
	
	/***************************************/
	/* Result Code 02:   IQ_RESULT_CH1_02: 11  */
	/***************************************/
	SM_STATE(IQ_RESULT_CH1_02,"Result 02 Code") {
	/* Init */
	
	/* Search For Next Command */
	SearchIQNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start( 0, 500 );
	/* End Init */
	}
	
	/* Channel 1 Fail: No Response */
	if ( ActiveValve->MessageFail[CH1] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedIQ2Response( Poll, ActiveValve, CH1 );
	   	
	/* Channel 1 OK: Good Response */
	if ( ActiveValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, ActiveValve, CH1 );   		
	
	/* Channel 1 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && !Poll->CommandReady ) {
		/* No Percent Poll: Flow Quip Valve Type */	
	   	if ( Poll->Turbo | (ActiveValve->Type == 1) )
	   		SM_NEXT(IQ_GETVALVE);
	   	/* Poll Percent */		
		else 
	   		SM_NEXT(IQ_POLL_CH1_04);
	   		    
	   	}	                          
	
	/* Channel 1 Message Complete and Command Pending: Issue Command on Channel 1 */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(IQ_CMD_CH1);  
	/**** END STATE: IQ_RESULT_CH1_02 ****/
	

	/***************************************/
	/* Result Code 02:   IQ_RESULT_CH2_02: 21  :V3.00 */
	/***************************************/
	SM_STATE(IQ_RESULT_CH2_02,"Result 02 Code") {
	/* Init */
	
	/* Search For Next Command */
	SearchIQNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start( 0, 500 );
	/* End Init */
	}
	
	/* Channel 2 Fail: No Response */
	if ( ActiveValve->MessageFail[CH2] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedIQ2Response( Poll, ActiveValve, CH2 );
	   	
	/* Channel 2 OK: Good Response */
	if ( ActiveValve->MessageOK[CH2] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, ActiveValve, CH2 );   		
	
	/* Channel 2 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH2] || ActiveValve->MessageFail[CH2] || SMT10Done(0) ) && !Poll->CommandReady ) {
		/* No Percent Poll: Flow Quip Valve Type */	
	   	if ( Poll->Turbo | (ActiveValve->Type == 1) )
	   		SM_NEXT(IQ_GETVALVE);
	   	/* Poll Percent */		
		else 
	   		SM_NEXT(IQ_POLL_CH2_04);
	   		    
	   	}	                          
	
	/* Channel 2 Message Complete and Command Pending: Issue Command on Channel 1 */
	if ( (ActiveValve->MessageOK[CH2] || ActiveValve->MessageFail[CH2] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(IQ_CMD_CH2);  
	/**** END STATE: IQ_RESULT_CH2_02 ****/


	
	/*********************************/
	/* Poll Code 04: IQ_POLL_CH1_04: 12  */
	/*********************************/
	SM_STATE(IQ_POLL_CH1_04,"Poll Code 04") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( ActiveValve->Comm_Stat[CH1] == OK ) {
	     	/* Poll Valve */
	     	SendIQPollMessage( Poll, ActiveValve, CH1, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_RESULT_CH1_04);
	     	}
	     
		/* Valve Retry has occurred: Poll Valve and Check Results */               
		else if ( ActiveValve->Retry[CH1] ){
	     	/* Poll Valve */
	     	SendIQPollMessage( Poll, ActiveValve, CH1, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_RESULT_CH1_04);
	     	}  
	       
		/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQ2Response( Poll, ActiveValve, CH1 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}
	    }	  
	
	/**** END STATE: IQ_POLL_CH1_04 ****/
	
	/*********************************/
	/* Poll Code 04: IQ_POLL_CH2_04: 22  :V3.00 */
	/*********************************/
	SM_STATE(IQ_POLL_CH2_04,"Poll Code 04") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( ActiveValve->Comm_Stat[CH2] == OK ) {
	     	/* Poll Valve */
	     	SendIQPollMessage( Poll, ActiveValve, CH2, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_RESULT_CH2_04);
	     	}
	     
		/* Valve Retry has occurred: Poll Valve and Check Results */               
		else if ( ActiveValve->Retry[CH2] ){
	     	/* Poll Valve */
	     	SendIQPollMessage( Poll, ActiveValve, CH2, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_RESULT_CH2_04);
	     	}  
	       
		/* Valve Comm is Bad and Retry has not occurred: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQ2Response( Poll, ActiveValve, CH2 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}
	    }	  
	
	/**** END STATE: IQ_POLL_CH2_04 ****/

	
	/***************************************/
	/* Result Channel 1: IQ_RESULT_CH1_04: 13  */
	/***************************************/
	SM_STATE(IQ_RESULT_CH1_04,"Result Code 04") {
	/* Init */
	
	/* Search For Next Command */
	SearchIQNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	
	/* Channel 1 Fail: No Response */
	if ( ActiveValve->MessageFail[CH1] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedIQ2Response( Poll, ActiveValve, CH1 );
	   	
	/* Channel 1 OK: Good Response */
	if ( ActiveValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, ActiveValve, CH1 );   		
	
	/* Channel 1 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && !Poll->CommandReady )
	   	/* Continue Polling */		
	   	SM_NEXT(IQ_GETVALVE);                              
	
	/* Channel 1 Message Complete and Command Pending: Issue Command on Channel 1 */
	if ( (ActiveValve->MessageOK[CH1] || ActiveValve->MessageFail[CH1] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(IQ_CMD_CH1);  
	/**** END STATE: IQ_RESULT_CH1_02 ****/
	
	/***************************************/
	/* Result Channel 2: IQ_RESULT_CH2_04: 23  :V3.00 */
	/***************************************/
	SM_STATE(IQ_RESULT_CH2_04,"Result Code 04") {
	/* Init */
	
	/* Search For Next Command */
	SearchIQNextCommand( Poll );
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}

	/* Channel 2 Fail: No Response */
	if ( ActiveValve->MessageFail[CH2] || SMT10Done(0) ) 
	   	/* Update Failure Points */
	   	FailedIQ2Response( Poll, ActiveValve, CH2 );
	   	
	/* Channel 2 OK: Good Response */
	if ( ActiveValve->MessageOK[CH2] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, ActiveValve, CH2 );   		
	
	/* Channel 2 Message Complete and No Command Pending: Continue Polling */
	if ( (ActiveValve->MessageOK[CH2] || ActiveValve->MessageFail[CH2] || SMT10Done(0) ) && !Poll->CommandReady )
	   	/* Continue Polling */		
	   	SM_NEXT(IQ_GETVALVE);                              
	
	/* Channel 2 Message Complete and Command Pending: Issue Command on Channel 1 */
	if ( (ActiveValve->MessageOK[CH2] || ActiveValve->MessageFail[CH2] || SMT10Done(0) ) && Poll->CommandReady )
		/* Issue Command */
		SM_NEXT(IQ_CMD_CH2);  
	/**** END STATE: IQ_RESULT_CH2_02 ****/

	
	/*************************************/
	/* Command Channel 1: IQ_CMD_CH1: 14     */
	/*************************************/
	SM_STATE(IQ_CMD_CH1,"Ch1 Command") {
	/* Init */
	
	/* Get Command Valve */
	CommandValve = GetIQCommandValve( Poll );
	  
	/* Valve Comm on Channel 1 OK: Send Command and Check Results */
	if ( CommandValve->Comm_Stat[CH1] == OK ) {
		/* Send Command */
		SendIQCommandMessage( Poll, CommandValve, CH1 );
	/*	CommandValve->NoReply[CH2] = 99; */
		/* Check Results */
		SM_NEXT(IQ_CMD_RESULT_CH1);    
		}
		
	/* Valve Comm on Channel 1 Fail and Channel 2 OK: Fail Channel 1 and Try Channel 2 */
	else if ( CommandValve->Comm_Stat[CH2] == OK ) {
		/* Command Failed on Channel 1 */
	   	IQ2CommandFail( Poll, CommandValve, CH1 );
	   	/* Try Channel 2 */
	   	SM_NEXT(IQ_CMD_CH2);
	   	}
	
	/* Valve Comm Fail on Both Channels: Fail Channel 1 and Continue Polling */
	else {
		/* Command Failed on Channel 1 */
	   	IQ2CommandFail( Poll, CommandValve, CH1 );
	   	/* Continue Polling */
	   	SM_NEXT(IQ_GETVALVE);
	   	}
	/* End Init */
	}
	/* Cyclic */
	   
	/**** END STATE: IQ_CMD_CH1 ****/
	
	/*************************************/
	/* Command Channel 2: IQ_CMD_CH2: 24  :V3.00  */
	/*************************************/
	SM_STATE(IQ_CMD_CH2,"Ch2 Command") {
	/* Init */
	
	/* Get Command Valve */
	CommandValve = GetIQCommandValve( Poll );
	  
	/* Valve Comm on Channel 2 OK: Send Command and Check Results */
	if ( CommandValve->Comm_Stat[CH2] == OK ) {
		/* Send Command */
		SendIQCommandMessage( Poll, CommandValve, CH2 );
		/* Check Results */
		SM_NEXT(IQ_CMD_RESULT_CH2);    
		}
		
	/* Valve Comm on Channel 2 Fail and Channel 1 OK: Fail Channel 2 and Try Channel 1 */
	else if ( CommandValve->Comm_Stat[CH1] == OK ) {
		/* Command Failed on Channel 2 */
	   	IQ2CommandFail( Poll, CommandValve, CH2 );
	   	/* Try Channel 1 */
	   	SM_NEXT(IQ_CMD_CH1);
	   	}
	
	/* Valve Comm Fail on Both Channels: Fail Channel 2 and Continue Polling */
	else {
		/* Command Failed on Channel 2 */
	   	IQ2CommandFail( Poll, CommandValve, CH2 );
	   	/* Continue Polling */
	   	SM_NEXT(IQ_GETVALVE);
	   	}
	/* End Init */
	}
	/* Cyclic */
	    
	/**** END STATE: IQ_CMD ****/

	
	/****************************************************/
	/* Command Results Channel 1: IQ_CMD_RESULT_CH1: 15 	*/
	/****************************************************/
	SM_STATE(IQ_CMD_RESULT_CH1,"Cmd Result") {
	/* Init */
	
	/* TimeOut: 1.5 Seconds */
	SMT10Start(0,150 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Command Executed: Update Good Command Statistics and Continue Polling */
	if ( CommandValve->MessageOK[CH1] ) { 
	    /* Update Good Command Statistics */ 
	    IQCommandExec( Poll, CommandValve, CH1 );
	    /* Continue Polling */
	    SM_NEXT( IQ_GETVALVE );
	    }
	                           
	/* Command Failed: Update Failure Command Statistics and Continue Polling */                                                
	if ( (CommandValve->MessageFail[CH1]) || SMT10Done(0) ) {
		/* Update Failure Command Statistics */
		IQ2CommandFail( Poll, CommandValve, CH1 );
	    /* Continue Polling */
	    SM_NEXT( IQ_GETVALVE );
	    }
	    
	/****************************************************/
	/* Command Results Channel 2: IQ_CMD_RESULT_CH2: 25 :V3.00	*/
	/****************************************************/
	SM_STATE(IQ_CMD_RESULT_CH2,"Cmd Result") {
	/* Init */
	
	/* TimeOut: 1.5 Seconds */
	SMT10Start(0,150 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Command Executed: Update Good Command Statistics and Continue Polling */
	if ( CommandValve->MessageOK[CH2] ) { 
	    /* Update Good Command Statistics */ 
	    IQCommandExec( Poll, CommandValve, CH2 );
	    /* Continue Polling */
	    SM_NEXT( IQ_GETVALVE );
	    }
	                           
	/* Command Failed: Update Failure Command Statistics and Continue Polling */                                                
	if ( (CommandValve->MessageFail[CH2]) || SMT10Done(0) ) {
		/* Update Failure Command Statistics */
		IQ2CommandFail( Poll, CommandValve, CH2 );
	    /* Continue Polling */
	    SM_NEXT( IQ_GETVALVE );
	    }
    
	    
	/***************************************************/
	/* Poll Moving Valve Code 02: IQ_POLL_MOVE_CH1_02:  16 */
	/***************************************************/
	SM_STATE(IQ_POLL_MOVE_CH1_02,"Poll Moving Valve 02") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( MovingValve->Comm_Stat[CH1] == OK ){
	     	/* Poll Function */
	     	SendIQPollMessage( Poll, MovingValve, CH1, 02 );
	     	/* Check Results */
	     	SM_NEXT(IQ_MOVE_RESULT_CH1_02);
	     	}
	     
		/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQ2Response( Poll, MovingValve, CH1 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}       
		}
		    
	/**** END STATE: IQ_POLL_MOVE_CH1_02 ****/
	
	/***************************************************/
	/* Poll Moving Valve Code 02: IQ_POLL_MOVE_CH2_02:  26 :V3.00 */
	/***************************************************/
	SM_STATE(IQ_POLL_MOVE_CH2_02,"Poll Moving Valve 02") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( MovingValve->Comm_Stat[CH2] == OK ){
	     	/* Poll Function */
	     	SendIQPollMessage( Poll, MovingValve, CH2, 02 );
	     	/* Check Results */
	     	SM_NEXT(IQ_MOVE_RESULT_CH2_02);
	     	}
	     
		/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQ2Response( Poll, MovingValve, CH2 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}       
		}
		    
	/**** END STATE: IQ_POLL_MOVE_CH2_02 ****/


	/*************************************************/
	/* Result Moving Code 02: IQ_MOVE_RESULT_CH1_02: 17  */
	/*************************************************/
	SM_STATE(IQ_MOVE_RESULT_CH1_02,"Result Move 02") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 1 Fail: No Response */
	if ( (MovingValve->MessageFail[CH1]) || SMT10Done(0) ) 
	   /* Update Failure Points */
	   FailedIQ2Response( Poll, MovingValve, CH1 );
	   
	/* Channel 1 OK: Good Response */
	if ( MovingValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, MovingValve, CH1 );      
	  
	/* Channel 1 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH1] || MovingValve->MessageFail[CH1] || SMT10Done(0) ) {
		/* Turbo or FlowQuip */
		if ( Poll->Turbo | (MovingValve->Type == 1) ) 
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
		else
			/* Continue Polling */
		   	SM_NEXT(IQ_POLL_MOVE_CH1_04);   
	}	   	                           
	/**** END STATE: IQ_MOVE_RESULT_CH1_02 ****/
	
	/*************************************************/
	/* Result Moving Code 02: IQ_MOVE_RESULT_CH2_02: 27 :V3.00 */
	/*************************************************/
	SM_STATE(IQ_MOVE_RESULT_CH2_02,"Result Move 02") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 2 Fail: No Response */
	if ( (MovingValve->MessageFail[CH2]) || SMT10Done(0) ) 
	   /* Update Failure Points */
	   FailedIQ2Response( Poll, MovingValve, CH2 );
	   
	/* Channel 2 OK: Good Response */
	if ( MovingValve->MessageOK[CH2] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, MovingValve, CH2 );      
	  
	/* Channel 2 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH2] || MovingValve->MessageFail[CH2] || SMT10Done(0) ) {
		/* Turbo or FlowQuip */
		if ( Poll->Turbo | (MovingValve->Type == 1) ) 
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
		else
			/* Continue Polling */
		   	SM_NEXT(IQ_POLL_MOVE_CH2_04);   
	}	   	                           
	/**** END STATE: IQ_MOVE_RESULT_CH2_02 ****/

	
	/***************************************************/
	/* Poll Moving Valve Code 04: IQ_POLL_MOVE_CH1_04:  18 */
	/***************************************************/
	SM_STATE(IQ_POLL_MOVE_CH1_04,"Poll Moving Valve 04") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( MovingValve->Comm_Stat[CH1] == OK ){
	     	/* Poll Function */
	     	SendIQPollMessage( Poll, MovingValve, CH1, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_MOVE_RESULT_CH1_04);
	     	}
	     
		/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQ2Response( Poll, MovingValve, CH1 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}
	    }	       
	
	/**** END STATE: IQ_POLL_MOVE_CH1_04 ****/
	
	/***************************************************/
	/* Poll Moving Valve Code 04: IQ_POLL_MOVE_CH2_04:  28 :V3.00 */
	/***************************************************/
	SM_STATE(IQ_POLL_MOVE_CH2_04,"Poll Moving Valve 04") {
	/* Init */
	
	/* Inter-Poll Delay */
	SMT10Start( 0, Poll->InterPollDelay );
	
	/* End Init */
	}
	/* Cyclic */
	
	/* Wait Until InterPoll Delay Has Expired */
	if ( SMT10Done(0) ) {
	
		/* Valve Comm OK: Poll Valve and Check Results */
		if ( MovingValve->Comm_Stat[CH2] == OK ){
	     	/* Poll Function */
	     	SendIQPollMessage( Poll, MovingValve, CH2, 04 );
	     	/* Check Results */
	     	SM_NEXT(IQ_MOVE_RESULT_CH2_04);
	     	}
	     
		/* Valve Comm is Bad: Update Failure Points and Continue Polling */ 
		else {
			/* Update Failure Points */
	   		FailedIQ2Response( Poll, MovingValve, CH2 );
			/* Continue Polling */
	    	SM_NEXT(IQ_GETVALVE);   
	    	}
	    }	       
	
	/**** END STATE: IQ_POLL_MOVE_CH2_04 ****/

	
	/*************************************************/
	/* Result Moving Code 04: IQ_MOVE_RESULT_CH1_04: 19  */
	/*************************************************/
	SM_STATE(IQ_MOVE_RESULT_CH1_04,"Result Move 04") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 1 Fail: No Response */
	if ( (MovingValve->MessageFail[CH1]) || SMT10Done(0) ) 
	   /* Update Failure Points */
	   FailedIQ2Response( Poll, MovingValve, CH1 );
	   
	/* Channel 1 OK: Good Response */
	if ( MovingValve->MessageOK[CH1] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, MovingValve, CH1 );      
	  
	/* Channel 1 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH1] || MovingValve->MessageFail[CH1] || SMT10Done(0) )
		/* Continue Polling */
	   	SM_NEXT(IQ_GETVALVE);                              
	/**** END STATE: IQ_MOVE_RESULT_CH1_04 ****/
	
	/*************************************************/
	/* Result Moving Code 04: IQ_MOVE_RESULT_CH2_04: 29 :V3.00 */
	/*************************************************/
	SM_STATE(IQ_MOVE_RESULT_CH2_04,"Result Move 04") {
	/* Init */
	
	/* TimeOut: 5.0 Seconds */
	SMT10Start(0, 500 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Channel 2 Fail: No Response */
	if ( (MovingValve->MessageFail[CH2]) || SMT10Done(0) ) 
	   /* Update Failure Points */
	   FailedIQ2Response( Poll, MovingValve, CH2 );
	   
	/* Channel 2 OK: Good Response */
	if ( MovingValve->MessageOK[CH2] ) 
	   	/* Update Good Response */
	   	GoodIQResponse( Poll, MovingValve, CH2 );      
	  
	/* Channel 1 Message Complete: Continue Polling */
	if ( MovingValve->MessageOK[CH2] || MovingValve->MessageFail[CH2] || SMT10Done(0) )
		/* Continue Polling */
	   	SM_NEXT(IQ_GETVALVE);                              
	/**** END STATE: IQ_MOVE_RESULT_CH2_04 ****/

	/*******************************************/
	/* Change Master Channels: IQ_CHANGE_CH: 2 :V3.00 */
	/*******************************************/
	SM_STATE(IQ_CHANGE_CH,"Change Channels") {
	/* Init */
	
	/* Wait 200 mSecs to change channels */
	SMT10Start(0, 20 );
	/* End Init */
	}
	/* Cyclic */
	
	/* Delay between Channels: Poll Channel 1 */
	if ( (SMT10Done(0)) && (Poll->Active_Ch == CH1) )
		/* Poll Channel 1 */ 
	   	SM_NEXT(IQ_POLL_CH1_02);
	   
	/* Delay between Channels: Poll Channel 2 */
	if ( (SMT10Done(0)) && (Poll->Active_Ch == CH2) ) 
	   	/* Poll Channel 2 */ 
	   	SM_NEXT(IQ_POLL_CH2_02);
	/**** END STATE: IQ_CHANGE_CH ****/

	      
	/***********************************************/
	/****  STATE MACHINE IQ_POLL: ENDS HERE     ****/
	/***********************************************/
	SM_END
	
	/* Valve Comm Failure */
	AnyIQ2ValveCommFail( Poll );
	
	/* Return Value */
	return sm->state;
}
/* iq2_serv function for Rotork valve: End */

/* This function determines how long to keep valves in table */
void UpdateIQMovingTable( POLL_obj *Poll )
{
USINT i;
IQVALVE_obj *CandidateValve;

/* Default Reference */
CandidateValve = (IQVALVE_obj *) Poll->Valve_adr;

/* Default Value */
Poll->MovingTableFull = OFF;

/* Count through All Valves */ 	
for (i=0;i<Poll->NumValves;i++,CandidateValve++) {
	
	/* Execute Timer for Valve */
	TON_10ms( (TON_10mstyp *) &CandidateValve->ScanTimer );
	/* When Complete: Remove from table */
	if ( CandidateValve->ScanTimer.Q )
		CandidateValve->ScanTimer.IN = OFF;
	/* At Least 1 Valve in Table */
    if ( CandidateValve->ScanTimer.IN )
    	Poll->MovingTableFull = ON;
    	
    }

/* No Valves in Table: Make sure Enable is OFF 	
if ( !Poll->MovingTableFull )
	Poll->MovingScanToggle  = OFF;*/

}


/*********************************/
/** Subroutine GetIQActiveValve **/
/*********************************/
IQVALVE_obj *GetIQActiveValve( POLL_obj *Poll ) 
{
IQVALVE_obj *Valve = (IQVALVE_obj *) Poll->ActiveValve_adr;
USINT i;

	/* Return if Moving Scan Interleave is Enabled */
	if ( Poll->MovingScanToggle ) 	
		/* Return Value */
		return Valve;
			
	/* Cycle Through Valves */
	for (i=0;i<Poll->NumValves;i++) {
		
		/* Increment Poll Number */
		Poll->Poll_Num++;
	  
		/* Check to see if at End */  
		if (Poll->Poll_Num >= Poll->NumValves)  
			/* Reset to Beginning */
	    	Poll->Poll_Num = 0;
	    	    	
	    /* Establish Active Valve Reference */
		Valve = (IQVALVE_obj *) Poll->Valve_adr + Poll->Poll_Num;
		
		/* Found Enabled Valve */
		if ( Valve->Enable )
			break;
		}		
	     
	       
	/* Store */
	Poll->ActiveValve_adr = (UDINT) Valve;
	
	/* Return Value */
	return Valve;
}


/*********************************/
/** Subroutine GetIQ2ActiveValve :V3.00 **/
/*********************************/
IQVALVE_obj *GetIQ2ActiveValve( POLL_obj *Poll ) 
{
IQVALVE_obj *Valve = (IQVALVE_obj *) Poll->ActiveValve_adr;
USINT i;
BOOL Change = OFF;

	/* Return if Moving Scan Interleave is Enabled */
	if ( Poll->MovingScanToggle ) 	
		/* Return Value */
		return Valve;

	/* Record Old Channel */
	Poll->Old_Ch = Poll->Active_Ch;
		
	/* Cycle Through Valves */
	for (i=0;i<Poll->NumValves;i++) {
		
		/* Increment Poll Number */
		Poll->Poll_Num++;
	  
		/* Check to see if at End */  
		if (Poll->Poll_Num >= Poll->NumValves)  {
			/* Reset to Beginning */
	    	Poll->Poll_Num = 0;
	    	/* Change Channel Signal */
    		Change = ON;
			}
	    	    	
	    /* Establish Active Valve Reference */
		Valve = (IQVALVE_obj *) Poll->Valve_adr + Poll->Poll_Num;
		
		/* Found Enabled Valve */
		if ( Valve->Enable )
			break;
		}	
			       
	/* Store */
	Poll->ActiveValve_adr = (UDINT) Valve;
	
	/* Change Channels */
	if ( Change ) {    
	    
	    /* Change Active Channel to CH2 */	/* V3.05 */
	    /* Change Active Channel to CH2 */	/* V3.05.1: Remove the change from V3.05 */
	    /* if (( Poll->Active_Ch == CH1 ) & ( Poll->PortOK[CH2] )){	*/
	    if ( Poll->Active_Ch == CH1 ) {	
	       Poll->Enable_Ch1 = OFF;
	       Poll->Enable_Ch2 = ON; 
	       Poll->Active_Ch 	= CH2;
	       } 
	       
	    /* Change Active Channel to CH1 */   
	    else{
	       Poll->Enable_Ch2 = OFF;
	       Poll->Enable_Ch1 = ON;
	       Poll->Active_Ch 	= CH1;
	       }
	    }

	/* Return Value */
	return Valve;
}
 
/*********************************/
/** Function: SendIQPollMessage **/
/*********************************/ 
void SendIQPollMessage( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel, USINT type )
{

	/* Active Channel Reset Fail/Ok bits */	
	Valve->MessageFail[Channel] 	= OFF;
	Valve->MessageOK[Channel]   	= OFF;
		
	/* Reset Valve Retry Statistics */
	Valve->Retry[Channel] 			= OFF;
	Valve->ReconnectCount[Channel]	= 0;
		
	/* Polls: Absolute Number per Valve per Channel */
	Valve->Polls[Channel]++;
	
	/* Polls: Absolute Number per Channel */
	Poll->Polls[Channel]++;
	
	/* Establish Command Enable */
	if (type == 2) 
		Valve->bPoll_02[Channel]	 	= ON;
	else if (type == 4) 
		Valve->bPoll_04[Channel]	 	= ON;
		
}

/************************************/
/** Function SearchIQNextCommand() **/
/************************************/
BOOL SearchIQNextCommand( POLL_obj *Poll )
{
IQVALVE_obj *Valve = (IQVALVE_obj *) Poll->Valve_adr;
VALVE_obj *GVO;
USINT i;

	/* If a command is not currently pending */
	if ( !Poll->CommandReady ) {
	
		/* Count through All Valves */ 	
		for (i=0;i<Poll->NumValves;i++,Valve++) {
		
			/* Only Process Command if IQ Valve is Enabled */
			if ( Valve->Enable ) {
		
				/* Generic Valve Reference */
				GVO	= (VALVE_obj *) Valve->Valve_adr;
				
				/* Mode: Open Attempt */
				if (GVO->open_cmd) {
					GVO->FailedToCloseAlarm	= OFF;
					GVO->FailedToOpenAlarm	= OFF;
					Valve->_open 			= ON;
				}	
				/* Mode: Close Attempt */ 
				else if (GVO->close_cmd) {
					GVO->FailedToCloseAlarm	= OFF;
					GVO->FailedToOpenAlarm	= OFF;
					Valve->_close 			= ON;		
				}			
				/* Stop Command: Reset Modes */ 
				else if (GVO->stop_cmd) {
					Valve->_close 			= OFF;
					Valve->_open 			= OFF;	
					GVO->FailedToCloseAlarm	= OFF;
					GVO->FailedToOpenAlarm	= OFF;	
				}
				
				/* Valve Details: */
				switch ( Valve->Type ) {
				
					/* IQ */
					case 0:
			
						/* Generic Valve Open Command */
						if ( GVO->open_cmd )  
							Valve->Command 		= IQ_OPEN_CMD;
							
						/* Generic Valve Close Command */
						if ( GVO->close_cmd ) 
							Valve->Command 		= IQ_CLOSE_CMD;
							
						/* Generic Valve Stop Command */
						if ( GVO->stop_cmd ) 
							Valve->Command 		= IQ_STOP_CMD;
					break;
					
					/* FlowQuip */
					case 1:
						/* Generic Valve Open Command */
						if ( GVO->open_cmd ) { 
							Valve->Command_FQ 	= ON;
							Poll->CommandReady 	= ON;
						}
							
						/* Generic Valve Close Command */
						if ( GVO->close_cmd ) {
							Valve->Command_FQ	= OFF;
							Poll->CommandReady 	= ON;
						}
					
					/* V2.20: IQ1_TYPE */
					case 2:
			
						/* Generic Valve Open Command */
						if ( GVO->open_cmd )  
							Valve->Command 		= IQ_OPEN_CMD;
							
						/* Generic Valve Close Command */
						if ( GVO->close_cmd ) 
							Valve->Command 		= IQ_CLOSE_CMD;
							
						/* Generic Valve Stop Command */
						if ( GVO->stop_cmd ) 
							Valve->Command 		= IQ_STOP_CMD;
					break;	
							
					break;	
				}	
				
				/* Reset Commands: OIP and Remote */
				if ( GVO->oip_hold ) 
					GVO->open_remote = GVO->open_oip = GVO->close_remote = GVO->close_oip = GVO->stop_remote = GVO->stop_oip = OFF;
		
				/* Store For One Scan */	
				GVO->oip_hold	= GVO->open_remote | GVO->open_oip | GVO->close_remote | GVO->close_oip | GVO->stop_remote | GVO->stop_oip;
					
				/* Reset Commands: OIP and Remote */
				GVO->open_cmd 	= OFF;
				GVO->close_cmd 	= OFF;
				GVO->stop_cmd 	= OFF;
							
				/* Valve is Requesting a Command Action: IQ Valve */
				/* V2.20: IQ1_TYPE */
				if ( (Valve->Command < 10) && ((Valve->Type == 0)|(Valve->Type == 2)) ) {				
					/* Set Command Pending Signal */
	   	    		Poll->CommandReady 	= ON;
	   				/* Set Valve number */
	   	    		Poll->CommandValve 	= Valve->ID;
			       	/* One Command at a Time */
					return(1);
				}
				/* Valve is Requesting a Command Action: FQ Valve */
				else if ( (Poll->CommandReady) && (Valve->Type == 1) ) {
					/* Set Valve number */
	   	    		Poll->CommandValve 	= Valve->ID;
			       	/* One Command at a Time */
					return(1);
				}	
			}
		} 
	}
	
	/* No New Command */	
	return(0);   

}

/********************************/
/** Function: FailedIQResponse **/
/********************************/
void FailedIQResponse( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel )
{
VALVE_obj *GVO;

/* Assign Pointer */
GVO	= (VALVE_obj *) Valve->Valve_adr;
	
/* Reset Comm Enable */
Valve->bPoll_02[Channel] = OFF;	
Valve->bPoll_04[Channel] = OFF;	

/* Valve Channel Comm OK: Handle Remove Count, Establish Comm Status, Increment Diagnostics */
if ( Valve->Comm_Stat[Channel] == OK ) {

	/* Increment Remove Counter */
	Valve->RemoveCount[Channel]++;

	/* Valve has Not replied in several attempts */
	if ( Valve->RemoveCount[Channel] > Poll->NumRetry ) {

    	/* Set Channel Communication Status to Failure */
    	Valve->Comm_Stat[Channel] = FAIL;
    	/* Reset Remove Counter Scratch Pad Variable */
    	Valve->RemoveCount[Channel]   = 0;
    	/* Both Channels are in Failure */
        Valve->Status = FAIL;
        }
        
    /* No Reply: Absolute Number per Valve per Channel */        
	Valve->NoReply[Channel]++;
	/* No Reply: Absolute Number per Channel */
	Poll->NoReply[Channel]++;   
        	
    }
    
/* Valve Channel Comm Failure: Increment and Check Reconnect Status */    
else {

	/* Increment Valve Reconnect Count */
	Valve->ReconnectCount[Channel]++;
	
	/* Valve Exceeded Reconnect Cycles */
	if ( Valve->ReconnectCount[Channel] > Poll->Reconnect ) {
		/* Reset Valve Reconnect Count */
		Valve->ReconnectCount[Channel] 	= 0;
		Valve->Retry[Channel] 			= ON;
		}
	}   
	
/* Update GVO Comm Failure Status */
if ( Valve->Status == FAIL ) {
	strcpy( GVO->message, "ComFail" );
	GVO->commfail	= ON;
	}	
}

 
/********************************/
/** Function: FailedIQ2Response :V3.00 **/
/********************************/
void FailedIQ2Response( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel )
{
USINT ChannelX = CH1;
VALVE_obj *GVO;

/* Assign Pointer */
GVO	= (VALVE_obj *) Valve->Valve_adr;

/* ChannelX is other channel */
if ( Channel == CH1 )
	ChannelX = CH2;
	
/* Reset Comm Enable */
Valve->bPoll_02[Channel] = OFF;	
Valve->bPoll_04[Channel] = OFF;	

/* Valve Channel Comm OK: Handle Remove Count, Establish Comm Status, Increment Diagnostics */
if ( Valve->Comm_Stat[Channel] == OK ) {

	/* Increment Remove Counter */
	Valve->RemoveCount[Channel]++;

	/* Valve has Not replied in several attempts */
	if ( Valve->RemoveCount[Channel] > Poll->NumRetry ) {

    	/* Set Channel Communication Status to Failure */
    	Valve->Comm_Stat[Channel] = FAIL;
    	/* Reset Remove Counter Scratch Pad Variable */
    	Valve->RemoveCount[Channel]   = 0;
    	/* Check Other Channel for Failure */
    	if ( Valve->Comm_Stat[ChannelX] == FAIL )
        	/* Both Channels are in Failure */
        	Valve->Status = FAIL;
        }
        
    /* No Reply: Absolute Number per Valve per Channel */        
	Valve->NoReply[Channel]++;
	/* No Reply: Absolute Number per Channel */
	Poll->NoReply[Channel]++;   
        	
    }
    
/* Valve Channel Comm Failure: Increment and Check Reconnect Status */    
else {

	/* Increment Valve Reconnect Count */
	Valve->ReconnectCount[Channel]++;
	
	/* Valve Exceeded Reconnect Cycles */
	if ( Valve->ReconnectCount[Channel] > Poll->Reconnect ) {
		/* Reset Valve Reconnect Count */
		Valve->ReconnectCount[Channel] 	= 0;
		Valve->Retry[Channel] 			= ON;
		}
	}   
	
/* Update GVO Comm Failure Status */
if ( Valve->Status == FAIL ) {
	strcpy( GVO->message, "ComFail" );
	GVO->commfail	= ON;
	}	
}
 
/********************************/
/** Function: GoodIQResponse   **/
/********************************/
void GoodIQResponse( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel )
{
VALVE_obj *GVO = (VALVE_obj *) Valve->Valve_adr;

	/* Reset Comm Enable */
	Valve->bPoll_02[Channel] 		= OFF;
	Valve->bPoll_04[Channel] 		= OFF;
	
	/* RemoveCount: Reset Scratch pad type variable */
	Valve->RemoveCount[Channel] = 0;
	
	/* Valve Communication Status: Set Channel 1 and Global to OK */
	Valve->Comm_Stat[Channel] 	= OK;
	Valve->Status        	  	= OK;
		
	/* Set GVO Comm Failure Status to Good */
	GVO->commfail	= OFF;
	
	/* Reply: Absolute Number per Valve per Channel */
	Valve->Reply[Channel]++;
	
	/* Reply: Absolute Number per Channel */
	Poll->Reply[Channel]++;
	
	/* Valve Details */
	switch (Valve->Type) {
		
		/* IQ_TYPE: */
		case 0:
	
			/* Convert Percent: 0 - 65535 */
			Valve->Percent       		= (UINT) ((float) Valve->code_04[1] / (float) 0xFFFF * 100.0 + 0.5);
			/* Update Valve Object Percent */
			GVO->percent				= Valve->Percent;
			/* Convert Position: */
			Valve->Position 	   		= GetIQPosition( Valve->code_02, Valve->ClockWiseClose );
			/* Convert Torque: 0 - 65535 */
			Valve->Torque 	   			= (UINT) ((float) Valve->code_04[0] / (float) 0xFFFF * 120.0 + 0.5);
			
			/* Convert 24 bit Code 2 Array to 3 USINT */
			BitToAdr( (UDINT) Valve->code_02, (UDINT) Valve->Alarm, 3 ); 
			
			/* Update GVO Local Status: Local Selected or Monitor Relay */
			GVO->local	= Valve->code_02[20] | Valve->code_02[21];
			
			/* Update GVO Fault Status */
			GVO->fault	= 	Valve->code_02[19] | 		/* Thermostat */
							Valve->code_02[20] | 		/* Monitor Relay */
							Valve->code_02[23] | 		/* Battery Low */
							Valve->code_02[15] | 		/* Error */
							Valve->code_02[0]  | 		/* SI - Torque Trip in mid travel */
							Valve->code_02[1];			/* S2 - Motor Stalled */
							
			/* Update Valve Object Position */
			switch ( Valve->Position ) {
				/* Opened */
				case MX_OPENED:
					GVO->position 	= OPENED; 
					GVO->LSO		= ON;
					GVO->LSC		= OFF;
					strcpy( GVO->message, "Opened" );
					break;
				/* Closed */
				case MX_CLOSED:
					GVO->position 	= CLOSED; 
					GVO->LSO		= OFF;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Closed" );
					break;	
				/* Travel */
				case MX_STOPPED:
					GVO->position 	= TRAVEL; 
					GVO->LSO		= ON;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Stopped" );
					break;
				/* Opening */	
				case MX_OPENING:
					GVO->position 	= TRAVEL; 
					GVO->LSO		= ON;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Opening" );
					break;
				/* Closing */	
				case MX_CLOSING:
					GVO->position 	= TRAVEL; 
					GVO->LSO		= ON;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Closing" );
					break;
				/* No Indication */
				default:
					GVO->position	= NOIND; 
					GVO->LSO		= OFF;
					GVO->LSC		= OFF;
					strcpy( GVO->message, "NoInd" );
					break;
				}
			break;
	
		/* V2.20: IQ1_TYPE */
		case 2:
	
			/* Convert Percent: 0 - 1000 */
			Valve->Percent       		= (UINT) ((float) Valve->code_04[1] / (float) 0x03E8 * 100.0 + 0.5);
			/* Update Valve Object Percent */
			GVO->percent				= Valve->Percent;
			/* Convert Position: */
			Valve->Position 	   		= GetIQ1Position( Valve->code_02, Valve->ClockWiseClose );
			/* Convert Torque: 0 - 120 */
			Valve->Torque 	   			= (UINT) ((float) Valve->code_04[0] / (float) 0x0078 * 120.0 + 0.5);
			
			/* Convert 24 bit Code 2 Array to 3 USINT */
			BitToAdr( (UDINT) Valve->code_02, (UDINT) Valve->Alarm, 3 ); 
			
			/* Update GVO Local Status: Local Selected or Monitor Relay */
			GVO->local	= Valve->code_02[7] | Valve->code_02[9];
			
			/* Update GVO Fault Status */
			GVO->fault	= 	Valve->code_02[8] | 		/* Thermostat Tripped */
							Valve->code_02[9] | 		/* Monitor Relay */
							Valve->code_02[16] | 		/* Battery Low */
							Valve->code_02[10] | 		/* Valve Obstructed */
							Valve->code_02[11];			/* Valve Jammed  */
							
			/* Update Valve Object Position */
			switch ( Valve->Position ) {
				/* Opened */
				case MX_OPENED:
					GVO->position 	= OPENED; 
					GVO->LSO		= ON;
					GVO->LSC		= OFF;
					strcpy( GVO->message, "Opened" );
					break;
				/* Closed */
				case MX_CLOSED:
					GVO->position 	= CLOSED; 
					GVO->LSO		= OFF;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Closed" );
					break;	
				/* Travel */
				case MX_STOPPED:
					GVO->position 	= TRAVEL; 
					GVO->LSO		= ON;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Stopped" );
					break;
				/* Opening */	
				case MX_OPENING:
					GVO->position 	= TRAVEL; 
					GVO->LSO		= ON;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Opening" );
					break;
				/* Closing */	
				case MX_CLOSING:
					GVO->position 	= TRAVEL; 
					GVO->LSO		= ON;
					GVO->LSC		= ON;
					strcpy( GVO->message, "Closing" );
					break;
				/* No Indication */
				default:
					GVO->position	= NOIND; 
					GVO->LSO		= OFF;
					GVO->LSC		= OFF;
					strcpy( GVO->message, "NoInd" );
					break;
				}
			break;
			
		/* Flow Quip */	
		case 1:
			
			/* Convert Position: */
			GVO->LSO 	   				= Valve->code_02[3];
			GVO->LSC					= Valve->code_02[4];
			
			/* Get Position */
			if ( GVO->LSC & !GVO->LSO ) {
				GVO->percent			= 0;
				GVO->position			= CLOSED;
				strcpy( GVO->message, "Closed" );
			}
			else if ( GVO->LSC & GVO->LSO ) {
				GVO->percent			= 50;
				GVO->position			= TRAVEL;
				strcpy( GVO->message, "Travel" );		
			}
			else if ( !GVO->LSC & GVO->LSO ) {
				GVO->percent			= 100;
				GVO->position			= OPENED;
				strcpy( GVO->message, "Opened" );		
			}
			else {	
				GVO->percent			= 0;
				GVO->position			= NOIND;
				strcpy( GVO->message, "NoInd" );		
			}
			
			/* Move code_02 to Alarm */
			memcpy( GVO->Alarm, Valve->code_02, 1 ); 
			
			/* Update GVO Local Status: Local Selected or Monitor Relay */
			GVO->local	= !Valve->code_02[5];
			
			/* Update GVO Fault Status: Low Press or Low Level */
			GVO->fault	= Valve->code_02[0] | Valve->code_02[1];
										
		break;
		
		}	
}

/**********************************/
/** Subroutine AnyIQValveCommFail **/
/**********************************/ 
void AnyIQValveCommFail( POLL_obj *Poll )
{
IQVALVE_obj *Valve = (IQVALVE_obj *) Poll->Valve_adr;
VALVE_obj *GVO;
BOOL AnyValveCommFail = OFF;
USINT i;

	/* Count through All Valves */ 	
	for (i=0;i<Poll->NumValves;i++,Valve++) {
	
		/* Get Generic Valve Object */
		GVO = (VALVE_obj *) Valve->Valve_adr;
	
		/* Valve in Comm Fail: Both Channels */
		if ( Valve->Status == FAIL ) 
			AnyValveCommFail = ON;
		
		/* Check Travel Times: */
		Valve->traveltimer.IN = Valve->_open | Valve->_close;		
		TON_10ms( (TON_10mstyp *) &Valve->traveltimer );			
		
		/* Alarms: */						
		/* Open Travel: Complete */		
		if ( Valve->_open & GVO->LSO )
			Valve->_open 			= OFF;
		/* Close Travel: Complete */	
		if ( Valve->_close & GVO->LSC )
			Valve->_close 			= OFF;
			
		/* Open Travel: Failure */
		if ( Valve->traveltimer.Q & Valve->_open ) {
			GVO->FailedToOpenAlarm	= ON;
			Valve->_open 			= OFF;
		}
		/* Close Travel: Failure  */	
		if ( Valve->traveltimer.Q & Valve->_close ) {
			GVO->FailedToCloseAlarm	= ON;
			Valve->_close			= OFF;
		}
	
	}
	
	/* Make Assignment */	
	Poll->ValveError = AnyValveCommFail;
		
}	

/**********************************/
/** Subroutine AnyIQ2ValveCommFail :V3.00 **/
/**********************************/
void AnyIQ2ValveCommFail( POLL_obj *Poll )
{
IQVALVE_obj *Valve = (IQVALVE_obj *) Poll->Valve_adr;
VALVE_obj *GVO;
BOOL AnyValveCommFail = OFF, Ch1Fail = OFF, Ch2Fail = OFF;
USINT i;

	/* Count through All Valves */ 	
	for (i=0;i<Poll->NumValves;i++,Valve++) {
	
		/* Get Generic Valve Object */
		GVO = (VALVE_obj *) Valve->Valve_adr;
	
		/* Valve in Comm Fail: Both Channels */
		if ( Valve->Status == FAIL ) 
			AnyValveCommFail = ON;
			
		/* Valve in Ch1 Comm Fail: */
		if ( Valve->Comm_Stat[CH1] == FAIL ) 
			Ch1Fail = ON;
			
		/* Valve in Ch2 Comm Fail: */
		if ( Valve->Comm_Stat[CH2] == FAIL ) 
			Ch2Fail = ON;		
	
		/* Check Travel Times: */
		Valve->traveltimer.IN = Valve->_open | Valve->_close;		
		TON_10ms( (TON_10mstyp *) &Valve->traveltimer );			
		
		/* Alarms: V3.02 */						
		/* Open Travel: Complete */		
		if ( Valve->_open & GVO->LSO & !GVO->LSC )
			Valve->_open 			= OFF;
		/* Close Travel: Complete */	
		if ( Valve->_close & GVO->LSC & !GVO->LSO )
			Valve->_close 			= OFF;
			
		/* Open Travel: Failure */
		if ( Valve->traveltimer.Q & Valve->_open ) {
			GVO->FailedToOpenAlarm	= ON;
			Valve->_open 			= OFF;
		}
		/* Close Travel: Failure  */	
		if ( Valve->traveltimer.Q & Valve->_close ) {
			GVO->FailedToCloseAlarm	= ON;
			Valve->_close			= OFF;
		}
	
	}
	
	/* Make Assignment */	
	Poll->ValveError = AnyValveCommFail;
	Poll->ValveChError[CH1]	= Ch1Fail;
	Poll->ValveChError[CH2]	= Ch2Fail;

}	
 
/**********************************/
/** Subroutine GetIQCommandValve **/
/**********************************/
IQVALVE_obj *GetIQCommandValve( POLL_obj *Poll ) 
{
IQVALVE_obj *Valve = (IQVALVE_obj *) Poll->Valve_adr + Poll->CommandValve;

	/* Store */
	Poll->CommandValve_adr = (UDINT) Valve;
	
	/* Return Value */
	return Valve;
}

/*******************************/
/** Function GetIQMovingValve **/
/*******************************/
IQVALVE_obj *GetIQMovingValve( POLL_obj *Poll, USINT Channel )
{
USINT i;
IQVALVE_obj *CandidateValve;

/* Moving Table is Empty: Reset Index and Return */
if ( !Poll->MovingTableFull ) { 
	/* Signal */
	Poll->MovingScan 		= -1;
	/* Toggle OFF */
	Poll->MovingScanToggle  = OFF;
	/* Return Value */
	return (IQVALVE_obj *) Poll->MovingValve_adr; 
	}

/* Return if Moving Scan Interleave is Not Enabled */
if ( Poll->MovingScanToggle ) {
	/* Previous Scan was ON: Disable scan this cycle */
	Poll->MovingScanToggle = OFF;
	/* Return Value */
	return (IQVALVE_obj *) Poll->MovingValve_adr; 
	}
else
	/* Disable Moving Scan */
	Poll->MovingScanToggle = ON;	

/* Get Valve Candidate */	
CandidateValve = (IQVALVE_obj *) Poll->Valve_adr;
    
/* Count through all valves unless return */	
for (i=0;i<Poll->NumValves;i++) {

	/* Increment Moving Scan Ring Index */
	Poll->MovingScan++;
	if (Poll->MovingScan >= Poll->NumValves)
    	Poll->MovingScan = 0;
    	
    /* Get Valve Candidate */	
    CandidateValve = (IQVALVE_obj *) Poll->Valve_adr + Poll->MovingScan;	
        
    /* Moving Valve Located and Enabled */
    if ( (CandidateValve->ScanTimer.IN) && (CandidateValve->Enable) ) {
    
    	/* Store */
		Poll->MovingValve_adr = (UDINT) CandidateValve;
		/* Establish Moving Valve Reference */
		return CandidateValve;
    	
    	}
	}
	
/* No Moving Valve Located */	

/* Signal */
Poll->MovingScan = -1;

/* Return Value */
return (IQVALVE_obj *) Poll->MovingValve_adr; 

}

/************************************/
/** Function: SendIQCommandMessage **/
/************************************/ 
void SendIQCommandMessage( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel )
{

	/* Is Channel OK to Send Command */
	if ( Valve->Comm_Stat[Channel] == OK ) {
	
	    /* Increment Diagnostics */
	    Valve->Polls[Channel]++;
		/* Polls: Absolute Number per Channel */
		Poll->Polls[Channel]++;
	
		/* Reset Fail/Ok bits */
		Valve->MessageFail[Channel]	= OFF;
		Valve->MessageOK[Channel]  	= OFF;
		
	    /* Establish Command Enable: Channel #1 */
	    /* Establish Command Enable */
		Valve->cPoll[Channel]		 	= ON;
				
		/* Place Valve into Moving Array */
	    AddIQToMovingTable( Valve );
	}

}


/*****************************/
/** Function: IQCommandExec **/
/*****************************/
void IQCommandExec( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel )
{

	/* Reset Command Comm Enable */
	Valve->cPoll[Channel] 		= OFF;
	/* Reset Valve Command Word */
	Valve->Command       		= 10;
	/* Reset Command Enable */
	Valve->Cmd_Stat[Channel]  	= OK;
	/* Reset Command Pending Flag */
	Poll->CommandReady 			= OFF;

}


/***************************/
/** Function: IQCommandFail **/
/***************************/

void IQCommandFail( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel )
{
  	
/* Reset Command Comm Enable */
Valve->cPoll[Channel] 		= OFF;
  	
/* Command Failed on this channel */  	
Valve->Cmd_Stat[Channel] 	= FAIL;
    
/* Reset Valve Command */ 
Valve->Command 				= 10;

/* Reset Poll Command Pending */
Poll->CommandReady 			= OFF;

}

/*************************************************/
/***************************/
/** Function: IQ2CommandFail :V3.00 **/
/***************************/
void IQ2CommandFail( POLL_obj *Poll, IQVALVE_obj *Valve, USINT Channel )
{
USINT ChannelX = CH1;

/* ChannelX is other channel */
if ( Channel == CH1 )
	ChannelX = CH2;
  	
/* Reset Command Comm Enable */
Valve->cPoll[Channel] 		= OFF;
  	
/* Command Failed on this channel */  	
Valve->Cmd_Stat[Channel] 	= FAIL;

/* Valve Command has Failed on both channels or
   General Valve Comm Failure on Both Channels */
if ( (Valve->Cmd_Stat[ChannelX] == FAIL) || (Valve->Status == FAIL) ) {
    
	/* Reset Valve Command */ 
	Valve->Command 		= 10;
    /* Reset Poll Command Pending */
    Poll->CommandReady 	= OFF;
    }

}

USINT GetIQPosition( BOOL *code02, BOOL ClockWiseClose ) 
{
/* Note: 	The bit order in the manual and that rec'd in code2 are not consistent 
			The real order is:	16-23, 8-15, 0-7. */

	/* Opened */
	if ( *(code02 + 18) )
		return MX_OPENED;
	/* Closed */
	else if ( *(code02 + 17) )
		return MX_CLOSED;
	
	/* Clockwise: with Center Column Movement */
	/* else if ( *(code02 + 8) & *(code02 + 16) ) { */
	else if ( *(code02 + 8) ) {
		if ( ClockWiseClose ) 
			return MX_OPENING;
		else
			return MX_CLOSING;
	}
	
	/* Counter Clockwise: with Center Column Movement  */
	/* else if ( *(code02 + 9) & *(code02 + 16) ) { */
	else if ( *(code02 + 9) ) {
		if ( ClockWiseClose ) 
			return MX_CLOSING;
		else
			return MX_OPENING;
	}
			
	/* Stopped */
	else 
		return MX_STOPPED;
	
	return 0;		
}

/* V2.2 */
USINT GetIQ1Position( BOOL *code02, BOOL ClockWiseClose ) 
{
/* Note: 	The bit order in the manual and that rec'd in code2 are not consistent 
			The real order is:	16-23, 8-15, 0-7. */

	/* Opened */
	if ( *(code02 + 2) )
		return MX_OPENED;
	/* Closed */
	else if ( *(code02 + 1) )
		return MX_CLOSED;
	
	/* Clockwise: with Center Column Movement */
	/* else if ( *(code02 + 8) & *(code02 + 16) ) { */
	else if ( *(code02 + 4) ) {
		if ( ClockWiseClose ) 
			return MX_OPENING;
		else
			return MX_CLOSING;
	}
	
	/* Counter Clockwise: with Center Column Movement  */
	/* else if ( *(code02 + 9) & *(code02 + 16) ) { */
	else if ( *(code02 + 3) ) {
		if ( ClockWiseClose ) 
			return MX_CLOSING;
		else
			return MX_OPENING;
	}
			
	/* Stopped */
	else 
		return MX_STOPPED;
	
	return 0;		
}


void AddIQToMovingTable( IQVALVE_obj *Valve )
{
 
	/* Enable Multiscan */	
	Valve->ScanTimer.IN = ON;

}

BOOL iqv_init( IQVALVE_obj *IQValve, UDINT Valve_adr, BOOL Enable, UDINT ScanTime, USINT ID, BOOL ClockWiseClose, 
				USINT Type )
{
VALVE_obj *valve;

	/* Assign Pointer */
	valve	= (VALVE_obj *) Valve_adr;
	
	/* Assign Generic Valve Object to IQ Valve */
	IQValve->Valve_adr  	= Valve_adr;
	
	/* Enable Valve if In Loop */
	IQValve->Enable			= Enable;
	valve->enable			= Enable;
	
	/* Cycle Time */
	IQValve->ScanTimer.PT 	= ScanTime * 100;
	IQValve->ScanTimer.IN 	= OFF;
	IQValve->traveltimer.PT = IQValve->ScanTimer.PT + 1000;		/* Cycle Time: + 10 seconds */
	
	/* Miscellaneous Defaults */
	IQValve->ID 				= ID;
	IQValve->Type				= Type;
	/* Reconnection Reset */
	IQValve->ReconnectCount[CH1]= 0;
	IQValve->ReconnectCount[CH2]= 0;
	IQValve->Retry[CH1]			= OFF;
	IQValve->Retry[CH2]			= OFF;
	/* General Diagnostic Reset */
	IQValve->NoReply[CH1] 		= 0;
	IQValve->NoReply[CH2] 		= 0;
	IQValve->Reply[CH1] 		= 0;
	IQValve->Reply[CH2] 		= 0;
	IQValve->Polls[CH1] 		= 0;
	IQValve->Polls[CH2] 		= 0;
	/* Comm Status Variables */
	IQValve->RemoveCount[CH1] 	= 0;
	IQValve->RemoveCount[CH2] 	= 0;
	IQValve->Comm_Stat[CH1] 	= OK;
	IQValve->Comm_Stat[CH2] 	= OK;
	IQValve->Status 			= OK;
	IQValve->Command			= 10;
	
	/* Return */
	return ON;
}

/* Init function for Sump motor control */
BOOL sump_init( SUMP_obj *sump, STRING name[20], UDINT terminal_adr, UDINT security_adr, UDINT analog_adr, UDINT secs_adr, 
				REAL h_level, REAL hh_level, REAL l_level, UINT h_alarm_secs, UINT fail_alarm_secs, UINT FlowDelaySecs )
{

	sump->terminal_adr		= terminal_adr;
	sump->security_adr		= security_adr;
	sump->analog_adr		= analog_adr;
	sump->secs_adr			= secs_adr;
	sump->HL_Sp				= h_level;
	sump->HHL_Sp			= hh_level;
	sump->LL_Sp				= l_level;
	sump->h_alarm_secs		= h_alarm_secs;
	sump->fail_alarm_secs 	= fail_alarm_secs;
	sump->FlowDelaySecs		= FlowDelaySecs;
	strcpy( sump->Name, name );
	
	/* Reset Outputs */
	sump->motor_run_cmd		= OFF;
	sump->shutof_valve_cmd	= OFF;
	
	/* Reset Local Variable */
	sump->_motor_run_cmd 	= OFF;
		
	/* Return Value */
	return ON;
}

/* Serv function for Sump motor control */
BOOL sump_serv( SUMP_obj *sump )
{
TERMINAL_obj *terminal = (TERMINAL_obj *) sump->terminal_adr;
SECURITY_obj *security = (SECURITY_obj *) sump->security_adr;
UINT	secs = * (UINT *) sump->secs_adr;

	/* Level Transmitter in Use */
	if ( sump->analog_adr ) {
	
		ANALOG_obj	*analog	= (ANALOG_obj *) sump->analog_adr;
		sump->level	= analog->value_f;								/* Get Level */
		
		/* Alarm Determination */
		sump->LL 	= OFF;
		sump->HL 	= OFF;
		sump->HHL 	= OFF;
	
		/* LL */
		if( sump->level < sump->LL_Sp ) 
			sump->LL 	= ON;
		/* HL */
		else if ( (sump->level >= sump->HL_Sp) && (sump->level < sump->HHL_Sp) )
			sump->HL 	= ON;
		/* V2.19: HHL */
		else if ( sump->level >= sump->HHL_Sp ) {
			sump->HL 	= ON;
			sump->HHL 	= ON;
		}
	}	
	
	/* HL Active: */
	if ( sump->HL ) {
		
		/* Just Entered HL Stage */
		if ( !sump->_hl ) 
			sump->_t1 = secs;
			
		/* HHL Alarm Logic */		
		if ( (secs - sump->_t1) > sump->h_alarm_secs )
			sump->AlarmHL = ON;		
	}
	/* HL Inactive: */		
	else {
		/* Reset Alarm: 15 Seconds Later! */
		if ( (secs - sump->_t1) > (sump->h_alarm_secs + 15) ) 
			sump->AlarmHL = OFF;
	}
	
	/* Store Old */	
	sump->_hl = sump->HL;
	
	/* Shutoff Valve Logic */
	sump->shutof_valve_cmd		= sump->Auto &  !sump->AlarmHL;		
	
	/* Toggle Command: Stop */
	if ( sump->motor_run & sump->toggle_oip ) 
		sump->stop_oip 	= ON;
	/* Toggle Command: Start */		
	else if ( sump->toggle_oip )
		sump->start_oip = ON;	
	
	/* Disable OIP pushbuttons in Remote or Level 0: Always allow Stop! */
	if ( terminal->remote || security->Level0 )  
		sump->start_oip = OFF;
				
	/* V2.20: Basic Pump Logic */
	sump->motor_run_cmd = ((sump->Auto & sump->HL) | sump->start_oip | sump->start_remote | sump->motor_run_cmd ) &
										!sump->LL & 
										!sump->stop_oip & 
										!sump->stop_remote & 
										!sump->AlarmStartFailure & 
										!sump->AlarmNoFlow;

	/* Motor Run: Enabled */
	if ( sump->motor_run_cmd ) {
	
		/* Start Command Received: Record Start Time */
		if ( !sump->_motor_run_cmd )
			sump->_t0 = secs;
	
		/* Check Failed to Start: 5 sec. */
		if ( ((secs - sump->_t0) > sump->fail_alarm_secs) & !sump->motor_run ) 
			sump->AlarmStartFailure = ON;		
		}
		
	/* V2.20: Motor Running: Check Flow Switch */ 	 
    if ( sump->motor_run ) { 
        
		/* Record Start Time */
		if ( sump->FS )
			sump->_t0 = secs;
	
		/* Check Flow Switch */		
	    if ( ((secs - sump->_t0) > sump->FlowDelaySecs) & !sump->FS ) 
	    	sump->AlarmNoFlow = ON;
		} 
	
	/* Store local */
	sump->_motor_run_cmd = sump->motor_run_cmd;
	
	/* Reset Alarm: 20 Seconds Later! */
	if ( (secs - sump->_t0) > 20 ) 
		sump->AlarmStartFailure = OFF;
		
	/* Reset Alarm: 20 Seconds Later! */
	if ( (secs - sump->_t0) > 20 ) 
		sump->AlarmNoFlow = OFF;		
	
	/* Commands: Always Reset */
	sump->start_oip = sump->stop_oip = sump->start_remote = sump->stop_remote = sump->toggle_oip = OFF;
	
	/* Sump Motor Status on HMI */
	if (sump->motor_run) {
		sump->on_led	= ON;
		sump->off_led	= OFF;
	}
	else {
		sump->off_led	= ON;
		sump->on_led	= OFF;
	}
	
	/* Return Value */
	return ON;
}		

/* V4.06 Flow Switch Function Block */
BOOL FSL(FSL_obj *fsl, BOOL MasterStart, BOOL Fsl, UDINT StartOverrideDly, UDINT AlarmLengthDly, UDINT NoFlowDly){

	/* Set Presets: In Sec */
	fsl->Timer_Override.PT 		= StartOverrideDly * 100;
	fsl->Timer_AlarmLength.PT 	= AlarmLengthDly * 100;
	fsl->Timer_NoFlow.PT 		= NoFlowDly * 100;
	
	fsl->MasterStart			= MasterStart;
	fsl->Fsl					= Fsl;

	/* Arm MasterStart */
	if (!fsl->_MasterStart && fsl->MasterStart) 
		fsl->Timer_Override.IN	= ON;
	else
		fsl->Timer_Override.IN	= OFF;
	
	/* Store Old */
	fsl->_MasterStart = fsl->MasterStart; 
	
	fsl->Start_Override = fsl->Timer_Override.Q ;
					
	/* Execute No Flow */
	if (!fsl->Start_Override && fsl->MasterStart && !fsl->Fsl)
			fsl->Timer_NoFlow.IN = ON;
		else
			fsl->Timer_NoFlow.IN = OFF;
	
	/* Hold No Flow Alarm */
	if (!fsl->_NoFlow && fsl->Timer_NoFlow.Q) 
		fsl->Timer_AlarmLength.IN = ON;
	else
		fsl->Timer_AlarmLength.IN = OFF;
	
	/* Store Old */
	fsl->_NoFlow = fsl->Timer_NoFlow.Q;

	/* No Flow Alarm */	 
	fsl->NoFlow = fsl->Timer_AlarmLength.Q;
		
	/* Timer Execute: Start Delay Override */
	TOF_10ms((TOF_10mstyp *) &fsl->Timer_Override);

	/* Timer Execute: Alarm Length Timer */
	TOF_10ms((TOF_10mstyp *) &fsl->Timer_AlarmLength);	
	
	/* Timer Execute: No Flow Alarm Delay Timer*/
	TON_10ms((TON_10mstyp *) &fsl->Timer_NoFlow);

	/* Return Value */
	return(fsl->NoFlow);
}

/* V1.01: Allow C100ms, C1s and C10s to increment to 65535 each */
BOOL timer_serv( TIMER_obj *timer )
{
UINT ticks;
UINT rem100ms, rem1s, rem2s, rem10s;

	/* Get Ticks */
	ticks = TIM_ticks();
	
	/* New Tick: Base */
	if ( ticks != timer->old ) 
	    timer->C10ms++;
	
	/* 100ms */
	rem100ms 	= timer->C10ms % 10;
	if ( rem100ms < 5 ) {
		if (timer->F100ms) 
			timer->C100ms++;
		timer->F100ms = OFF;
	}	
	else 
		timer->F100ms = ON;
	
	/* 1s */
	rem1s 		= timer->C100ms % 10;
	if ( rem1s < 5 ) {
		if (timer->F1s) 
			timer->C1s++;
		timer->F1s = OFF;
	}	
	else 
		timer->F1s = ON;
		
	/* 2s */
	rem2s 	= timer->C100ms % 20;
	if ( rem2s  < 5 ) {
		if (timer->F2s) 
			timer->C2s++;
		timer->F2s = OFF;
	}	
	else 
		timer->F2s = ON;

	
	/* 10s */
	rem10s 		= timer->C1s % 10;
	if ( rem10s < 5 ) {
		if (timer->F10s) 
			timer->C10s++;
		timer->F10s = OFF;
	}	
	else 
		timer->F10s = ON;
	    
	/* Update Old */
	timer->old = ticks;
	  
	/* Return Value */
	return OFF;
}


/* V3.06: Scale Init */
DINT scale_init(SCALE_obj *scale, REAL rawMin, REAL rawMax, REAL scaledMin, REAL scaledMax, UDINT adrRawValue, USINT typ)
{
	if(!scale)
		return 1;

	if( !adrRawValue){
		scale->initStatus = 2;
		return scale->initStatus;	
	}
	scale->ptrRawValue	= adrRawValue;
	
	scale->RawMax		= rawMax;
	scale->RawMin		= rawMin;
	scale->ScaledMax		= scaledMax;
	scale->ScaledMin		= scaledMin;
	scale->typ			= typ;	
	scale->initStatus = 0;
	
	return scale->initStatus;
}

/* V3.06: */
DINT scale_serv(SCALE_obj *scale)
{
	float m, b;
		
	if(!scale) 
		return 1;

	if( scale->initStatus )
		return scale->initStatus;

	if (scale->typ == cUINT){
		scale->_valueUINT = *( (UINT*) scale->ptrRawValue );
		scale->RawValue = (float) scale->_valueUINT;
		}
	else if (scale->typ == cINT){
		scale->_valueINT = *( (INT*) scale->ptrRawValue );
		scale->RawValue = (float) scale->_valueINT;		
		}
	else if (scale->typ == cFLOAT){
		scale->_valueFLOAT = *( (REAL*) scale->ptrRawValue );
		scale->RawValue = scale->_valueFLOAT;		
		}

	if (scale->RawValue >= scale->RawMin){ /* V4.02: && (scale->RawValue != 0) ){ */
		m = (scale->ScaledMax - scale->ScaledMin)/(scale->RawMax - scale->RawMin);
		b = scale->ScaledMax - m * scale->RawMax;
					
		scale->ScaledValue 	= m * scale->RawValue + b;
		scale->Error 		= 0;
		scale->value_i 		= (INT) scale->ScaledValue ;
		}
	else {
		scale->Error = 0;
		scale->ScaledValue = 0.0;
		scale->value_i = 0;
		}
	
	if (scale->RawValue <= ((scale->RawMin) - 1000)){
		scale->Error = 1;	/*	Less than minimum input	*/
		scale->value_i = 0;
		}
	else
	{
		}

/* Calculate RCA Value */
scale->value_64k =  (UINT) ((float) scale->value_i * (64000.0/32767.0));

	return 0;
}

/* Destination will stay high for one scan*/
BOOL GetBitWrite( UDINT src_adr, UDINT dest_adr )
{
BOOL *dest = (BOOL *) dest_adr;
BOOL *src  = (BOOL *) src_adr;

	/* Set Dest Value */
	*dest	= *src ;
		
	/* Reset Source */
	*src	= 0;
		
	/* Return BOOL */
	return *dest;
}

/* Convert Bit Array to USINT Array of length 'l' */
/* Format: BitToAdr( adr(IQValve[0].code2), adr(byte_array), 3 ) */
BOOL BitToAdr( UDINT bit_adr, UDINT byte_adr, USINT l )
{
BOOL *bit_ptr   = (BOOL *) bit_adr;
USINT *b        = (USINT *) byte_adr;
USINT y,i,j; 

    /* */
    for (j=0;j<l;j++,b++) {
        y   = 0;
        /* First Byte */
        for (i=0;i<8;i++,bit_ptr++) 
            y += ((USINT) *bit_ptr << i);
        *b  = y;
    }

    /* */
    return OFF; 
}

/* Init function for Omni Flow Comupter */
BOOL omni_init( OMNI_obj *o, UDINT horn_mask, UDINT crash_mask, UDINT duration )
{
UINT h0 = horn_mask >> 16;
UINT h1 = horn_mask & 0x0000FFFF;
UINT c0 = crash_mask >> 16;
UINT c1 = crash_mask & 0x0000FFFF;

	/*No Object*/
	if ( !o ) 
		return OFF;

	/* Clear Object */
	memset( o, 0, sizeof(OMNI_obj) );
	
	/* Alarm Group Definition */
	alarm_init( &o->StatusGroup[0], h0, c0, duration );
	alarm_init( &o->StatusGroup[1], h1, c1, duration );
	
	/* Return Value */
	return ON;
}

/* Serv function for Solatron device */
BOOL omni_serv( OMNI_obj *o)
{
USINT i;
/*No Object*/
	if ( !o ) 
		return OFF;

	/* UnPack Read Prover Status Bits from F770-F785 [0] - [15] */
	o->Status.Abort_TempU 			= o->Status.Raw1909[0];
	o->Status.Abort_TempD 			= o->Status.Raw1909[1];
	o->Status.Sequence_Comp 		= o->Status.Raw1909[2];
	o->Status.Sequence_Abort 		= o->Status.Raw1909[3];
	o->Status.Flight_Det1_Tripped 	= o->Status.Raw1909[4];
	o->Status.Flight_Det3_Tripped 	= o->Status.Raw1909[5];
	o->Status.OTravel_Det2_Tripped 	= o->Status.Raw1909[6];
	o->Status.OTravel_Det4_Tripped 	= o->Status.Raw1909[7];
	o->Status.Ball_Forward 			= o->Status.Raw1909[8];
	o->Status.Ball_Reverse 			= o->Status.Raw1909[9];
	o->Status.Abort_RunD			= o->Status.Raw1909[10];
	o->Status.Abort_Seal_NotOK		= o->Status.Raw1909[11];
	o->Status.Abort_FlowrateU		= o->Status.Raw1909[12];
	o->Status.Abort_No_Permis		= o->Status.Raw1909[13];
	o->Status.Factor_Not_Imple 		= o->Status.Raw1909[14];
	o->Status.Abort_Not_Running		= o->Status.Raw1909[15];
	/*o->Status.Pack1909			= BitToWord( o->Status.Raw1909 );	 Pack 1909 */  

	/* UnPack Read Prover Status Bits from F786-F789 [0] - [3]*/
	o->Status.Sequence_Progress 	= o->Status.Raw1080[0];
	o->Status.Pump_Run 				= o->Status.Raw1080[1];
	o->Status.Seal 					= o->Status.Raw1080[2];
	o->Status.Low_N2_Pressure 		= o->Status.Raw1080[3];
	/*o->Status.Pack1080			= BitToWord( o->Status.Raw1080 )	 Pack 1080 */  

	/* UnPack Word Data: 3901 */
	o->Data.RunNum					= o->Data.Raw3901[0];
	o->Data.MeterNum				= o->Data.Raw3901[1];
	
	/* Prove Run # 1 */
	o->Data.Tempreture[0] 			= o->Data.Raw7991[0];			/* 7991 */
	o->Data.Pressure[0] 			= o->Data.Raw7991[1];			/* 7992 */
	o->Data.Time[0] 				= o->Data.Raw7991[4];			/* 7995 */
	o->Data.MeterFactor[0] 			= o->Data.Raw7991[5];			/* 7996 */
	
	/* Prove Run # 2 */
	o->Data.Tempreture[1] 			= o->Data.Raw7991[6];			/* 7997 */
	o->Data.Pressure[1] 			= o->Data.Raw7991[7];			/* 7998 */
	o->Data.Time[1] 				= o->Data.Raw7991[10];			/* 8001 */
	o->Data.MeterFactor[1] 			= o->Data.Raw7991[11];			/* 8002 */
	
	/* Prove Run # 3 */
	o->Data.Tempreture[2] 			= o->Data.Raw7991[12];			/* 8003 */
	o->Data.Pressure[2] 			= o->Data.Raw7991[13];			/* 8004 */
	o->Data.Time[2] 				= o->Data.Raw7991[16];			/* 8007 */
	o->Data.MeterFactor[2] 			= o->Data.Raw7991[17];			/* 8008 */
	
	/* Prove Run # 4 */
	o->Data.Tempreture[3] 			= o->Data.Raw7991[18];			/* 8009 */
	o->Data.Pressure[3] 			= o->Data.Raw7991[19];			/* 8010 */
	o->Data.Time[3] 				= o->Data.Raw7991[22];			/* 8013 */
	o->Data.MeterFactor[3] 			= o->Data.Raw7991[23];			/* 8014 */
	
	/* Prove Run # 5 */
	o->Data.Tempreture[4] 			= o->Data.Raw7991[24];			/* 8015 */
	o->Data.Pressure[4] 			= o->Data.Raw7991[25];			/* 8016 */			
	o->Data.Time[4] 				= o->Data.Raw7991[28];			/* 8019 */
	o->Data.MeterFactor[4] 			= o->Data.Raw7991[29];			/* 8020 */
	
	/* Prove Run # 6 */
	o->Data.Tempreture[5] 			= o->Data.Raw7991[30];			/* 8021 */
	o->Data.Pressure[5] 			= o->Data.Raw7991[31];			/* 8022 */			
	o->Data.Time[5] 				= o->Data.Raw7991[34];			/* 8025 */
	o->Data.MeterFactor[5] 			= o->Data.Raw7991[35];			/* 8026 */

	/* Prover Run Counts */
	for(i=0;i<6;i++)
		o->Data.Counts[i]		= o->Data.Raw5501[i]; 
		

	/* Set S StatusGroup based on Status bits received from RCA */
	memcpy( o->StatusGroup[0].IN, o->Status.Raw1909, 16 );
	memcpy( o->StatusGroup[1].IN, o->Status.Raw1080, 16 );
	
	/* Handle Solatron Alarm group */
	alarm_serv( &o->StatusGroup[0] );
	alarm_serv( &o->StatusGroup[1] );
	
	/* Return Value */
	return ON;
}

/* Init function for Sump motor control */
BOOL dsump_init( DSUMP_obj *dsump, STRING name[20], UDINT terminal_adr, UDINT security_adr, UDINT secs_adr, 
				UINT FlowDelaySecs )
{

	/* Assignments */
	dsump->terminal_adr		= terminal_adr;
	dsump->security_adr		= security_adr;
	dsump->secs_adr			= secs_adr;
	dsump->FlowDelaySecs	= FlowDelaySecs;	
	strcpy( dsump->Name, name );
	
	/* Reset Outputs */
	dsump->motor_run_cmd	= OFF;
	dsump->shutof_valve_cmd	= OFF;
	
	/* Return Value */
	return ON;
}

/* Serv function for Sump motor control */
BOOL dsump_serv( DSUMP_obj *dsump )
{
TERMINAL_obj *terminal = (TERMINAL_obj *) dsump->terminal_adr;
SECURITY_obj *security = (SECURITY_obj *) dsump->security_adr;
UINT	secs = * (UINT *) dsump->secs_adr;
	
	/* Toggle Command: Stop */
	if ( dsump->motor_run & dsump->toggle_oip ) 
		dsump->stop_oip 	= ON;
	/* Toggle Command: Start */		
	else if ( dsump->toggle_oip )
		dsump->start_oip = ON;	
	
	/* Disable OIP pushbuttons in Remote or Level 0: Always allow Stop! */
	if ( terminal->remote || security->Level0 )  
		dsump->start_oip = OFF;
	
	/* Lockout Logic: */
	dsump->Lockout = (dsump->Lockout | dsump->HL) & !dsump->Reset_oip;
	
	/* Shutoff Valve Logic */
	dsump->shutof_valve_cmd		= !dsump->Lockout;
				
	/* Basic Pump Logic */
	dsump->motor_run_cmd = (dsump->start_oip | dsump->start_remote | dsump->motor_run_cmd) & 
							!dsump->LL & !dsump->Lockout & 
							!dsump->AlarmStartFailure & 
							!dsump->AlarmNoFlow &  
							!dsump->stop_oip &
							!dsump->stop_remote;
							
	/* Motor Run: Enabled */
	if ( dsump->motor_run_cmd ) {
	
		/* Start Command Received: Record Start Time */
		if ( dsump->start_oip ) 
			dsump->_t0 = secs;
	
		/* Check Failed to Start */
		if ( ((secs - dsump->_t0) > 5) & !dsump->motor_run ) 
			dsump->AlarmStartFailure = ON;	
			
		/* Check Flow Switch */
		if ( ((secs - dsump->_t0) > dsump->FlowDelaySecs) & !dsump->FS ) 
			dsump->AlarmNoFlow = ON;		
	}
	
	/* Reset Alarm: 15 Seconds Later! */
	if ( (secs - dsump->_t0) > 20 ) 
		dsump->AlarmStartFailure = OFF;
		
	/* Reset Alarm: 15 Seconds Later! */
	if ( (secs - dsump->_t0) > 20 ) 
		dsump->AlarmNoFlow = OFF;	
	
	/* Commands: Always Reset */
	dsump->start_oip = dsump->stop_oip = dsump->start_remote = dsump->stop_remote = dsump->toggle_oip = dsump->Reset_oip = OFF;
	
	/* Sump Motor Status on HMI */
	if (dsump->motor_run) {
		dsump->on_led	= ON;
		dsump->off_led	= OFF;
	}
	else {
		dsump->off_led	= ON;
		dsump->on_led	= OFF;
	}
	
	/* Return Value */
	return ON;
}

/* V2.12: Run Time Init */
BOOL RunTime_init( RUNTIME_obj *runtime , UDINT run_adr, UDINT tick_adr )
{
	/* Assign Object */
	runtime->run_adr	= run_adr;
	runtime->tick_adr	= tick_adr;
	
	/* Return Value */
	return ON;
}

/* V2.12: Run Time Counter */
BOOL RunTime( RUNTIME_obj *runtime )
{
float _Partial;
STRING MinuteString[3];

	/* De-Reference */
	runtime->run 	= * (BOOL *) runtime->run_adr;
	runtime->tick 	= * (BOOL *) runtime->tick_adr;
	
	/* Reset */
	if ( runtime->Reset_oip ) {
		runtime->Reset_oip 	= OFF;
		runtime->_Hours		= 0.0;
	} 
	
	/* New Seconds */
	if ( runtime->run & runtime->tick & !runtime->_old )
		runtime->_Hours+= 1.0/3600.0;
			
	/* Hours */
	runtime->Hours 		= runtime->_Hours;							/* 10 */
	_Partial   			= runtime->_Hours - (float) runtime->Hours;	/* 0.500 */
	
	/* Minutes */
	runtime->Minutes 	= _Partial * 60.0;							/* 30    */
	
	/* Generate String */
	itoa( runtime->Hours, 	(UDINT) &runtime->TimeString);
	itoa( runtime->Minutes, (UDINT) &MinuteString);
	strcat( (char *) &runtime->TimeString, ":");
	strcat( (char *) &runtime->TimeString, (char *) &MinuteString);  
	
	/* Store Old */
	runtime->_old = runtime->tick;
	
	/* Return Value */
	return ON;	
}

BOOL hscount_init( HSCOUNT_obj *h, UDINT count_adr, REAL ppg, UDINT timer_adr, REAL FlowFilter, USINT type, UDINT ppg10_adr )
{
USINT i;

	/* Establish References: */
	h->timer_adr	= timer_adr;
	h->count_adr	= count_adr;
	h->ppg10_adr	= ppg10_adr;
	/* Get Values: */
	h->timer_0		= * (UINT *) h->timer_adr;
	h->count_0		= * (UINT *) h->count_adr;
	/* Defaults: */
	h->Flow			= 0.0;
	h->GrossCount	= 0;
	h->GrossPulses	= 0;
	h->Sim			= OFF;
	h->FlowFilter	= (UINT) (FlowFilter * 10.0 + 0.5);
	h->type			= type;
	/* Clear Well */	
	for (i=0;i<200;i++) 
		h->well[i] = 0;
			
	if (ppg > 0 )
		h->ppg		= ppg;
	else
		h->ppg		= 1;
	
	/* Preset*/	
	h->ppg10timer.PT	= 50;			/* 500msec pulse ppg10 */	
	h->MF				= 1.0;			/* Preset Meter Factor */ /* V1.01 */
	
	/* */
	h->Reset		= OFF;
	/* */
	return OFF;
}

BOOL hscount_serv( HSCOUNT_obj *h )
{
UINT count, new, pulses =0, ppg10;
UDINT timer, difftime;
USINT i;

	/* Bad Object */
	if ( !h )
		return OFF;
	
	/* De-Reference */	
	timer 		= * (UDINT *) h->timer_adr;					/* 10ms Ticks */
	count 		= * (UINT *) h->count_adr;
	difftime	= (UDINT) (timer - h->timer_0);
	
	/* New Tick */
	if ( difftime > 0 ) {
	
		/* Determine New Counts */
		new	= count - h->count_0;
		
		/* Shift Well */	
		for (i=199;i>0;i--) 
			h->well[i] = h->well[i-1];
		/* Insert New Point in Well */		
		h->well[0] = new;
	
		/* Check Bounds */
		if (h->FlowFilter > 200)	h->FlowFilter 	= 200;
		if (h->FlowFilter == 0) 	h->FlowFilter 	= 1;
		if (h->ppg == 0)			h->ppg			= 1;
		
		/* Sum Pulses */
		for (i=0;i<h->FlowFilter;i++) 
			pulses+=h->well[i];
		
		/* V4.00: Determine Flow */	
		switch ( h->type ) {
			/* GPM */
			case 0:
				h->Flow		= (float) pulses / h->FlowFilter / h->ppg * 60.00 * h->MF;				
				break;
			/* GPH */	
			case 1:
				h->Flow		= (float) pulses / h->FlowFilter / h->ppg * 360.00 * h->MF;
				break;
			/* BPH */	
			case 2:
				h->Flow		= (float) pulses / h->FlowFilter / h->ppg * 60.00 / 0.7 * h->MF;
				break;	
			default:
				h->Flow		= (float) pulses / h->FlowFilter / h->ppg * 60.00 / 0.7 * h->MF;
				break;
		}
		
		/* Determine: Pulses needed for DO (10 ppg) */
		ppg10	= h->ppg / 10;
	
		/* Increment: count10ppg */
		h->count10 += new;
		
		/* PPG10: Acquired */
		if ( h->count10 > ppg10 ) {
			h->count10 -= ppg10;
			h->ppg10timer.IN	= ON;
		}
		/* PPG10: Not Yet */
		else 
			h->ppg10timer.IN	= OFF;						/* PPG10: Not Yet */	
			
		/* Calculate Gross Count */
		h->GrossPulses	+= new;
		h->GrossCount	= h->GrossPulses / h->ppg;
		h->NetCount		= (UDINT) ( (float) h->GrossCount * h->MF + 0.5);	
		h->count_0		= count;
		h->timer_0		= timer;				
	}
	/* Same Tick */
	else 
		h->ppg10timer.IN	= OFF;						/* PPG10: Not Yet */
	
	/* GPH Type Only */
	if ( h->type == GPH ) { 
		/* Execute ppg10 TOF Timer */
		TOF_10ms( (TOF_10mstyp *) &h->ppg10timer );
		/* Set Output: */
		* (BOOL *) h->ppg10_adr 	= h->ppg10timer.Q;
	}	
	
	/* Reset */
	if ( h->Reset ) {
		h->Reset 		= OFF;
		h->GrossPulses	= 0;	
	}
	
	/* Return */
	return ON;	
}

/* V3.06: UV/IR Detector Init */
BOOL det_init( DETECTOR_obj *det, UDINT raw_adr, UDINT filter_pt, UDINT faultalarm_pt, UDINT firealarm_pt )
{
	/* Assign */
	det->FilterTimer.PT	= filter_pt;
	det->FaultAlarmTimer.PT	= faultalarm_pt;
	det->FireAlarmTimer.PT	= firealarm_pt;

	det->raw_adr = raw_adr;
	strcpy( det->message, "Init" );

	/* Return Value */
	return OFF;
}

/* V3.06: UV/IR Detector Server */
UINT det_serv( DETECTOR_obj *det )
{
INT *rawh = (INT *) det->raw_adr;
float raw = (float) *rawh;
	
	/* Filter Input Signal */
	if ( raw == det->_rawold ) 
		det->FilterTimer.IN = ON;
	else
		det->FilterTimer.IN = OFF;

	if ( det->FilterTimer.Q )
		det->_raw = raw;
		
	/* Store */
	det->_rawold = raw;	
	
	TON_10ms( (TON_10mstyp *) &det->FilterTimer );
	
	/* Detector Current: 20mA -> 32760 */
	det->Current = (UINT) ( 0.000611 * det->_rawold );

	/* Equipment Fault Alarm -> SCADA */
	if (det->Current <= 3) 
		det->FaultAlarmTimer.IN = ON;
	else
		det->FaultAlarmTimer.IN = OFF;

	det->Fault_alarm = det->FaultAlarmTimer.Q;
		
	/* Fire Alarm -> SCADA */
	if (det->Current >= 16) 
		det->FireAlarmTimer.IN = ON;
	else
		det->FireAlarmTimer.IN = OFF;
	
	det->Fire_alarm = det->FireAlarmTimer.Q;

	TON_10ms( (TON_10mstyp *) &det->FaultAlarmTimer );
	TON_10ms( (TON_10mstyp *) &det->FireAlarmTimer );
				
	switch ( det->Current ) {
	
		/* 0 mA */
		case 0:
			strcpy( det->message, "Open Circuit" );
			break;
		/* 1 mA */
		case 1:
			strcpy( det->message, "General Fault" );
			break;
		/* 2 mA */
		case 2:
			strcpy( det->message, "Oi Fault" );
			break;
		/* 3 mA */
		case 3:
			strcpy( det->message, "Background IR" );
			break;	
		/* 4 mA */
		case 4:
			strcpy( det->message, "Normal Operation" );
			break;
		/* 16 mA */
		case 16:
			strcpy( det->message, "Instant Alarm" );
			break;
		/* 20 mA */
		case 20:
			strcpy( det->message, "Timed Alarm" );
			break;
		default:
			strcpy( det->message, "Void" );
			break;		
	}
	
	/* Return */
	return det->Current;
}	

#if 0
/* V5.01: Step_Raw */
UINT CalculateStep( UINT Step_Request, USINT *pDiv )
{
BOOL bitarray[16];
USINT i;

	/* Recipe Request: Convert to BOOL Table */
	WordToBit( Step_Request, bitarray );

	/* Cycle through UINT */	
	for ( i=0;i<16;i++ ) {
		/*  */
		if ( (bitarray[i]) && (*(pDiv + i) < 16) ) 
			bitarray[*(pDiv + i)] = ON;	/* Set Pier Valve */
				
		}	
	/* Set Recipe Current Step */	
	return BitToWord( (UDINT) bitarray);	

}

/* V5.01: Step_Raw - Redone below*/
UDINT CalculateStep32( UDINT Step_Request, USINT *pDiv )
{
BOOL bitarray[32];
USINT i;

	/* Recipe Request: Convert to BOOL Table */
	LongToBit( Step_Request, bitarray );

	/* Cycle through UINT */	
	for ( i=0;i<32;i++ ) {
		/*  */
		if ( (bitarray[i]) && (*(pDiv + i) < 32) ) 
			bitarray[*(pDiv + i)] = ON;	/* Set Pier Valve */
				
		}	
	/* Set Recipe Current Step */	
	return BitToLong( (BOOL *) bitarray);	

}
#endif

/* V5.02: recursive */
UINT CalculateDividerValue( UINT Step_Request, USINT arraynum, USINT *pDiv)
{
	UINT tempStep = Step_Request;
	
	/* Unused valve if greater than 16 */
	if (arraynum >= 16)
		return tempStep;

	/* Valve already set to open */
	if ( tempStep & (1 << *(pDiv + arraynum)) )
		return tempStep;

	tempStep |= 1 << *(pDiv + arraynum);
	
	return CalculateDividerValue( tempStep, *(pDiv + arraynum), pDiv);
}

/* V5.02: Step_Raw */
UINT CalculateStep( UINT Step_Request, USINT *pDiv )
{
UINT tempStep = Step_Request;
USINT i;

	/* Cycle through UINT */	
	for ( i=0;i<16;i++, tempStep >>= 1 ) 
	{
		/*  */
		if ( tempStep & 1 ) 
			Step_Request = CalculateDividerValue( Step_Request, i, pDiv);		
	}	

	/* Set Recipe Current Step */	
	return Step_Request;	

}

/* V5.02: recursive */
UDINT CalculateDividerValue32( UDINT Step_Request, USINT arraynum, USINT *pDiv)
{
	UDINT tempStep = Step_Request;
	
	/* Unused valve if greater than 32 */
	if (arraynum >= 32)
		return tempStep;

	/* Valve already set to open */
	if ( tempStep & (1 << *(pDiv + arraynum)) )
		return tempStep;

	tempStep |= 1 << *(pDiv + arraynum);
	
	return CalculateDividerValue32( tempStep, *(pDiv + arraynum), pDiv);
}

/* V5.02: Step_Raw */
UDINT CalculateStep32( UDINT Step_Request, USINT *pDiv )
{
UDINT tempStep = Step_Request;
USINT i;

	/* Cycle through UINT */	
	for ( i=0; i<32; i++, tempStep >>= 1 ) 
	{
		/*  */
		if ( tempStep & 1 ) 
			Step_Request = CalculateDividerValue32( Step_Request, i, pDiv);		
	}
		
	/* Set Recipe Current Step */	
	return Step_Request;	

}

void CalculateStep64( UDINT *Step64_Rec, UDINT *Step64_Cur, USINT *pier )
{
BOOL bitl1[32], bitl2[32];
UDINT *current[2], *rec[2];
USINT i;

rec[0] 		= Step64_Rec;
rec[1] 		= Step64_Rec + 1;
current[0] 	= Step64_Cur;
current[1] 	= Step64_Cur + 1;
 	
/* Convert To Recipe Step Information */
LongToBit( *current[0], bitl1 );
LongToBit( *current[1], bitl2 );
/* Cycle through UDINTs */	
for ( i=0;i<32;i++ ) {
	/* Long #1 */
	if ( (bitl1[i]) && (*(pier + i) < 4) ) 
		bitl1[*(pier + i)] 		= ON;	/* Set Pier Valve */
	/* Long #2 */		
	if ( (bitl2[i]) && (*(pier + i + 32) < 4) )
		bitl1[*(pier + i + 32)] 	= ON;	/* Set Pier Valve */
			
	}	
/* Set Recipe Current Step */	
*rec[0] = BitToLong( bitl1 );	
*rec[1] = BitToLong( bitl2 );	


}

BOOL FindNewStep64( UDINT *Step64_Rec, UDINT *Step64_Raw )
{
BOOL bit_raw1[32], bit_raw2[32], bit_rec1[32], bit_rec2[32];
UDINT *raw[2], *rec[2];
USINT i;

rec[0] 		= Step64_Rec;
rec[1] 		= Step64_Rec + 1;
raw[0] 	= Step64_Raw;
raw[1] 	= Step64_Raw + 1;
 	
/* Convert To Recipe Step Information */
LongToBit( *raw[0], bit_raw1 );
LongToBit( *rec[0], bit_rec1 );
/* Cycle UDINT 1 */	
for ( i=8;i<32;i++ ) {
	/* Long #1 */
	if ( (bit_raw1[i]) != bit_rec1[i] ) 
		return 1;	
	}	
	
/* Convert To Recipe Step Information */
LongToBit( *raw[1], bit_raw2 );
LongToBit( *rec[1], bit_rec2 );
/* Cycle UDINT 2 */	
for ( i=0;i<32;i++ ) {
	/* Long #1 */
	if ( (bit_raw2[i]) != bit_rec2[i] ) 
		return 1;	
	}	
return 0;

}

void LongToBit( UDINT longh, plcbit bith[32] )
{
UDINT Hold = 0;
UDINT Mask = 1;
USINT i;

/* Assign Masks */
for (i=0;i<32;i++) {
	 Hold = longh & Mask;
	 if (Hold > 0)
	 	bith[i] = 1;
	 else
	 	bith[i] = 0;	
     Mask = Mask * 2;
     }
}

UDINT BitToLong( plcbit BitArray[32] )
{
UDINT longh = 0;


longh = longh + ( 0x00000001 * BitArray[0] );
longh = longh + ( 0x00000002 * BitArray[1] );
longh = longh + ( 0x00000004 * BitArray[2] );
longh = longh + ( 0x00000008 * BitArray[3] );

longh = longh + ( 0x00000010 * BitArray[4] );
longh = longh + ( 0x00000020 * BitArray[5] );
longh = longh + ( 0x00000040 * BitArray[6] );
longh = longh + ( 0x00000080 * BitArray[7] );

longh = longh + ( 0x00000100 * BitArray[8] );
longh = longh + ( 0x00000200 * BitArray[9] );
longh = longh + ( 0x00000400 * BitArray[10] );
longh = longh + ( 0x00000800 * BitArray[11] );

longh = longh + ( 0x00001000 * BitArray[12] );
longh = longh + ( 0x00002000 * BitArray[13] );
longh = longh + ( 0x00004000 * BitArray[14] );
longh = longh + ( 0x00008000 * BitArray[15] );

longh = longh + ( 0x00010000 * BitArray[16] );
longh = longh + ( 0x00020000 * BitArray[17] );
longh = longh + ( 0x00040000 * BitArray[18] );
longh = longh + ( 0x00080000 * BitArray[19] );

longh = longh + ( 0x00100000 * BitArray[20] );
longh = longh + ( 0x00200000 * BitArray[21] );
longh = longh + ( 0x00400000 * BitArray[22] );
longh = longh + ( 0x00800000 * BitArray[23] );

longh = longh + ( 0x01000000 * BitArray[24] );
longh = longh + ( 0x02000000 * BitArray[25] );
longh = longh + ( 0x04000000 * BitArray[26] );
longh = longh + ( 0x08000000 * BitArray[27] );

longh = longh + ( 0x10000000 * BitArray[28] );
longh = longh + ( 0x20000000 * BitArray[29] );
longh = longh + ( 0x40000000 * BitArray[30] );
longh = longh + ( 0x80000000 * BitArray[31] );

return longh;
}
