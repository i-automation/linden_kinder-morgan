
FUNCTION scale_serv : DINT
	VAR_INPUT
		scale : SCALE_obj;
	END_VAR
END_FUNCTION

FUNCTION hwv_serv : UINT
	VAR_INPUT
		HWValve : HWVALVE_obj;
		number : UINT;
	END_VAR
END_FUNCTION

FUNCTION analog_serv : BOOL
	VAR_INPUT
		anal : ANALOG_obj;
	END_VAR
END_FUNCTION

FUNCTION alarm_init : BOOL
	VAR_INPUT
		alarm : ALARM_obj;
		horn_mask : UINT;
		crash_mask : UINT;
		duration : UDINT;
	END_VAR
END_FUNCTION

FUNCTION alarm_serv : UINT
	VAR_INPUT
		alarm : ALARM_obj;
	END_VAR
END_FUNCTION

FUNCTION record_init : BOOL
	VAR_INPUT
		record : RECORDER_obj;
		num_ch : USINT;
		c1 : UDINT;
		c2 : UDINT;
		c3 : UDINT;
		c4 : UDINT;
		c5 : UDINT;
		c6 : UDINT;
		c7 : UDINT;
		c8 : UDINT;
		c9 : UDINT;
		c10 : UDINT;
		duration : UDINT;
	END_VAR
END_FUNCTION

FUNCTION record_serv : BOOL
	VAR_INPUT
		record : RECORDER_obj;
	END_VAR
END_FUNCTION

FUNCTION vgroup_serv : BOOL
	VAR_INPUT
		vg : VALVEGROUP_obj;
	END_VAR
END_FUNCTION

FUNCTION clock_init : BOOL
	VAR_INPUT
		clock : CLOCK_obj;
		rcatime_adr : UDINT;
		duration : UDINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK AND4
	VAR_INPUT
		input1 : BOOL;
		input2 : BOOL;
		input3 : BOOL;
		input4 : BOOL;
	END_VAR
	VAR_OUTPUT
		output : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION mx_init : BOOL
	VAR_INPUT
		Poll : POLL_obj;
		Valve_adr : UDINT;
		SM_adr : UDINT;
		NumValves : USINT;
		NumRetry : USINT;
		Reconnect : USINT;
		InterPollDelay : REAL;
		PortOKCh1_adr : UDINT;
		PortOKCh2_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION mx_serv : UINT
	VAR_INPUT
		Poll : POLL_obj;
	END_VAR
END_FUNCTION

FUNCTION mx_command : BOOL
	VAR_INPUT
		Poll : POLL_obj;
		Valve : USINT;
		Command : UINT;
	END_VAR
END_FUNCTION

FUNCTION mxv_init : BOOL
	VAR_INPUT
		m : MXVALVE_obj;
		Valve_adr : UDINT;
		Enable : BOOL;
		ScanTime : UDINT;
		ID : USINT;
		Type : USINT;
	END_VAR
END_FUNCTION

FUNCTION hwv_init : BOOL
	VAR_INPUT
		HWValve : HWVALVE_obj;
		Valve_adr : UDINT;
		LSO_adr : UDINT;
		LSC_adr : UDINT;
		LCL_adr : UDINT;
		open_cmd_adr : UDINT;
		close_cmd_adr : UDINT;
		stop_cmd_adr : UDINT;
		cmdduration : UDINT;
		stopduration : UDINT;
		travelduration : UDINT;
		cmd_typ : USINT;
	END_VAR
END_FUNCTION

FUNCTION floop_serv : BOOL
	VAR_INPUT
		fl : FL_obj;
	END_VAR
END_FUNCTION

FUNCTION pump_init : BOOL
	VAR_INPUT
		p : PUMP_obj;
		status_adr : UDINT;
		run_adr : UDINT;
		pb_pt : UDINT;
	END_VAR
END_FUNCTION

FUNCTION pump_serv : BOOL
	VAR_INPUT
		p : PUMP_obj;
		Auto : BOOL;
		Hand : BOOL;
		Overload : BOOL;
	END_VAR
END_FUNCTION

FUNCTION BitToAdr : BOOL
	VAR_INPUT
		bit_adr : UDINT;
		byte_adr : UDINT;
		l : USINT;
	END_VAR
END_FUNCTION

FUNCTION RunTime : BOOL
	VAR_INPUT
		runtime : RUNTIME_obj;
	END_VAR
END_FUNCTION

FUNCTION term_serv : BOOL
	VAR_INPUT
		terminal : TERMINAL_obj;
	END_VAR
END_FUNCTION

FUNCTION booster_init : BOOL
	VAR_INPUT
		b : BOOSTER_obj;
		StartupOverride : UDINT;
		StartDelay : UDINT;
		LowFlow : UDINT;
		horn_mask : UINT;
		crash_mask : UINT;
		duration : UDINT;
	END_VAR
END_FUNCTION

FUNCTION iq2_serv : UINT
	VAR_INPUT
		Poll : POLL_obj;
	END_VAR
END_FUNCTION

FUNCTION dsump_init : BOOL
	VAR_INPUT
		dsump : DSUMP_obj;
		name : STRING[20];
		terminal_adr : UDINT;
		security_adr : UDINT;
		secs_adr : UDINT;
		FlowDelaySecs : UINT;
	END_VAR
END_FUNCTION

FUNCTION dsump_serv : BOOL
	VAR_INPUT
		dsump : DSUMP_obj;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK RTUV
	VAR_INPUT
		cmd_in : BOOL;
		stop_in : BOOL;
		first : BOOL;
	END_VAR
	VAR_OUTPUT
		stop : BOOL;
		cmd : BOOL;
		active : BOOL;
	END_VAR
	VAR
		stop_old : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION det_serv : UINT
	VAR_INPUT
		det : DETECTOR_obj;
	END_VAR
END_FUNCTION

FUNCTION det_init : BOOL
	VAR_INPUT
		det : DETECTOR_obj;
		raw_adr : UDINT;
		filter_pt : UDINT;
		faultalarm_pt : UDINT;
		firealarm_pt : UDINT;
	END_VAR
END_FUNCTION

FUNCTION RunTime_init : BOOL
	VAR_INPUT
		runtime : RUNTIME_obj;
		run_adr : UDINT;
		tick_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION rmv_serv : BOOL
	VAR_INPUT
		r : RMVALVE_obj;
	END_VAR
END_FUNCTION

FUNCTION rmv_init : BOOL
	VAR_INPUT
		r : RMVALVE_obj;
		v_adr : UDINT;
		timerPT : TIME;
	END_VAR
END_FUNCTION

FUNCTION hscount_serv : BOOL
	VAR_INPUT
		h : HSCOUNT_obj;
	END_VAR
END_FUNCTION

FUNCTION hscount_init : BOOL
	VAR_INPUT
		h : HSCOUNT_obj;
		count_adr : UDINT;
		ppg : REAL;
		timer_adr : UDINT;
		Flow_Type : REAL;
		type : USINT;
		ppg10_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION scale_init : DINT
	VAR_INPUT
		scale : SCALE_obj;
		rawMin : REAL;
		rawMax : REAL;
		scaledMin : REAL;
		scaledMax : REAL;
		adrRawValue : UDINT;
		typ : USINT;
	END_VAR
END_FUNCTION

FUNCTION sump_init : BOOL
	VAR_INPUT
		sump : SUMP_obj;
		name : STRING[20];
		terminal_adr : UDINT;
		security_adr : UDINT;
		analog_adr : UDINT;
		secs_adr : UDINT;
		h_level : REAL;
		hh_level : REAL;
		l_level : REAL;
		hh_alarm_secs : UINT;
		fail_alarm_secs : UINT;
		FlowDelaySecs : UINT;
	END_VAR
END_FUNCTION

FUNCTION iqv_init : BOOL
	VAR_INPUT
		IQValve : IQVALVE_obj;
		Valve_adr : UDINT;
		Enable : BOOL;
		ScanTime : UDINT;
		ID : USINT;
		ClockWiseClose : BOOL;
		Type : USINT;
	END_VAR
END_FUNCTION

FUNCTION omni_serv : BOOL
	VAR_INPUT
		o : OMNI_obj;
	END_VAR
END_FUNCTION

FUNCTION omni_init : BOOL
	VAR_INPUT
		o : OMNI_obj;
		horn_mask : UDINT;
		crash_mask : UDINT;
		duration : UDINT;
	END_VAR
END_FUNCTION

FUNCTION sump_serv : BOOL
	VAR_INPUT
		sump : SUMP_obj;
	END_VAR
END_FUNCTION

FUNCTION timer_serv : BOOL
	VAR_INPUT
		timer : TIMER_obj;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK LD_UINT
	VAR_INPUT
		EN : BOOL;
		IN1 : UINT;
	END_VAR
	VAR_OUTPUT
		OUT : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION vgroup_init : BOOL
	VAR_INPUT
		vg : VALVEGROUP_obj;
		valve_adr : UDINT;
		number : USINT;
		type : BOOL;
	END_VAR
END_FUNCTION

FUNCTION secur_init : BOOL
	VAR_INPUT
		s : SECURITY_obj;
		intruder_adr : UDINT;
		bypass_adr : UDINT;
		pw1 : UINT;
		pw2 : UINT;
		duration : UDINT;
	END_VAR
END_FUNCTION

FUNCTION secur_serv : BOOL
	VAR_INPUT
		s : SECURITY_obj;
	END_VAR
END_FUNCTION

FUNCTION analog_init : BOOL
	VAR_INPUT
		anal : ANALOG_obj;
		raw_adr : UDINT;
		high : REAL;
		low : REAL;
		type : USINT;
		mult : UINT;
	END_VAR
END_FUNCTION

FUNCTION BitToWord : UINT
	VAR_INPUT
		bit_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION WtoB : BOOL
	VAR_INPUT
		w : UINT;
		bit_adr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION floop_init : BOOL
	VAR_INPUT
		fl : FL_obj;
		psi_pt : UDINT;
	END_VAR
END_FUNCTION

FUNCTION booster_serv : BOOL
	VAR_INPUT
		b : BOOSTER_obj;
	END_VAR
END_FUNCTION

FUNCTION clock_serv : BOOL
	VAR_INPUT
		clock : CLOCK_obj;
	END_VAR
END_FUNCTION

FUNCTION term_init : BOOL
	VAR_INPUT
		terminal : TERMINAL_obj;
		security_adr : UDINT;
		mlpressure_adr : UDINT;
		terminalvolume : UINT;
		provervolume : UINT;
	END_VAR
END_FUNCTION

FUNCTION meter_serv : BOOL
	VAR_INPUT
		meter : METER_obj;
	END_VAR
END_FUNCTION

FUNCTION meter_init : BOOL
	VAR_INPUT
		meter : METER_obj;
		security_adr : UDINT;
		net1ppb_adr : UDINT;
		snet1ppb_adr : UDINT;
		batch_sts_adr : UDINT;
		eob_cmd_adr : UDINT;
		type : USINT;
	END_VAR
END_FUNCTION

FUNCTION recipe_serv : BOOL
	VAR_INPUT
		recipe : RECIPE_obj;
	END_VAR
END_FUNCTION

FUNCTION recipe_init : BOOL
	VAR_INPUT
		recipe : RECIPE_obj;
		security_adr : UDINT;
		terminal_adr : UDINT;
		vg_adr : UDINT;
		meter_adr : UDINT;
		gravity_adr : UDINT;
		closedelay : UDINT;
		RoomWarningBbls : UDINT;
		OverRunPT : UDINT;
		repulsePT : UDINT;
		divider_adr : UDINT;
		type : USINT;
	END_VAR
END_FUNCTION

FUNCTION iq_serv : UINT
	VAR_INPUT
		Poll : POLL_obj;
	END_VAR
END_FUNCTION

FUNCTION FSL : BOOL
	VAR_INPUT
		fsl : FSL_obj;
		MasterStart : BOOL;
		Fsl : BOOL;
		StartOverrideDly : UDINT;
		AlarmLengthDly : UDINT;
		NoFlowDly : UDINT;
	END_VAR
END_FUNCTION
