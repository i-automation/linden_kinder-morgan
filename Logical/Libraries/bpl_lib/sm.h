#ifndef	_SM_H

#define	_SM_H 1
#include <bur/plctypes.h>

/*===========================================================================
State machine implementation :
===========================================================================*/

#define SMT_NUMBEROFTIMERS 4
/*
typedef struct {
	INT	time;
	INT	elapse;
	USINT	done;
	USINT	donepulse;
}SMTIMER;

typedef struct {
    INT  	state;            
    USINT  	state1Scan;       
    INT  	statenext;        
    INT  	statereturn;      
    TON_10mstyp	t10[SMT_NUMBEROFTIMERS];
	SMTIMER	t[SMT_NUMBEROFTIMERS];			
	USINT	description[64];
}STATEMACHINE;
*/

void    SMInit            	(STATEMACHINE *sm);
void	SMDo                (void);
INT   	SMGetCurrentState	(void);
USINT	SMGetState1stScan	(void);
void    SMSetNextState      (INT value);
void	SMT10Start			(INT tindex, UDINT PT );
USINT	SMT10Done			(INT tindex);
void	SMTStart			(INT tindex, INT tticks);
USINT	SMTDone				(INT tindex);
INT		SMTElapse			(INT tindex);
USINT	SMTDonePulse		(INT tindex);
void	SMSetCurrentDescription(char *description);
void	SMGoNext			(void);
void	SMGoPrev			(void);


#define SM_BEGIN( x ) 		SMInit(&x); SMDo(); switch( SMGetCurrentState() ){case -1: SMSetNextState(0);
#define SM_STATE( x, y ) 	break; case x: SMSetCurrentDescription(y);if( SMGetState1stScan() )
#define SM_DEFAULT( x, y ) 	break; default: SMSetCurrentDescription(y);if( SMGetState1stScan() )
#define SM_END        		break;}

#define SM_NEXT( x )  		SMSetNextState(x)

#endif


