#include <standard.h>

/* */
#include "bpl_lib.h"
#include "sm.h"

/*State machine usage sample*/

/*------ HS state machine -------------------------------------------------*/
/*SM_BEGIN ( SMhs )*/                           /*State machine structure*/
/*----------------------------*/
/*    SM_STATE (HS_ZERO ){					*/
/*		SM_TSET(0, 0)    					*/
/*        hs.HSActive = 0;  				*/ 
/*        hs.Elapse	= 0;					*/
/*    }										*/
/*    if ( FBKio->Enable ){					*/
/*        SM_NEXT(HS_WAITTEMPOK)			*/
/*    }										*/
/*----------------------------*/
/*    SM_STATE  (HS_WAITTEMPOK){			*/
/*    }										*/
/*    if ( FBKio->i.TempOK){				*/
/*        hs.HSActive = 1;					*/
/*        SM_NEXT(HS_ACTIVE)				*/
/*    }										*/
/*    if ( !FBKio->Enable ){				*/
/*        SM_NEXT(HS_ZERO)					*/
/*    }   									*/
/*----------------------------*/
/*SM_END									*/
/*------ HS state machine END ----------------------------------------------*/

static STATEMACHINE *sm;
/*------------------------------------------------------------------------------------------*/
void SMInit( STATEMACHINE *smptr)
{
	sm 		= smptr;
}
/*------------------------------------------------------------------------------------------*/
INT   SMGetCurrentState	(void){
		return sm->state;
}
/*------------------------------------------------------------------------------------------*/
USINT	SMGetState1stScan	(void){
		return sm->state1Scan;
}
/*------------------------------------------------------------------------------------------*/
void    SMSetNextState      (INT nextstate){
		sm->statenext = nextstate;
}
void	SMSetCurrentDescription(char *description){
	if( (sm->state1Scan == 0) || (description == 0) ) return;

	memcpy(&sm->description[0], description, 10);
	sm->description[10] = 0;
/*	
	strcpy(&sm->description[0], description);
*/
}
/*------------------------------------------------------------------------------------------*/
void	SMT10Start			(INT tindex, UDINT PT){
	if( tindex > (SMT_NUMBEROFTIMERS - 1) )	return;
	
	sm->t10[tindex].IN		= 0;
	TON_10ms( (TON_10mstyp *) &sm->t10[tindex] );
	
	sm->t10[tindex].PT		= PT;
	sm->t10[tindex].IN		= 1;
	
}
/*------------------------------------------------------------------------------------------*/
USINT	SMT10Done				(INT tindex){
	if( tindex > (SMT_NUMBEROFTIMERS - 1) )	return 0;

	return sm->t10[tindex].Q;	
}
/*------------------------------------------------------------------------------------------*/
void	SMTStart			(INT tindex, INT tticks){
	if( tindex > (SMT_NUMBEROFTIMERS - 1) )	return;
	
	sm->t[tindex].time		= tticks;
	sm->t[tindex].done		= 0;
	sm->t[tindex].elapse	= tticks;
}
/*------------------------------------------------------------------------------------------*/
USINT	SMTDone				(INT tindex){
	if( tindex > (SMT_NUMBEROFTIMERS - 1) )	return 0;

	return sm->t[tindex].done;	
}
/*------------------------------------------------------------------------------------------*/
INT	SMTElapse			(INT tindex){
	if( tindex > (SMT_NUMBEROFTIMERS - 1) )	return 0;

	return sm->t[tindex].elapse;	
}
/*------------------------------------------------------------------------------------------*/
void	SMGoNext		(void){
	sm->statenext = sm->state + 1;
}
/*------------------------------------------------------------------------------------------*/
void	SMGoPrev		(void){
	sm->statenext = sm->statereturn;
}
/*------------------------------------------------------------------------------------------*/
USINT	SMTDonePulse		(INT tindex){
	if( tindex > (SMT_NUMBEROFTIMERS - 1) )	return 0;

	return sm->t[tindex].donepulse;	
}
/*------------------------------------------------------------------------------------------*/
void SMDo(void){
int i;

	sm->state1Scan = 0;

	if( sm->state != sm->statenext){
		sm->statereturn	= sm->state;
		sm->state 		= sm->statenext;
		sm->state1Scan 	= 1;
	}
	
	/*process all isntalled timers*/
	for(i=0;i<SMT_NUMBEROFTIMERS;i++){
		TON_10ms( (TON_10mstyp *) &sm->t10[i] );
		sm->t[i].donepulse = 0;
		if( sm->t[i].elapse != 0 ){
			if( --sm->t[i].elapse == 0){
				sm->t[i].done		= 1;
				sm->t[i].donepulse	= 1;
			}
		}
	}
	/*timers*/	
}
/*------------------------------------------------------------------------------------------*/


