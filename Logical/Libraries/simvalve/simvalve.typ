TYPE
	VALVESIM_typ : STRUCT
		LSC	:BOOL;	
		LSO	:BOOL;	
		position_act	:INT;	
		position_open	:INT;	
		status	:USINT;	
		OpenCmd	:BOOL;	
		CloseCmd	:BOOL;	
		StopCmd	:BOOL;	
		timer_adr	:UDINT;	
	END_STRUCT;
END_TYPE
