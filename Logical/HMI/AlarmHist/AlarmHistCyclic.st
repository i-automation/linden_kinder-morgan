(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AlarmHist
 * File: AlarmHistCyclic.st
 * Author: Bernecker + Rainer
 * Created: April 07, 2008
 ********************************************************************
 * Implementation of program AlarmHist
 ********************************************************************)
PROGRAM _INIT
	DiagState := WAIT;
END_PROGRAM
PROGRAM _CYCLIC

IF DevLink_0.handle = 0 THEN
	DevLink_0.enable := TRUE;
	DevLink_0.pDevice := ADR('CF');
	DevLink_0.pParam := ADR('/DEVICE=C:');
ELSE
	DevLink_0.enable := FALSE;
END_IF

CASE DiagState OF
WAIT:
	(*Visualization command interface*)
	IF ui_SaveDiag THEN
		ui_SaveDiag := FALSE;
		ui_SaveDiagActive := TRUE;
		ui_SaveState := SAVING;
		DiagState := CREATE_FILE;
	END_IF

(*State to create the alarm history file*)
CREATE_FILE:
	(*Get system time to name file appropriately*)
	DTStructureGetTime_0.enable := TRUE;
	DTStructureGetTime_0.pDTStructure := ADR(timeStruct);

	IF (DTStructureGetTime_0.status = 0) THEN
		memset(ADR(fileName), 0, SIZEOF(fileName));
		strcpy(ADR(fileName), ADR('AlarmHist_'));
		itoa(timeStruct.month, ADR(dtString));
		strcat(ADR(fileName), ADR(dtString));
		strcat(ADR(fileName), ADR('_'));
		itoa(timeStruct.day, ADR(dtString));
		strcat(ADR(fileName), ADR(dtString));
		strcat(ADR(fileName), ADR('_'));
		itoa(timeStruct.year, ADR(dtString));
		strcat(ADR(fileName), ADR(dtString));
		strcat(ADR(fileName), ADR('_'));
		itoa(timeStruct.hour, ADR(dtString));
		strcat(ADR(fileName), ADR(dtString));
		strcat(ADR(fileName), ADR('_'));
		itoa(timeStruct.minute, ADR(dtString));
		strcat(ADR(fileName), ADR(dtString));
		strcat(ADR(fileName), ADR('_'));
		itoa(timeStruct.second, ADR(dtString));
		strcat(ADR(fileName), ADR(dtString));
		strcat(ADR(fileName), ADR('_'));
		strcat(ADR(fileName), ADR(VersionNum));
		DTStructureGetTime_0.enable := FALSE;
		strcat(ADR(fileName), ADR('.csv'));
		FileCreate_0.enable := TRUE;
		IF ui_SaveCF THEN
			FileCreate_0.pDevice := ADR('CF');
		ELSE
			FileCreate_0.pDevice := ADR('USB0');
		END_IF
		FileCreate_0.pFile := ADR(fileName);
		DiagState := GET_ALARM;
	END_IF

(*State to read alarm from history into created file*)
GET_ALARM:
	IF FileCreate_0.status = 0 THEN
		fileIdent := FileCreate_0.ident;
		FileCreate_0.enable := FALSE;
	ELSIF FileCreate_0.status = fiERR_FILE_DEVICE THEN
		FileCreate_0.enable := FALSE;
		ui_SaveState := ERROR;
		DiagState := WAIT;
	END_IF
	IF FileCreate_0.status = ERR_FUB_ENABLE_FALSE THEN
	(* Get Visualization Handle *)
			IF uiHandle = 0 THEN
				uiHandle := VA_Setup(1,'Panel');
			END_IF	
				
			(* Get Semaphore *)
			uiStatus := VA_Saccess(1,uiHandle);
			
			IF (  uiStatus = 0 ) THEN
			
				CASE uiStep OF
					0:  (* Read first/recent alarm *)
						AlarmList.AlarmLen:= 80;
						AlarmList.uiFunction := 1  (* First alarm *);
						uiStatus := VA_GetExAlarmList(TRUE,uiHandle,ADR(AlarmList.pcAlarmString),ADR(AlarmList.AlarmLen),AlarmList.uiFunction, 44, 0);
						
						IF uiStatus = 0 THEN
							uiAlarmReadCount := uiAlarmReadCount+1;
							uiStep := 3;
							FileWrite_0.offset := 0;
						END_IF	
						
					1:  (* Read next alarm*)
					 	AlarmList.AlarmLen :=80;
					 	AlarmList.uiFunction := 2  (* Next alarm *);		
						uiStatus := VA_GetExAlarmList(TRUE,uiHandle,ADR(AlarmList.pcAlarmString),ADR(AlarmList.AlarmLen),AlarmList.uiFunction, 44, 0);	
						
						IF uiStatus = 240 THEN  (* End of alarmlist or max alarmcount stored to list *)
							uiStep := 0;
							DiagState := CLOSE_FILE;
						ELSIF uiStatus = 0 THEN  (* Alarm read OK *)
							uiStep := 3;
							uiAlarmReadCount := uiAlarmReadCount+1;
						END_IF

					3: (* Write alarm to file *)
						strcat(ADR(AlarmList.pcAlarmString), ADR('$r'));
						FileWrite_0.enable := TRUE;
						FileWrite_0.ident := fileIdent;
						FileWrite_0.pSrc := ADR(AlarmList.pcAlarmString);
						FileWrite_0.len := SIZEOF(AlarmList.pcAlarmString);
						IF FileWrite_0.status = 0 THEN
							uiStep := 1;
							FileWrite_0.enable := FALSE;
							FileWrite_0.offset := FileWrite_0.offset + AlarmList.AlarmLen + 1;
						END_IF
				
				END_CASE
				
				(* Release Semaphore *)
				VA_Srelease(1,uiHandle);
			
			END_IF
	END_IF

(*Close AlarmHist File*)
CLOSE_FILE:

	FileClose_0.enable := TRUE;
	FileClose_0.ident := fileIdent;

	IF FileClose_0.status = 0 THEN
		FileClose_0.enable := FALSE;
		FileCreate_0.enable := FALSE;
		IF ui_SaveHist THEN
			DiagState := WAIT;
			ui_SaveState := COMPLETE;
		ELSE
			DiagState := SYSTEM_DUMP;
		END_IF
	END_IF

(*Save the system dump from the SDM*)
SYSTEM_DUMP:
	SdmSystemDump_0.enable := TRUE;
	SdmSystemDump_0.configuration := sdm_SYSTEMDUMP_DATA;
	IF ui_SaveCF THEN
		SdmSystemDump_0.pDevice := ADR('CF');
	ELSE
		SdmSystemDump_0.pDevice := ADR('USB0');
	END_IF

	IF SdmSystemDump_0.status = 0 THEN
		ui_SaveState := COMPLETE;
		SdmSystemDump_0.enable := FALSE;
		DiagState := WAIT;
	END_IF
END_CASE

FileClose_0();
DevLink_0();
FileCreate_0();
FileWrite_0();
SdmSystemDump_0();
DTStructureGetTime_0();

END_PROGRAM
