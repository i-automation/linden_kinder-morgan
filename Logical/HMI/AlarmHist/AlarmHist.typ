(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AlarmHist
 * File: AlarmHist.typ
 * Author: Bernecker + Rainer
 * Created: April 07, 2008
 ********************************************************************
 * Local data types of program AlarmHist
 ********************************************************************)

TYPE
	DiagStates_enum : 
		(
		CREATE_FILE,
		SYSTEM_DUMP,
		WAIT,
		CLOSE_FILE,
		GET_ALARM
		);
	Get_AlarmList_type : 	STRUCT  (*Data Type for reading alarms*)
		uiFunction : USINT; (*Function (position of alarm)*)
		AlarmLen : DINT; (*Lenght of alarm text*)
		pcAlarmString : STRING[80]; (*Alarm string*)
	END_STRUCT;
END_TYPE
