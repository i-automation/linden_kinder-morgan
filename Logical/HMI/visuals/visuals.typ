(********************************************************************
 * COPYRIGHT -- HP
 ********************************************************************
 * Program: visuals
 * File: visualstyp
 * Author: Paul
 * Created: September 16, 211
 ********************************************************************
 * Local data types of program visuals
 ********************************************************************)

TYPE
	UnitDetails_typ : 	STRUCT 
		vRunTime : USINT;
		vCurrent : USINT;
		vPower : USINT;
		vEfficiency : USINT;
		vDifferentialPressure : USINT;
		vMotor : USINT;
		vPump : USINT;
		vMtrVibWarnSP : USINT;
		vMtrVibIbd : USINT;
		vMtrVibObd : USINT;
		vMtrTempWnd : USINT;
		vMtrTempIbdBrg : USINT;
		vMtrTempObdBrg : USINT;
		vPmpTempObdBrg : USINT;
		vPmpVibWarnSP : USINT;
		vPmpVibIbd : USINT;
		vPmpVibObd : USINT;
		vPmpTempCase : USINT;
		vPmpTempIbdBrg : USINT;
	END_STRUCT;
	UnitData_typ : 	STRUCT 
		pU_TE_Case : REAL;
		pU_TE_MtrIbdBrg : REAL;
		pU_TE_MtrObdBrg : REAL;
		pU_TE_PmpIbdBrg : REAL;
		pU_TE_PmpObdBrg : REAL;
		pU_TE_PmpObdBrg2 : REAL;
		pU_TE_Winding : REAL;
		pU_WAHH : UINT;
		pU_WAH_Mtr_Sp : REAL;
		pU_WAHH_Pmp : BOOL;
		pU_WAH_Pmp : BOOL;
		pU_WAHH_Mtr : BOOL;
		pU_WAH_Mtr : BOOL;
		pU_WAH_Mtr_SpWrite : BOOL;
		pU_WAH_Pmp_Sp : REAL;
		pU_WAH_Pmp_SpWrite : BOOL;
		pU_Amp : INT;
		pU_TAH_Winding : BOOL;
		pU_TAH_PmpIbdBrg : BOOL;
		pU_TAH_PmpObdBrg : BOOL;
		pU_TAH_Case : BOOL;
		pU_TAH_MtrObdBrg : BOOL;
		pU_TAH_MtrIbdBrg : BOOL;
		pU_Amps : REAL;
		pU_DPT : REAL;
		pU_Horsepower : REAL;
		pU_Hours : DINT;
		pULockout : BOOL;
		pUStatus : BOOL;
		pUx_Status : UINT;
		pU_WT_Mtr : REAL;
		pU_WT_Pmp : REAL;
		pU_WT_MtrIbd : REAL;
		pU_WT_MtrObd : REAL;
		pU_WT_PmpIbd : REAL;
		pU_WT_PmpObd : REAL;
		pU_Vib : REAL;
		pU_PumpEff : REAL;
	END_STRUCT;
END_TYPE
