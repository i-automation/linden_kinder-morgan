#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif

/********************************************************************
 * COPYRIGHT -- PAI by KK
 ********************************************************************

 ********************************************************************
 *
 ********************************************************************/

#include <bur/plctypes.h>
#include <FileIO.h>
#include <AsMem.h>
#include <Visapi.h>
#include <asstring.h>
#include <astime.h>
#include <string.h>

#include "bgsprintf.h"
//#include "bgsprintf.c"


UINT AllocateBlockMemory( UDINT len, UDINT *_adr );
DINT getAlarm();
DINT getHistAlarm();


void _INIT saveAlarmsINIT( void )
{
	BlockAllocStatus = AllocateBlockMemory(2000000, (UDINT *)&buffer);
	VC_HANDLE = VA_Setup(1 ,"Panel" ); 					//Must be called cyclically for a Terminal
	_VCready = VC_HANDLE;
	TimeDateForm = 1; /*DD.MM.YY HH.MM.SS*/
}

void _CYCLIC saveAlarmsCYCLIC( void )
{
  CntrVis = !byStep;

  switch (byStep)
  {
    case 0: /**** First step ****/

		offset = 0;

		if(btnSaveAlarms){
			btnSaveAlarms = 0;
			AlarmListType = 1;
			byStep++;
		}

		if(btnSaveAlarmHist){
			btnSaveAlarmHist 	= 0;
			AlarmListType 		= 2;
			byStep++;
		}

    break;

    case 1: /**** Create file ****/

		DTGetTime_1.enable = 1;
		DTGetTime(&DTGetTime_1);
		DT_TO_DTStructure(DTGetTime_1.DT1 , (UDINT) &DTStruct);

		if ( AlarmListType == 1 ) {
			sprintf(fileName , "ActivAlarms %d%d%d_%d%d%d.csv",
					  DTStruct.month ,DTStruct.day , DTStruct.year , DTStruct.hour, DTStruct.minute, DTStruct.second);
		} else if ( AlarmListType == 2 ) {
			sprintf(fileName , "AlarmHist %d%d%d_%d%d%d.csv",
					  DTStruct.month ,DTStruct.day , DTStruct.year , DTStruct.hour, DTStruct.minute, DTStruct.second);
		}

		FCreate.enable    = 1;
		FCreate.pDevice = (UDINT) "CSV";
		FCreate.pFile   = (UDINT) fileName;
		FileCreate(&FCreate);

		dwIdent = FCreate.ident;
		wStatus = FCreate.status;

		/* Verify status */
		if (wStatus == 0)
		{
			FWrite.enable   = 1;
			FWrite.ident    = dwIdent;
			FWrite.pSrc     = (UDINT)buffer;
			offset			= 0;
			iFunction		= 1; /* First Alarm */
			byStep = 2;
		}
		else if (wStatus != 65535)
		{
			 byStep = 0;
			 if (wStatus == 20799)
			 {
			    wError = FileIoGetSysError();
			 }
		}
	break;

	 case 2:
//	 	if ( !_VCready ){											// Cyclic call for terminal mode
//	 	 	VC_HANDLE = VA_Setup(1 ,"panel1" );
//	 	 		if ( VC_HANDLE )	_VCready = 1;
//	 	 }
	 	if ( _VCready ){
					/* Retrieve alarms */
			if ( AlarmListType == 1 ) 		accessStatus = getAlarm();
			else if ( AlarmListType == 2 ) 	accessStatus = getHistAlarm();

			if(accessStatus == 240){ /* End of Alarms */
				byStep ++;
				break;
			}
			else if( (lLen > 0) && (accessStatus == 0) ){
				strcpy(buffer + offset , pcAlarmline);
				strcat(buffer, "\n");
				offset+=(lLen+1);
				iFunction = 2; /* Next Alarm */
			}
		}
		else byStep = 0;					// Reset
	 break;

     case 3: /**** Write data ****/

		FWrite.offset   = 0;
		FWrite.len = offset;

		FileWrite(&FWrite);

		wStatus = FWrite.status;
		if (wStatus == 0)
		{
		}
		else if (wStatus && (wStatus != 65535) )
		{
			byStep=0;
			if (wStatus == 20799)
			{
			 	wError = FileIoGetSysError();
			}

		}
		if (wStatus == 0xFFFF)	byStep++;

      break;

     case 4: /**** Close file ****/

       FClose.enable     = 1;
       FClose.ident      = dwIdent;
       FileClose(&FClose);

       wStatus = FClose.status;
       if (wStatus == 0)
       {
          byStep = 0;
       }
       else if (wStatus != 65535)
       {
          byStep = 0;
          if (wStatus == 20799)
          {
             wError = FileIoGetSysError();
          }
        }
        break;
  }
}

/*********************************************/

UINT AllocateBlockMemory( UDINT len, UDINT *_adr )
{
AsMemPartCreate_typ 	asCreate;
AsMemPartAllocClear_typ 	asAllocClear;

	asCreate.enable		= 1;
	asAllocClear.enable 	= 1;
	asCreate.len			= len+108;			// Increased from 100/AS2.7 to 108/AS3.0
	asAllocClear.len		= len;

	AsMemPartCreate (&asCreate );
	if ( asCreate.status == 0 ) {
		 asAllocClear.ident  = asCreate.ident;
		 AsMemPartAllocClear (&asAllocClear );
		 if ( asAllocClear.status == 0 ) {
			  *_adr = asAllocClear.mem;
			  return 1;
		 }
	 else
		  return asAllocClear.status;
	}
	else
		 return asCreate.status;

}

DINT getAlarm(){
UINT ret;

	lLen = sizeof(pcAlarmline);

	if (!VA_Saccess(1,VC_HANDLE))
  		{
	        ret = VA_GetActAlarmList (1, VC_HANDLE,(DINT)pcAlarmline,(DINT)&lLen , iFunction ,Seperator,TimeDateForm);

   			VA_Srelease (1,VC_HANDLE);
			return (DINT)ret;
	}
	else {
		return -1;
		lLen = 0;
	}

}
DINT getHistAlarm(){
UINT i , ret;

	lLen = sizeof(pcAlarmline);

	if (!VA_Saccess(1,VC_HANDLE))
  		{

			ret = VA_GetExAlarmList(1,VC_HANDLE, (DINT)pcAlarmline,(DINT)&lLen,iFunction,Seperator,TimeDateForm);
			for(i = 0 ; i < strlen( pcAlarmline) ; i++){
				if(pcAlarmline[i]==';')
					pcAlarmline[i]=',';
			}

   			VA_Srelease (1,VC_HANDLE);
			return (DINT)ret;
	}
	else {
		return -1;
		lLen = 0;
	}

}





