/*
File: 
bgSprintf.h

About:
------
- short and limited implementation of the sprintf() function

*/
#ifndef _BG_SPRINTF_H
#define _BG_SPRINTF_H

#include <stdarg.h>

/*
Public data types:
-----------------
*/

/*
Public functions:
-----------------
*/
int sprintf(char *out, const char *format, ...);

int vsprintf(char *out, const char *format, va_list args);

#endif
