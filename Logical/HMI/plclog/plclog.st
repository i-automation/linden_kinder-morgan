PROGRAM _INIT
(*-----------------------------------------------------------------------------------

Info: 	This task Reads out the PLC Logbook into a string array
		A function to read out the details for each entry is provided
		The functions are started when setting the variable to 1:

			cmdLogRead:		Refreshes the string array
			cmdDetailInfo:	Reads out the details for the selected error
			cmdNextEntry:	Moves the cursor one entry down
			cmdPrevEntry:	Moves the cursor one entry up
-----------------------------------------------------------------------------------*)

MaxRead:= SIZEOF(LogbookInfo) / SIZEOF(Plclog.DetailInfo);

Plclog.cmdLogRead:= 1;
END_PROGRAM

PROGRAM _CYCLIC
IF OIP.ReadPage <> PAGE_LOGBOOK THEN
	RETURN;
END_IF

(* Read the logbook information *)
IF Plclog.cmdLogRead = 1 THEN
	Plclog.InfoHide:= 1;
	Plclog.NumberDetailInfo:= 0;

	FOR CountLog:= 0 TO (MaxRead - 1) DO
		ERRxStatus:= ERRxread(CountLog, ADR(LogbookInfo[CountLog]));

		IF ERRxStatus = 0 THEN
			ErrorNb:= LogbookInfo[CountLog].err_nr;
			Plclog.LogInfoString[CountLog]:= '';
			CharCount:= 4;

			(* Calculate, how many 0s have to be added in front of the Error number string *)
			FOR xcnt:= 0 TO 4 DO
				IF ErrorNb <> 0 THEN
					CharCount:= CharCount - 1;
				ELSE EXIT;
				END_IF

				ErrorNb:= ErrorNb / 10;
			END_FOR

			FOR xcnt:= 0 TO CharCount DO
				strcat(ADR(Plclog.LogInfoString[CountLog]), ADR('0'));
			END_FOR

			itoa(LogbookInfo[CountLog].err_nr, ADR(ErrorNbString));

			IF LogbookInfo[CountLog].err_nr <> 0 THEN
				strcat(ADR(Plclog.LogInfoString[CountLog]), ADR(ErrorNbString));
			END_IF

			strcat(ADR(Plclog.LogInfoString[CountLog]), ADR(','));
			strcat(ADR(Plclog.LogInfoString[CountLog]), ADR(LogbookInfo[CountLog].err_string));
		END_IF
	END_FOR

	Plclog.cmdLogRead:= 0;
END_IF

(* Move cursor to next entry *)
IF Plclog.cmdNextEntry = 1 THEN
	Plclog.cmdNextEntry:= 0;
	IF Plclog.NumberDetailInfo < (MaxRead - 1) THEN
		Plclog.NumberDetailInfo:= Plclog.NumberDetailInfo + 1;
		memcpy(ADR(Plclog.DetailInfo), ADR(LogbookInfo[Plclog.NumberDetailInfo]), SIZEOF(Plclog.DetailInfo));
		Plclog.InfoHide:= 0;
	END_IF

(* Move cursor to previous entry *)
ELSIF Plclog.cmdPrevEntry = 1 THEN
	Plclog.cmdPrevEntry:= 0;
	IF Plclog.NumberDetailInfo > 0 THEN
		Plclog.NumberDetailInfo:= Plclog.NumberDetailInfo - 1;
		memcpy(ADR(Plclog.DetailInfo), ADR(LogbookInfo[Plclog.NumberDetailInfo]), SIZEOF(Plclog.DetailInfo));
		Plclog.InfoHide:= 0;
	END_IF
END_IF
END_PROGRAM

