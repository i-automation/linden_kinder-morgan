(********************************************************************
 * COPYRIGHT -- Piedmont Automation
 ********************************************************************
 * PROGRAM: USB_Detect
 * File: read_data.st
 * Author: Bernecker & Rainer
 * Created: July 12, 2016
 * Author: Joseph Seymour
 ********************************************************************
 * Implementation OF PROGRAM read_data
 This PROGRAM serves TO scan AND grab the first USB connected TO the
 host. It is currently intended TO make seamless the use OF USB FOR
 mass storage on AR targets using dynamic USB node allocation.
 ********************************************************************)

PROGRAM _INIT

	step := WAIT;  

END_PROGRAM


PROGRAM _CYCLIC

TON_0(IN := TRUE, PT := USB_CHECK_TIME);

IF TON_0.Q THEN
	start_reading_usb_data := TRUE;
	TON_0(IN := FALSE);
END_IF

CASE step OF
 	
	WAIT:
		
		IF start_reading_usb_data = TRUE THEN
			start_reading_usb_data := FALSE;
			step := CREATE_NODE_ID_LIST;  (*start FUBs below*)
		ELSE
			step := WAIT;
		END_IF

	CREATE_NODE_ID_LIST:  (*Library AsUSB - Functionblock USBNodeListGet()*)
	
		UsbNodeListGet_0.enable := TRUE;
		UsbNodeListGet_0.pBuffer := ADR(node_id_buffer);  (*pointer to buffer - UDINT array is assigned*)
		UsbNodeListGet_0.bufferSize := SIZEOF(node_id_buffer);  (*size of node-id-buffer-array*)
		UsbNodeListGet_0.filterInterfaceClass := asusb_CLASS_MASS_STORAGE;  (*filter on mass storage devices is set*)
		UsbNodeListGet_0.filterInterfaceSubClass := 0;  (*no filer is set*)
								
		IF UsbNodeListGet_0.status = 0 THEN
			IF UsbNodeListGet_0.listNodes > 0 THEN
				IF Usb_present = FALSE THEN
        			step := READ_DEVICE_DATA;  (*FUB worked correctly => next step*)
				ELSE
					step := WAIT;
				END_IF
			ELSE
				IF Usb_present THEN
					Usb_present := FALSE;
					step := UNLINK_DEVICE;
				ELSE
					step := WAIT;
				END_IF
			END_IF


			UsbNodeListGet_0.enable := FALSE;	
		ELSIF UsbNodeListGet_0.status = ERR_FUB_BUSY OR UsbNodeListGet_0.status = ERR_FUB_ENABLE_FALSE THEN
			step := CREATE_NODE_ID_LIST;  (*FUB work asynchron => called until status isn't BUSY*)
		ELSIF UsbNodeListGet_0.status = 32900 THEN
			UsbNodeListGet_0.enable := FALSE;
			Usb_present := FALSE;
			step := UNLINK_DEVICE;
		ELSE
			step := ERROR_CASE;  (*error occured*)
			UsbNodeListGet_0.enable := FALSE;
		END_IF

	READ_DEVICE_DATA:  (*Library AsUSB - Functionblock USBNodeGet()*)
						
		UsbNodeGet_0.enable := 1;
		UsbNodeGet_0.nodeId := node_id_buffer[node];  (*specific node is read out of node_id_buffer*)
		UsbNodeGet_0.pBuffer := ADR(usb_data_buffer[node]);  (*data of specific node get stored in usb_data_buffer*)
		UsbNodeGet_0.bufferSize := SIZEOF (usb_data_buffer[node]);  (*size of specific node is read out usb_data_buffer*)
	
	
		IF UsbNodeGet_0.status = 0 THEN  (*FUB worked correctly*)
			UsbNodeGet_0.enable := 0;	
			node := node + 1;  (*next node to be read out of buffer*)
			IF node = UsbNodeListGet_0.listNodes THEN  (*last existing node is reached*)
				node := 0;
				step := GET_DESCRIPTOR;  (*all nodes are read out of buffer*)
			END_IF
		
		ELSIF UsbNodeGet_0.status = ERR_FUB_BUSY OR UsbNodeGet_0.status = ERR_FUB_ENABLE_FALSE THEN
			step := READ_DEVICE_DATA;  (*FUB work asynchron => called until status isn't BUSY*)
		ELSE
			UsbNodeGet_0.enable := 0;
			step := ERROR_CASE;  (*error occured*)
		END_IF

	GET_DESCRIPTOR:  (*Library AsUSB - Functionblock USBDescriptorGet()*)
					
		UsbDescriptorGet_0.enable := 1;
		UsbDescriptorGet_0.nodeId := node_id_buffer[node];  (*specific node is read out of node_id_buffer*) 
		UsbDescriptorGet_0.requestType := 0;  (*Request for device*)
        UsbDescriptorGet_0.descriptorType := 1;  (*Determines the device descriptor*)
		UsbDescriptorGet_0.languageId := 0;  (*for device and configuration descriptors*)
		UsbDescriptorGet_0.pBuffer := ADR(device_descriptor[node]);  (*descriptor-data of specific node get stored in device_descriptor-buffer*) 
		UsbDescriptorGet_0.bufferSize := SIZEOF(device_descriptor[node]);  (*size of specific node is read out device_descriptor-buffer*)
		
	
		IF UsbDescriptorGet_0.status = 0 THEN  (*FUB worked correctly*)
			UsbDescriptorGet_0.enable := 0;
			node := node + 1;  (*next node to be read out of buffer*)
			IF node = UsbNodeListGet_0.listNodes THEN  (*last existing node is reached*)
				node := 0;
				IF NOT(Usb_present) THEN
					step := CREATE_FILE_DEVICE;  (*all nodes are read out of buffer*)
				END_IF
			END_IF
	
		ELSIF UsbDescriptorGet_0.status = ERR_FUB_BUSY OR UsbDescriptorGet_0.status = ERR_FUB_ENABLE_FALSE THEN
			step := GET_DESCRIPTOR;  (*FUB work asynchron => called until status isn't BUSY*)
		ELSE
			UsbDescriptorGet_0.enable := 0;
			step := ERROR_CASE;  (*error occured*)
		END_IF

	CREATE_FILE_DEVICE:  (*Library FileIO - Functionblock DevLink() - create file out of data just from 1. USB*)
						
		strcpy(ADR(device_name), ADR('USB0'));  (*fixed Device-Name get copied to device_name-Variable*)															
		strcpy(ADR(device_param), ADR('/DEVICE='));  (*first part of parameter get copied to device_param-Variable*)		
		strcat(ADR(device_param), ADR(usb_data_buffer[0].ifName));  (*second part get added to device_param-Variable*) 																												
	
		DevLink_0.enable := 1;
		DevLink_0.pDevice := ADR(device_name);  (*Devicename is assigned*)
		DevLink_0.pParam := ADR(device_param);  (*the path of the Device is assigned*)
		
	
		IF DevLink_0.status = 0 THEN
			DevLink_0.enable := 0;
			devHandle := DevLink_0.handle;
			step := FINISH;  (*FUB worked correctly => next step*)
			Usb_present := TRUE;	
		ELSIF DevLink_0.status = ERR_FUB_BUSY OR DevLink_0.status = ERR_FUB_ENABLE_FALSE THEN
			step := CREATE_FILE_DEVICE;  (*FUB work asynchron => called until status isn't BUSY*)
		ELSE
			DevLink_0.enable := 0;
			step := ERROR_CASE;  (*error occured*)
		END_IF

	UNLINK_DEVICE:  (*Library FileIO - Functionblock DevUnlink()*)
		IF  devHandle <> 0 THEN		
			DevUnlink_0.enable := 1;
			DevUnlink_0.handle := devHandle;  (*handle from DevLink is assigned to cut the connection to specific file-device*)
			IF DevUnlink_0.status = 0 THEN
				devHandle := 0;
				DevUnlink_0.enable := 0;
				step := FINISH;  (*FUB worked correctly => next step*)	
			ELSIF DevUnlink_0.status = ERR_FUB_BUSY OR DevUnlink_0.status = ERR_FUB_ENABLE_FALSE THEN
				step := UNLINK_DEVICE;  (*FUB work asynchron => called until status isn't BUSY*)
			ELSE
				DevUnlink_0.enable := 0;
				step := FINISH;  (*error occured*)
			END_IF
		ELSE
			step := FINISH;
		END_IF

	FINISH:  (*successfully finished*)
			
		start_reading_usb_data := FALSE;
		step := WAIT;  (*back to beginning - wait for start_reading_usb_data to be set*)
															


	ERROR_CASE:	(*error-handling*)												
		Usb_present := FALSE;
		//step := UNLINK_DEVICE;

END_CASE;

UsbNodeListGet_0();
UsbNodeGet_0();
UsbDescriptorGet_0();
DevLink_0();
DevUnlink_0();

END_PROGRAM