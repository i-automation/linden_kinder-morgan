/************************************************************************************************************************/
/* Object name: iostatus.c																								*/
/* Author:      Andreas Hager, B&R																						*/
/* Site:        Eggelsberg                                                                                            	*/
/* Created:     24-Feb-2006                                                                                           	*/
/* Restriction: 													      		                                      	*/
/* Description: Function to handle configured IO data points	                                                      	*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 1.00			24-Feb-2006		 Andreas Hager			Official release								  		*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 1.01			12-Dec-2006		 Andreas Hager			More IOs can be handled and commment added 		  		*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 2.00			16-Jul-2008		 Andreas Hager			AS3 - Changes for GCC 4.1.1								*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 2.04			21-Jun-2010		 Andreas Hager			ActValue & ForceValue changed to data type LREAL		*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 2.05			02-Jul-2010		 Andreas Hager			Problem with force value and LREAL fixed				*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Development tool: B&R Automation Studio V3.0.80.25 																  	*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Comment:                                                                                                           	*/
/************************************************************************************************************************/

#include <ModDiag.h>
#include <bur/plctypes.h>
#include <AsIOMMan.h>
#include <DataObj.h>
#include <string.h>
#include <AsIO.h>
#include <AsIODiag.h>
#include <Sys_lib.h>
#include "internal_h.h"

#define	FORCE_STATUS	0x04

#define	CHECK_DP_ACTIONS	0
#define	ENABLE_FORCE		1
#define	SET_FORCE_VALUE		2
#define	GET_DP_STATUS		3
#define	DISABLE_FORCE		4
#define	NEXT_DP				5

#define	FUNCTION_BUSY	65535

static	AsIODPStatus_typ		IODPStatus;
static	AsIOEnableForcing_typ	IODPEnableForce;
static	AsIODisableForcing_typ	IODPDisableForce;
static	AsIOSetForceValue_typ	IODPSetForceValue;

typedef union
{
	BOOL	bool;
	SINT	si;
	INT		i;
	DINT	di;
	USINT	usi;
	UINT	ui;
	UDINT	udi;
	REAL	real;
	STRING	string;
	//ULINT	uli;
	DATE_AND_TIME	dt;
	TIME	t;
	DATE	d;
	//LREAL	lreal;
	TIME_OF_DAY	tod;
	BYTE	byte;
	WORD	word;
	DWORD	dword;
	//LWORD	lword;
	WSTRING	wstring;
	//LINT	li;
} allPrimitives;

unsigned short IOStatus(unsigned long pIOList, unsigned long pStatusBuffer, unsigned short StartElement, unsigned short EndElement, unsigned long pEnableForce, unsigned long pForceValue, unsigned long pAnyIOForced)
{	static	USINT		Step = CHECK_DP_ACTIONS;
	static	UINT		DPIndex = 0;
	static	BOOL		IOForced = 0;
	
	allPrimitives *myActualValue;
	
	USINT	StatusFlag = 0, ForceStatus = 0;
	UINT	Status = 0;
			
	SingleIOEntry_typ		*SingleIOEntry = 0;
	SingleIOStatus_typ		*SingleIOStatus = 0;
	BOOL					*EnableForce = 0;
	LREAL					*ForceValue = 0;
	BOOL					*AnyIOForced = 0;

	*(UDINT*)&SingleIOEntry = pIOList;
	*(UDINT*)&SingleIOStatus = pStatusBuffer;
	*(UDINT*)&EnableForce = pEnableForce;
	*(UDINT*)&ForceValue = pForceValue;
	*(UDINT*)&AnyIOForced = pAnyIOForced;

	if(EndElement <= 0)
	{	return(0);
	}
	
	/* If there is an invalid pointer, return an error */
	if(pIOList == 0)
		return 255;
	
	switch(Step)
	{	/* Check actions for data point */
		case CHECK_DP_ACTIONS:
			/* Force data point - forcing data point */
			if(*(EnableForce + DPIndex) == 1)
			{	if((SingleIOStatus + DPIndex)->ForceActive == 0)
				{	/* Call funtion block to enable forcing */
					IODPEnableForce.enable = 1;
					IODPEnableForce.pDatapoint = (UDINT)(SingleIOEntry + DPIndex)->ID;
					
					AsIOEnableForcing(&(IODPEnableForce));
					Status = IODPEnableForce.status;
														
					if(IODPEnableForce.status == 0)
					{	Status = FUNCTION_BUSY;
						Step = GET_DP_STATUS;//SET_FORCE_VALUE;
					}
				}
				else
				{	Status = FUNCTION_BUSY;
					Step = GET_DP_STATUS;//SET_FORCE_VALUE;
				}
			}
			else
			{	Status = FUNCTION_BUSY;
				Step = GET_DP_STATUS;
			}
			break;	
		
		/* Set force value for data point */				
		case SET_FORCE_VALUE:
			/* Call function block to set force value */
			IODPSetForceValue.enable = 1;
			IODPSetForceValue.pDatapoint = (UDINT)(SingleIOEntry + DPIndex)->ID;
			/* Force value */
			IODPSetForceValue.value = (UDINT)*(ForceValue + DPIndex);
			
			AsIOSetForceValue(&(IODPSetForceValue));
			Status = IODPSetForceValue.status;
				
			if(IODPSetForceValue.status == 0)
			{	Status = FUNCTION_BUSY;
				Step = GET_DP_STATUS;
			}
			break;
	
		/* Get data point status */
		case GET_DP_STATUS:	
			/* Call functio block to get data point status */
			IODPStatus.enable = 1;
			IODPStatus.pDatapoint = (UDINT)(SingleIOEntry + DPIndex)->ID;
			
			AsIODPStatus(&(IODPStatus));
			Status = IODPStatus.status;
			
			if(IODPStatus.status == 0)
			{	/* Copy data to io status buffer */
				if(sizeof( *myActualValue) == sizeof(IODPStatus.value) )
				{
					myActualValue = (allPrimitives *)(&IODPStatus.value);
					switch (IODPStatus.datatype)
					{
						case 1:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).bool);		break;
						case 2:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).si);		break;
						case 3:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).i);			break;
						case 4:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).di);		break;
						case 5:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).usi);		break;
						case 6:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).ui);		break;
						case 7:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).udi);		break;
						case 8:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).real);		break;
						case 9:		(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).string);	break;
						//case 10:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).uli);		break;
						case 11:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).dt);		break;
						case 12:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).t);			break;
						case 13:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).d);			break;
						//case 14:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).lreal);		break;
						case 16:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).tod);		break;
						case 17:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).byte);		break;
						case 18:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).word);		break;
						case 19:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).dword);		break;
						//case 20:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).lword);		break;
						//case 21:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).wstring);	break;
						//case 23:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).lint);		break;
						default:	(SingleIOStatus + DPIndex)->ActValue = (LREAL)((*myActualValue).udi);		break;
					}
				} 
				else 	
					(SingleIOStatus + DPIndex)->ActValue = (LREAL)IODPStatus.value;
				(SingleIOStatus + DPIndex)->ActValue = (LREAL)IODPStatus.value;
				(SingleIOStatus + DPIndex)->ActForceValue = (LREAL)IODPStatus.forceValue;
				(SingleIOStatus + DPIndex)->DataType = IODPStatus.datatype;
				
				StatusFlag = IODPStatus.flags;
				ForceStatus = IODPStatus.flags & FORCE_STATUS;
				
				if(ForceStatus == 4)
				{	(SingleIOStatus + DPIndex)->ForceActive = 1;	
					IOForced	= 1;
				}
				else
				{	(SingleIOStatus + DPIndex)->ForceActive = 0;
				}
				
				if(*(EnableForce + DPIndex) == 1 && (SingleIOStatus + DPIndex)->ForceActive == 0)
				{
					Status = FUNCTION_BUSY;
					Step = CHECK_DP_ACTIONS;
				} 
				else if(*(EnableForce + DPIndex) == 0 && (SingleIOStatus + DPIndex)->ForceActive == 1)
				{
					Status = FUNCTION_BUSY;
					Step = CHECK_DP_ACTIONS;//DISABLE_FORCE;
				}
				else
				{
					if(DPIndex < EndElement - 1)
					{	DPIndex++;
						Status = FUNCTION_BUSY;
					}
					else
					{	DPIndex = StartElement;
						Status = 0;
					}
				}
			}
			break;
		
		/* Disable forcing */
		case DISABLE_FORCE:
			/* Forcing data point */
			if(*(EnableForce + DPIndex) == 0 && (SingleIOStatus + DPIndex)->ForceActive == 1)
			{	/* Call function block to disable forcing */
				IODPDisableForce.enable = 1;
				IODPDisableForce.pDatapoint = (UDINT)(SingleIOEntry + DPIndex)->ID;
				
				AsIODisableForcing(&(IODPDisableForce));
				Status = IODPDisableForce.status;
				
				if(IODPDisableForce.status == 0)		
				{	Status = FUNCTION_BUSY;
					Step = NEXT_DP;
				}
			}
			else
			{	Status = FUNCTION_BUSY;
				Step = NEXT_DP;
			}
			break;
		
		/* Select next io data point */
		case NEXT_DP:
			if(DPIndex < EndElement - 1)
			{	DPIndex++;
				Status = FUNCTION_BUSY;
				Step = CHECK_DP_ACTIONS;
			}
			else
			{	DPIndex = StartElement;
				Status = 0;
				AnyIOForced = IOForced;
				IOForced = 0;
				Step = CHECK_DP_ACTIONS;
			}
			break;
	}
	return(Status);
}
