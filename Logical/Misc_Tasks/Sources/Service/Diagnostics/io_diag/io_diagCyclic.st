PROGRAM _CYCLIC
(************************************************************************************************************************)
(* Object name: 		io_diag																							*)
(* Author:      		B&R																								*)
(* Site:        		Eggelsberg , International Applications                                                       	*)
(* Created:     		22-Nov-2007                                                                                    	*)
(* Restriction: 		consider task order;								      		                              	*)
(* Description: 		HMI I/O Diagnostics										                                     	*)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version History:		1.00	01-May-2008 		Official release											  		*)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Development tool: 	B&R Automation Studio V2.6.0.12 SP02														  	*)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Dependencies: 		Standard, Operator, ModDiag								                                      	*)
(*																														*)
(************************************************************************************************************************)

IF ( NOT(ModuleDiagnoseLoaded) ) THEN
	ModuleDiagnose_1.AdrModuleInformationBuffer:= ADR(ModuleInformation);
	ModuleDiagnose_1.Option:= DiagnoseOption;
	ModuleDiagnose_1.AdrAlarmInformation:= ADR(AlarmInformation);
	ModuleDiagnose_1();
	ModuleDiagnoseLoaded := NOT(UINT_TO_BOOL(ModuleDiagnose_1.status));
END_IF	

ModuleDiagnoseLoaded := FALSE;

IF(ModuleDiagnose_1.status = 0) THEN
	NumberOfTotalModules:= ModuleDiagnose_1.NumberOfTotalModules - ModuleDiagnose_1.NumberNotCheckedModules;
END_IF


IF ( (StartIndexIO = EndIndexIO) ) THEN
	Status:= IOStatus(pIOInformation.StartAdrBuffer, ADR(pStatusBuffer), 0, Elements, ADR(pEnableForce), ADR(pForceValue), ADR(pAnyIOForced));
ELSE
	Status:= IOStatus(pIOInformation.StartAdrBuffer, ADR(pStatusBuffer), StartIndexIO, EndIndexIO+1, ADR(pEnableForce), ADR(pForceValue), ADR(pAnyIOForced));
END_IF	

AsIOGlobalDisableForcing_1(enable := IO_Diag.IOInfo.CmdReset);
disableForceStatus := AsIOGlobalDisableForcing_1.status;		

IF disableForceStatus = 0 THEN
	IO_Diag.IOInfo.CmdReset	:= 0;
END_IF
		
pIOData ACCESS pIOInformation.StartAdrBuffer + (IOReadIndex * SIZEOF(pIOData));
Elements:= pIOInformation.TotalIONumber;

(****** Get the right display size 10.4'' or 5.7'' version ***********)
FindResult:= FIND(ModuleInformation[0].ModuleName,'.1043');
IF (FindResult >= 1) THEN
	mDisplayVersion:= DISPLAY_VERSION_10;
	MAX_IO_LINES:= 11;
	MAX_LINES:= MAX_NUMBER_MODULE_LINES + 1;
ELSIF (FindResult = 0) THEN
	FindResult:= FIND(ModuleInformation[0].ModuleName,'.0571');
	IF (FindResult >= 1) THEN
		mDisplayVersion:= DISPLAY_VERSION_5;
		MAX_IO_LINES:= 6;
		MAX_LINES:= 8;
	ELSE
		mDisplayVersion:= DISPLAY_VERSION_10;
		MAX_IO_LINES:= 11;
		MAX_LINES:= MAX_NUMBER_MODULE_LINES + 1;;
	END_IF
ELSE
	mDisplayVersion:= DISPLAY_VERSION_10;
	MAX_IO_LINES:= 11;
	MAX_LINES:= MAX_NUMBER_MODULE_LINES + 1;;
END_IF

IF (strcmp(ADR(ModuleInformation[0].ModuleName),ADR('1A4000.00')) = 0) THEN
	mSimulationMode:= 1;
ELSE
	mSimulationMode:= 0;
END_IF

IF ( (FIND(ModuleInformation[0].ModuleName,'X20CP')) >= 1) THEN
	mX20CPU:= 1;
ELSE
	mX20CPU:= 0;
END_IF



IF (OIP.ReadPage = PAGE_SERVICE_DIAGNOSTICS) THEN

	CASE Step OF

		INIT:

			(* Initialize the list of modules *)
			IO_Diag.ModuleInfoLayerStatus:= 0;
(*			IO_Diag.IOInfoLayerStatus:= 1;*)

			memset(ADR(IO_Diag.ModuleInfo),0,SIZEOF(IO_Diag.ModuleInfo));
			IF (NumberOfTotalModules > MAX_LINES) THEN
				EndIndex:= (MAX_LINES-1);
			ELSIF (NumberOfTotalModules <= MAX_LINES) AND (NumberOfTotalModules > 0) THEN
				EndIndex:= (NumberOfTotalModules-1);
			ELSIF (NumberOfTotalModules = 0) THEN
				EndIndex:= 0;
			END_IF

			FOR Index:= 0 TO EndIndex DO
				memcpy(ADR(IO_Diag.ModuleInfo.ModuleName[Index]),ADR(ModuleInformation[Index].ModuleName),SIZEOF(IO_Diag.ModuleInfo.ModuleName[Index]));
				memcpy(ADR(IO_Diag.ModuleInfo.ModulePath[Index]),ADR(ModuleInformation[Index].ModulePath),SIZEOF(IO_Diag.ModuleInfo.ModulePath[Index]));
				memcpy(ADR(IO_Diag.ModuleInfo.ErrorModuleName[Index]),ADR(ModuleInformation[Index].ErrorModuleName),SIZEOF(IO_Diag.ModuleInfo.ErrorModuleName[Index]));
				IO_Diag.ModuleInfo.ModuleState[Index]:= ModuleInformation[Index].ModuleState;
				IF (IO_Diag.ModuleInfo.ModuleState[Index] = 4) THEN
					IO_Diag.ModuleInfo.WrongModulStatus[Index]:= 0;
				ELSE
					IO_Diag.ModuleInfo.WrongModulStatus[Index]:= 1;
				END_IF
			END_FOR

			(* Color data point for the cursor of the module list*)
			ColorOld:= WHITE;
			IO_Diag.ModuleInfo.Color[0]:= ORANGE;
			SelectedIndex:= 0;
			SelectedModuleIndex:= 0;
			FOR Index:= 2 TO (MAX_LINES-1) BY 2 DO
				IO_Diag.ModuleInfo.Color[Index]:= WHITE;
			END_FOR

			FOR Index:= 1 TO (MAX_LINES-1) BY 2 DO
				IO_Diag.ModuleInfo.Color[Index]:= GREY;
			END_FOR

			StartIndex:= 0;
			(* I/O layer invisible at startup*)
			IO_Diag.StatusLayerMessage:= 1;
			Step:= COMMAND;



		COMMAND:


			(** Wait for commands set from the visualization ****)
			IF (NumberOfTotalModules <> NumberOfTotalModulesOld) THEN
				NumberOfTotalModulesOld:= NumberOfTotalModules;
				Step:= INIT;
			END_IF

			IF (IO_Diag.ModuleInfo.CmdLineDown = 1) THEN
				IO_Diag.CmdDetails := 1;
				IO_Diag.ModuleInfo.CmdLineDown:= 0;
				IF (SelectedIndex < (MAX_LINES-1)) AND (SelectedIndex < (NumberOfTotalModules-1)) AND (NumberOfTotalModules > 0) THEN
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ColorOld;
					SelectedIndex:= SelectedIndex +1;
					SelectedModuleIndex:= SelectedModuleIndex + 1;
					ColorOld:= IO_Diag.ModuleInfo.Color[SelectedIndex];
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ORANGE;
				ELSIF (SelectedModuleIndex < (NumberOfTotalModules-1)) AND (SelectedIndex >= (MAX_LINES-1)) THEN
					SelectedModuleIndex:= SelectedModuleIndex + 1;
					StartIndex:= SelectedModuleIndex-MAX_LINES+1;
					EndIndex:= StartIndex+MAX_LINES-1;
					IF (EndIndex > (NumberOfTotalModules-1)) THEN
						EndIndex:= NumberOfTotalModules-1;
					END_IF
					Step:= UPDATE_LIST;
				END_IF

			ELSIF (IO_Diag.ModuleInfo.CmdLineUp = 1) THEN
				IO_Diag.CmdDetails := 1;
				IO_Diag.ModuleInfo.CmdLineUp:= 0;
				IF (SelectedIndex = 0) AND (SelectedModuleIndex > 0) THEN
					SelectedModuleIndex:= SelectedModuleIndex - 1;
					StartIndex:= SelectedModuleIndex;
					EndIndex:= SelectedModuleIndex+MAX_LINES-1;
					Step:= UPDATE_LIST;
				ELSIF (SelectedIndex > 0) THEN
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ColorOld;
					SelectedIndex:= SelectedIndex - 1;
					SelectedModuleIndex:= SelectedModuleIndex - 1;
					ColorOld:= IO_Diag.ModuleInfo.Color[SelectedIndex];
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ORANGE;
				END_IF

			ELSIF (IO_Diag.ModuleInfo.CmdPageDown = 1) THEN
				IO_Diag.CmdDetails := 1;
				IO_Diag.ModuleInfo.CmdPageDown:= 0;
				IF ((SelectedModuleIndex-SelectedIndex+MAX_LINES-1) < (NumberOfTotalModules-1)) AND (NumberOfTotalModules > 0) THEN
					StartIndex:= StartIndex + MAX_LINES-1;
					EndIndex:= StartIndex + MAX_LINES-1;
					IF (EndIndex > (NumberOfTotalModules -1)) THEN
						EndIndex:= NumberOfTotalModulesOld-1;
						StartIndex:= EndIndex-MAX_LINES+1;
					END_IF
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ColorOld;
					SelectedModuleIndex:= StartIndex;
					SelectedIndex:= 0;
					ColorOld:= IO_Diag.ModuleInfo.Color[SelectedIndex];
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ORANGE;
					Step:= UPDATE_LIST;
				END_IF
			ELSIF (IO_Diag.ModuleInfo.CmdPageUp = 1) THEN
				IO_Diag.CmdDetails := 1;
				IO_Diag.ModuleInfo.CmdPageUp:= 0;
				IF ((SelectedModuleIndex-SelectedIndex) > 0) THEN
					LineStartIndex:= SelectedModuleIndex-SelectedIndex-MAX_LINES-1;
					IF (LineStartIndex > 0) THEN
						StartIndex:= SelectedModuleIndex-SelectedIndex-MAX_LINES+1;
						EndIndex:= StartIndex+MAX_LINES-1;
					ELSE
						StartIndex:= 0;
						EndIndex:= MAX_LINES-1;
					END_IF
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ColorOld;
					SelectedModuleIndex:= StartIndex;
					SelectedIndex:= 0;
					ColorOld:= IO_Diag.ModuleInfo.Color[SelectedIndex];
					IO_Diag.ModuleInfo.Color[SelectedIndex]:= ORANGE;
					Step:= UPDATE_LIST;
				END_IF

			ELSIF (IO_Diag.ModuleInfo.CmdEndList = 1) THEN
				IO_Diag.CmdDetails := 1;
				IO_Diag.ModuleInfo.CmdEndList:= 0;
				IO_Diag.ModuleInfo.Color[SelectedIndex]:= ColorOld;
				EndIndex:= NumberOfTotalModules-1;
				IF (NumberOfTotalModules < (MAX_LINES-1)) THEN
					StartIndex:= 0;
				ELSE
					StartIndex:= EndIndex-MAX_LINES+1;
				END_IF
				SelectedModuleIndex:= EndIndex;
				SelectedIndex:= MAX_LINES-1;
				IF (SelectedIndex > (NumberOfTotalModules-1)) THEN
					SelectedIndex:= NumberOfTotalModules-1;
				END_IF
				ColorOld:= IO_Diag.ModuleInfo.Color[SelectedIndex];
				IO_Diag.ModuleInfo.Color[SelectedIndex]:= ORANGE;
				Step:= UPDATE_LIST;
			ELSIF (IO_Diag.ModuleInfo.CmdStartList = 1) THEN
				IO_Diag.CmdDetails := 1;
				IO_Diag.ModuleInfo.CmdStartList:= 0;
				IO_Diag.ModuleInfo.Color[SelectedIndex]:= ColorOld;
				StartIndex:= 0;
				EndIndex:= StartIndex+MAX_LINES-1;
				IF (EndIndex > (NumberOfTotalModules -1)) THEN
					EndIndex:= NumberOfTotalModulesOld-1;
				END_IF
				SelectedModuleIndex:= StartIndex;
				SelectedIndex:= 0;
				ColorOld:= IO_Diag.ModuleInfo.Color[SelectedIndex];
				IO_Diag.ModuleInfo.Color[SelectedIndex]:= ORANGE;
				Step:= UPDATE_LIST;
			END_IF


			(* Update all lists after 10s*)
			IF (UpdateCounter <= 200) THEN
				UpdateCounter:= UpdateCounter + 1;
			ELSIF (UpdateCounter > 200) THEN
				UpdateCounter:= 0;
				Step:= UPDATE_LIST;
			END_IF


			(* The I/O layer is displayed if I/Os are configured on this module, otherwise a warning will be displayed*)
			IF (IO_Diag.CmdDetails = 1) THEN
				IO_Diag.CmdDetails:= 0;
				DetailsAvailable:= 0;
				IF (pIOInformation.StartAdrBuffer <> 0) THEN
					EndValue:= pIOInformation.TotalIONumber-1;
					FOR IOStartIndex:= 0 TO EndValue DO
						IOReadIndex:= IOStartIndex;
						pIOData ACCESS pIOInformation.StartAdrBuffer + (IOReadIndex * SIZEOF(pIOData));
						Equal:= strcmp(ADR(ModuleInformation[SelectedModuleIndex].ModulePath), ADR(pIOData.Device));
						IF (Equal = 0) THEN
							DetailsAvailable:= 1;
(*							IO_Diag.ModuleInfoLayerStatus:= 1;*)
							IO_Diag.IOInfoLayerStatus:= 0;
							StepIO:= IO_INIT;
							Step:= UPDATE_LIST;
							EXIT;
						ELSE
(*							IO_Diag.StatusLayerMessage:= 0;*)
						END_IF
					END_FOR
				ELSE
(*					IO_Diag.StatusLayerMessage:= 0;*)
				END_IF
			END_IF



		UPDATE_LIST:

			(* Update the list of modules*)
			LineIndex:= 0;
			FOR Index:= StartIndex TO EndIndex DO
				memcpy(ADR(IO_Diag.ModuleInfo.ModuleName[LineIndex]),ADR(ModuleInformation[Index].ModuleName),SIZEOF(IO_Diag.ModuleInfo.ModuleName[LineIndex]));
				memcpy(ADR(IO_Diag.ModuleInfo.ModulePath[LineIndex]),ADR(ModuleInformation[Index].ModulePath),SIZEOF(IO_Diag.ModuleInfo.ModulePath[LineIndex]));
				memcpy(ADR(IO_Diag.ModuleInfo.ErrorModuleName[LineIndex]),ADR(ModuleInformation[Index].ErrorModuleName),SIZEOF(IO_Diag.ModuleInfo.ErrorModuleName[LineIndex]));
				IO_Diag.ModuleInfo.ModuleState[LineIndex]:= ModuleInformation[Index].ModuleState;
				IF (IO_Diag.ModuleInfo.ModuleState[LineIndex] = 4) THEN
					IO_Diag.ModuleInfo.WrongModulStatus[LineIndex]:= 0;
				ELSE
					IO_Diag.ModuleInfo.WrongModulStatus[LineIndex]:= 1;
				END_IF
				LineIndex:= LineIndex + 1;
			END_FOR
			Step:= COMMAND;
	END_CASE

(********************************************************************************************************************************************************)
(*							IO Handling Layer																											*)
(********************************************************************************************************************************************************)
IF DetailsAvailable THEN
	IO_Diag.IOInfoLayerStatus:= 0;

	CASE StepIO OF

		IO_INIT:

			(*Initialize the I/O list*)
			memset(ADR(IO_Diag.IOInfo),0,SIZEOF(IO_Diag.IOInfo));
			memset(ADR(IOIndexArray),0,SIZEOF(IOIndexArray));
			NumberIO:= 0;
			EndValue:= pIOInformation.TotalIONumber-1;

			LineIndex:= 0;
			FOR Index:= IOStartIndex TO EndValue DO
				IOReadIndex:= Index;
				pIOData ACCESS pIOInformation.StartAdrBuffer + (IOReadIndex * SIZEOF(pIOData));
				Equal:= strcmp(ADR(ModuleInformation[SelectedModuleIndex].ModulePath), ADR(pIOData.Device));
				IF (Equal = 0) THEN
					IF (LineIndex < MAX_IO_LINES) THEN
						memcpy(ADR(IO_Diag.IOInfo.PVName[LineIndex]),ADR(pIOData.PVName),SIZEOF(IO_Diag.IOInfo.PVName[LineIndex]));
						memcpy(ADR(IO_Diag.IOInfo.LogicalName[LineIndex]),ADR(pIOData.LogicalName),SIZEOF(IO_Diag.IOInfo.LogicalName[LineIndex]));
						IO_Diag.IOInfo.Type[LineIndex]:= pStatusBuffer[Index].DataType;
						IO_Diag.IOInfo.ActValue[LineIndex]:= pStatusBuffer[Index].ActValue;
						IO_Diag.IOInfo.ForceActive[LineIndex]:= pStatusBuffer[Index].ForceActive;
						IO_Diag.IOInfo.ForceValue[LineIndex]:= pForceValue[Index];
						IO_Diag.IOInfo.EnableForcing[LineIndex]:= pEnableForce[Index];
						IO_Diag.IOInfo.InivisibleLines[LineIndex]:= 0;
						IOIndexArray[LineIndex]:= Index;
						(* Set min and max value of the numeric input fields *)
						CASE IO_Diag.IOInfo.Type[LineIndex] OF

							1:(*BOOL*)
								IO_Diag.IOInfo.MinValue[LineIndex]:= BOOL_TO_LREAL(MIN_VALUE_BOOL);
								IO_Diag.IOInfo.MaxValue[LineIndex]:= BOOL_TO_LREAL(MAX_VALUE_BOOL);

							2:(*SINT*)
								IO_Diag.IOInfo.MinValue[LineIndex]:= SINT_TO_LREAL(MIN_VALUE_SINT);
								IO_Diag.IOInfo.MaxValue[LineIndex]:= SINT_TO_LREAL(MAX_VALUE_SINT);

							3:(*INT*)
								IO_Diag.IOInfo.MinValue[LineIndex]:= INT_TO_LREAL(MIN_VALUE_INT);
								IO_Diag.IOInfo.MaxValue[LineIndex]:= INT_TO_LREAL(MAX_VALUE_INT);

							4:(*DINT*)
								IO_Diag.IOInfo.MinValue[LineIndex]:= DINT_TO_LREAL(MIN_VALUE_DINT);
								IO_Diag.IOInfo.MaxValue[LineIndex]:= DINT_TO_LREAL(MAX_VALUE_DINT);

							5:(*USINT*)
								IO_Diag.IOInfo.MinValue[LineIndex]:= USINT_TO_LREAL(MIN_VALUE_USINT);
								IO_Diag.IOInfo.MaxValue[LineIndex]:= USINT_TO_LREAL(MAX_VALUE_USINT);

							6:(*UINT*)
								IO_Diag.IOInfo.MinValue[LineIndex]:= UINT_TO_LREAL(MIN_VALUE_UINT);
								IO_Diag.IOInfo.MaxValue[LineIndex]:= UINT_TO_LREAL(MAX_VALUE_UINT);

							7:(*UDINT*)
								IO_Diag.IOInfo.MinValue[LineIndex]:= UDINT_TO_LREAL(MIN_VALUE_UDINT);
								IO_Diag.IOInfo.MaxValue[LineIndex]:= UDINT_TO_LREAL(MAX_VALUE_UDINT);

						END_CASE


						LineIndex:= LineIndex+1;
					END_IF
					NumberIO:= NumberIO+1;
				ELSE
					EXIT;
				END_IF
			END_FOR

			IO_Diag.IOInfo.CmdForcing:= IO_Diag.IOInfo.EnableForcing[0];

			memcpy(ADR(IO_Diag.IOInfo.ModuleName),ADR(ModuleInformation[SelectedModuleIndex].ModuleName),SIZEOF(IO_Diag.IOInfo.ModuleName));
			memcpy(ADR(IO_Diag.IOInfo.ModulePath),ADR(ModuleInformation[SelectedModuleIndex].ModulePath),SIZEOF(IO_Diag.IOInfo.ModulePath));
			IO_Diag.IOInfo.ModuleStatus:= ModuleInformation[SelectedModuleIndex].ModuleState;

			IF (LineIndex < (MAX_IO_LINES-1)) THEN
				FOR InvisibleIndex:= LineIndex TO (MAX_IO_LINES-1) DO
					IO_Diag.IOInfo.InivisibleLines[InvisibleIndex]:= 1;
				END_FOR
			END_IF

			(*Color of the cursor of the I/O list*)
			ColorOldIO:= WHITE;
			ColorNumericOldIO:= NUMERIC_WHITE;
			IO_Diag.IOInfo.Color[0]:= ORANGE;
			IO_Diag.IOInfo.NumericColor[0]:= NUMERIC_ORANGE;
			SelectedLineIndex:= 0;
			SelectedIOIndex:= IOStartIndex;
			FOR Index:= 2 TO (MAX_IO_LINES-1) BY 2 DO
				IO_Diag.IOInfo.Color[Index]:= WHITE;
				IO_Diag.IOInfo.NumericColor[Index]:= NUMERIC_WHITE;
			END_FOR

			FOR Index:= 1 TO (MAX_IO_LINES-1) BY 2 DO
				IO_Diag.IOInfo.Color[Index]:= GREY;
				IO_Diag.IOInfo.NumericColor[Index]:= NUMERIC_GREY;
			END_FOR

			IO_Diag.IOInfoLayerStatus:= 0;
(*			IO_Diag.ModuleInfoLayerStatus:= 1;*)
			StartIndexIO:= IOStartIndex;
			IF (NumberIO = 1) THEN
   				EndIndexIO:= StartIndexIO;			
			ELSIF (NumberIO < MAX_IO_LINES-1) THEN
				EndIndexIO:= IOStartIndex+NumberIO-1;
			ELSE
				EndIndexIO:= StartIndexIO+MAX_IO_LINES-1;
			END_IF

			IO_Diag.StatusLayerMessage:= 1;
			StepIO:= IO_COMMAND;


		IO_COMMAND:


			(* Wait for commands set from the visualization*)

			FOR ForcingIndex:= 0 TO (MAX_IO_LINES-1) DO
				IF (IO_Diag.IOInfo.ForcingCompletion[ForcingIndex] = 1) THEN
					IO_Diag.IOInfo.ForcingCompletion[ForcingIndex]:= 0;
					pForceValue[IOIndexArray[ForcingIndex]]:= IO_Diag.IOInfo.ForceValue[ForcingIndex];
					EXIT;
				END_IF
			END_FOR

//			IF (IO_Diag.IOInfo.CmdBack = 1) THEN
//				IO_Diag.IOInfo.CmdBack:= 0;
//				IO_Diag.ModuleInfoLayerStatus:= 0;
//(*				IO_Diag.IOInfoLayerStatus:= 1;*)
//				StepIO:= INIT;
			(*ELS*)IF (IO_Diag.IOInfo.CompletionCmdForcing = 1) THEN
				IO_Diag.IOInfo.CompletionCmdForcing:= 0;
				IO_Diag.IOInfo.EnableForcing[SelectedLineIndex]:= IO_Diag.IOInfo.CmdForcing;
				pEnableForce[SelectedIOIndex]:= IO_Diag.IOInfo.CmdForcing;
//			ELSIF (IO_Diag.IOInfo.CmdReset = 1) THEN
//				IO_Diag.IOInfo.CmdReset:= 0;
//				IO_Diag.IOInfo.CmdForcing:= 0;
//				FOR ResetIndex:= 0 TO (UINT_TO_USINT(Elements)-1) DO
//					pEnableForce[ResetIndex]:= 0;
//				END_FOR
//				StepIO:= UPDATE_IO_LIST;
			ELSIF (IO_Diag.IOInfo.CmdLineDown = 1) THEN
				IO_Diag.IOInfo.CmdLineDown:= 0;
				IF (SelectedLineIndex < (MAX_IO_LINES-1)) AND (SelectedLineIndex < (NumberIO-1)) AND (NumberIO > 0) THEN
					IO_Diag.IOInfo.Color[SelectedLineIndex]:= ColorOldIO;
					IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= ColorNumericOldIO;
					SelectedLineIndex:= SelectedLineIndex +1;
					SelectedIOIndex:= SelectedIOIndex + 1;
					ColorOldIO:= IO_Diag.IOInfo.Color[SelectedLineIndex];
					IO_Diag.IOInfo.Color[SelectedLineIndex]:= ORANGE;
					ColorNumericOldIO:= IO_Diag.IOInfo.NumericColor[SelectedLineIndex];
					IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= NUMERIC_ORANGE;
					IO_Diag.IOInfo.CmdForcing:= IO_Diag.IOInfo.EnableForcing[SelectedLineIndex];
				ELSIF ((SelectedIOIndex-StartIndexIO) < (NumberIO-1)) AND (SelectedLineIndex >= (MAX_IO_LINES-1)) THEN
					SelectedIOIndex:= SelectedIOIndex + 1;
					StartIndexIO:= SelectedIOIndex-MAX_IO_LINES+1;
					EndIndexIO:= StartIndexIO+MAX_IO_LINES-1;
					IF (EndIndexIO > (NumberIO-1+IOStartIndex)) THEN
						EndIndexIO:= NumberIO-1;
					END_IF
					StepIO:= UPDATE_IO_LIST;
					IO_Diag.IOInfo.CmdForcing:= IO_Diag.IOInfo.EnableForcing[SelectedLineIndex];
				END_IF

			ELSIF (IO_Diag.IOInfo.CmdLineUp = 1) THEN
				IO_Diag.IOInfo.CmdLineUp:= 0;
				IF (SelectedLineIndex = 0) AND (SelectedIOIndex > IOStartIndex) THEN
					SelectedIOIndex:= SelectedIOIndex - 1;
					StartIndexIO:= SelectedIOIndex;
					EndIndexIO:= SelectedIOIndex+MAX_IO_LINES-1;
					StepIO:= UPDATE_IO_LIST;
					IO_Diag.IOInfo.CmdForcing:= IO_Diag.IOInfo.EnableForcing[SelectedLineIndex];
				ELSIF (SelectedLineIndex > 0) THEN
					IO_Diag.IOInfo.Color[SelectedLineIndex]:= ColorOldIO;
					IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= ColorNumericOldIO;
					SelectedLineIndex:= SelectedLineIndex - 1;
					SelectedIOIndex:= SelectedIOIndex - 1;
					ColorOldIO:= IO_Diag.IOInfo.Color[SelectedLineIndex];
					IO_Diag.IOInfo.Color[SelectedLineIndex]:= ORANGE;
					ColorNumericOldIO:= IO_Diag.IOInfo.NumericColor[SelectedLineIndex];
					IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= NUMERIC_ORANGE;
					IO_Diag.IOInfo.CmdForcing:= IO_Diag.IOInfo.EnableForcing[SelectedLineIndex];
				END_IF
			ELSIF (IO_Diag.IOInfo.CmdEndList = 1) THEN
				IO_Diag.IOInfo.CmdEndList:= 0;
				IO_Diag.IOInfo.Color[SelectedLineIndex]:= ColorOldIO;
				IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= ColorNumericOldIO;
				EndIndexIO:= NumberIO-1+IOStartIndex;
				IF (NumberIO < (MAX_IO_LINES-1)) THEN
					StartIndexIO:= IOStartIndex;
				ELSE
					StartIndexIO:= EndIndexIO-MAX_IO_LINES+1;
				END_IF
				SelectedIOIndex:= EndIndexIO;
				SelectedLineIndex:= MAX_IO_LINES-1;
				IF (SelectedLineIndex > (NumberIO-1)) THEN
					SelectedLineIndex:= NumberIO-1;
				END_IF
				ColorOldIO:= IO_Diag.IOInfo.Color[SelectedLineIndex];
				IO_Diag.IOInfo.Color[SelectedLineIndex]:= ORANGE;
				ColorNumericOldIO:= IO_Diag.IOInfo.NumericColor[SelectedLineIndex];
				IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= NUMERIC_ORANGE;
				StepIO:= UPDATE_IO_LIST;
				IO_Diag.IOInfo.CmdForcing:= IO_Diag.IOInfo.EnableForcing[SelectedLineIndex];
			ELSIF (IO_Diag.IOInfo.CmdStartList = 1) THEN
				IO_Diag.IOInfo.CmdStartList:= 0;
				IO_Diag.IOInfo.Color[SelectedLineIndex]:= ColorOldIO;
				IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= ColorNumericOldIO;
				StartIndexIO:= IOStartIndex;
				EndIndexIO:= StartIndexIO+MAX_IO_LINES-1;
				IF (EndIndexIO > (NumberIO-1+IOStartIndex)) THEN
					EndIndexIO:= NumberIO-1;
				END_IF
				SelectedIOIndex:= StartIndexIO;
				SelectedLineIndex:= 0;
				ColorOldIO:= IO_Diag.IOInfo.Color[SelectedLineIndex];
				IO_Diag.IOInfo.Color[SelectedLineIndex]:= ORANGE;
				ColorNumericOldIO:= IO_Diag.IOInfo.NumericColor[SelectedLineIndex];
				IO_Diag.IOInfo.NumericColor[SelectedLineIndex]:= NUMERIC_ORANGE;
				StepIO:= UPDATE_IO_LIST;
				IO_Diag.IOInfo.CmdForcing:= IO_Diag.IOInfo.EnableForcing[SelectedLineIndex];


			(* Update all lists after 5s*)
			//ELSIF (UpdateIOCounter <= 100) THEN
			//	UpdateIOCounter:= UpdateIOCounter + 1;
			//ELSIF (UpdateIOCounter > 100) THEN
			//	UpdateIOCounter:= 0;
			//	StepIO:= UPDATE_IO_LIST;
			ELSE
				StepIO:= UPDATE_IO_LIST;
			END_IF

			IO_Diag.IOInfo.ModuleStatus:= ModuleInformation[SelectedModuleIndex].ModuleState;

		UPDATE_IO_LIST:
					
			(*Update the I/O list entries*)
			LineIndex:= 0;
			FOR Index:= StartIndexIO TO EndIndexIO DO
				IOReadIndex:= Index;
				pIOData ACCESS pIOInformation.StartAdrBuffer + (IOReadIndex * SIZEOF(pIOData));
				memcpy(ADR(IO_Diag.IOInfo.PVName[LineIndex]),ADR(pIOData.PVName),SIZEOF(IO_Diag.IOInfo.PVName[LineIndex]));
				memcpy(ADR(IO_Diag.IOInfo.LogicalName[LineIndex]),ADR(pIOData.LogicalName),SIZEOF(IO_Diag.IOInfo.LogicalName[LineIndex]));
				IO_Diag.IOInfo.Type[LineIndex]:= pStatusBuffer[Index].DataType;
				IO_Diag.IOInfo.ActValue[LineIndex]:= pStatusBuffer[Index].ActValue;
				IO_Diag.IOInfo.ForceValue[LineIndex]:= pForceValue[Index];
				IO_Diag.IOInfo.EnableForcing[LineIndex]:= pEnableForce[Index];
				IO_Diag.IOInfo.InivisibleLines[LineIndex]:= 0;
				IOIndexArray[LineIndex]:= Index;
				CASE IO_Diag.IOInfo.Type[LineIndex] OF

					1:(*BOOL*)
						IO_Diag.IOInfo.MinValue[LineIndex]:= MIN_VALUE_BOOL;
						IO_Diag.IOInfo.MaxValue[LineIndex]:= MAX_VALUE_BOOL;

					2:(*SINT*)
						IO_Diag.IOInfo.MinValue[LineIndex]:= MIN_VALUE_SINT;
						IO_Diag.IOInfo.MaxValue[LineIndex]:= MAX_VALUE_SINT;

					3:(*INT*)
						IO_Diag.IOInfo.MinValue[LineIndex]:= MIN_VALUE_INT;
						IO_Diag.IOInfo.MaxValue[LineIndex]:= MAX_VALUE_INT;

					4:(*DINT*)
						IO_Diag.IOInfo.MinValue[LineIndex]:= MIN_VALUE_DINT;
						IO_Diag.IOInfo.MaxValue[LineIndex]:= MAX_VALUE_DINT;

					5:(*USINT*)
						IO_Diag.IOInfo.MinValue[LineIndex]:= MIN_VALUE_USINT;
						IO_Diag.IOInfo.MaxValue[LineIndex]:= MAX_VALUE_USINT;

					6:(*UINT*)
						IO_Diag.IOInfo.MinValue[LineIndex]:= MIN_VALUE_UINT;
						IO_Diag.IOInfo.MaxValue[LineIndex]:= MAX_VALUE_UINT;

					7:(*UDINT*)
						IO_Diag.IOInfo.MinValue[LineIndex]:= MIN_VALUE_UDINT;
						IO_Diag.IOInfo.MaxValue[LineIndex]:= MAX_VALUE_UDINT;

				END_CASE

				LineIndex:= LineIndex + 1;
			END_FOR

			StepIO:= IO_COMMAND;


	END_CASE
ELSE
	IO_Diag.IOInfoLayerStatus:= 1;
	
END_IF

ELSE	(*Other page*)

	Step:= INIT;
	StepIO:= IO_INIT;			


END_IF
END_PROGRAM