
TYPE
	UPS_obj : 	STRUCT 
		PowerFailure : BOOL;
		BatteryLow : BOOL;
		BatteryLowTrip : BOOL;
		Failure : BOOL;
		Aux1 : BOOL;
		Aux2 : BOOL;
	END_STRUCT;
	OIP_obj : 	STRUCT 
		LifeSign : UINT;
		_LastLifeSign : UINT;
		ReadPage : UINT;
		SetPage : UINT;
		CommFail : BOOL;
		PT : TIME;
		ET : TIME;
		Disconnected : BOOL;
	END_STRUCT;
	UNITSTS_obj : 	STRUCT 
		MotorOn : BOOL;
		StUpOvr : BOOL;
		VFD_Enabled : BOOL;
		SafeRun : BOOL;
		SeqOverrun : BOOL;
		Start : BOOL;
		Stop : BOOL;
		EquipFault : BOOL;
		TAH_Bearing : BOOL;
		Lockout : BOOL;
		MotorOnInd : BOOL;
	END_STRUCT;
	MULTILIN_typ : 	STRUCT 
		OutRlySts : UINT;
		Trip : BOOL;
		LastTrip : UINT;
		TAH_Winding : BOOL;
		TAH_MtrIbdBrg : BOOL;
		TAH_MtrObdBrg : BOOL;
		TAH_PmpIbdBrg : BOOL;
		TAH_PmpObdBrg : BOOL;
		TAH_Case : BOOL;
		WAHH_Mtr_MMR : BOOL;
		WAHH_Pmp_MMR : BOOL;
		MtrOverload : BOOL;
		GenSts : UINT;
		AlarmMMR : BOOL;
		RTD_Fault : BOOL;
		StartInhibit : BOOL;
		Amp_MMR : INT;
		TE_Winding : INT;
		TE_MtrIbdBrg : INT;
		TE_MtrObdBrg : INT;
		TE_PmpIbdBrg : INT;
		TE_PmpObdBrg : INT;
		TE_Case : INT;
		WT_Mtr : DINT;
		WT_Pmp : DINT;
		Horsepower : UINT;
		DPT : INT;
		Hours : DINT;
		TE_PmpObdBrg2 : INT;
		WT_Fault : BOOL;
		WAH_Mtr : BOOL;
		WAHH_Mtr : BOOL;
		WAH_Pmp : BOOL;
		WAHH_Pmp : BOOL;
		WAH : BOOL;
		WAHH : BOOL;
		CommError : BOOL;
		CommFail : BOOL;
	END_STRUCT;
	UNITSP_obj : 	STRUCT 
		WAH_Mtr_Sp : REAL;
		WAH_Pmp_Sp : REAL;
		WAHH_Sp : DINT;
		WT_FaultSp : DINT;
		TAH_MtrIbdBrgSp : REAL;
		TAH_MtrObdBrgSp : REAL;
		TAH_PmpIbdBrgSp : REAL;
		TAH_PmpObdBrgSp : REAL;
		TAH_WindingSp : REAL;
		TAH_CaseSp : REAL;
	END_STRUCT;
	UNITVALVE_obj : 	STRUCT 
		SucVvOpSts : BOOL;
		SucVvClSts : BOOL;
		DscVvOpSts : BOOL;
		DscVvClSts : BOOL;
		SucVvOpCmd : BOOL;
		SucVvClCmd : BOOL;
		DscVvOpCmd : BOOL;
		DscVvClCmd : BOOL;
	END_STRUCT;
	UNITDELAY_obj : 	STRUCT 
		MotorOn_TOF_PT : TIME;
		WAHH_Pmp_PT : TIME;
		WAHH_Mtr_PT : TIME;
		StUpOvrDly_PT : TIME;
		LowSucPrs2_PT : TIME;
		LowSucPrs_PT : TIME;
		LowFlow_PT : TIME;
		DscVvOpDly_PT : TIME;
		StUpOvr_PT : TIME;
		HighDscPrs_PT : TIME;
		HighCasePrs_PT : TIME;
		CommFailMMR_PT : TIME;
	END_STRUCT;
	BOOST_obj : 	STRUCT 
		Multilin : MULTILIN_typ;
		Sts : UNITSTS_obj;
		Sp : UNITSP_obj;
		Valve : UNITVALVE_obj;
		Delay : UNITDELAY_obj;
		LO_Reset : BOOL;
		Start_Oip : BOOL;
		Stop_Oip : BOOL;
		Amp : INT;
		FSL : BOOL;
		XS : BOOL;
		PSL_LUBE : BOOL;
		PSH_LUBE : BOOL;
		TSH : BOOL;
		TSL : BOOL;
		TS : BOOL;
		CtrPwrFail : BOOL;
		AmpPVrange : REAL;
		DSC_PSH : BOOL;
		SAR : BOOL;
		BLP1_RUN : BOOL;
		BLP2_RUN : BOOL;
		BLP1_CMD : BOOL;
		BLP2_CMD : BOOL;
		BLHXP_CMD : BOOL;
		BLH_CMD : BOOL;
		LSL1 : BOOL;
		IMMHTR_RUN : BOOL;
		BLHXP_RUN : BOOL;
		VFD_Fault : BOOL;
		CommFailVFD : BOOL;
		VFD_Warning : BOOL;
		ESTOP : BOOL;
		PAL : BOOL;
		FAL : BOOL;
		PSH : BOOL;
		CommFailMMR : BOOL;
		RunTime : RUNTIME_obj;
		Amps : SCALE_obj;
		Start_Rmt : BOOL;
		StartCmd : BOOL;
		Stop_Rmt : BOOL;
		StopCmd : BOOL;
		MasterStart : BOOL;
		PSL : BOOL;
		Shutdown : BOOL;
	END_STRUCT;
	UNIT_obj : 	STRUCT 
		Multilin : MULTILIN_typ;
		Sts : UNITSTS_obj;
		Sp : UNITSP_obj;
		Valve : UNITVALVE_obj;
		Delay : UNITDELAY_obj;
		Eff : PUMPEFF_obj;
		Vib : ANALOG_obj;
		Vib_mtr : ARRAY[0..1]OF ANALOG_obj;
		Vib_pmp : ARRAY[0..1]OF ANALOG_obj;
		VFD : UNITVFD_obj;
		LO_Reset : BOOL;
		Start_Oip : BOOL;
		Stop_Oip : BOOL;
		Amp : INT;
		FSL : BOOL;
		XS : BOOL;
		PSL_LUBE : BOOL;
		PSH_LUBE : BOOL;
		TSH : BOOL;
		TSL : BOOL;
		TS : BOOL;
		CtrPwrFail : BOOL;
		AmpPVrange : REAL;
		DSC_PSH : BOOL;
		SAR : BOOL;
		VFD_Fault : BOOL;
		CommFailVFD : BOOL;
		VFD_Warning : BOOL;
		ESTOP : BOOL;
		PAL : BOOL;
		FAL : BOOL;
		Start_Local : BOOL;
		Stop_Local : BOOL;
		PSH : BOOL;
		CommFailMMR : BOOL;
		RunTime : RUNTIME_obj;
		Amps : SCALE_obj;
		Start_Rmt : BOOL;
		StartCmd : BOOL;
		Stop_Rmt : BOOL;
		StopCmd : BOOL;
		PSL : BOOL;
		Shutdown : BOOL;
		Remote_Shutdown : BOOL; (*Remote Shutdown*)
		Case_PSH : BOOL; (*High Case Pressure*)
		Bypass_Valve_Open : BOOL; (*Bypass Valve Open for Boosters*)
		Lockout_Reset_PBtn : BOOL; (*Lockout Reset Push button from the Field*)
		Lockout_Reset : BOOL; (*Lockout Reset Either from Field or OIP*)
		LineSelect : UINT;
		Mode : USINT; (*LPG/Conventional*)
	END_STRUCT;
	SEQ_obj : 	STRUCT 
		State : USINT;
		State_Timer_Done : BOOL;
		State_Alarm : BOOL;
		State_Abort : BOOL;
		State_Timer_On : BOOL;
		State_Timer_PT : UDINT;
		State_Timer_ET : UDINT;
		State_Alarm2 : BOOL;
		State_Complete : BOOL;
		State_Timer : UDINT;
		State_Active : BOOL;
		State_Error : USINT;
		State_Description : WSTRING[65];
	END_STRUCT;
	UNITVFD_obj : {REDUND_UNREPLICABLE} 	STRUCT  (* *) (* *) (*181*)
		Sts : {REDUND_UNREPLICABLE} ARRAY[0..3]OF UINT; (* *) (* *) (*182*)
		Hours : {REDUND_UNREPLICABLE} ARRAY[0..1]OF UINT; (* *) (* *) (*183*)
		Signal : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*184*)
		RPM : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*185*)
		Amps : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*186*)
		Volts : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*187*)
		KW : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*188*)
		HP : {REDUND_UNREPLICABLE} REAL; (* *) (* *) (*189*)
		Speed : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*190*)
		Speed_f : {REDUND_UNREPLICABLE} REAL; (* *) (* *) (*191*)
		Cmd : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*192*)
		Freq : {REDUND_UNREPLICABLE} USINT; (* *) (* *) (*193*)
		Enabled : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*194*)
		Running : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*195*)
		Warning : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*196*)
		Fault : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*197*)
		Local : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*198*)
		CommError : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*199*)
		CommFail : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*200*)
		Auto : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*201*)
	END_STRUCT;
	LOOPASSIGN_obj : 	STRUCT 
		DscPres : USINT;
		SucPres : USINT;
		Flow : USINT;
		Amps : ARRAY[0..11]OF USINT;
	END_STRUCT;
	PROSOFT_obj : {REDUND_UNREPLICABLE} 	STRUCT  (* *) (* *) (*452*)
		PROSOFT_Read : {REDUND_UNREPLICABLE} ARRAY[0..8]OF UINT; (* *) (* *) (*453*)
		Read_Heartbeat : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*454*)
		Write_Heartbeat : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*455*)
		CommFail : {REDUND_UNREPLICABLE} BOOL; (* *) (* *) (*456*)
		BatchSequence : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*457*)
		Valve_status : {REDUND_UNREPLICABLE} ARRAY[0..2]OF UINT; (* *) (* *) (*458*)
		GrossTotal_Read : {REDUND_UNREPLICABLE} ARRAY[0..1]OF UINT; (* *) (* *) (*459*)
		CumulativeGrossTotal : {REDUND_UNREPLICABLE} UDINT; (* *) (* *) (*460*)
		MeterFactor : {REDUND_UNREPLICABLE} UINT; (* *) (* *) (*461*)
	END_STRUCT;
END_TYPE
